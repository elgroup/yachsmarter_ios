//
//  OwnerYachtCollectionViewCell.m
//  YachtMasters
//
//  Created by Anvesh on 28/07/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "OwnerYachtCollectionViewCell.h"
#import "CommonMethods.h"

@implementation OwnerYachtCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [[CommonMethods sharedInstance]CornerRadius:_viewBaseYachDetail Radius:5.0 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
    [[CommonMethods sharedInstance]CornerRadius:_imageViewYacht Radius:5.0 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
    [[CommonMethods sharedInstance]CornerRadius:_viewBaseImageYacht Radius:5.0 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
    [self Shadow:_viewBaseYachDetail];
    [self Shadow:_viewBaseImageYacht];
     // Initialization code
}

-(void)Shadow:(UIView *)view{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
    view.layer.shadowColor = [UIColor blackColor] .CGColor;
    view.layer.shadowOffset = CGSizeMake(0.0, 5.0);
    view.layer.shadowOpacity = 0.5;
    view.layer.shadowRadius = 4.0;
    view.layer.masksToBounds = false;
    view.layer.shadowPath = shadowPath.CGPath;

}

@end
