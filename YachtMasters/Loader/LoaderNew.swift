//
//  LoaderNew.swift
//  YachtMasters
//
//  Created by EL INFO on 02/03/20.
//  Copyright © 2020 EL. All rights reserved.
//

import Foundation

@objc class LoaderNew : NSObject
{
    @objc static let sharedLoader = LoaderNew()
    let view:UIView
    
    let loaderImageView = UIImageView()
    override init() {
        view = UIView(frame: UIScreen.main.bounds)
        //view.backgroundColor = UIColor.init(red: 41.0/255.0, green: 141.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        view.backgroundColor = UIColor.clear
        let backGroundView = UIView(frame: CGRect(x:UIScreen.main.bounds.size.width/2 - 35, y:UIScreen.main.bounds.size.height/2 - 35, width:70, height:70))
        view.addSubview(backGroundView)
        backGroundView.backgroundColor = UIColor.init(red: 41.0/255.0, green: 141.0/255.0, blue: 240.0/255.0, alpha: 0.7)
        backGroundView.layer.cornerRadius = 8.0
        backGroundView.clipsToBounds = true
        
        
        let image =   UIImage(named: "logo")
        loaderImageView.frame = CGRect(x:backGroundView.frame.size.width/2 - 15,y:backGroundView.frame.size.height/2 - 15,  width:30, height:30)
        loaderImageView.layer.cornerRadius = loaderImageView.frame.size.width / 2;
        loaderImageView.clipsToBounds = true;
        loaderImageView.image = image
        loaderImageView.backgroundColor = UIColor.clear
        loaderImageView.contentMode = UIView.ContentMode.scaleAspectFit
        backGroundView.addSubview(loaderImageView)
        
    }
    
    @objc open func showLoaderOnScreen(vc:UIView) {
        
        DispatchQueue.main.async {
            vc.addSubview(self.view)
            let kAnimationKey = "rotation"
            if self.loaderImageView.layer.animation(forKey: kAnimationKey) == nil {
                let animate = CABasicAnimation(keyPath: "transform.rotation")
                animate.duration = 1.0
                animate.repeatCount = Float.infinity
                animate.fromValue = 0.0
                animate.toValue = Float(M_PI * 2.0)
                self.loaderImageView.layer.add(animate, forKey: kAnimationKey)
            }
        }
    }
    
    @objc open func hideLoader()
    {
        DispatchQueue.main.async {
            self.view.removeFromSuperview()
        }
    }
    
    
}

