//
//  ForgotPasswordViewController.m
//  YachtMasters
//
//  Created by Anvesh on 12/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "ChangePasswordViewController.h"
#import "CommonMethods.h"
#import "Validation.h"
#import "Constants.h"
#import "ViewController.h"
#import "ChangePasswordViewController.h"
#import "WebServiceManager.h"
#import "MBProgressHUD.h"
#import "YachtMasters-Swift.h"
@interface ForgotPasswordViewController ()<UITextFieldDelegate>{
    
    __weak IBOutlet UITextField *textFieldEmail;
    __weak IBOutlet UIButton *buttonSendPassword;
    __weak IBOutlet UIButton *buttonSignUp;
    __weak IBOutlet UIButton *buttonBack;
    MBProgressHUD *hud;
}

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"gbpic"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    UIColor *color = [UIColor whiteColor];
    textFieldEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email or Phone Number" attributes:@{NSForegroundColorAttributeName: color}];
    
    buttonSendPassword.layer.cornerRadius = 20.0;
    buttonSendPassword.clipsToBounds = true;
    // Do any additional setup after loading the view.
}
- (IBAction)ButtonSavePressed:(id)sender {
    
    if (textFieldEmail.text.length == 0 ) {
        [[CommonMethods sharedInstance]AlertMessage:@"Enter registered Email id or Phone Number" andViewController:self];
    }
    else if ([textFieldEmail.text containsString:@"@"]){
        if (![Validation validateEmail:textFieldEmail.text]){
            [[CommonMethods sharedInstance]AlertMessage:@"Enter valid email id" andViewController:self];
        }
        else if(![[CommonMethods sharedInstance] validateEmailForDots:textFieldEmail.text]){
            [[CommonMethods sharedInstance]AlertMessage:@"Enter valid email id" andViewController:self];
        }
        else{
            [self apiForCode];
        }
    }
    else if ([[CommonMethods sharedInstance]validatePhoneNumber:textFieldEmail.text]){
        
         [[CommonMethods sharedInstance]AlertMessage:@"Enter valid Phone Number" andViewController:self];
    }
    
    else{
        [self apiForCode];
//        if ([[CommonMethods sharedInstance]checkInternetConnection]) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                //                hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//                //                hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
//                [[Loader sharedLoader]showLoaderOnScreenWithVc:self.view];
//            });
//
//            NSString *strEmail = textFieldEmail.text;
//            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//            [dict setObject:strEmail forKey:@"email"];
//            NSMutableDictionary *dictRequest = [[NSMutableDictionary alloc]init];
//            [dictRequest setObject:dict forKey:@"users"];
//            [WebServiceManager PostRequest:[NSString stringWithFormat:@"%@users/password",kBaseUrl] andParam:dictRequest andcompletionhandler:^(NSArray *returArray, NSError *error){
//                if (!error) {
//                    if ([[returArray valueForKey:@"status"] boolValue]){
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            //                            [MBProgressHUD hideHUDForView:self.view animated:YES];
//                            [[Loader sharedLoader]hideLoader];
//                            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                            ChangePasswordViewController *controller = [story instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
//                            [self.navigationController pushViewController:controller animated:true];
//                        });
//                    }else{
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            //                            [MBProgressHUD hideHUDForView:self.view animated:YES];
//                            [[Loader sharedLoader]hideLoader];
//                            [[CommonMethods sharedInstance]AlertMessage:[returArray valueForKey:@"message"] andViewController:self];
//                        });
//                    }
//                }
//                else{
//                    [[CommonMethods sharedInstance]AlertMessage:[returArray valueForKey:@"message"] andViewController:self];
//                }
//                //                else{
//                //                    NSLog( @"%@",error);
//                //                }
//            }];
//        }
//        else{
//            [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
//        }
    }
}
-(void)apiForCode{
        if ([[CommonMethods sharedInstance]checkInternetConnection]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //                hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                //                hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
                [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
            });
            
            NSString *strEmail = textFieldEmail.text;
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:strEmail forKey:@"email"];
            NSMutableDictionary *dictRequest = [[NSMutableDictionary alloc]init];
            [dictRequest setObject:dict forKey:@"users"];
            [WebServiceManager PostRequest:[NSString stringWithFormat:@"%@users/password",kBaseUrl] andParam:dictRequest andcompletionhandler:^(NSArray *returArray, NSError *error){
                if (!error) {
                    if ([[returArray valueForKey:@"status"] boolValue]){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                            [[LoaderNew sharedLoader]hideLoader];
                            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            ChangePasswordViewController *controller = [story instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
                            [self.navigationController pushViewController:controller animated:true];
                        });
                    }else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                            [[LoaderNew sharedLoader]hideLoader];
                            [[CommonMethods sharedInstance]AlertMessage:[returArray valueForKey:@"message"] andViewController:self];
                        });
                    }
                }
                else{
                    [[CommonMethods sharedInstance]AlertMessage:[returArray valueForKey:@"message"] andViewController:self];
                }
                //                else{
                //                    NSLog( @"%@",error);
                //                }
            }];
        }
        else{
            [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
        }
    }


- (IBAction)Tapped:(id)sender {
    [self.view endEditing:true];
}
- (IBAction)BackButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
- (IBAction)ButtonSignUpPressed:(id)sender {
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController *controller = [story instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:controller animated:true];
}


#pragma mark - TextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [textFieldEmail becomeFirstResponder];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textFieldEmail resignFirstResponder];
    return true;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
