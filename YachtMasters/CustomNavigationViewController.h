//
//  CustomNavigationViewController.h
//  YachtMasters
//
//  Created by Anvesh on 25/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavigationViewController : UINavigationController
@property (nonatomic,strong)UIImage *imageNav;
@property (nonatomic,strong)UIImageView *imageViewNav;
-(void)CustomNav;
@end
