//
//  YachtDetailViewController.m
//
//
//  Created by Anvesh on 03/07/17.
//
//

#import "YachtDetailTableViewController.h"
#import "CommonMethods.h"
#import "WebServiceManager.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"//SDWebImage/UIImageView+WebCache.h
#import "AmenitiesCollectionViewCell.h"
#import "YachtImageCollectionViewCell.h"
#import "YachtMapAnnotationViewController.h"
#import "OwnerDetailTVC.h"
#import "CalenderViewController.h"
#import "ReviewViewController.h"
#import "PaymentsTVC.h"
#import "MakePaymentViewController.h"
#import "ChatViewController.h"
#import "AppDelegate.h"
#import "YachtMasters-Swift.h"
#import "ShowPriceViewController.h"
#import "CheckAvailabiltyViewController.h"


@interface YachtDetailTableViewController ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,GoogleMapDelegate,DurationDelegate>
{
    NSMutableArray *arrYachtDetail;
    NSDictionary *location;
}
@property (weak, nonatomic) IBOutlet UILabel *labelPrice;
@end

@implementation YachtDetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrYachtDetail = [[NSMutableArray alloc]init];
    // [self DropShadow];
    [[CommonMethods sharedInstance]CornerRadius:imageViewFollwer1 Radius:12.5 BorderWidth:0.7 andColorForBorder:[UIColor whiteColor]];
    [[CommonMethods sharedInstance]CornerRadius:imageViewFollwer2 Radius:12.5 BorderWidth:0.7 andColorForBorder:[UIColor whiteColor]];
    [[CommonMethods sharedInstance]CornerRadius:imageViewFollwer3 Radius:12.5 BorderWidth:0.7 andColorForBorder:[UIColor whiteColor]];
    [[CommonMethods sharedInstance]CornerRadius:imageViewFollwer4 Radius:12.5 BorderWidth:0.7 andColorForBorder:[UIColor whiteColor]];
    [[CommonMethods sharedInstance]CornerRadius:imageViewFollwer5 Radius:12.5 BorderWidth:0.7 andColorForBorder:[UIColor whiteColor]];
    [[CommonMethods sharedInstance]CornerRadius:imageViewProfilePic Radius:25 BorderWidth:2.0 andColorForBorder:[UIColor lightGrayColor]];
    [[CommonMethods sharedInstance]CornerRadius:viewOwnerDetail Radius:5.0 BorderWidth:0.0 andColorForBorder:nil];
    [[CommonMethods sharedInstance]DropShadow:viewOwnerDetail UIColor:[UIColor lightGrayColor] andShadowRadius:5.0];
    
    
    [[CommonMethods sharedInstance]setBorderOfButton:buttonFollow borderColor:[UIColor colorWithRed:40.0/255.0 green:171.0/255.0 blue:241.0/255.0 alpha:1.0] borderWidth:1.0 cornerRadius:15.0];
    //[[CommonMethods sharedInstance]setButtonShadow:buttonBookNow];
    buttonBookNow.clipsToBounds = NO;
    buttonBookNow.layer.shadowOffset = CGSizeMake(0, 0);
    buttonBookNow.layer.shadowRadius = 3;
    buttonBookNow.layer.shadowOpacity = 0.5;
    buttonBookNow.layer.masksToBounds = NO;
    buttonBookNow.layer.shadowColor = [UIColor colorWithRed:41.0/255.0 green:171.0/255.0 blue:251.0/255.0 alpha:1.0].CGColor;
    
    // Do any additional setup after loading the view.
}

-(BOOL)hidesBottomBarWhenPushed{
    return YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [buttonBack removeFromSuperview];
    // self.navigationController.navigationBar.hidden = true;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.hidesBottomBarWhenPushed = true;
    self.tabBarController.tabBar.hidden = true;
    self.navigationItem.leftBarButtonItem=nil;
    self.navigationItem.hidesBackButton=YES;
    self.navigationController.navigationBar.hidden = true;
    buttonBack = [[UIButton alloc]init];
    buttonBack.frame = CGRectMake(15.0, 37.0, 30.0, 30.0);
    [buttonBack setImage:[UIImage imageNamed:@"back button.png"] forState:UIControlStateNormal];
    [buttonBack addTarget:self action:@selector(BackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.view addSubview:buttonBack];
    [[CommonMethods sharedInstance]DropShadow:buttonBack UIColor:[UIColor blackColor] andShadowRadius:3.0];
    [[CommonMethods sharedInstance]DropShadow:buttonFavorite UIColor:[UIColor blackColor] andShadowRadius:3.0];
    [[CommonMethods sharedInstance]DropShadow:buttonMap UIColor:[UIColor blackColor] andShadowRadius:3.0];
    
    [self ApiForYachtDetail];
    
}

#pragma mark- Table View Data source and delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (arrYachtDetail.count>0) {
        return 9;
    }
    else{
        return 1;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 6) {
        _lcDesctiption.constant = _tvDescription.contentSize.height;
        return _tvDescription.contentSize.height+50;
    }
    
    else if (indexPath.row == 7){
        NSArray *arrAmenities = [arrYachtDetail valueForKey:@"amenities"];
        if (arrAmenities.count > 0){
            return [super tableView:tableView heightForRowAtIndexPath:indexPath];
        }
        else{
            return  0;
        }
    }
    else{
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 5) {
        NSString *strOwnerId = [[arrYachtDetail valueForKey:@"user"] valueForKey:@"id"];
        OwnerDetailTVC *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"OwnerDetailTVC"];
        controller.lang = self.lang;
        controller.lat = self.lat;
        controller.strId = strOwnerId;
        [controller viewDidLoad];
        [self.navigationController pushViewController:controller animated:true];
    }
    else if (indexPath.row == 2){
        [self CheckAvailabilityPressed];
        //        NSString *strYachtId = [[arrYachtDetail valueForKey:@"id"] stringValue];
        //        CalenderViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CalenderViewController"];
        //        controller.dictLocation = location;
        //        controller.strId = strYachtId;
        //        [self.navigationController pushViewController:controller animated:true];
        
    }
    else if(indexPath.row == 4){
        NSString *strYachtId = [[arrYachtDetail valueForKey:@"id"] stringValue];
        ReviewViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ReviewViewController"];
        controller.isOwner = true;
        controller.strId = strYachtId;
        [self.navigationController pushViewController:controller animated:true];
        
    }
}

#pragma mark - Back Button Action
-(IBAction)BackButtonPressed:(id)sender{
    //self.navigationController.navigationBar.hidden = true;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [[NSUserDefaults standardUserDefaults]setObject:dict forKey:@"JourneyDate"];
    // self.navigationController.navigationBar.hidden = false;
    [buttonBack removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
    self.tabBarController.tabBar.hidden = true;
}


-(void)DropShadow{
    
    viewArrival.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    viewArrival.layer.shadowOffset = CGSizeMake(-5, 5);
    viewArrival.layer.shadowOpacity = 0.3;
    viewArrival.layer.shadowRadius = 6.0;
    
    viewDeparture.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    viewDeparture.layer.shadowOffset = CGSizeMake(-5, 5);
    viewDeparture.layer.shadowOpacity = 0.3;
    viewDeparture.layer.shadowRadius = 8.0;
    
    
    viewOwnerDetail.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    viewOwnerDetail.layer.shadowOffset = CGSizeMake(-5, 5);
    viewOwnerDetail.layer.shadowOpacity = 0.3;
    viewOwnerDetail.layer.shadowRadius = 5.0;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
    
    
}

#pragma mark - Button Follow Action / Api for Follow
- (IBAction)ButtonFollowPressed:(id)sender {
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        if (buttonFollow.selected) {
            NSString *strOwnerId = [[[arrYachtDetail valueForKey:@"user"] valueForKey:@"id"] stringValue];
            [WebServiceManager deleteRequestWithUrlString:[NSString stringWithFormat:@"%@follows/%@",kBaseUrl,strOwnerId] emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
                buttonFollow.selected = false;
            } failure:^(NSString *msg, BOOL success) {
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            }];
        }
        else{
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:[[[arrYachtDetail valueForKey:@"user"]valueForKey:@"id"] stringValue] forKey:@"owner_id"];
            [WebServiceManager postRequestWithUrlString:[NSString stringWithFormat:@"%@follows",kBaseUrl] withPostString:dict emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
                buttonFollow.selected = true;
            } failure:^(NSString *msg, BOOL success) {
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            }];
        }
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}

#pragma mark - Api for gettin Yacht Detail

-(void)ApiForYachtDetail{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view.window];
        });
        
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSString *strUrl = [NSString stringWithFormat:@"%@yachts/%@",kBaseUrl,_strId];
        if (self.lang && self.lat) {
            strUrl = [NSString stringWithFormat:@"%@?latitude=%f&longitude=%f",strUrl,self.lat,self.lang];
            //   strUrl = [NSString stringWithFormat:@"%@?latitude=%s&longitude=%s",strUrl,"25.034281","-77.396278"]; //Mexico
            //            strUrl = [NSString stringWithFormat:@"%@?latitude=%s&longitude=%s",strUrl,"51.507351","-0.127758"];
        }
        [WebServiceManager getRequestUrlString:strUrl emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            arrYachtDetail = [result valueForKey:@"yacht"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                // NSString *strPrice = [[CommonMethods sharedInstance] priceFormatting:[[arrYachtDetail valueForKey:@"price"]valueForKey:@"price"]];
                NSDictionary *dictPrice= [arrYachtDetail valueForKey:@"price"];
                NSString *strPrice = [NSString stringWithFormat:@"%0.2f",[[dictPrice valueForKey:@"price"]floatValue]];
                NSString *strTerm = [dictPrice valueForKey:@"term"];
                if ([strTerm isEqualToString:@"per_day"]){
                    strTerm = @"/Full Day";
                }
                else if ([strTerm isEqualToString:@"per_hour"]){
                    strTerm = @"/Hour";
                }
                else if ([strTerm isEqualToString:@"half_day"]){
                    strTerm = @"/Half Day";
                }
                else if ([strTerm isEqualToString:@"week"]){
                    strTerm = @"/Week";
                }
                
                NSString *strCurrency = [arrYachtDetail valueForKey:@"currency_symbol"];
                NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@%@%@",strCurrency,strPrice,strTerm]];
                NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:attrText];
                strPrice  = [strPrice stringByAppendingString:strCurrency];
                NSRange range = [strPrice rangeOfString:strPrice];
                [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:FontOpenSansSemiBold(12)} range:range];
                [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor],NSFontAttributeName:FontOpenSans(11)} range:NSMakeRange(range.length, strTerm.length)];
                [buttonPrice setAttributedTitle:attributedText forState:UIControlStateNormal];
                labelOwnerName.text = [[arrYachtDetail valueForKey:@"user"] valueForKey:@"name"];
                
                BOOL isCaptain = [[arrYachtDetail valueForKey:@"is_captain"] boolValue];
                if (isCaptain){
                    labelCaptain.text = @"CAPTAINED? YES";
                }
                else{
                    labelCaptain.text = @"CAPTAINED? NO";
                }
                
                NSInteger intRating = [[[arrYachtDetail valueForKey:@"user"] valueForKey:@"avg_rating"] integerValue];
                [[CommonMethods sharedInstance]RatingAndStar1:imageViewStar1 Star2:imageView2 Star3:imageViewStar3 Star4:imageViewStar4 Star5:imageViewStar5 andRating:intRating];
                
                _tvDescription.text = [arrYachtDetail valueForKey:@"description"];
                
                labelYachtName.text = [arrYachtDetail valueForKey:@"name"];
                
                pageControlImage.numberOfPages = [[arrYachtDetail valueForKey:@"yacht_images"] count];
                
                [self UserFollowers:[[arrYachtDetail valueForKey:@"user"] valueForKey:@"user_images"]];
                
                [collectionViewAmenities reloadData];
                [collectionViewYachtImages reloadData];
                if ([[arrYachtDetail valueForKey:@"is_favoritre"] boolValue]) {
                    [buttonFavorite setImage:[UIImage imageNamed:@"heart fill@2x.png"] forState:UIControlStateNormal];
                    buttonFavorite.selected = true;
                }
                else{
                    buttonFavorite.selected = false;
                    [buttonFavorite setImage:[UIImage imageNamed:@"heart@2x.png"] forState:UIControlStateNormal];
                }
                
                if ([[arrYachtDetail valueForKey:@"is_following"] boolValue]) {
                    [buttonFollow setTitle:@"Following" forState:UIControlStateNormal];
                    buttonFollow.selected = true;
                }
                else{
                    [buttonFollow setTitle:@"Follow" forState:UIControlStateNormal];
                    buttonFollow.selected = false;
                }
                
                NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[arrYachtDetail valueForKey:@"user"] valueForKey:@"image"]];
                NSURL *url = [NSURL URLWithString:strUrl];
                [imageViewProfilePic sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"PlaceHolder_yachtOwner"]];
                [self.tableView reloadData];
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}
- (IBAction)buttonPricePressed:(id)sender {
    ShowPriceViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowPriceViewController"];
    [self.navigationController addChildViewController:controller];
    controller.view.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    [self.navigationController.view addSubview:controller.view];
    [controller didMoveToParentViewController:self.navigationController];
    //controller.viewBottom.frame = CGRectMake(0.0, self.navigationController.view.frame.size.height + 300, self.view.frame.size.width, 300);
    controller.arrPrices = [arrYachtDetail valueForKey:@"prices"];
    controller.strCurrencySymbol = [arrYachtDetail valueForKey:@"currency_symbol"];
    [controller viewDidLoad];
    [controller viewWillAppear:true];
//    [UIView animateWithDuration:0.4
//                          delay:0.0
//                        options:UIViewAnimationOptionCurveEaseIn
//                     animations:^{
//        controller.viewBottom.frame = CGRectMake(0.0, self.navigationController.view.frame.size.height - 300, self.view.frame.size.width, 300);
//    } completion:^(BOOL finished){
//        
//        
//    }];
}

-(void)CheckAvailabilityPressed{
    CheckAvailabiltyViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CheckAvailabiltyViewController"];
    [self.navigationController addChildViewController:controller];
    controller.view.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    [self.navigationController.view addSubview:controller.view];
    [controller didMoveToParentViewController:self.navigationController];
    controller.lcBottomConstraints.constant = -300;
    // controller.viewBottom.frame = CGRectMake(0.0, self.navigationController.view.frame.size.height + 300, self.view.frame.size.width, 300);
    controller.arrPrices = [arrYachtDetail valueForKey:@"prices"];
    NSString *strYachtId = [[arrYachtDetail valueForKey:@"id"] stringValue];
    controller.strYachtId = strYachtId;
    controller.delegate = self;
    controller.dictLocation = location;
    [controller viewDidLoad];
    [controller viewWillAppear:true];
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
        controller.lcBottomConstraints.constant = 0;
        //controller.viewBottom.frame = CGRectMake(0.0, self.navigationController.view.frame.size.height - 300, self.view.frame.size.width, 300);
    } completion:^(BOOL finished){
        
        
    }];
}

-(void)pushToNextViewController:(NSMutableArray *)arrSelectedDuration{
    NSString *strYachtId = [[arrYachtDetail valueForKey:@"id"] stringValue];
    CalenderViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CalenderViewController"];
    controller.dictLocation = location;
    controller.strId = strYachtId;
    controller.strDuration = [[arrSelectedDuration valueForKey:@"term"]objectAtIndex:0];
    controller.intDurationId = [[[arrSelectedDuration valueForKey:@"id"] objectAtIndex:0] intValue];
    [self.navigationController pushViewController:controller animated:true];
}

#pragma mark - Method for Favorite button Click
- (IBAction)ButtonFavoritePressed:(id)sender {
    if (buttonFavorite.selected) {
        [self ApiForDislike];
    }
    else{
        [self ApiForFavorite];
    }
}

#pragma mark - Api for fovorite

-(void)ApiForFavorite{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:[arrYachtDetail valueForKey:@"id"] forKey:@"yacht_id"];
        NSMutableDictionary *dictPostdata = [[NSMutableDictionary alloc]init];
        [dictPostdata setObject:dict forKey:@"favorites"];
        [WebServiceManager postRequestWithUrlString:[NSString stringWithFormat:@"%@favorites",kBaseUrl] withPostString:dictPostdata emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            [buttonFavorite setImage:[UIImage imageNamed:@"heart fill@2x.png"] forState:UIControlStateNormal];
            buttonFavorite.selected = true;
        } failure:^(NSString *msg, BOOL success) {
            [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
        }];
        
    }
}

#pragma mark - Api for dislike the yacht

-(void)ApiForDislike{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSString *strYachtId = [arrYachtDetail valueForKey:@"id"];
        [WebServiceManager deleteRequestWithUrlString:[NSString stringWithFormat:@"%@favorites/%@",kBaseUrl,strYachtId] emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            [buttonFavorite setImage:[UIImage imageNamed:@"heart@2x.png"] forState:UIControlStateNormal];
            buttonFavorite.selected = false;
        } failure:^(NSString *msg, BOOL success) {
            [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
        }];
    }
}

#pragma mark - Method for showing followers

-(void)UserFollowers:(NSMutableArray *)arrUserFollowers{
    
    switch ([arrUserFollowers count]) {
        case 0:
            break;
        case 1:{
            imageViewFollwer1.hidden = false;
            imageViewFollwer2.hidden = true;
            imageViewFollwer3.hidden = true;
            imageViewFollwer4.hidden = true;
            imageViewFollwer5.hidden = true;
            
            [self DownloadImage:[arrUserFollowers objectAtIndex:0] andUIImageView:imageViewFollwer1];
            
        }
            break;
            
        case 2:
        {
            imageViewFollwer1.hidden = false;
            imageViewFollwer2.hidden = false;
            imageViewFollwer3.hidden = true;
            imageViewFollwer4.hidden = true;
            imageViewFollwer5.hidden = true;
            [self DownloadImage:[arrUserFollowers objectAtIndex:0] andUIImageView:imageViewFollwer1];
            [self DownloadImage:[arrUserFollowers objectAtIndex:1] andUIImageView:imageViewFollwer2];
        }
            break;
        case 3:{
            imageViewFollwer1.hidden = false;
            imageViewFollwer2.hidden = false;
            imageViewFollwer3.hidden = false;
            imageViewFollwer4.hidden = true;
            imageViewFollwer5.hidden = true;
            [self DownloadImage:[arrUserFollowers objectAtIndex:0] andUIImageView:imageViewFollwer1];
            [self DownloadImage:[arrUserFollowers objectAtIndex:1] andUIImageView:imageViewFollwer2];
            [self DownloadImage:[arrUserFollowers objectAtIndex:2] andUIImageView:imageViewFollwer3];
        }
            break;
        case 4:{
            imageViewFollwer1.hidden = false;
            imageViewFollwer2.hidden = false;
            imageViewFollwer3.hidden = false;
            imageViewFollwer4.hidden = false;
            imageViewFollwer5.hidden = true;
            
            [self DownloadImage:[arrUserFollowers objectAtIndex:0] andUIImageView:imageViewFollwer1];
            [self DownloadImage:[arrUserFollowers objectAtIndex:1] andUIImageView:imageViewFollwer2];
            [self DownloadImage:[arrUserFollowers objectAtIndex:2] andUIImageView:imageViewFollwer3];
            [self DownloadImage:[arrUserFollowers objectAtIndex:3] andUIImageView:imageViewFollwer4];
        }
            break;
        case 5:{
            imageViewFollwer1.hidden = false;
            imageViewFollwer2.hidden = false;
            imageViewFollwer3.hidden = false;
            imageViewFollwer4.hidden = false;
            imageViewFollwer5.hidden = false;
            
            [self DownloadImage:[arrUserFollowers objectAtIndex:0] andUIImageView:imageViewFollwer1];
            [self DownloadImage:[arrUserFollowers objectAtIndex:1] andUIImageView:imageViewFollwer2];
            [self DownloadImage:[arrUserFollowers objectAtIndex:2] andUIImageView:imageViewFollwer3];
            [self DownloadImage:[arrUserFollowers objectAtIndex:3] andUIImageView:imageViewFollwer4];
            [self DownloadImage:[arrUserFollowers objectAtIndex:4] andUIImageView:imageViewFollwer5];
        }
            break;
        default:
        {
            imageViewFollwer1.hidden = false;
            imageViewFollwer2.hidden = false;
            imageViewFollwer3.hidden = false;
            imageViewFollwer4.hidden = false;
            imageViewFollwer5.hidden = false;
            
            [self DownloadImage:[arrUserFollowers objectAtIndex:0] andUIImageView:imageViewFollwer1];
            [self DownloadImage:[arrUserFollowers objectAtIndex:1] andUIImageView:imageViewFollwer2];
            [self DownloadImage:[arrUserFollowers objectAtIndex:2] andUIImageView:imageViewFollwer3];
            [self DownloadImage:[arrUserFollowers objectAtIndex:3] andUIImageView:imageViewFollwer4];
            [self DownloadImage:[arrUserFollowers objectAtIndex:4] andUIImageView:imageViewFollwer5];
            NSInteger count = [arrUserFollowers count] - 5;
            labelFollowers.text = [NSString stringWithFormat:@"+%ld Followers",(long)count];
        }
            break;
    }
}

- (IBAction)ButtonBookNowPressed:(id)sender {
    NSMutableDictionary *dictJourneyDates = [[NSMutableDictionary alloc]init];
    dictJourneyDates =  [[NSUserDefaults standardUserDefaults]valueForKey:@"JourneyDate"];
    NSLog(@"%@",dictJourneyDates);
    if (dictJourneyDates.count == 0) {
        [[CommonMethods sharedInstance]AlertMessage:@"Please check the availabilty of yacht then book the yacht." andViewController:self];
    }
    //&& [[dictJourneyDates valueForKey:@"secondDate"] isEqualToString:@""]
    else if ([[dictJourneyDates valueForKey:@"firstDate"] isEqualToString:@""] ) {
        [[CommonMethods sharedInstance]AlertMessage:@"Please select the journey date for booking" andViewController:self];
        NSLog(@"journey date missing");
    }
    else{
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"dd-MM-yyyy"];
        NSString *strDate = [dictJourneyDates valueForKey:@"firstDate"];
        // NSString *strendDate =[dictJourneyDates valueForKey:@"secondDate"];
        // NSString *strPrice = [arrYachtDetail valueForKey:@"price"];
        NSString *strCurrency = [arrYachtDetail valueForKey:@"currency"];
        // float TotalPrice = [[dictJourneyDates valueForKey:@"NumberOfNights"] integerValue] * [strPrice floatValue];
        [dict setObject:strDate forKey:@"start_date"];
        //   [dict setObject:strendDate forKey:@"end_date"];
        [dict setObject:[dictJourneyDates valueForKey:@"Departure_time"] forKey:@"start_time"];
        [dict setObject:[dictJourneyDates valueForKey:@"Duration"] forKey:@"Duration"];
        [dict setObject:[arrYachtDetail valueForKey:@"id"] forKey:@"yacht_id"];
        //  [dict setObject:[NSNumber numberWithFloat:TotalPrice] forKey:@"total_price"];
        [dict setObject:strCurrency forKey:@"currency"];
        int priceId = [[dictJourneyDates valueForKey:@"price_id"]intValue];
        [dict setObject:[NSNumber numberWithInt:priceId] forKey:@"price_id"];
        
        
        MakePaymentViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MakePaymentViewController"];
        controller.dictYachtJourneyDetails = dict;
        [self.navigationController pushViewController:controller animated:true];
    }
}

#pragma mark - Downloading the image

-(void)DownloadImage:(NSString *)strUrl andUIImageView:(UIImageView *)imageViewFollower{
    strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage ,strUrl];
    NSURL *url = [NSURL URLWithString:strUrl];
    [imageViewFollower sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
}

#pragma mark - Calling method

-(IBAction)Call:(id)sender{
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",[[arrYachtDetail valueForKey:@"user"] valueForKey:@"contact"]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        [[CommonMethods sharedInstance]AlertMessage:@"Call facility is not available!!!" andViewController:self];
        //calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        // [calert show];
    }
}


#pragma mark - Switch to Yach Map
-(IBAction)SwitchToYachAnnotationMap:(id)sender{
    YachtMapAnnotationViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"YachtMapAnnotationViewController"];
    NSString *strAddress = [NSString stringWithFormat:@"%@%@,%@",[arrYachtDetail valueForKey:@"address"],[arrYachtDetail valueForKey:@"city"],[arrYachtDetail valueForKey:@"country"]];
    controller.latUser = _lat;
    controller.langUser = _lang;
    controller.strAddress = strAddress;
    controller.arrYachtDetail = arrYachtDetail;
    controller.delegate = self;
    controller.strId = _strId;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)googleMap:(NSString *)strId{
    _strId = strId;
    //[self ApiForYachtDetail];
}

#pragma mark - collectionView Delegate and datasource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == collectionViewYachtImages) {
        return [[arrYachtDetail valueForKey:@"yacht_images"] count];
    }
    else{
        return [[arrYachtDetail valueForKey:@"amenities"] count];
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView == collectionViewYachtImages) {
        YachtImageCollectionViewCell *cell = [collectionViewYachtImages dequeueReusableCellWithReuseIdentifier:@"YachtImageCollectionViewCell" forIndexPath:indexPath];
        if (cell == nil)    {
            NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"YachtImageCollectionViewCell" owner:self options:nil];
            cell = nibArray[0];
        }
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[[arrYachtDetail valueForKey:@"yacht_images"]objectAtIndex:indexPath.row]valueForKey:@"image_url"]];
        NSURL *url = [NSURL URLWithString:strUrl];
        [cell.imageViewYacht sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        UIImageView *image = [[UIImageView alloc]init];
        [image sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        cell.imageViewYacht.image = image.image; //[self imageWithImage:image.image convertToSize:cell.imageViewYacht.frame.size];
        cell.imageViewYacht.contentMode = UIViewContentModeScaleAspectFill;
        return cell;
    }
    else{
        AmenitiesCollectionViewCell *cell = [collectionViewAmenities dequeueReusableCellWithReuseIdentifier:@"AmenitiesCollectionViewCell" forIndexPath:indexPath];
        if (cell == nil)    {
            NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"AmenitiesCollectionViewCell" owner:self options:nil];
            cell = nibArray[0];
        }
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[[arrYachtDetail valueForKey:@"amenities"]objectAtIndex:indexPath.row]valueForKey:@"icon"]];
        NSURL *url = [NSURL URLWithString:strUrl];
        [cell.imageViewAmenities sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        cell.labelAmenitiesName.text = [[[arrYachtDetail valueForKey:@"amenities"]objectAtIndex:indexPath.row]valueForKey:@"name"];
        return cell;
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == collectionViewYachtImages) {
        return 0.0;
    }
    else{
        return 1.0;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == collectionViewYachtImages) {
        return CGSizeMake([[UIScreen mainScreen] bounds].size.width, collectionViewYachtImages.frame.size.height);
        
    }
    else{
        if (IS_IPHONE_5) {
            return CGSizeMake(collectionViewAmenities.frame.size.width/4,collectionViewAmenities.frame.size.height);
        }
        else if (IS_IPHONE_6 || IS_IPHONE_X){
            return CGSizeMake(collectionViewAmenities.frame.size.width/5,collectionViewAmenities.frame.size.height);
        }
        else if (IS_IPHONE_6P || IS_IPHONE_XSMAX){
            return CGSizeMake(collectionViewAmenities.frame.size.width/6,collectionViewAmenities.frame.size.height);
        }
        else{
            return CGSizeMake(collectionViewAmenities.frame.size.width/3,collectionViewAmenities.frame.size.height);
        }
    }
    // return CGSizeMake(100, 60);_lcDesctiption
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (collectionViewYachtImages) {
        CGFloat pageWidth = collectionViewYachtImages.frame.size.width;
        float currentPage = collectionViewYachtImages.contentOffset.x / pageWidth;
        
        if (0.0f != fmodf(currentPage, 1.0f))
        {
            pageControlImage.currentPage = currentPage + 1;
        }
        else
        {
            pageControlImage.currentPage = currentPage;
        }
        NSLog(@"finishPage: %ld", (long)pageControlImage.currentPage);
    }
}

#pragma mark - Method for get Location from address
-(void) getLocationFromAddress: (NSString*) addressStr {
    __block double latitude = 0, longitude = 0;
    
    NSString *esc_addr = [addressStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    NSString *req = [NSString stringWithFormat:@"https://maps.google.com/maps/api/geocode/json?sensor=true&address=%@", esc_addr];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSString *cachePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"/nsurlsessiondemo.cache"];
    NSURLCache *myCache = [[NSURLCache alloc] initWithMemoryCapacity: 16384
                                                        diskCapacity: 268435456
                                                            diskPath: cachePath];
    defaultConfigObject.URLCache = myCache;
    defaultConfigObject.requestCachePolicy = NSURLRequestUseProtocolCachePolicy;
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject
                                                                      delegate: nil
                                                                 delegateQueue: [NSOperationQueue mainQueue]];
    
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:req]];
    [request setHTTPMethod:@"GET"];
    
    [[delegateFreeSession dataTaskWithRequest:request
                            completionHandler:^(NSData *data, NSURLResponse *response,
                                                NSError *error)
      {
        //      NSLog(@"Got response %@ with error %@.\n", response, error);
        NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        //          completionBlock (dataArray, error);
        //
        NSLog(@"%@", dataArray);
        
        if ([[dataArray valueForKey:@"status"] isEqualToString:@"ZERO_RESULTS"]) {
            location = [[NSDictionary alloc]init];
        }
        else if ([[dataArray valueForKey:@"status"] isEqualToString:@"OVER_QUERY_LIMIT"]){
            location = [[NSDictionary alloc]init];
        }
        else{
            location = [[NSDictionary alloc]init];
            location = [[[[dataArray valueForKey:@"results"] objectAtIndex:0] valueForKey:@"geometry"]valueForKey:@"location"];
            NSLog(@"%@",location);
            
            
            
            NSNumber *number = location[@"lat"];
            
            latitude = number.doubleValue;
            
            number = location[@"lng"];
            
            longitude = number.doubleValue;
            
            
            
            CLLocationCoordinate2D center;
            
            center.latitude=latitude;
            
            center.longitude = longitude;
        }
        
        
    }
      
      ]resume];
    
    // [self requestForVendor];
}

#pragma mark - Buttom Message clicked
- (IBAction)ButtonMessageClicked:(id)sender {
    BOOL isInternet = [[CommonMethods sharedInstance]checkInternetConnection];
    if (isInternet){
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSString *strOwnerId = [[arrYachtDetail valueForKey:@"user"] valueForKey:@"id"];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:strOwnerId forKey:@"recipient_id"];
        NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
        [dictResponse setObject:dict forKey:@"conversations"];
        [WebServiceManager postRequestWithUrlString:[NSString stringWithFormat:@"%@conversations",kBaseUrl] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            NSLog(@"%@",result);
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate.socket joinChannel:@"ConversationChannel"];
            ChatViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
            controller.arrCurrentUser = [arrYachtDetail valueForKey:@"current_user"];
            controller.arrConversations = [[result valueForKey:@"conversation"] mutableCopy];
            [self.navigationController pushViewController:controller animated:true];
            
        } failure:^(NSString *msg, BOOL success) {
            [[CommonMethods sharedInstance]AlertMessage:@"Unable to connect to chat.\nPlease try again." andViewController:self];
        }];
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
    
}


- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
