//
//  YachtImageCollectionViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 19/07/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YachtImageCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewYacht;

@end
