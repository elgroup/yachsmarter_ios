//
//  AmenitiesCollectionViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 19/07/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AmenitiesCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewAmenities;
@property (weak, nonatomic) IBOutlet UILabel *labelAmenitiesName;

@end
