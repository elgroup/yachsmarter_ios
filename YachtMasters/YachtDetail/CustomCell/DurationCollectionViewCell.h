//
//  DurationCollectionViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 03/10/18.
//  Copyright © 2018 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DurationCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIButton *buttonDuration;

@end
