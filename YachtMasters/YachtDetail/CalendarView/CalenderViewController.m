//
//  CalenderViewController.m
//  YachtMasters
//
//  Created by Anvesh on 08/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "CalenderViewController.h"
#import "Constants.h"
#import "WebServiceManager.h"
#import "CommonMethods.h"
#import "MBProgressHUD.h"
#import <CoreLocation/CoreLocation.h>
#import "APTimeZones.h"
#import "YachtMasters-Swift.h"

typedef enum MonthType{
    Current,
    Middle,
    Last
} SelectedMonthType;

@interface CalenderViewController ()<UITextFieldDelegate>{
    int monthNo;
    int yearNo;
    NSDateFormatter *dateFormatter;
    NSArray *arrGroupedDataOfCurrentMonth;
    NSMutableDictionary *dictDate,*selectedMonths;
    UIButton *btnfirst;
    UIButton *btnSecond;
    UIDatePicker *pickerTime;
    UIBarButtonItem *doneButton;
    UIToolbar *toolBar;
    UITextField *txtField;
    NSMutableDictionary *dictDateSelected;
    __weak IBOutlet NSLayoutConstraint *lcConstraintHeight;
    NSRange range;
    MBProgressHUD *hud;
    NSMutableArray *arrDates;
    __weak IBOutlet NSLayoutConstraint *lcTopHeight;
    __weak IBOutlet NSLayoutConstraint *lcConstraintBottomView;
}

@end

@implementation CalenderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrDates = [[NSMutableArray alloc]init];
    dictDate = [[NSMutableDictionary alloc]init];
    selectedMonths = [NSMutableDictionary new];
    dictDateSelected = [[NSMutableDictionary alloc]init];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    [calendar setMinimumDaysInFirstWeek:1];
    NSDateComponents *dateComponent = [calendar components:(NSCalendarUnitYear | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitWeekday) fromDate:[NSDate date]];
    dateComponent.day = 1;
    monthNo = (int)dateComponent.month;
    yearNo = (int)dateComponent.year;
    
    _buttonPreviuos.tag = 1;
    [_buttonPreviuos addTarget:self action:@selector(btnPreviousOrNextClick:) forControlEvents:UIControlEventTouchUpInside];
    _buttonNext.tag = 2;
    [_buttonNext addTarget:self action:@selector(btnPreviousOrNextClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    pickerTime = [[UIDatePicker alloc]init];
    [pickerTime addTarget:self action:@selector(date:)  forControlEvents:UIControlEventValueChanged];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [pickerTime setLocale:locale];
    pickerTime.backgroundColor = [UIColor whiteColor];
    pickerTime.datePickerMode = UIDatePickerModeTime;
    //    [pickerTime addTarget:self action:@selector() forControlEvents:UIControlEventTouchUpInside];
    _textFieldDeparture.inputView = pickerTime;
    //    _textFieldArrival.inputView = pickerTime;
    
    
    doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(hideKeyboard)];
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 50)];
    //[toolBar setBarStyle:UIBarStyleBlackOpaque];
    toolBar.translucent=NO;
    toolBar.barTintColor = [UIColor colorWithRed:40.0/255.0 green:107.0/255.0 blue:240.0/255.0 alpha:1.0];
    
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton, nil];
    [toolBar setItems:toolbarItems];
    doneButton.tintColor = [UIColor whiteColor];
    //  _textFieldArrival.inputAccessoryView = toolBar;
    _textFieldDeparture.inputAccessoryView = toolBar;
    [self ApiForGettingDates];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self CustomNavigation];
    lcConstraintHeight.constant = _containerView.frame.size.width+120;
    _viewBottomBar.layer.shadowColor = [UIColor blackColor].CGColor;
    _viewBottomBar.layer.shadowOffset = CGSizeMake(0.0, 8.0);
    _viewBottomBar.layer.shadowOpacity = 2.0;
    _viewBottomBar.layer.shadowRadius = 10.0;
    _viewBottomBar.layer.masksToBounds = false;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
-(void)viewWillLayoutSubviews{
    if(IS_IPHONE_X || IS_IPHONE_XSMAX){
        lcTopHeight.constant = 110;
    }
}
-(void)CustomNavigation{
    self.navigationController.navigationBar.hidden = false;
    self.title = @"Yacht Calendar";
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 10.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    
    UIImage* image = [UIImage imageNamed:@"check-mark"];
    CGRect frame = CGRectMake(UIScreen.mainScreen.bounds.size.width - 40, 10, 30,30);
    UIButton *buttonTick = [[UIButton alloc] initWithFrame:frame];
    [buttonTick addTarget:self action:@selector(buttonTickClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButton =[[UIBarButtonItem alloc] initWithCustomView:buttonTick];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 10.0, 15.0, 15.0)];
    imageView.image = image;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [buttonTick addSubview:imageView];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
    
}

-(void)BackButtonPressed{
    [self.navigationController popViewControllerAnimated:true];
}


- (void) hideKeyboard {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"hh:mm a"];
    self.textFieldDeparture.text = [dateFormat stringFromDate:[pickerTime date]];
    
    [dateFormat setDateFormat:@"HH:mm"];
    NSString *dateString = [dateFormat stringFromDate:[pickerTime date]];
    NSString *strtimeStamp = [NSString stringWithFormat:@"%f",[[pickerTime date] timeIntervalSince1970] * 1000];
    // int timestamp = [strtimeStamp intValue];
    [dictDateSelected setObject:dateString forKey:@"Departure_time"];
    
    [self.view endEditing:true];
    [UIView animateWithDuration:0.25 animations:^{
        toolBar.frame = CGRectMake(0.0, SCREEN_HEIGHT, SCREEN_WIDTH, 50);
    }];
    lcConstraintBottomView.constant = 0;
    [UIView animateWithDuration:0.25 animations:^{[self.view layoutIfNeeded];}];
}

#pragma mark - Button tick Method
-(void)buttonTickClicked{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //    if (dictDateSelected.count == 2) {
    //        [dictDate setObject:[[dictDateSelected valueForKey:@"FirstDay"] valueForKey:@"Day"] forKey:@"firstDate"];
    //        [dictDate setObject:[[dictDateSelected valueForKey:@"SecondDay"] valueForKey:@"Day"] forKey:@"secondDate"];
    //    }
    
    //    if (dictDate.count == 0) {
    //        [dictDate setObject:@"" forKey:@"firstDate"];
    //        [dictDate setObject:@"" forKey:@"secondDate"];
    //        [dictDate setObject:_textFieldArrival.text forKey:@"Arrival_time"];
    //        [dictDate setObject:_textFieldDeparture.text forKey:@"Departure_time"];
    //
    //    }
    //    else if (dictDate.count == 1){
    //        [dictDate setObject:@"" forKey:@"secondDate"];
    //        [dictDate setObject:_textFieldArrival.text forKey:@"Arrival_time"];
    //        [dictDate setObject:_textFieldDeparture.text forKey:@"Departure_time"];
    //
    //    }
    //    else{
    //        [dictDate setObject:_textFieldArrival.text forKey:@"Arrival_time"];
    //        [dictDate setObject:_textFieldDeparture.text forKey:@"Departure_time"];
    //
    //
    //        NSDate *date1 = [[dictDateSelected valueForKey:@"FirstDay"] valueForKey:@"Day"];
    //        NSDate *date2 = [[dictDateSelected valueForKey:@"SecondDay"] valueForKey:@"Day"];
    //
    //        NSTimeInterval secondsBetween = [date2 timeIntervalSinceDate:date1];
    //
    //        int numberOfDays = secondsBetween / 86400;
    //        NSLog(@"%d",numberOfDays);
    //        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    //        [formatter setDateFormat:@"dd-MM-yyyy"];
    //        NSDate *startDate = [dictDate valueForKey:@"firstDate"];
    //        NSDate *endDate = [dictDate valueForKey:@"secondDate"];
    //        [dictDate setObject:[formatter stringFromDate:startDate] forKey:@"firstDate"];
    //        [dictDate setObject:[formatter stringFromDate:endDate] forKey:@"secondDate"];
    //        [dictDate setObject:[NSNumber numberWithInt:numberOfDays] forKey:@"NumberOfNights"];
    //        [defaults setObject:dictDate forKey:@"JourneyDate"];
    //        [defaults synchronize];
    //    }
    
    
    if (dictDateSelected.count > 0){
        
        if([dictDateSelected valueForKey:@"FirstDay"] == nil){
            [[CommonMethods sharedInstance]AlertMessage:@"Please select Date to proceed!!!" andViewController:self];
        }
        
        else if ([dictDateSelected valueForKey:@"Departure_time"] == nil){
            [[CommonMethods sharedInstance]AlertMessage:@"Please select Departure Time to proceed!!!" andViewController:self];
        }
        else{
            [dictDate setObject:[[dictDateSelected valueForKey:@"FirstDay"] valueForKey:@"Day"] forKey:@"firstDate"];
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"dd-MM-yyyy"];
            NSDate *startDate = [dictDate valueForKey:@"firstDate"];
            [dictDate setObject:[formatter stringFromDate:startDate] forKey:@"firstDate"];
            [dictDate setObject:[dictDateSelected valueForKey:@"Departure_time"] forKey:@"Departure_time"];
            // [dictDate setObject:self.intDurationId forKey:@"price_id"];
            [dictDate setObject:[NSNumber numberWithInt:self.intDurationId] forKey:@"price_id"];
            [dictDate setObject:self.strDuration forKey:@"Duration"];
            [defaults setObject:dictDate forKey:@"JourneyDate"];
            [self.navigationController popViewControllerAnimated:true];
        }
        
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:@"Please select Departure Date & Time to proceed!!!" andViewController:self];
    }
}

- (void) updateUI{
    [self setCalenderForMonthNumber:monthNo andYear:yearNo];
}

# pragma mark : Setting time to the TextField
-(void)date:(UITextField *)sender{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"hh:mm a"];
    self.textFieldDeparture.text = [dateFormat stringFromDate:[pickerTime date]];
    
    [dateFormat setDateFormat:@"HH:mm"];
    NSString *dateString = [dateFormat stringFromDate:[pickerTime date]];
    NSString *strtimeStamp = [NSString stringWithFormat:@"%f",[[pickerTime date] timeIntervalSince1970] * 1000];
    int timestamp = [strtimeStamp intValue];
    [dictDateSelected setObject:dateString forKey:@"Departure_time"];
}

# pragma mark: Setting up the frame for the calender container view
-(void)CalendarCreation{
    int width;
    if (IS_IPHONE_5) {
        width = 245/7;
    }
    else if(IS_IPHONE_6){
        width = 300/7;
    }
    else if (IS_IPHONE_X){
        width = 300.0/7;
    }
    else {
        width = 339/7;
    }
}

- (UIButton *) addButtonForDatesWithFrame:(CGRect) frame1 andTag:(int) tag1{
    UIButton *btn = [[UIButton alloc] initWithFrame:frame1];
    [_containerView addSubview:btn];
    [btn addTarget:self action:@selector(dateButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [btn setTag:tag1];
    [btn setTitle:[NSString stringWithFormat:@"%d",tag1] forState:UIControlStateNormal];
    btn.titleLabel.font = FontOpenSans(15);
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *strCurrentDate = [formatter stringFromDate:currentDate];
    NSString *strCurrentYear = [strCurrentDate substringWithRange:NSMakeRange(0, 4)];
    NSString *strCurrentMonth = [strCurrentDate substringWithRange:NSMakeRange(5,2)];
    if ([strCurrentYear intValue] == yearNo) {
        if ([strCurrentMonth intValue] == monthNo) {
            strCurrentDate = [strCurrentDate substringFromIndex:8];
            if ([strCurrentDate intValue] >= tag1) {
                btn.userInteractionEnabled = false;
                [btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            }
            if ([strCurrentDate intValue] == tag1) {
                btn.backgroundColor = [UIColor colorWithRed:149.0/255.0 green:135.0/255.0 blue:238.0/255.0 alpha:1.0];
                [[CommonMethods sharedInstance]CornerRadius:btn Radius:_containerView.frame.size.width/14 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
            }
        }
    }
    return btn;
}


#pragma mark - Click of date Buttons

-(void)dateButtonClick:(UIButton *)sender{
    NSLog(@"%ld",(long)[sender tag]);
    UIButton *btn = (UIButton *)sender;
    if ([self.strDuration isEqualToString:@"per_hour"]){
        [self daySelection:btn];
    }
    else if ([self.strDuration isEqualToString:@"per_day"]){
        [self daySelection:btn];
    }
    else if ([self.strDuration isEqualToString:@"half_day"]){
        [self daySelection:btn];
    }
    else if ([self.strDuration isEqualToString:@"week"]){
        [self WeekSelection:btn];
    }
    //    my_date_start < saved_date_start && my_date_end > saved_Date_end
}

-(void)halfDaySelection:(UIButton *)sender{
    
}

-(void)daySelection:(UIButton *)sender{
    UIButton *btn = (UIButton *)sender;
    dictDateSelected = [[NSMutableDictionary alloc]init];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[NSNumber numberWithInteger:[sender tag]] forKey:@"Date"];
    [dict setObject:[NSNumber numberWithInteger:monthNo] forKey:@"month"];
    [dict setObject:[NSNumber numberWithInteger:yearNo] forKey:@"year"];
    
    NSString *strDate = [NSString stringWithFormat:@"%ld-%d-%d",(long)[sender tag],monthNo,yearNo];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *firstDate = [formatter dateFromString:strDate];
    [dict setObject:firstDate forKey:@"Day"];
    
    [dictDateSelected setObject:dict forKey:@"FirstDay"];
    btn.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
    btn.userInteractionEnabled = false;
    [[CommonMethods sharedInstance]CornerRadius:btn Radius:_containerView.frame.size.width/14 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btn.userInteractionEnabled = false;
    if (btnfirst){
        btnfirst.backgroundColor = [UIColor whiteColor];
        [btnfirst setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btnfirst.userInteractionEnabled = true;
    }
    btnfirst = btn;
}

-(void)WeekSelection:(UIButton *)sender{
    
    if (dictDateSelected.count == 2){
        [self DeselectDates:dictDateSelected];
    }
    
    dictDateSelected = [[NSMutableDictionary alloc]init];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[NSNumber numberWithInteger:[sender tag]] forKey:@"Date"];
    [dict setObject:[NSNumber numberWithInteger:monthNo] forKey:@"month"];
    [dict setObject:[NSNumber numberWithInteger:yearNo] forKey:@"year"];
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    //NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSString *strDate = [NSString stringWithFormat:@"%ld-%d-%d",(long)[sender tag],monthNo,yearNo];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setTimeZone:sourceTimeZone];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *firstDate = [formatter dateFromString:strDate];
    [dict setObject:firstDate forKey:@"Day"];
    
    [dictDateSelected setObject:dict forKey:@"FirstDay"];
    
    int daysToAdd = 7;  // or 60 :-)
    NSDate *newDate1 = [firstDate initWithTimeInterval:60*60*24*daysToAdd - 1 sinceDate:firstDate]; //[firstDate initWithTimeIntervalSince1970:60*60*24*daysToAdd];
    NSString *strSecondDate = [formatter stringFromDate:newDate1];
    
    NSMutableDictionary *dict2 = [[NSMutableDictionary alloc]init];
    NSString *strSecondDate1 = [strSecondDate substringWithRange:NSMakeRange(0,2)];
    NSInteger secondDate = [strSecondDate1 integerValue];
    NSString *strSecondMonth1 = [strSecondDate substringWithRange:NSMakeRange(3,2)];
    NSInteger secondMonth = [strSecondMonth1 integerValue];
    
    NSString *strSecondyear1 = [strSecondDate substringWithRange:NSMakeRange(6,4)];
    NSInteger secondYear = [strSecondyear1 integerValue];
    [dict2 setObject:[NSNumber numberWithInteger:secondDate] forKey:@"Date"];
    [dict2 setObject:[NSNumber numberWithInteger:secondMonth] forKey:@"month"];
    [dict2 setObject:[NSNumber numberWithInteger:secondYear] forKey:@"year"];
    [dict2 setObject:newDate1 forKey:@"Day"];
    
    [dictDateSelected setObject:dict2 forKey:@"SecondDay"];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@" StartDay >= %@ && StartDay <= %@" , [[dictDateSelected valueForKey:@"FirstDay"] valueForKey:@"Day"],[[dictDateSelected valueForKey:@"SecondDay"] valueForKey:@"Day"]];
    NSArray *arr = [arrDates filteredArrayUsingPredicate:predicate];
    if ([arr count] >0 ) {
        [dictDateSelected removeObjectForKey:@"FirstDay"];
        [dictDateSelected removeObjectForKey:@"SecondDay"];
        [[CommonMethods sharedInstance]AlertMessage:@"Can't select the date as the Yacht is booked between the selected dates!!!" andViewController:self];
    }
    else{
        [self SetColorStripOntheCalendar:dictDateSelected];
    }
}


//-(void)WeekSelection:(UIButton *)sender{
//    UIButton *btn = (UIButton *)sender;
//    if (!btnSecond && btnfirst){
//        btnSecond = sender;
//        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//        [dict setObject:[NSNumber numberWithInteger:[sender tag]] forKey:@"Date"];
//        [dict setObject:[NSNumber numberWithInteger:monthNo] forKey:@"month"];
//        [dict setObject:[NSNumber numberWithInteger:yearNo] forKey:@"year"];
//        NSString *strDate = [NSString stringWithFormat:@"%ld-%d-%d",(long)[sender tag],monthNo,yearNo];
//        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
//        [formatter setDateFormat:@"dd-MM-yyyy"];
//        NSDate *firstDate = [formatter dateFromString:strDate];
//        [dict setObject:firstDate forKey:@"Day"];
//        [dictDateSelected setObject:dict forKey:@"SecondDay"];
//        NSComparisonResult result;
//        result = [[[dictDateSelected valueForKey:@"FirstDay"] valueForKey:@"Day"]compare:[[dictDateSelected valueForKey:@"SecondDay"]valueForKey:@"Day" ]]; // comparing two dates
//        if(result == NSOrderedAscending){
//            NSPredicate *predicate = [NSPredicate predicateWithFormat:@" StartDay >= %@ && LastDay <= %@" , [[dictDateSelected valueForKey:@"FirstDay"] valueForKey:@"Day"],firstDate];
//            NSArray *arr = [arrDates filteredArrayUsingPredicate:predicate];
//            NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"StartDay <= %@ && LastDay >= %@",firstDate,firstDate];
//            NSArray *arr1 = [arrDates filteredArrayUsingPredicate:predicate2];
//            if ([arr count] >0 || [arr1 count]>0) {
//                [dictDateSelected removeObjectForKey:@"SecondDay"];
//                btnSecond = nil;
//            }
//            else{
//                [self SetColorStripOntheCalendar:dictDateSelected];
//                NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//                [dict setObject:[[dictDateSelected valueForKey:@"FirstDay"]valueForKey:@"Date"] forKey:@"StartDate"];
//                [dict setObject:[[dictDateSelected valueForKey:@"FirstDay"]valueForKey:@"month"] forKey:@"StartMonth"];
//                [dict setObject:[[dictDateSelected valueForKey:@"FirstDay"]valueForKey:@"year"] forKey:@"StartYear"];
//                [dict setObject:[[dictDateSelected valueForKey:@"FirstDay"]valueForKey:@"Day"] forKey:@"StartDay"];
//                [dict setObject:[[dictDateSelected valueForKey:@"SecondDay"]valueForKey:@"Date"] forKey:@"EndDate"];
//                [dict setObject:[[dictDateSelected valueForKey:@"SecondDay"]valueForKey:@"month"] forKey:@"EndMonth"];
//                [dict setObject:[[dictDateSelected valueForKey:@"SecondDay"]valueForKey:@"year"] forKey:@"EndYear"];
//                [dict setObject:[[dictDateSelected valueForKey:@"SecondDay"]valueForKey:@"Day"] forKey:@"LastDay"];
//                [dict setObject:[NSNumber numberWithBool:false] forKey:@"isApi"];
//                [arrDates addObject:dict];
//                btnSecond = nil;
//                btnfirst = nil;
//            }
//        }
//        else if(result == NSOrderedDescending){
//            NSDictionary *lastDate = [dictDateSelected valueForKey:@"FirstDay"];
//            dictDateSelected = nil;
//            dictDateSelected = [[NSMutableDictionary alloc]init];
//            [dictDateSelected setObject:lastDate forKey:@"SecondDay"];
//            [dictDateSelected setObject:dict forKey:@"FirstDay"];
//            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(StartDay >= %@ && LastDay <= %@) or (StartDay <= %@ && LastDay >= %@)" , [[dictDateSelected valueForKey:@"FirstDay"] valueForKey:@"Day"],[[dictDateSelected valueForKey:@"SecondDay"] valueForKey:@"Day"],firstDate,firstDate];
//            NSArray *arr = [arrDates filteredArrayUsingPredicate:predicate];
//            // NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"StartDay <= %@ && LastDay >= %@",firstDate,firstDate];
//            // NSArray *arr1 = [arrDates filteredArrayUsingPredicate:predicate2];
//            if ([arr count] >0) {
//                NSDictionary *lastDate = [dictDateSelected valueForKey:@"SecondDay"];
//                dictDateSelected = nil;
//                dictDateSelected = [[NSMutableDictionary alloc]init];
//                [dictDateSelected setObject:lastDate forKey:@"FirstDay"];
//                btnSecond = nil;
//            }
//            else{
//                [self SetColorStripOntheCalendar:dictDateSelected];
//                NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//                [dict setObject:[[dictDateSelected valueForKey:@"FirstDay"]valueForKey:@"Date"] forKey:@"StartDate"];
//                [dict setObject:[[dictDateSelected valueForKey:@"FirstDay"]valueForKey:@"month"] forKey:@"StartMonth"];
//                [dict setObject:[[dictDateSelected valueForKey:@"FirstDay"]valueForKey:@"year"] forKey:@"StartYear"];
//                [dict setObject:[[dictDateSelected valueForKey:@"FirstDay"]valueForKey:@"Day"] forKey:@"StartDay"];
//                [dict setObject:[[dictDateSelected valueForKey:@"SecondDay"]valueForKey:@"Date"] forKey:@"EndDate"];
//                [dict setObject:[[dictDateSelected valueForKey:@"SecondDay"]valueForKey:@"month"] forKey:@"EndMonth"];
//                [dict setObject:[[dictDateSelected valueForKey:@"SecondDay"]valueForKey:@"year"] forKey:@"EndYear"];
//                [dict setObject:[[dictDateSelected valueForKey:@"SecondDay"]valueForKey:@"Day"] forKey:@"LastDay"];
//                [dict setObject:[NSNumber numberWithBool:false] forKey:@"isApi"];
//                [arrDates addObject:dict];
//                btnSecond = nil;
//                btnfirst = nil;
//            }
//            NSLog(@"newDate is less");
//        }
//        else if(result == NSOrderedSame){
//            NSLog(@"Both dates are same");
//        }
//        else{
//            NSLog(@"Date cannot be compared");
//        }
//    }
//    else{
//        if (dictDateSelected.count == 2) {
//            [self DeselectDates:dictDateSelected];
//            dictDateSelected = [NSMutableDictionary new];
//            for (NSDictionary *dict in arrDates) {
//                if (![[dict valueForKey:@"isApi"]boolValue]) {
//                    [arrDates removeObject:dict];
//                }
//            }
//        }
//        btnfirst = sender;
//        NSString *strDate = [NSString stringWithFormat:@"%ld-%d-%d",(long)[sender tag],monthNo,yearNo];
//        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
//        [formatter setDateFormat:@"dd-MM-yyyy"];
//        NSDate *firstDate = [formatter dateFromString:strDate];
//
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"StartDay <= %@ && LastDay >= %@",firstDate,firstDate];
//        NSArray *arrFileterd = [arrDates filteredArrayUsingPredicate:predicate];
//
//        if (!([arrFileterd count]>0)) {
//            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//            [dict setObject:[NSNumber numberWithInteger:[sender tag]] forKey:@"Date"];
//            [dict setObject:[NSNumber numberWithInteger:monthNo] forKey:@"month"];
//            [dict setObject:[NSNumber numberWithInteger:yearNo] forKey:@"year"];
//            [dict setObject:firstDate forKey:@"Day"];
//            [dictDateSelected setObject:dict forKey:@"FirstDay"];
//            btn.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
//            btn.userInteractionEnabled = false;
//            [[CommonMethods sharedInstance]CornerRadius:btn Radius:_containerView.frame.size.width/14 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
//            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        }
//    }
//}


#pragma mark - setting Calendar for month

- (void) setCalenderForMonthNumber:(int) month andYear:(int) year{
    [_labelMonth setText:[NSString stringWithFormat:@"%@, %d", [self monthNameForNumber:month], year]];
    NSNumber *number = _dictLocation[@"lat"];
    double latitude = 0, longitude = 0;
    latitude = number.doubleValue;
    number = _dictLocation[@"lng"];
    longitude = number.doubleValue;
    NSTimeZone *timeZone = [[CommonMethods sharedInstance]TimeZoneWithRespectToLatitiue:latitude andLongitude:longitude];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    calendar.timeZone = [NSTimeZone timeZoneWithAbbreviation:timeZone.abbreviation];
    [calendar setMinimumDaysInFirstWeek:1];
    NSDateComponents *dateComponent = [calendar components:(NSCalendarUnitYear | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitWeekday) fromDate:[NSDate date]];
    dateComponent.day = 1;
    dateComponent.month = month;
    dateComponent.year = year;
    NSDate *dayOneInCurrentMonth = [calendar dateFromComponents:dateComponent];
    NSString *dayName = [dateFormatter stringFromDate:dayOneInCurrentMonth];
    range = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:dayOneInCurrentMonth];
    [self setDatesOnButtons:[self dayNumberForString:dayName] andNumberOfDays:(int)range.length];
}


- (int) dayNumberForString:(NSString*) dayName{
    return (int)[[@"Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday" componentsSeparatedByString:@","] indexOfObject:dayName]+1;
}


#pragma mark - Method for setting buttons
- (void) setDatesOnButtons:(int) firstDay andNumberOfDays:(int) dayCount{
    for(UIButton *subview in [_containerView subviews]) {
        [subview removeFromSuperview];
    }
    int width;
    if (IS_IPHONE_5) {
        width = 245/7;
    }
    else if(IS_IPHONE_6){
        width = 300/7;
    }
    else if (IS_IPHONE_X){
        width = 300/7;
    }
    else{
        width = 339/7;
    }
    
    int j = 0;
    int l = firstDay-1;
    int m = 0;
    // int first = firstDay;
    //int  width = 300/7;
    int k = _containerView.frame.size.width/7;
    for(int i = 1; i <= dayCount; i++){
        
        [self addButtonForDatesWithFrame:CGRectMake(l*k, 0.0 + m*j+13, width, width) andTag:i];
        
        k = width;
        l++;
        if (l %7 == 0) {
            k = 0;
            j = width+7;
            l=0;
            m++;
        }
    }
    [self PreviousButtonClickAction:dictDateSelected andDayCount:dayCount];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"StartMonth == %d or EndMonth == %d", monthNo,monthNo];
    NSArray *arr = [arrDates filteredArrayUsingPredicate:predicate];
    
    for (NSDictionary *dict in arr) {
        [self ColorStripFOrSelectedDatesForArray:dict];
    }
}

#pragma mark - Getting month name from month number
- (NSString*)monthNameForNumber:(int) monthNumber{
    if(monthNumber > 12)
        monthNumber -= 12;
    else if(monthNumber < 1)
        monthNumber = 12;
    return [[@"January,February,March,April,May,June,July,August,September,October,November,December" componentsSeparatedByString:@","] objectAtIndex:monthNumber-1];
}

#pragma mark - Button Previous and Next click
- (void) btnPreviousOrNextClick:(UIButton*) sender{
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *strCurrentDate = [formatter stringFromDate:currentDate];
    NSString *strCurrentYear = [strCurrentDate substringWithRange:NSMakeRange(0, 4)];
    NSString *strCurrentMonth = [strCurrentDate substringWithRange:NSMakeRange(5,2)];
    if(sender.tag == 1){
        if ([strCurrentYear intValue] != yearNo) {
            if(monthNo == 1){
                monthNo = 12;
                yearNo --;
            }
            else{
                monthNo --;
            }
            [self setCalenderForMonthNumber:monthNo andYear:yearNo];
        }
        else if ([strCurrentMonth intValue] != monthNo) {
            if(monthNo == 1){
                monthNo = 12;
                yearNo --;
            }
            else{
                monthNo --;
            }
            [self setCalenderForMonthNumber:monthNo andYear:yearNo];
        }
    }
    else{
        if ([strCurrentYear intValue] != yearNo && [strCurrentMonth intValue] != monthNo ) {
        }
        if(monthNo == 12){
            monthNo = 1;
            yearNo ++;
        }
        else{
            monthNo++;
        }
        [self setCalenderForMonthNumber:monthNo andYear:yearNo];
    }
}


#pragma mark - Changing color strip while clikcing Next and previous buttons

-(void)PreviousButtonClickAction:(NSMutableDictionary *)dictCalendarDate andDayCount:(int)lastday{
    NSInteger firstDay = [[[dictCalendarDate valueForKey:@"FirstDay"] valueForKey:@"Date"] integerValue];
    NSInteger firstMonth = [[[dictCalendarDate valueForKey:@"FirstDay"] valueForKey:@"month"] integerValue];
    NSInteger firstYear = [[[dictCalendarDate valueForKey:@"FirstDay"]valueForKey:@"year"]integerValue];
    
    if (dictCalendarDate.count == 1) {
        if (yearNo == firstYear) {
            if (monthNo == firstMonth) {
                for (NSInteger i =firstDay; i == firstDay ; i++) {
                    UIButton *btn = [_containerView viewWithTag:i];
                    btn.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                    [[CommonMethods sharedInstance]CornerRadius:btn Radius:_containerView.frame.size.width/14 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
                    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                }
            }
        }
    }
    else{
        NSInteger secondDay  = [[[dictCalendarDate valueForKey:@"SecondDay"] valueForKey:@"Date"] integerValue];
        NSInteger secondMonth = [[[dictCalendarDate valueForKey:@"SecondDay"] valueForKey:@"month"] integerValue];
        NSInteger secondYear = [[[dictCalendarDate valueForKey:@"SecondDay"] valueForKey:@"year"] integerValue];
        
        if (yearNo == firstYear && yearNo == secondYear) {
            if (monthNo == firstMonth && monthNo == secondMonth) {
                for (NSInteger i =firstDay; i <= secondDay ; i++) {
                    UIButton *dateDay = [_containerView viewWithTag:i];
                    dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                    [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    dateDay.userInteractionEnabled = false;
                    dateDay.layer.cornerRadius = 0.0;
                    if (i == firstDay) {
                        dateDay.layer.cornerRadius = 0.0;
                        [self LeftCornerRadius:dateDay];
                    }
                    if (i == secondDay) {
                        dateDay.layer.cornerRadius = 0.0;
                        [self RightCornerradius:dateDay];
                    }
                }
            }
            else if (monthNo == firstMonth || monthNo == secondMonth) {
                if(monthNo == firstMonth){
                    for (NSInteger i = firstDay; i <= lastday; i++) {
                        UIButton *dateDay = [_containerView viewWithTag:i];
                        dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                        [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        dateDay.userInteractionEnabled = false;
                        dateDay.layer.cornerRadius = 0.0;
                        if (i == firstDay) {
                            dateDay.layer.cornerRadius = 0.0;
                            [self LeftCornerRadius:dateDay];
                        }
                    }
                    
                }
                else if(monthNo == secondMonth){
                    for (NSInteger i = 1; i <= secondDay; i++) {
                        UIButton *dateDay = [_containerView viewWithTag:i];
                        dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                        [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        dateDay.userInteractionEnabled = false;
                        dateDay.layer.cornerRadius = 0.0;
                        if (i == secondDay) {
                            dateDay.layer.cornerRadius = 0.0;
                            [self RightCornerradius:dateDay];
                        }
                        
                    }
                }
            }
        }
        else if (yearNo == secondYear && yearNo != firstYear) {
            if (firstMonth > secondMonth && secondMonth == monthNo) {
                for (NSInteger i =1; i <= secondDay; i++) {
                    UIButton *dateDay = [_containerView viewWithTag:i];
                    dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                    [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    dateDay.userInteractionEnabled = false;
                    dateDay.layer.cornerRadius = 0.0;
                    if (i == secondDay) {
                        dateDay.layer.cornerRadius = 0.0;
                        [self RightCornerradius:dateDay];
                    }
                    
                }
                
            }
        }
        else if (yearNo == firstYear && secondYear != yearNo){
            if (firstMonth > secondMonth && firstMonth == monthNo) {
                for (NSInteger i =firstDay; i <= lastday; i++) {
                    UIButton *dateDay = [_containerView viewWithTag:i];
                    dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                    [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    dateDay.userInteractionEnabled = false;
                    dateDay.layer.cornerRadius = 0.0;
                    if (i == firstDay) {
                        dateDay.layer.cornerRadius = 0.0;
                        [self LeftCornerRadius:dateDay];
                    }
                }
                
            }
            
        }
        NSString *strFirstDate = [NSString stringWithFormat:@"%d-%d-%d",01,monthNo,yearNo];
        NSString *strLastDate = [NSString stringWithFormat:@"%lu-%d-%d",(unsigned long)range.length,monthNo,yearNo];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"dd-MM-yyyy"];
        NSDate *firstDate = [formatter dateFromString:strFirstDate];
        NSDate *lastDate = [formatter dateFromString:strLastDate];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"StartDay < %@ AND LastDay > %@", firstDate,lastDate];
        NSArray *arr = [arrDates filteredArrayUsingPredicate:predicate];
        if ([arr count ] >0 ) {
            for (NSInteger i = 1; i <= range.length; i++) {
                UIButton *dateDay = [_containerView viewWithTag:i];
                if ([[[arr valueForKey:@"isApi"] objectAtIndex:0]boolValue]) {
                    dateDay.backgroundColor = [UIColor lightGrayColor];
                }
                else{
                    dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                }
                
                [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                dateDay.userInteractionEnabled = false;
                dateDay.layer.cornerRadius = 0.0;
            }
        }
    }
}

#pragma mark - Method for showing the selected dates
-(void)ColorStripFOrSelectedDatesForArray:(NSDictionary *)arr{
    
    if ([[arr valueForKey:@"isApi"] boolValue]) {
        NSInteger firstDay = [[arr valueForKey:@"StartDate"] integerValue];
        NSInteger firstMonth = [[arr valueForKey:@"StartMonth"] integerValue];
        NSInteger firstYear = [[arr valueForKey:@"StartYear"]integerValue];
        
        
        NSInteger secondDay  = [[arr valueForKey:@"EndDate"] integerValue];
        NSInteger secondMonth = [[arr valueForKey:@"EndMonth"] integerValue];
        NSInteger secondYear = [[arr valueForKey:@"EndYear"] integerValue];
        
        
        
        if (yearNo == firstYear && yearNo == secondYear) {
            
            if (firstDay == secondDay){
                for (NSInteger i =firstDay; i <= secondDay ; i++) {
                    UIButton *dateDay = [_containerView viewWithTag:i];
                    dateDay.backgroundColor = [UIColor lightGrayColor];
                    [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    dateDay.userInteractionEnabled = false;
                    dateDay.layer.cornerRadius = 0.0;
                    dateDay.layer.cornerRadius = dateDay.frame.size.height/2;
                    dateDay.clipsToBounds = true;
                    //                    if (i == firstDay) {
                    //                        dateDay.layer.cornerRadius = 0.0;
                    //                        [self LeftCornerRadius:dateDay];
                    //                    }
                    //                    if (i == secondDay) {
                    //                        dateDay.layer.cornerRadius = 0.0;
                    //                        [self RightCornerradius:dateDay];
                    //                    }
                }
            }
            else if (monthNo == firstMonth && monthNo == secondMonth) {
                for (NSInteger i =firstDay; i <= secondDay ; i++) {
                    UIButton *dateDay = [_containerView viewWithTag:i];
                    dateDay.backgroundColor = [UIColor lightGrayColor];
                    [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    dateDay.userInteractionEnabled = false;
                    dateDay.layer.cornerRadius = 0.0;
                    if (i == firstDay) {
                        dateDay.layer.cornerRadius = 0.0;
                        [self LeftCornerRadius:dateDay];
                    }
                    if (i == secondDay) {
                        dateDay.layer.cornerRadius = 0.0;
                        [self RightCornerradius:dateDay];
                    }
                }
            }
            else if (monthNo == firstMonth || monthNo == secondMonth) {
                if(monthNo == firstMonth){
                    for (NSInteger i = firstDay; i <= range.length; i++) {
                        UIButton *dateDay = [_containerView viewWithTag:i];
                        dateDay.backgroundColor = [UIColor lightGrayColor];
                        [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        dateDay.userInteractionEnabled = false;
                        dateDay.layer.cornerRadius = 0.0;
                        if (i == firstDay) {
                            dateDay.layer.cornerRadius = 0.0;
                            [self LeftCornerRadius:dateDay];
                        }
                    }
                    
                }
                else if(monthNo == secondMonth){
                    for (NSInteger i = 1; i <= secondDay; i++) {
                        UIButton *dateDay = [_containerView viewWithTag:i];
                        dateDay.backgroundColor = [UIColor lightGrayColor];
                        [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        dateDay.userInteractionEnabled = false;
                        dateDay.layer.cornerRadius = 0.0;
                        if (i == secondDay) {
                            dateDay.layer.cornerRadius = 0.0;
                            [self RightCornerradius:dateDay];
                        }
                        
                    }
                }
            }
        }
        else if (yearNo == secondYear && yearNo != firstYear) {
            if (firstMonth > secondMonth && secondMonth == monthNo) {
                for (NSInteger i =1; i <= secondDay; i++) {
                    UIButton *dateDay = [_containerView viewWithTag:i];
                    dateDay.backgroundColor = [UIColor lightGrayColor];
                    [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    dateDay.userInteractionEnabled = false;
                    dateDay.layer.cornerRadius = 0.0;
                    if (i == secondDay) {
                        dateDay.layer.cornerRadius = 0.0;
                        [self RightCornerradius:dateDay];
                    }
                    
                }
                
            }
        }
        //range.length
        else if (yearNo == firstYear && secondYear != yearNo){
            if (firstMonth > secondMonth && firstMonth == monthNo) {
                for (NSInteger i =firstDay; i <= range.length ; i++) {
                    UIButton *dateDay = [_containerView viewWithTag:i];
                    dateDay.backgroundColor = [UIColor lightGrayColor];
                    [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    dateDay.userInteractionEnabled = false;
                    dateDay.layer.cornerRadius = 0.0;
                    if (i == firstDay) {
                        dateDay.layer.cornerRadius = 0.0;
                        [self LeftCornerRadius:dateDay];
                        //dateDay.layer.cornerRadius = dateDay.frame.size.height/2;
                    }
                }
                
            }
            
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Deselct dates and make color strip to white

-(void)DeselectDates:(NSMutableDictionary *)dictCalendarDate{
    NSInteger firstDay = [[[dictCalendarDate valueForKey:@"FirstDay"] valueForKey:@"Date"] integerValue];
    NSInteger firstMonth = [[[dictCalendarDate valueForKey:@"FirstDay"] valueForKey:@"month"] integerValue];
    NSInteger firstYear = [[[dictCalendarDate valueForKey:@"FirstDay"]valueForKey:@"year"]integerValue];
    
    
    NSInteger secondDay  = [[[dictCalendarDate valueForKey:@"SecondDay"] valueForKey:@"Date"] integerValue];
    NSInteger secondMonth = [[[dictCalendarDate valueForKey:@"SecondDay"] valueForKey:@"month"] integerValue];
    NSInteger secondYear = [[[dictCalendarDate valueForKey:@"SecondDay"] valueForKey:@"year"] integerValue];
    
    if (firstYear == yearNo && yearNo != secondYear) {
        if (firstMonth > secondMonth && firstMonth == monthNo) {
            for (NSInteger i =firstDay; i <= range.length; i++) {
                UIButton *dateDay = [_containerView viewWithTag:i];
                dateDay.backgroundColor = [UIColor whiteColor];
                [dateDay setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                dateDay.userInteractionEnabled = true;
                dateDay.layer.cornerRadius = 0.0;
                if (i == firstDay) {
                    dateDay.layer.cornerRadius = 0.0;
                    [self RemoveLeftCornerRadius:dateDay];
                }
                if (i == secondDay) {
                    dateDay.layer.cornerRadius = 0.0;
                    [self RemoveLeftCornerRadius:dateDay];
                }
            }
        }
        
    }
    else if (firstYear !=  yearNo && secondYear == yearNo){
        for (NSInteger i =1; i <= secondDay; i++) {
            UIButton *dateDay = [_containerView viewWithTag:i];
            dateDay.backgroundColor = [UIColor whiteColor];
            [dateDay setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            dateDay.userInteractionEnabled = true;
            dateDay.layer.cornerRadius = 0.0;
            if (i == firstDay) {
                dateDay.layer.cornerRadius = 0.0;
                [self RemoveLeftCornerRadius:dateDay];
            }
            if (i == secondDay) {
                dateDay.layer.cornerRadius = 0.0;
                [self RemoveLeftCornerRadius:dateDay];
            }
        }
        
    }
    else{
        if (firstMonth == monthNo && secondMonth == monthNo) {
            for (NSInteger i = firstDay; i <= secondDay ; i++) {
                UIButton *dateDay = [_containerView viewWithTag:i];
                dateDay.backgroundColor = [UIColor whiteColor];
                [dateDay setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                dateDay.userInteractionEnabled = true;
                dateDay.layer.cornerRadius = 0.0;
                if (i == firstDay) {
                    dateDay.layer.cornerRadius = 0.0;
                    [self RemoveLeftCornerRadius:dateDay];
                }
                if (i == secondDay) {
                    dateDay.layer.cornerRadius = 0.0;
                    [self RemoveLeftCornerRadius:dateDay];
                }
            }
            
        }
        else if (monthNo == firstMonth) {
            for (NSInteger i = firstDay; i <= range.length ; i++) {
                UIButton *dateDay = [_containerView viewWithTag:i];
                dateDay.backgroundColor = [UIColor whiteColor];
                [dateDay setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                dateDay.userInteractionEnabled = true;
                dateDay.layer.cornerRadius = 0.0;
                if (i == firstDay) {
                    dateDay.layer.cornerRadius = 0.0;
                    [self RemoveLeftCornerRadius:dateDay];
                }
                if (i == secondDay) {
                    dateDay.layer.cornerRadius = 0.0;
                    [self RemoveLeftCornerRadius:dateDay];
                }
            }
            
        }
        else if (monthNo == secondMonth){
            for (NSInteger i = 1; i <= secondDay ; i++) {
                UIButton *dateDay = [_containerView viewWithTag:i];
                dateDay.backgroundColor = [UIColor whiteColor];
                [dateDay setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                dateDay.userInteractionEnabled = true;
                dateDay.layer.cornerRadius = 0.0;
                if (i == firstDay) {
                    dateDay.layer.cornerRadius = 0.0;
                    [self RemoveLeftCornerRadius:dateDay];
                }
                if (i == secondDay) {
                    dateDay.layer.cornerRadius = 0.0;
                    [self RemoveLeftCornerRadius:dateDay];
                }
            }
            
        }
        
    }
    
    
}

#pragma mark - Selection of Date and making color strip

-(void)SetColorStripOntheCalendar:(NSMutableDictionary *)dictCalendarDate{
    
    NSInteger firstDay = [[[dictCalendarDate valueForKey:@"FirstDay"] valueForKey:@"Date"] integerValue];
    NSInteger firstMonth = [[[dictCalendarDate valueForKey:@"FirstDay"] valueForKey:@"month"] integerValue];
    NSInteger firstYear = [[[dictCalendarDate valueForKey:@"FirstDay"]valueForKey:@"year"]integerValue];
    
    NSInteger secondDay  = [[[dictCalendarDate valueForKey:@"SecondDay"] valueForKey:@"Date"] integerValue];
    NSInteger secondMonth = [[[dictCalendarDate valueForKey:@"SecondDay"] valueForKey:@"month"] integerValue];
    NSInteger secondYear = [[[dictCalendarDate valueForKey:@"SecondDay"] valueForKey:@"year"] integerValue];
    
    if (firstYear != yearNo && yearNo == secondYear) {
        if (firstMonth > secondMonth && secondMonth == monthNo) {
            for (NSInteger i =1; i <= secondDay; i++) {
                UIButton *dateDay = [_containerView viewWithTag:i];
                dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                dateDay.layer.cornerRadius = 0.0;
                if (i == secondDay) {
                    dateDay.layer.cornerRadius = 0.0;
                    [self RightCornerradius:dateDay];
                }
            }
        }
    }
    else if (firstYear != yearNo && secondYear == yearNo){
        if (firstMonth > secondMonth && firstMonth == monthNo) {
            for (NSInteger i =firstDay; i <= range.length; i++) {
                UIButton *dateDay = [_containerView viewWithTag:i];
                dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                dateDay.layer.cornerRadius = 0.0;
                if (i == firstDay) {
                    dateDay.userInteractionEnabled = false;
                    dateDay.layer.cornerRadius = 0.0;
                    [self LeftCornerRadius:dateDay];
                }
            }
        }
    }
    else{
        
        if (firstMonth == monthNo && secondMonth == monthNo) {
            for (NSInteger i =firstDay; i <= secondDay; i++) {
                UIButton *dateDay = [_containerView viewWithTag:i];
                dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                dateDay.layer.cornerRadius = 0.0;
                if (i == firstDay) {
                    dateDay.layer.cornerRadius = 0.0;
                    dateDay.userInteractionEnabled = false;
                    [self LeftCornerRadius:dateDay];
                }
                if (i == secondDay) {
                    dateDay.layer.cornerRadius = 0.0;
                    [self RightCornerradius:dateDay];
                }
            }
        }
        else if (firstMonth == monthNo) {
            for (NSInteger i =firstDay; i <= range.length; i++) {
                UIButton *dateDay = [_containerView viewWithTag:i];
                dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                dateDay.layer.cornerRadius = 0.0;
                if (i == firstDay) {
                    dateDay.userInteractionEnabled = false;
                    dateDay.layer.cornerRadius = 0.0;
                    [self LeftCornerRadius:dateDay];
                }
            }
        }
        else{
            for (NSInteger i = 1; i <= secondDay ; i++) {
                UIButton *dateDay = [_containerView viewWithTag:i];
                dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                //                dateDay.userInteractionEnabled = false;
                dateDay.layer.cornerRadius = 0.0;
                if (i == secondDay) {
                    dateDay.layer.cornerRadius = 0.0;
                    [self RightCornerradius:dateDay];
                }
            }
        }
        
    }
}


#pragma mark- Test
- (void) markDateBoundFromIndex:(NSInteger)startIndex  toIndex:(NSInteger)endIndex {
    
    for (NSInteger i = startIndex; i <= endIndex; i++) {
        UIButton *dateDay = [_containerView viewWithTag:i];
        dateDay.backgroundColor = [UIColor lightGrayColor];
        [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        dateDay.userInteractionEnabled = false;
        if (i == startIndex) {
            [self LeftCornerRadius:dateDay];
        }
        if (i == endIndex) {
            [self RightCornerradius:dateDay];
        }
        
    }
}

#pragma mark - Left corner radius
-(void)LeftCornerRadius:(UIButton *)view{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(view.frame.size.width/2, view.frame.size.width/2)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    view.layer.mask = maskLayer;
}

#pragma mark - Right corner radius

-(void)RightCornerradius:(UIButton *)view{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(view.frame.size.width/2, view.frame.size.width/2)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    view.layer.mask = maskLayer;
}

#pragma mark - Removing  left corner radius

-(void)RemoveLeftCornerRadius:(UIButton *)view{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(0,0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    view.layer.mask = maskLayer;
}

#pragma mark - removing right corner radius
-(void)RemoveRightCornerradius:(UIButton *)view{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(0,0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    view.layer.mask = maskLayer;
}


#pragma mark - TableviewDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == _textFieldDeparture) {
        lcConstraintBottomView.constant = 260;
        [UIView animateWithDuration:0.25 animations:^{[self.view layoutIfNeeded];}];
        txtField = textField;
    }
}


# pragma mark - Api method for getting booked dates
-(void)ApiForGettingDates{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@booking_dates?id=%@",kBaseUrlVersion2,_strId] emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                arrDates = [result valueForKey:@"date"];
                for (NSMutableDictionary *dict in arrDates) {
                    NSString *strDate = [dict valueForKey:@"start_date"];
                    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
                    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                    [formatter setTimeZone:sourceTimeZone];
                    [formatter setDateFormat:@"yyyy-MM-dd"];
                    NSDate *date = [formatter dateFromString:strDate];
                    [dict setObject:date forKey:@"StartDay"];
                    NSDictionary *dictDates = [self GetFormattedDatefromString:strDate];
                    [dict setObject:[dictDates valueForKey:@"day"] forKey:@"StartDate"];
                    [dict setObject:[NSNumber numberWithInteger:[[dictDates valueForKey:@"month"] integerValue]] forKey:@"StartMonth"];
                    [dict setObject:[dictDates valueForKey:@"year"] forKey:@"StartYear"];
                    
                    
                    if ([[dict valueForKey:@"term"] isEqualToString:@"week"]){
                        int daysToAdd = 7;  // or 60 :-)
                        NSDate *firstDate = [dict valueForKey:@"StartDay"];
                        NSDate *newDate1 = [firstDate initWithTimeInterval:60*60*24*daysToAdd - 1 sinceDate:firstDate]; //[firstDate initWithTimeIntervalSince1970:60*60*24*daysToAdd];
                        NSString *strSecondDate = [formatter stringFromDate:newDate1];
                        NSDate *dateLast = [formatter dateFromString:strSecondDate];
                        [dict setObject:dateLast forKey:@"LastDay"];
                        NSDictionary *dictEndDates = [self GetFormattedDatefromString:strSecondDate];
                        [dict setObject:[dictEndDates valueForKey:@"day"] forKey:@"EndDate"];
                        [dict setObject:[NSNumber numberWithInteger:[[dictEndDates valueForKey:@"month"] integerValue]] forKey:@"EndMonth"];
                        [dict setObject:[dictEndDates valueForKey:@"year"] forKey:@"EndYear"];
                    }
                    else{
                        int daysToAdd = 0;  // or 60 :-)
                        NSDate *firstDate = [dict valueForKey:@"StartDay"];
                        NSDate *newDate1 = [firstDate initWithTimeInterval:60*60*24*daysToAdd sinceDate:firstDate]; //[firstDate initWithTimeIntervalSince1970:60*60*24*daysToAdd];
                        NSString *strSecondDate = [formatter stringFromDate:newDate1];
                        NSDate *dateLast = [formatter dateFromString:strSecondDate];
                        [dict setObject:dateLast forKey:@"LastDay"];
                        NSDictionary *dictEndDates = [self GetFormattedDatefromString:strSecondDate];
                        [dict setObject:[dictEndDates valueForKey:@"day"] forKey:@"EndDate"];
                        [dict setObject:[NSNumber numberWithInteger:[[dictEndDates valueForKey:@"month"] integerValue]] forKey:@"EndMonth"];
                        [dict setObject:[dictEndDates valueForKey:@"year"] forKey:@"EndYear"];
                    }
                    //
                    //                                        NSString *strLastDate = [dict valueForKey:@"end_date"];
                    //                                        NSDate *dateLast = [formatter dateFromString:strLastDate];
                    //                                        [dict setObject:dateLast forKey:@"LastDay"];
                    //                                        NSDictionary *dictEndDates = [self GetFormattedDatefromString:strLastDate];
                    //                                        [dict setObject:[dictEndDates valueForKey:@"day"] forKey:@"EndDate"];
                    //                                        [dict setObject:[NSNumber numberWithInteger:[[dictEndDates valueForKey:@"month"] integerValue]] forKey:@"EndMonth"];
                    //                                        [dict setObject:[dictEndDates valueForKey:@"year"] forKey:@"EndYear"];
                    [dict setObject:[NSNumber numberWithBool:true] forKey:@"isApi"];
                }
                [self updateUI];
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"StartMonth == %d or EndMonth == %d", monthNo];
                NSArray *arr = [arrDates filteredArrayUsingPredicate:predicate];
                
                for (NSMutableDictionary *dict in arr) {
                    [self ColorStripFOrSelectedDatesForArray:dict];
                }
                NSLog(@"%@",arr);
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
    }
}

-(NSMutableDictionary *)GetFormattedDatefromString:(NSString *)strDate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *startDate = [formatter dateFromString:strDate];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    NSString *dayString = [df stringFromDate:startDate];
    
    [df setDateFormat:@"MM"];
    NSString *monthString = [df stringFromDate:startDate];
    
    [df setDateFormat:@"yyyy"];
    NSString  *yearString = [df stringFromDate:startDate];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:dayString forKey:@"day"];
    [dict setObject:monthString forKey:@"month"];
    [dict setObject:yearString forKey:@"year"];
    return dict;
}

@end
