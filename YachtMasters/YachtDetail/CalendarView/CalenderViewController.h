//
//  CalenderViewController.h
//  YachtMasters
//
//  Created by Anvesh on 08/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CalenderViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *viewBottomBar;
@property (weak, nonatomic) IBOutlet UIView *CalendarView;
@property (weak, nonatomic) IBOutlet UILabel *labelMonth;
@property (weak, nonatomic) IBOutlet UIButton *buttonNext;
@property (weak, nonatomic) IBOutlet UIButton *buttonPreviuos;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *buttonDeparture;
@property (weak, nonatomic) IBOutlet UIButton *buttonReturn;
@property (weak, nonatomic) IBOutlet UITextField *textFieldDeparture;
@property (weak, nonatomic) IBOutlet UITextField *textFieldArrival;
@property(nonatomic,strong) NSDictionary *dictLocation;
@property(nonatomic,weak)NSString *strId;
@property(nonatomic, weak) NSString *strDuration;
@property int intDurationId;
@end
