//
//  YachtMapAnnotationViewController.h
//  YachtMasters
//
//  Created by Anvesh on 20/07/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>

@class YachtMapAnnotationViewController;
@protocol GoogleMapDelegate <NSObject>

- (void) googleMap:(NSString *)strId;
- (void) googleMap:(YachtMapAnnotationViewController *)googleMap tappedAtMarker:(GMSMarker *)marker;
- (void) googleMap:(YachtMapAnnotationViewController *)googleMap didChangedCameraPosition:(CLLocation *)centerLocatoin;
//- (void) googleMap:(YachtMapAnnotationViewController *)googleMap didTouchedMarkerWindow:(MarkerInfoWindow *)markerWindow atRateButton:(UIButton *)reteButton;

@end

@interface YachtMapAnnotationViewController : UIViewController
{
    __weak IBOutlet UIView *viewtextField;
    __weak IBOutlet UIView *viewMap;
    __weak IBOutlet UIButton *buttonMapWithYacht;
    __weak IBOutlet UIButton *buttonMapView;
    __weak IBOutlet UIView *viewButtonNavigation;
    __weak IBOutlet UIImageView *imageViewYacht;
    __weak IBOutlet UIButton *buttonFavorite;
    __weak IBOutlet UIView *viewYachtDetail;
    __weak IBOutlet UILabel *labelyachtname;
    __weak IBOutlet UILabel *labelyachtPrice;
    __weak IBOutlet UIImageView *imageViewStar1;
    __weak IBOutlet UIImageView *imageViewStar2;
    __weak IBOutlet UIImageView *imageViewStar3;
    __weak IBOutlet UIImageView *imageViewStar4;
    __weak IBOutlet UIImageView *imageViewStar5;
    __weak IBOutlet UILabel *labelNumberOfReview;
    __weak IBOutlet UILabel *labelYachtAddres;
    __weak IBOutlet UILabel *labelDistanceofYacht;
    __weak IBOutlet UITextField *textFieldSearch;
}
@property (nonatomic,strong) NSString *strAddress;
@property(nonatomic,strong) NSString *strId;
@property(nonatomic,strong) NSDictionary *dictLocation;
@property(nonatomic,strong) NSMutableArray *arrYachtDetail;
@property(nonatomic) CLLocation *locationUser;
@property(nonatomic) CLLocation *locationDestination;
@property(nonatomic) BOOL showDeviceLocation;
@property(nonatomic) NSArray *arrMarkerInfo;
@property float latUser;
@property float langUser;
@property(nonatomic) id<GoogleMapDelegate> delegate;
- (void) reloadMap;
- (void) bringMarkerToFrontAtIndex :(NSInteger)index;
@end
