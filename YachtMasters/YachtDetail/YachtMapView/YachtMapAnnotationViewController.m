//
//  YachtMapAnnotationViewController.m
//  YachtMasters
//
//  Created by Anvesh on 20/07/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "YachtMapAnnotationViewController.h"
#import "CommonMethods.h"
#import "WebServiceManager.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import <GooglePlaces/GooglePlaces.h>
#import "SDKDemoAPIKey.h"
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import "AppDelegate.h"
#import "YachtMasters-Swift.h"


@interface YachtMapAnnotationViewController ()<GMSMapViewDelegate,UITextFieldDelegate,GMSAutocompleteViewControllerDelegate,CLLocationManagerDelegate,GMSMapViewDelegate>{
    GMSMapView *_googleMapView;
    NSMutableArray *_markers;
    GMSMarker *_selectedMarker;
    NSInteger _selectedMarkerIndex;
    BOOL _shouldAllowTapping;
    GMSCameraPosition *_myCameraPosition;
    MBProgressHUD *hud;
    __weak IBOutlet UIButton *buttonBack;
    AppDelegate *appDelegate;
}

@end

@implementation YachtMapAnnotationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:imageViewYacht];
    [self.view addSubview:viewYachtDetail];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [self GetLatLongFromAddress:_strAddress];
    viewYachtDetail.hidden = true;
    imageViewYacht.hidden = true;
    buttonFavorite.hidden = true;
    [[CommonMethods sharedInstance]CornerRadius:viewtextField Radius:20.0 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
    [[CommonMethods sharedInstance]CornerRadius:imageViewYacht Radius:5.0 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
    [[CommonMethods sharedInstance]CornerRadius:viewYachtDetail Radius:5.0 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
    [[CommonMethods sharedInstance]DropShadow:viewtextField UIColor:[UIColor colorWithRed:100.0/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:1] andShadowRadius:3.0];
    [[CommonMethods sharedInstance]DropShadow:buttonMapWithYacht UIColor:[UIColor colorWithRed:41.0/255.0 green:171.0/255.0 blue:231.0/255.0 alpha:1.0] andShadowRadius:4.0];
    [[CommonMethods sharedInstance]DropShadow:buttonBack UIColor:[UIColor blackColor] andShadowRadius:3.0];
    // [[CommonMethods sharedInstance]DropShadow:viewYachtDetail UIColor:[UIColor grayColor] andShadowRadius:3.0];
    
    buttonMapWithYacht.userInteractionEnabled = false;
    
    UIImageView *search = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 12)];
    search.image = [UIImage imageNamed:@"search@2x.png"];
    textFieldSearch.rightViewMode = UITextFieldViewModeAlways;
    textFieldSearch.rightView = search;
    _showDeviceLocation = NO;
    
    _googleMapView = [[GMSMapView alloc] initWithFrame:self.view.bounds];
    _googleMapView.indoorEnabled = NO;
    _googleMapView.delegate = self;
    [viewMap addSubview:_googleMapView];
    _shouldAllowTapping = YES;
    _markers = [NSMutableArray new];
    buttonMapWithYacht.selected = true;
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    imageViewYacht.frame = CGRectMake(12, SCREEN_HEIGHT - 186 - 76, SCREEN_WIDTH - 24, 186);
    viewYachtDetail.frame = CGRectMake(25, SCREEN_HEIGHT - 105, SCREEN_WIDTH - 50, 100);
    buttonFavorite.frame = CGRectMake(12+imageViewYacht.frame.size.width - 15, imageViewYacht.frame.origin.y + 15, 15, 15);
    [imageViewYacht addSubview:buttonFavorite];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self reloadMap];
    NSNumber *lat = _dictLocation[@"lat"];
    NSNumber *lon = _dictLocation[@"lng"];
    CLLocation *loc = [[CLLocation alloc] initWithLatitude:lat.doubleValue longitude:lon.doubleValue];
    self.locationUser = loc;
}
- (IBAction)ButtonBackPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (void)setLocationUser:(CLLocation *)locationUser {
    _locationUser = locationUser;
    if (_myCameraPosition == nil) {
        _myCameraPosition = [GMSCameraPosition cameraWithLatitude:locationUser.coordinate.latitude
                                                        longitude:locationUser.coordinate.latitude
                                                             zoom:12];
        [_googleMapView setCamera:_myCameraPosition];
    }
    //    GMSMarker *marker    = [[GMSMarker alloc]init];
    //    marker.position      = locationUser.coordinate;// CLLocationCoordinate2DMake(@(28.612912).doubleValue,@(77.229510).doubleValue);
    //    AnnotationView *view          = [[AnnotationView alloc] initWithColor:[UIColor orangeColor]] ;
    //    view.frame = CGRectMake(0, 0, 40, 40);
    //    view.textLabel.text = @"You";
    //    marker.iconView = view;
    //    marker.groundAnchor  = CGPointMake(0.5,0.5);
    //    marker.map           = _googleMapView;
    
    [_googleMapView animateToLocation:locationUser.coordinate];
}


- (void)setShowDeviceLocation:(BOOL)showDeviceLocation {
    
    _showDeviceLocation = showDeviceLocation;
    GMSMarker *marker    = [[GMSMarker alloc]init];
    marker.position      = _locationUser.coordinate;
    UIImageView *currentLocationImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_current_location"]];
    currentLocationImage.frame = CGRectMake(0, 0, 20, 20);
    marker.iconView = currentLocationImage;
    marker.groundAnchor  = CGPointMake(0.5,0.5);
    marker.map           = _googleMapView;
    //  [self drawPath];
}


- (void)reloadMap {
    
    [_googleMapView clear];
    for (int j = (int)(_arrMarkerInfo.count-1); j >= 0; j--) {
        
        NSDictionary *markerInfo = _arrMarkerInfo[j];
        GMSMarker *marker    = [[GMSMarker alloc]init];
        marker.position      = CLLocationCoordinate2DMake(((NSNumber *)markerInfo[@"latitude"]).doubleValue,((NSNumber *)markerInfo[@"longitude"]).doubleValue);
        [_googleMapView animateToLocation:marker.position];
        marker.zIndex = (int)(_arrMarkerInfo.count-j);
        marker.iconView = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"bubble@2x.png"]];
        UILabel *label = [[UILabel alloc]initWithFrame:marker.iconView.bounds];
        [marker.iconView addSubview:label];
        NSDictionary *dictPrice = markerInfo[@"price"];
        NSString *strPrice = dictPrice[@"price"];
        label.text = [ NSString stringWithFormat:@"%@%@", markerInfo[@"currency_symbol"],strPrice];
        label.font = FontOpenSansSemiBold(10);
        label.textColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        marker.title = [markerInfo valueForKey:@"id"];
        marker.infoWindowAnchor  = CGPointMake(0.5,-0.5);
        marker.map  = _googleMapView;
        
        marker.accessibilityHint = @(j).description;
        
        [_markers addObject:marker];
    }
}


/**
 * Bring selected marker in front of others markers
 */
- (void) bringMarkerToFrontAtIndex :(NSInteger)index {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_googleMapView clear];
        for (int j = (int)(_arrMarkerInfo.count-1); j >= 0; j--) {
            NSDictionary *markerInfo = _arrMarkerInfo[j];
            GMSMarker *marker    = [[GMSMarker alloc]init];
            marker.infoWindowAnchor = CGPointMake(0.44f, 0.45f);
            marker.position      = CLLocationCoordinate2DMake(((NSNumber *)markerInfo[@"latitude"]).doubleValue,((NSNumber *)markerInfo[@"longitude"]).doubleValue);
            marker.zIndex = (int)(_arrMarkerInfo.count-j);
            marker.iconView = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"bubble@2x.png"]];
            marker.infoWindowAnchor  = CGPointMake(0.5,-0.5);
            marker.map           = _googleMapView;
            marker.accessibilityHint = @(j).description;
        }
        if (_selectedMarker) {
            _selectedMarker.iconView = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"bubble@2x.png"]];
        }
    });
}

#pragma mark - Api for getting yacht list

-(void)ApiForGettingYachtLatLong:(double )Lat andLang:(double )Lang{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@search/coordinates?latitude=%f&longitude=%f",kBaseUrl,Lat,Lang];
        NSDictionary *dictPriceRange = [appDelegate.dictFilterItems valueForKey:@"PriceRange"];
        NSDictionary *dictGuests = [appDelegate.dictFilterItems valueForKey:@"NumberOfGuests"];
        NSDictionary *dictAmenities = [appDelegate.dictFilterItems valueForKey:@"SelectedAmenities"];
        if (!([dictPriceRange count] == 0)) {
            strUrl = [NSString stringWithFormat:@"%@&min_price=%@&max_price=%@",strUrl,[dictPriceRange valueForKey:@"min_price"],[dictPriceRange valueForKey:@"max_price"]];
        }
        if (!([dictGuests count] == 0)) {
            strUrl = [NSString stringWithFormat:@"%@&max_guests=%@",strUrl,[dictGuests valueForKey:@"max_guests"]];
            
        }
        if (!([dictAmenities count] == 0)) {
            if (!([[dictAmenities valueForKey:@"amenity_ids"] count] == 0)) {
                NSArray *arr = dictAmenities[@"amenity_ids"];
                arr = [arr valueForKeyPath:@"description"];
                NSString *str = [arr componentsJoinedByString:@","];
                str = [NSString stringWithFormat:@"[%@]",str];
                strUrl = [NSString stringWithFormat:@"%@&amenity_ids=%@",strUrl,str];
            }
        }
        if (self.langUser && self.latUser) {
            strUrl = [NSString stringWithFormat:@"%@&user_lat=%f&user_lon=%f",strUrl,_latUser,_langUser];
        }
        
        
        [WebServiceManager getRequestUrlString:strUrl emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            _arrMarkerInfo = [result valueForKey:@"yachts"];
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.superview animated:YES];
                // [[Loader sharedLoader]hideLoader];
                if (_arrMarkerInfo.count >0) {
                    [self reloadMap];
                    int Id  = [_strId intValue];
                    _markers = nil;
                    _markers = [NSMutableArray new];
                    viewYachtDetail.hidden = false;
                    imageViewYacht.hidden = false;
                    buttonFavorite.hidden = false;
                    [self UpdateUiOnClickOfMarker:_arrMarkerInfo];
                }
                else{
                    [[CommonMethods sharedInstance]AlertMessage:@"No yacht found for the given location" andViewController:self];
                    
                }
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.superview animated:YES];
                
                // [Loader ]
                //                [[Loader sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:@"Unable to fetch yacht for the given location" andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            //            hud = [MBProgressHUD showHUDAddedTo:self.view.superview animated:YES];
            //            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            //[[Loader sharedLoader]showLoaderOnScreenWithVc:self.view];
            
        });
        
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}

#pragma mark- GOOGLE Map
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
    //  NSArray *data = [NSArray arrayWithObject:[NSMutableDictionary dictionaryWithObject:@"foo" forKey:@"BAR"]];
    
    int Id  = [marker.title intValue];
    NSArray *filtered = [_arrMarkerInfo filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(id == %d)", Id]];
    NSLog(@"%@",filtered);
    
    [self UpdateUiOnClickOfMarker:filtered];
    
    
    //            CGPoint point = [mapView.projection pointForCoordinate:marker.position];
    //            point.y = point.y - 100;
    //            GMSCameraUpdate *camera =
    //            [GMSCameraUpdate setTarget:[mapView.projection coordinateForPoint:point]];
    //            [mapView animateWithCameraUpdate:camera];
    //
    //        if (!_shouldAllowTapping) {
    //
    //            return NO;
    //        }
    //
    //        _shouldAllowTapping = NO;
    //        [NSTimer timerWithTimeInterval:2 repeats:NO block:^(NSTimer * _Nonnull timer) {
    //
    //            _shouldAllowTapping = YES;
    //        }];
    //
    //    if (marker.position.latitude == _locationUser.coordinate.latitude && marker.position.longitude == _locationUser.coordinate.longitude ) {
    //
    //        return NO;
    //    }
    //
    //
    //    if (_selectedMarker) {
    //
    //        _selectedMarker.iconView = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"icon_unselected_location"]];
    //    }
    
    //
    //    _selectedMarker = marker;
    //    _selectedMarker.iconView = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"icon_selected_location"]];
    //
    //    mapView.selectedMarker = marker;
    
    //    [self bringMarkerToFrontAtIndex:[_markers indexOfObject:marker]];
    
    return YES;
}


//- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
//
//    if (marker.position.latitude == _locationUser.coordinate.latitude && marker.position.longitude == _locationUser.coordinate.longitude ) {
//
//        return nil;
//    }
//
//    _viewInfoWindow = [MarkerInfoWindow infoWindow];// [[MarkerInfoWindow  alloc] initWithFrame:CGRectMake(0, 0, 310, 80)];
//
//    NSInteger index = marker.accessibilityHint.integerValue;
//    NSDictionary *dic = [_arrMarkerInfo objectAtIndex:index];
//
//    marker.infoWindowAnchor  = CGPointMake(0.5,-0.3);
//
//    ((MarkerInfoWindow *)_viewInfoWindow).lblMarkerTitle.text = dic[KEY_WHEELER_NAME];
//
//    NSNumber *number = dic[KEY_WHEELER_RATING];
//    ((MarkerInfoWindow *)_viewInfoWindow).lblMarkerSubtitle.text = [NSString stringWithFormat:@"%.1f KM",number.floatValue];
//    number = dic[KEY_WHEELER_RATING];
//    ((MarkerInfoWindow *)_viewInfoWindow).srvWheelerRating.currentRating = number.integerValue;
//    ((MarkerInfoWindow *)_viewInfoWindow).markerID = [NSString stringWithFormat:@"%@", dic[KEY_WHEELER_ID]];
//    ((MarkerInfoWindow *)_viewInfoWindow).delegate = self;
//
//    [[ImageDownloader sharedImageDownloader] setImageInImageView:((MarkerInfoWindow *)_viewInfoWindow).ivMarkerImage fromURL:[NSURL URLWithString:dic[KEY_WHEELER_IMAGE]] andDefaultImage:[UIImage imageNamed:@"default_user_image.png"] showDownloadIndicator:YES];
//
//    _viewInfoWindow.backgroundColor = COLOR_APP_HEADER;
//    _viewInfoWindow.layer.masksToBounds = NO;
//    _viewInfoWindow.layer.cornerRadius = 3;
//    _viewInfoWindow.layer.shadowColor = [UIColor grayColor].CGColor;
//    _viewInfoWindow.layer.shadowOffset = CGSizeMake(-2, 2);
//    _viewInfoWindow.layer.shadowRadius = 50;
//    _viewInfoWindow.layer.shadowOpacity = 1;
//    _viewInfoWindow.clipsToBounds = NO;
//
//    //    [((MarkerInfoWindow *)_viewInfoWindow).btnRateWheeler addTarget:self action:@selector(rateWheeler:) forControlEvents:UIControlEventTouchUpInside];
//
//    return _viewInfoWindow;
//}
//


-(void)UpdateUiOnClickOfMarker:(NSArray *)arrFiltered{
    [self ButtonMapWithYachtDetail:buttonMapWithYacht];
    viewYachtDetail.tag = [[[arrFiltered valueForKey:@"id"] objectAtIndex:0] intValue];
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[arrFiltered valueForKey:@"image"] objectAtIndex:0]];
    NSURL *url = [NSURL URLWithString:strUrl];
    [imageViewYacht sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"lower strip"]];
    labelyachtname.text = [[arrFiltered valueForKey:@"name"] objectAtIndex:0];
    labelYachtAddres.text = [NSString stringWithFormat:@"%@, %@, %@, %@",[[arrFiltered valueForKey:@"address"] objectAtIndex:0],[[arrFiltered valueForKey:@"city"] objectAtIndex:0], [[arrFiltered valueForKey:@"state"] objectAtIndex:0],[[arrFiltered valueForKey:@"country"] objectAtIndex:0]];
    NSDictionary *dictPrice = [[arrFiltered valueForKey:@"price"] objectAtIndex:0];
    NSString *strPrice = [[dictPrice valueForKey:@"price"] stringValue];
    NSString *strCurrency = [[arrFiltered valueForKey:@"currency_symbol"]objectAtIndex:0 ];
    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@%@/n",strCurrency,strPrice]];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:attrText];
    strPrice  = [strPrice stringByAppendingString:strCurrency];
    NSRange range = [strPrice rangeOfString:strPrice];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:FontOpenSansSemiBold(12)} range:range];
    labelyachtPrice.attributedText = attributedText;
    NSInteger rating = [[[arrFiltered valueForKey:@"rating"] objectAtIndex:0] integerValue];
    switch (rating) {
        case 0:
            imageViewStar1.image = [UIImage imageNamed:@"star outline.png"];
            imageViewStar2.image = [UIImage imageNamed:@"star outline.png"];
            imageViewStar3.image = [UIImage imageNamed:@"star outline.png"];
            imageViewStar4.image = [UIImage imageNamed:@"star outline.png"];
            imageViewStar5.image = [UIImage imageNamed:@"star outline.png"];
            break;
        case 1:
            imageViewStar1.image = [UIImage imageNamed:@"star@2x.png"];
            imageViewStar2.image = [UIImage imageNamed:@"star outline.png"];
            imageViewStar3.image = [UIImage imageNamed:@"star outline.png"];
            imageViewStar4.image = [UIImage imageNamed:@"star outline.png"];
            imageViewStar5.image = [UIImage imageNamed:@"star outline.png"];
            break;
        case 2:
            imageViewStar1.image = [UIImage imageNamed:@"star@2x.png"];
            imageViewStar2.image = [UIImage imageNamed:@"star@2x.png"];
            imageViewStar3.image = [UIImage imageNamed:@"star outline.png"];
            imageViewStar4.image = [UIImage imageNamed:@"star outline.png"];
            imageViewStar5.image = [UIImage imageNamed:@"star outline.png"];
            break;
        case 3:
            imageViewStar1.image = [UIImage imageNamed:@"star@2x.png"];
            imageViewStar2.image = [UIImage imageNamed:@"star@2x.png"];
            imageViewStar3.image = [UIImage imageNamed:@"star@2x.png"];
            imageViewStar4.image = [UIImage imageNamed:@"star outline.png"];
            imageViewStar5.image = [UIImage imageNamed:@"star outline.png"];
            break;
        case 4:
            imageViewStar1.image = [UIImage imageNamed:@"star@2x.png"];
            imageViewStar2.image = [UIImage imageNamed:@"star@2x.png"];
            imageViewStar3.image = [UIImage imageNamed:@"star@2x.png"];
            imageViewStar4.image = [UIImage imageNamed:@"star@2x.png"];
            imageViewStar5.image = [UIImage imageNamed:@"star outline.png"];
            break;
        case 5:
            imageViewStar1.image = [UIImage imageNamed:@"star@2x.png"];
            imageViewStar2.image = [UIImage imageNamed:@"star@2x.png"];
            imageViewStar3.image = [UIImage imageNamed:@"star@2x.png"];
            imageViewStar4.image = [UIImage imageNamed:@"star@2x.png"];
            imageViewStar5.image = [UIImage imageNamed:@"star@2x.png"];
            break;
        default:
            break;
    }
    labelNumberOfReview.text = [NSString stringWithFormat:@"(%@)",[[[arrFiltered valueForKey:@"rating"] objectAtIndex:0] stringValue]];
    
    
}

- (IBAction)ButtonMapWithYachtDetail:(id)sender {
    
    buttonMapWithYacht.selected = true;
    buttonMapView.selected = false;
    [[CommonMethods sharedInstance]DropShadow:buttonMapWithYacht UIColor:[UIColor colorWithRed:41.0/255.0 green:171.0/255.0 blue:231.0/255.0 alpha:1.0] andShadowRadius:4.0];
    buttonMapView.backgroundColor = [UIColor whiteColor];
    buttonMapView.layer.shadowOpacity = 0.0;
    buttonMapWithYacht.backgroundColor = [UIColor colorWithRed:41.0/255.0 green:141.0/255.0 blue:251.0/255.0 alpha:1.0];
    [UIView animateWithDuration:0.45 animations:^{
        
        imageViewYacht.frame = CGRectMake(12, SCREEN_HEIGHT - 186 - 76, SCREEN_WIDTH - 24, 186);
        viewYachtDetail.frame = CGRectMake(25.0, SCREEN_HEIGHT - 105, SCREEN_WIDTH - 50, 100);
        
        //        viewYachtDetail.hidden = true;
        //        imageViewYacht.hidden = true;
        
    } completion:^(BOOL finished) {
        viewYachtDetail.hidden = false;
        // imageViewYacht.hidden = false;
        // buttonFavorite.hidden = false;
    } ];
    
    buttonMapWithYacht.userInteractionEnabled = false;
    buttonMapView.userInteractionEnabled = true;
}
- (IBAction)ButtonMapOnlyWithSearch:(id)sender {
    buttonMapView.selected = true;
    buttonMapWithYacht.selected = false;
    [[CommonMethods sharedInstance]DropShadow:buttonMapView UIColor:[UIColor colorWithRed:41.0/255.0 green:171.0/255.0 blue:231.0/255.0 alpha:1.0] andShadowRadius:4.0];
    buttonMapWithYacht.layer.shadowOpacity = 0.0;
    buttonMapWithYacht.backgroundColor = [UIColor whiteColor];
    buttonMapView.backgroundColor = [UIColor colorWithRed:41.0/255.0 green:141.0/255.0 blue:251.0/255.0 alpha:1.0];
    //viewYachtDetail.frame.origin.y+viewYachtDetail.frame.size.height+5
    //imageViewYacht.frame.origin.y+imageViewYacht.frame.size.height+76
    [UIView animateWithDuration:0.45 animations:^{
        //   viewYachtDetail.hidden = true;
        //   imageViewYacht.hidden = true;
        viewYachtDetail.frame = CGRectMake(25.0, SCREEN_HEIGHT, SCREEN_WIDTH - 50, viewYachtDetail.frame.size.height);
        imageViewYacht.frame = CGRectMake(12.0, SCREEN_HEIGHT, SCREEN_WIDTH - 24, imageViewYacht.frame.size.height);
        buttonFavorite.hidden = true;
    } ];
    
    buttonMapWithYacht.userInteractionEnabled = true;
    buttonMapView.userInteractionEnabled = false;
}

- (IBAction)ViewYachtDetailTapped:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(googleMap:)]) {
        NSInteger tag = [viewYachtDetail tag];
        NSString *strId = [NSString stringWithFormat:@"%ld",(long)tag];
        [_delegate googleMap:strId];
    }
    [self.navigationController popViewControllerAnimated:true];
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {
    
    if (_delegate && [_delegate respondsToSelector:@selector(googleMap:didChangedCameraPosition:)]) {
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:position.target.latitude longitude:position.target.longitude];
        [_delegate googleMap:self didChangedCameraPosition:location];
    }
}


#pragma mark - TextField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [textField resignFirstResponder];
    [self onLaunchClicked];
}

// Present the autocomplete view controller when the button is pressed.
- (void)onLaunchClicked {
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    [self presentViewController:acController animated:YES completion:nil];
}

// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
    [self getLocationFromAddress:place.formattedAddress];
    textFieldSearch.text = place.formattedAddress;
}

- (void)viewController:(GMSAutocompleteViewController *)viewController didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}


-(void) getLocationFromAddress: (NSString*) addressStr {
    __block double latitude = 0, longitude = 0;
    NSString *esc_addr = [addressStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    NSString *req = [NSString stringWithFormat:@"https://maps.google.com/maps/api/geocode/json?sensor=true&address=%@&key=%@",esc_addr,kAPIKey];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSString *cachePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"/nsurlsessiondemo.cache"];
    NSURLCache *myCache = [[NSURLCache alloc] initWithMemoryCapacity: 16384
                                                        diskCapacity: 268435456
                                                            diskPath: cachePath];
    defaultConfigObject.URLCache = myCache;
    defaultConfigObject.requestCachePolicy = NSURLRequestUseProtocolCachePolicy;
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject
                                                                      delegate: nil
                                                                 delegateQueue: [NSOperationQueue mainQueue]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:req]];
    [request setHTTPMethod:@"GET"];
    [[delegateFreeSession dataTaskWithRequest:request
                            completionHandler:^(NSData *data, NSURLResponse *response,
                                                NSError *error){
        NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        if ([[dataArray valueForKey:@"results"] count] > 0){
            NSLog(@"%@", dataArray);
            NSDictionary *location = [[[[dataArray valueForKey:@"results"] objectAtIndex:0] valueForKey:@"geometry"]valueForKey:@"location"];
            NSLog(@"%@",location);
            NSNumber *number = location[@"lat"];
            latitude = number.doubleValue;
            number = location[@"lng"];
            longitude = number.doubleValue;
            CLLocationCoordinate2D center;
            center.latitude=latitude;
            center.longitude = longitude;
            [self ApiForGettingYachtLatLong:latitude andLang:longitude];
        }
    }
      ]resume];
}

//&key=YOUR_API_KEY
-(void)GetLatLongFromAddress:(NSString *)addressStr {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view.window];
    });
    __block double latitude = 0, longitude = 0;
    NSString *esc_addr = [addressStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    NSString *req = [NSString stringWithFormat:@"https://maps.google.com/maps/api/geocode/json?sensor=true&address=%@&key=%@", esc_addr,kAPIKey];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSString *cachePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"/nsurlsessiondemo.cache"];
    NSURLCache *myCache = [[NSURLCache alloc] initWithMemoryCapacity: 16384
                                                        diskCapacity: 268435456
                                                            diskPath: cachePath];
    defaultConfigObject.URLCache = myCache;
    defaultConfigObject.requestCachePolicy = NSURLRequestUseProtocolCachePolicy;
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject
                                                                      delegate: nil
                                                                 delegateQueue: [NSOperationQueue mainQueue]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:req]];
    [request setHTTPMethod:@"GET"];
    [[delegateFreeSession dataTaskWithRequest:request
                            completionHandler:^(NSData *data, NSURLResponse *response,
                                                NSError *error){
        //      NSLog(@"Got response %@ with error %@.\n", response, error);
        NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        //          completionBlock (dataArray, error);
        //
        NSLog(@"%@", dataArray);
        dispatch_async(dispatch_get_main_queue(), ^{
        [[LoaderNew sharedLoader]hideLoader];
        });
        if ([[dataArray valueForKey:@"status"] isEqualToString:@"ZERO_RESULTS"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[CommonMethods sharedInstance]AlertMessage:@"Unable to fetch address. Please try again" andViewController:self];
            });
            _dictLocation = [[NSDictionary alloc]init];
        }
        else if ([[dataArray valueForKey:@"status"] isEqualToString:@"OVER_QUERY_LIMIT"]){
            dispatch_async(dispatch_get_main_queue(), ^{
                [[CommonMethods sharedInstance]AlertMessage:@"Unable to fetch address. Please try again" andViewController:self];
            });
            _dictLocation = [[NSDictionary alloc]init];
        }
        else if ([[dataArray valueForKey:@"status"] isEqualToString:@"REQUEST_DENIED"]){
            dispatch_async(dispatch_get_main_queue(), ^{
                [[CommonMethods sharedInstance]AlertMessage:@"Unable to fetch address. Please try again" andViewController:self];
            });
            
            _dictLocation = [[NSDictionary alloc]init];
        }
        else{
            _dictLocation = [[NSDictionary alloc]init];
            _dictLocation = [[[[dataArray valueForKey:@"results"] objectAtIndex:0] valueForKey:@"geometry"]valueForKey:@"location"];
            NSLog(@"%@",_dictLocation);
            NSNumber *number = _dictLocation[@"lat"];
            latitude = number.doubleValue;
            number = _dictLocation[@"lng"];
            longitude = number.doubleValue;
            [self ApiForGettingYachtLatLong:latitude andLang:longitude];
        }
    }
      ]resume];
}
//

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
