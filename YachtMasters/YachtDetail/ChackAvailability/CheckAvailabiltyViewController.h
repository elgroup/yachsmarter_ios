//
//  CheckAvailabiltyViewController.h
//  YachtMasters
//
//  Created by Anvesh on 01/10/18.
//  Copyright © 2018 EL. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol DurationDelegate <NSObject>
@required
- (void) pushToNextViewController:(NSMutableArray *)arrSelectedDuration;
@end
@interface CheckAvailabiltyViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *viewBottom;
@property (weak, nonatomic) IBOutlet UIView *viewBar;
@property (weak, nonatomic) IBOutlet UIView *viewDuration;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewDuration;
@property (weak, nonatomic) IBOutlet UIButton *checkAvailability;
@property (strong, nonatomic) NSMutableArray *arrPrices;
@property (strong, nonatomic) NSString *strYachtId;
@property(nonatomic,strong) NSDictionary *dictLocation;
@property (nonatomic,strong) id<DurationDelegate> delegate;
@property IBOutlet NSLayoutConstraint* lcBottomConstraints;

@end
