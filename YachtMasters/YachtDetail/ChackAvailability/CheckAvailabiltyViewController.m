//
//  CheckAvailabiltyViewController.m
//  YachtMasters
//
//  Created by Anvesh on 01/10/18.
//  Copyright © 2018 EL. All rights reserved.
//

#import "CheckAvailabiltyViewController.h"
#import "DurationCollectionViewCell.h"
#import "CommonMethods.h"
#import "CalenderViewController.h"

@interface CheckAvailabiltyViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSMutableArray *arrSelectedDuration;
    NSMutableArray *arrButtons;
    NSIndexPath *indexPath;
}

@end

@implementation CheckAvailabiltyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UINib *cellNib = [UINib nibWithNibName:@"DurationCollectionViewCell" bundle:nil];
    [self.collectionViewDuration registerNib:cellNib forCellWithReuseIdentifier:@"DurationCollectionViewCell"];
    self.checkAvailability.clipsToBounds = NO;
    self.checkAvailability.layer.shadowOffset = CGSizeMake(0, 0);
    self.checkAvailability.layer.shadowRadius = 3;
    self.checkAvailability.layer.shadowOpacity = 0.5;
    self.checkAvailability.layer.masksToBounds = NO;
    self.checkAvailability.layer.shadowColor = [UIColor colorWithRed:41.0/255.0 green:171.0/255.0 blue:251.0/255.0 alpha:1.0].CGColor;
    arrSelectedDuration = [[NSMutableArray alloc]init];
    arrButtons = [[NSMutableArray alloc]init];
    // Do any additional setup after loading the view.
}



-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.viewBar.layer.cornerRadius = 2.5;
    self.viewBar.clipsToBounds = true;
    self.viewBottom.layer.cornerRadius = 20.0;
    self.viewDuration.layer.cornerRadius = 20.0;
    self.viewDuration.clipsToBounds = true;
    
}

- (IBAction)buttonBackPressed:(id)sender {
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
        self.lcBottomConstraints.constant = - self.viewBottom.frame.size.height;
        // self.viewBottom.frame = CGRectMake(0.0, self.view.frame.size.height, self.view.frame.size.width, 270);
    } completion:^(BOOL finished){
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        
    }];
    
    
}

#pragma mark- Collection View Delegate and Data Source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.arrPrices.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    DurationCollectionViewCell *cell = [self.collectionViewDuration dequeueReusableCellWithReuseIdentifier:@"DurationCollectionViewCell" forIndexPath:indexPath];
    if (cell == nil)    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"DurationCollectionViewCell" owner:self options:nil];
        cell = nibArray[0];
    }
    cell.buttonDuration.layer.cornerRadius = 45/2;//cell.buttonDuration.frame.size.height/2;
    cell.buttonDuration.clipsToBounds = true;
    cell.buttonDuration.layer.borderWidth = 1.0;
    cell.buttonDuration.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.buttonDuration.tag = indexPath.row;
    if ([[[self.arrPrices objectAtIndex:indexPath.row] valueForKey:@"term"] isEqualToString:@"per_hour"]){
        [cell.buttonDuration setTitle:@"Per Hour" forState:UIControlStateNormal];
    }
    else if ([[[self.arrPrices objectAtIndex:indexPath.row] valueForKey:@"term"] isEqualToString:@"half_day"]){
        [cell.buttonDuration setTitle:@"Half Day" forState:UIControlStateNormal];
    }
    else if ([[[self.arrPrices objectAtIndex:indexPath.row] valueForKey:@"term"] isEqualToString:@"per_day"]){
        [cell.buttonDuration setTitle:@"Per Day" forState:UIControlStateNormal];
        
    }
    else if ([[[self.arrPrices objectAtIndex:indexPath.row] valueForKey:@"term"] isEqualToString:@"week"]){
        [cell.buttonDuration setTitle:@"Per Week" forState:UIControlStateNormal];
    }
    cell.buttonDuration.tag = indexPath.row;
    [cell.buttonDuration addTarget:self action:@selector(buttonDurationClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.buttonDuration setSelected:false];
    [arrButtons addObject:cell.buttonDuration];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(collectionView.frame.size.width/2 - 5, 60);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

-(void)buttonDurationClicked:(id)sender{
    arrSelectedDuration = [[NSMutableArray alloc]init];
    UIButton *btn = (UIButton *)sender;
    if (btn.isSelected){
        [btn setSelected:false];
    }
    else{
        DurationCollectionViewCell *cell = (DurationCollectionViewCell*)[self.collectionViewDuration cellForItemAtIndexPath:indexPath];
        [cell.buttonDuration setSelected:false];
        indexPath = [NSIndexPath indexPathForRow:btn.tag inSection:0];
        [btn setSelected:true];
        [arrSelectedDuration addObject:[self.arrPrices objectAtIndex:btn.tag]];
    }
    
}
- (IBAction)checkAvailabilityClicked:(id)sender {
    if (arrSelectedDuration.count > 0){
        
        [UIView animateWithDuration:0.4
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
            self.lcBottomConstraints.constant = - self.viewBottom.frame.size.height;
            
            //self.viewBottom.frame = CGRectMake(0.0, self.view.frame.size.height, self.view.frame.size.width, 270);
        } completion:^(BOOL finished){
            [self willMoveToParentViewController:nil];
            [self.view removeFromSuperview];
            [self removeFromParentViewController];
            [self.delegate pushToNextViewController:arrSelectedDuration];
        }];
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:@"Please select the duration for checking the availability." andViewController:self];
    }
    //    CalenderViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CalenderViewController"];
    //    controller.dictLocation = self.dictLocation;
    //    controller.strId = self.strYachtId;
    //    UINavigationController *nav = (UINavigationController *)self.parentViewController;
    //    [nav pushViewController:controller animated:true];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
