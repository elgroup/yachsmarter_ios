//
//  YachtDetailViewController.h
//  
//
//  Created by Anvesh on 03/07/17.
//
//

#import <UIKit/UIKit.h>

@interface YachtDetailTableViewController : UITableViewController{
    
    UIButton *buttonBack;
    __weak IBOutlet UIButton *buttonMap;
    __weak IBOutlet UIButton *buttonFavorite;
    __weak IBOutlet UIScrollView *scrollViewImage;
    __weak IBOutlet UIPageControl *pageControlImage;
    __weak IBOutlet UIView *viewDeparture;
    __weak IBOutlet UIView *viewArrival;
    __weak IBOutlet UIImageView *imageViewProfilePic;
    __weak IBOutlet UILabel *labelOwnerName;
    __weak IBOutlet UIButton *buttonCall;
    __weak IBOutlet UIButton *buttonMessage;
    __weak IBOutlet UILabel *labelRatings;
    __weak IBOutlet UIImageView *imageViewStar1;
    __weak IBOutlet UILabel *labelYachtName;

    __weak IBOutlet UIImageView *imageViewYacht;
    __weak IBOutlet UIImageView *imageView2;

    __weak IBOutlet UIImageView *imageViewStar3;
    __weak IBOutlet UIImageView *imageViewStar4;
    __weak IBOutlet UIImageView *imageViewStar5;
    __weak IBOutlet UILabel *labelFollowers;
    __weak IBOutlet UIImageView *imageViewFollwer1;
    __weak IBOutlet UIImageView *imageViewFollwer2;
    __weak IBOutlet UIImageView *imageViewFollwer3;
    __weak IBOutlet UIImageView *imageViewFollwer4;
    __weak IBOutlet UIImageView *imageViewFollwer5;
    __weak IBOutlet UIButton *buttonFollow;
    __weak IBOutlet UIView *viewOwnerDetail;
    
    __weak IBOutlet UIButton *buttonBookNow;
    __weak IBOutlet UICollectionView *collectionViewAmenities;
    __weak IBOutlet UITextView *_tvDescription;
    IBOutlet NSLayoutConstraint *_lcDesctiption;
    __weak IBOutlet UICollectionView *collectionViewYachtImages;
    IBOutlet UIButton *buttonPrice;
    IBOutlet UIImageView *imageViewDrop;
    __weak IBOutlet UILabel *labelCaptain;
}

@property (strong,nonatomic) NSString *strId;
@property (nonatomic) float lat;
@property (nonatomic) float lang;
@end

@interface HelloCell : UITableViewCell

@end
