//
//  PhotosCollectionViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 14/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPhotos;
@property (weak, nonatomic) IBOutlet UIButton *buttonPlus;

@end
