//
//  ImageProfileTableViewCell.m
//  YachtMasters
//
//  Created by Anvesh on 21/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "ImageProfileTableViewCell.h"

@implementation ImageProfileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _imageViewProfile.layer.cornerRadius = _imageViewProfile.frame.size.height/2;
    _imageViewProfile.clipsToBounds = true;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
