//
//  EditProfileTableViewCell.m
//  YachtMasters
//
//  Created by Anvesh on 23/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "EditProfileTableViewCell.h"

@implementation EditProfileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _imageViewProfilePicture.layer.cornerRadius = 47.0;
    _imageViewProfilePicture.clipsToBounds = true;
    _imageViewProfilePicture.layer.borderWidth = 3.0;
    _imageViewProfilePicture.layer.borderColor = [UIColor colorWithRed:194.0/255.0 green:230.0/255.0 blue:249.0/255.0 alpha:1].CGColor;
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
