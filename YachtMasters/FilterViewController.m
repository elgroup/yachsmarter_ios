//
//  FilterViewController.m
//  YachtMasters
//
//  Created by Anvesh on 30/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "FilterViewController.h"
#import "FilterAmenitiesCollectionViewCell.h"
#import "MBProgressHUD.h"
#import "CommonMethods.h"
#import "WebServiceManager.h"
#import "Constants.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
#import "TTRangeSlider.h"
#import "YachtMasters-Swift.h"


@interface FilterViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,TTRangeSliderDelegate,CLLocationManagerDelegate>{
    NSString *strValueForSlider;
    __weak IBOutlet UICollectionView *collectionViewAmenities;
    NSMutableArray* arrAmenities;
    MBProgressHUD *hud;
    AppDelegate *appDelegate;
    NSMutableArray *arrSelectedAmenities;
    __weak IBOutlet UIButton *buttonSave;
    __weak IBOutlet UIButton *buttonBack;
    IBOutlet UILabel *labelFilter;
    NSMutableArray *arrFilter;
    IBOutlet TTRangeSlider *guestNumberSlider;
    IBOutlet TTRangeSlider *priceRangeSlider;
    NSInteger intMaxGuest;
    NSInteger intMaxPrice;
    NSInteger intMinPrice;
    NSInteger amenitiesCount;
    
    //    CLLocationManager *locationManager;
    __weak IBOutlet UIButton *buttonReset;
    
    
}
@end

@implementation FilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ApiForGettingAmenitiesList];
    self.navigationController.navigationBar.hidden = true;
    self.navigationItem.hidesBackButton = true;
    // [self configureLabelSlider];
    //[self configureLabelSliderForPrice];
    self.tabBarController.hidesBottomBarWhenPushed = true;
    self.tabBarController.tabBar.hidden = true;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    arrAmenities = [[NSMutableArray alloc]init];
    arrSelectedAmenities = [[NSMutableArray alloc]init];
    
    guestNumberSlider.handleImage = [UIImage imageNamed:@"slider-default7-handle@2x.png"];
    guestNumberSlider.selectedHandleDiameterMultiplier = 1.0;
    guestNumberSlider.handleDiameter = 40.0;
    guestNumberSlider.delegate = self;
    guestNumberSlider.lineHeight = 1.5;
    
    priceRangeSlider.handleImage = [UIImage imageNamed:@"slider-default7-handle@2x.png"];
    priceRangeSlider.selectedHandleDiameterMultiplier = 1.0;
    priceRangeSlider.handleDiameter = 40.0;
    priceRangeSlider.delegate = self;
    priceRangeSlider.lineHeight = 1.5;
    buttonSave.userInteractionEnabled = false;
    if (appDelegate.dictFilterItems != nil){
        NSDictionary *dictAmenities = [appDelegate.dictFilterItems valueForKey:@"SelectedAmenities"];
        amenitiesCount = [[dictAmenities valueForKey:@"amenity_ids"] count];
    }
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //    theViewSlider.showTooltip = YES;
    if([self.view respondsToSelector:@selector(setTintColor:)]){
        _labelSliderPrice.tintColor = [UIColor colorWithRed:0.0/255.0 green:163.0/255.0 blue:238.0/255.0 alpha:1.0];
    }
    [[CommonMethods sharedInstance]DropShadow:buttonSave UIColor:[UIColor colorWithRed:65.0/255.0 green:140.0/255.0 blue:240.0/255.0 alpha:1.0] andShadowRadius:2.0];
    UIImage *image = [[CommonMethods sharedInstance] useColor:[UIColor colorWithRed:65.0/255.0 green:140.0/255.0 blue:240.0/255.0 alpha:1.0] forImage:[UIImage imageNamed:@"back arrow.png"]];
    [buttonBack setImage:image forState:UIControlStateNormal];
    collectionViewAmenities.allowsMultipleSelection = true;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}
- (IBAction)ButtonBackPressed:(id)sender {
    NSInteger count = 0;
    if (appDelegate.dictFilterItems != nil){
        NSDictionary *dictAmenities = [appDelegate.dictFilterItems valueForKey:@"SelectedAmenities"];
        count = [[dictAmenities valueForKey:@"amenity_ids"] count];
    }
    if (priceRangeSlider.maxValue == self.maxPrice && priceRangeSlider.minValue == self.minPrice && guestNumberSlider.maxValue == [[arrFilter valueForKey:@"max_guest"] intValue] && count == amenitiesCount) {
        appDelegate.dictFilterItems = [[NSMutableDictionary alloc]init];
    }
    self.navigationController.navigationBar.hidden = false;
    self.navigationItem.hidesBackButton = true;
    
    self.tabBarController.tabBar.hidden = false;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Delegate Method
-(void)ValueOnSlider:(NSString *)strValue{
    
    strValueForSlider = [[NSString alloc]init];
    strValueForSlider = strValue;
    
}


- (IBAction)ButtonSavePressed:(id)sender {
    appDelegate.dictFilterItems = [[NSMutableDictionary alloc]init];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    if (intMinPrice == [[arrFilter valueForKey:@"min_price"]floatValue]) {
        [dict setObject:[NSNumber numberWithInteger:self.minPrice] forKey:@"min_price"];
    }
    else{
        [dict setObject:[NSNumber numberWithInteger:intMinPrice] forKey:@"min_price"];
        
    }
    if (intMaxPrice == [[arrFilter valueForKey:@"max_price"]floatValue]) {
         [dict setObject:[NSNumber numberWithInteger:self.maxPrice] forKey:@"max_price"];
    }
    else{
        [dict setObject:[NSNumber numberWithInteger:self.maxPrice] forKey:@"max_price"];
    }
    
    [appDelegate.dictFilterItems setObject:dict forKey:@"PriceRange"];
    if (intMaxGuest == 0) {
        dict = [NSMutableDictionary new];
        // [dict setObject:[arrFilter valueForKey:@"max_guest"] forKey:@"max_guests"];
        [appDelegate.dictFilterItems setObject:dict forKey:@"NumberOfGuests"];
    }
    else{
        dict = [NSMutableDictionary new];
        [dict setObject:[NSNumber numberWithInteger:intMaxGuest] forKey:@"max_guests"];
        [appDelegate.dictFilterItems setObject:dict forKey:@"NumberOfGuests"];
    }
    dict = [NSMutableDictionary new];
    [dict setObject:arrSelectedAmenities forKey:@"amenity_ids"];
    [appDelegate.dictFilterItems setObject:dict forKey:@"SelectedAmenities"];
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - collectionView Delegate and datasource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if ( [arrAmenities count] == 0) {
        return 0;
    }else{
        return [arrAmenities count];
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    FilterAmenitiesCollectionViewCell *cell = [collectionViewAmenities dequeueReusableCellWithReuseIdentifier:@"FilterAmenitiesCollectionViewCell" forIndexPath:indexPath];
    if (cell == nil)    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"AmenitiesCollectionViewCell" owner:self options:nil];
        cell = nibArray[0];
    }
    // cell.imageViewAmenities.image = [UIImage imageNamed:@"helicopter@2x.png"];
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[[arrAmenities objectAtIndex:indexPath.row]valueForKey:@"icon"] valueForKey:@"url"]];
    NSURL *url = [NSURL URLWithString:strUrl];
    [cell.imageViewAmenities sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    // cell.imageViewAmenities.image = [[CommonMethods sharedInstance] useColor:[UIColor colorWithRed:70.0/255.0 green:140.0/255.0 blue:240.0/255.0 alpha:1.0] forImage:cell.imageViewAmenities.image];
    
    cell.labelAmenitiesName.text = [[arrAmenities objectAtIndex:indexPath.row]valueForKey:@"name"];
    if ([arrSelectedAmenities count] > 0) {
        
        if ([arrSelectedAmenities containsObject:[[arrAmenities objectAtIndex:indexPath.row] valueForKey:@"id"]]) {
            cell.imageViewAmenities.image = [[CommonMethods sharedInstance] useColor:[UIColor colorWithRed:65.0/255.0 green:140.0/255.0 blue:240.0/255.0 alpha:1.0] forImage:cell.imageViewAmenities.image];
            cell.labelAmenitiesName.textColor = [UIColor colorWithRed:65.0/255.0 green:140.0/255.0 blue:240.0/255.0 alpha:1.0];
        }
        else{
            cell.labelAmenitiesName.textColor = [UIColor blackColor];
        }
    }
    return cell;
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPHONE_5) {
        return CGSizeMake(collectionViewAmenities.frame.size.width/4,collectionViewAmenities.frame.size.height);
    }
    else if (IS_IPHONE_6|| IS_IPHONE_X){
        return CGSizeMake(collectionViewAmenities.frame.size.width/5,collectionViewAmenities.frame.size.height);
    }
    else if (IS_IPHONE_6P || IS_IPHONE_XSMAX){
        return CGSizeMake(collectionViewAmenities.frame.size.width/6,collectionViewAmenities.frame.size.height);
    }
    else{
        return CGSizeMake(collectionViewAmenities.frame.size.width/3,collectionViewAmenities.frame.size.height);
    }
    // return CGSizeMake(100, 60);_lcDesctiption
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    FilterAmenitiesCollectionViewCell *cell = (FilterAmenitiesCollectionViewCell *)[collectionViewAmenities cellForItemAtIndexPath:indexPath];
    cell.imageViewAmenities.image = [[CommonMethods sharedInstance] useColor:[UIColor colorWithRed:70.0/255.0 green:140.0/255.0 blue:240.0/255.0 alpha:1.0] forImage:cell.imageViewAmenities.image];
    cell.labelAmenitiesName.textColor = [UIColor colorWithRed:70.0/255.0 green:140.0/255.0 blue:240.0/255.0 alpha:1.0];
    [arrSelectedAmenities addObject:[[arrAmenities valueForKey:@"id"] objectAtIndex:indexPath.row]];
    if (arrSelectedAmenities.count > 0) {
        buttonSave.userInteractionEnabled = true;
    }
}


-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    FilterAmenitiesCollectionViewCell *cell = (FilterAmenitiesCollectionViewCell *)[collectionViewAmenities cellForItemAtIndexPath:indexPath];
    cell.imageViewAmenities.image = [[CommonMethods sharedInstance] useColor:[UIColor clearColor] forImage:cell.imageViewAmenities.image];
    cell.imageViewAmenities.image = [[CommonMethods sharedInstance] useColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0] forImage:cell.imageViewAmenities.image];
    
    cell.labelAmenitiesName.textColor = [UIColor blackColor];
    // [arrSelectedAmenities addObject:[[arrAmenities valueForKey:@"id"] objectAtIndex:indexPath.row]];
    NSDictionary *dict = [arrAmenities  objectAtIndex:indexPath.row];
    if ([arrSelectedAmenities containsObject:[dict valueForKey:@"id"]]) {
        [arrSelectedAmenities removeObject:[dict valueForKey:@"id" ]];
    }
    if (arrSelectedAmenities.count > 0) {
        buttonSave.userInteractionEnabled = true;
    }
    
}

#pragma mark - Delegates for TTRange Slider
-(void)rangeSlider:(TTRangeSlider *)sender didChangeSelectedMinimumValue:(float)selectedMinimum andMaximumValue:(float)selectedMaximum{
    NSLog(@"%f",selectedMaximum);
    if (sender == priceRangeSlider) {
        intMaxPrice = (int)roundf(selectedMaximum);
        intMinPrice = (int)roundf(selectedMinimum);
        if (intMaxPrice == self.maxPrice || intMinPrice == 0) {
            buttonSave.userInteractionEnabled = true;
        }
    }
    else{
        intMaxGuest = (int)roundf(selectedMaximum);
        if (intMaxGuest == [[arrFilter valueForKey:@"max_guest"] integerValue]) {
            buttonSave.userInteractionEnabled = false;
        }
        else{
            buttonSave.userInteractionEnabled = true;
            
        }
    }
}

#pragma mark - Api for Getting Amenities List
-(void)ApiForGettingAmenitiesList{
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        NSString *strRole = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@amenities",kBaseUrl] emailString:strRole accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            NSDictionary *dict = [[result valueForKey:@"amenities"] mutableCopy];
            NSArray *arrAllKeys = [dict allKeys];
            
            arrAmenities = [[NSMutableArray alloc]init];
            for (NSString *str in arrAllKeys) {
                [arrAmenities addObjectsFromArray:dict[str]];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [collectionViewAmenities reloadData];
                [self ApiForMaximumPrice];
            });
        } failure:^(NSString *msg, BOOL success) {
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [self ApiForMaximumPrice];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
            
        });
        
    });
    
}

-(void)ApiForMaximumPrice{
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        NSString *strRole = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@price_range?latitude=%f&longitude=%f",kBaseUrl,self.location.coordinate.latitude,self.location.coordinate.longitude] emailString:strRole accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            
            arrFilter = [[NSMutableArray alloc]init];
            arrFilter = [result mutableCopy];
            //            theViewSlider.maximumValue = [[arrFilter valueForKey:@"max_guest"]intValue];
            //            self.labelSliderPrice.maximumValue = [[arrFilter valueForKey:@"max_price"]floatValue];
            //            self.labelSlider.minimumValue = [[arrFilter valueForKey:@"min_price"]floatValue];
            labelFilter.text = [NSString stringWithFormat:@"Price range ( in %@)",[arrFilter valueForKey:@"symbol"]];
            guestNumberSlider.maxValue = [[arrFilter valueForKey:@"max_guest"]floatValue];
            guestNumberSlider.minValue = [[arrFilter valueForKey:@"min_guest"] floatValue];
            guestNumberSlider.selectedMaximum = [[arrFilter valueForKey:@"max_guest"]floatValue];
            
            priceRangeSlider.maxValue = self.maxPrice;
            priceRangeSlider.minValue = self.minPrice; //[[arrFilter valueForKey:@"min_price"]floatValue];
            priceRangeSlider.selectedMinimum = self.minPrice; //[[arrFilter valueForKey:@"min_price"] floatValue];
            priceRangeSlider.selectedMaximum = self.maxPrice; //[[arrFilter valueForKey:@"max_price"]floatValue];
            
            //[self configureLabelSliderForPrice];
            //[self updateSliderLabelsForPrice];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [collectionViewAmenities reloadData];
            });
        } failure:^(NSString *msg, BOOL success) {
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // [MBProgressHUD hideHUDForView:self.view animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            //            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            //            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
            
        });
        
    });
    
    
}


#pragma mark - CLLocationManagerDelegate
//- (void)locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
//    switch (status) {
//        case kCLAuthorizationStatusNotDetermined: {
//            NSLog(@"User still thinking..");
////            [self ApiForActiveBookings];
//            
//        } break;
//        case kCLAuthorizationStatusDenied: {
//            NSLog(@"User hates you");
//         //   [self ApiForActiveBookings];
//            
//        } break;
//        case kCLAuthorizationStatusAuthorizedWhenInUse:{
//            [locationManager startUpdatingLocation];
//        }
//            break;
//        case kCLAuthorizationStatusAuthorizedAlways: {
//            //Will update location immediately
//        } break;
//        default:
//            break;
//    }
//}

//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
//    NSLog(@"didUpdateToLocation: %@", newLocation);
//    location = newLocation;
//
//    if (location != nil) {
//        NSString *strLongi =  [NSString stringWithFormat:@"%.8f", location.coordinate.longitude];
//        NSString *strLAt = [NSString stringWithFormat:@"%.8f", location.coordinate.latitude];
//        NSLog(@"LOngi =>%@   Lat =.%@", strLongi,strLAt);
//    }
//   // [self ApiForActiveBookings];
//    [locationManager stopUpdatingLocation];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
