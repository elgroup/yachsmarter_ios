//
//  MakePaymentViewController.m
//  YachtMasters
//
//  Created by Anvesh on 05/09/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "MakePaymentViewController.h"
#import "Constants.h"
#import "CommonMethods.h"
#import "PaymentsTVC.h"
#import "WebServiceManager.h"
#import "MBProgressHUD.h"


@interface MakePaymentViewController ()
{
    int monthNo;
    int yearNo;
    NSDateFormatter *dateFormatter;
    NSArray *arrGroupedDataOfCurrentMonth;
    NSMutableDictionary *dictDate,*selectedMonths;
    __weak IBOutlet NSLayoutConstraint *lcConstraintHeight;
    NSRange range;
    NSMutableDictionary *dictDates;
    __weak IBOutlet UIButton *buttonProceed;
    __weak IBOutlet UILabel *labelAmount;
    MBProgressHUD *hud;
}
@end

@implementation MakePaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    [calendar setMinimumDaysInFirstWeek:1];
    NSDateComponents *dateComponent = [calendar components:(NSCalendarUnitYear | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitWeekday) fromDate:[NSDate date]];
    dateComponent.day = 1;
    monthNo = (int)dateComponent.month;
    yearNo = (int)dateComponent.year;
    
    _buttonPreviuos.tag = 1;
    [_buttonPreviuos addTarget:self action:@selector(btnPreviousOrNextClick:) forControlEvents:UIControlEventTouchUpInside];
    _buttonNext.tag = 2;
    [_buttonNext addTarget:self action:@selector(btnPreviousOrNextClick:) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = true;
    [self ApiForGettingTotalAmount];
    //labelAmount.text = [NSString stringWithFormat:@"%@ %@",[_dictYachtJourneyDetails valueForKey:@"currency"],[_dictYachtJourneyDetails valueForKey:@"total_price"]];
    [self CustomNavigation];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [[CommonMethods sharedInstance]DropShadow:buttonProceed UIColor:[UIColor colorWithRed:90.0/255.0 green:190.0/255.0 blue:240.0/255.0 alpha:1.0] andShadowRadius:3.0];
    lcConstraintHeight.constant = _containerView.frame.size.width+120;
    dictDates = [[NSMutableDictionary alloc]init];
    NSString *strFirstDate = [_dictYachtJourneyDetails valueForKey:@"start_date"];
    NSDictionary *dictFirstdate = [self GetFormattedDatefromString:strFirstDate];
    [dictDates   setObject:dictFirstdate forKey:@"FirstDay"] ;
    int lastDay = [[[_dictYachtJourneyDetails valueForKey:@"FirstDay"] valueForKey:@"date"] intValue];
    
    if ([[self.dictYachtJourneyDetails valueForKey:@"Duration"] isEqualToString:@"week"]){
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
        [formatter setDateFormat:@"dd-MM-yyyy"];
        [formatter setTimeZone:timeZone];
        NSDate *firstDate = [formatter dateFromString:strFirstDate];
        int daysToAdd = 7;  // or 60 :-)
        NSDate *newDate1 = [firstDate initWithTimeInterval:60*60*24*daysToAdd - 1 sinceDate:firstDate]; //[firstDate initWithTimeIntervalSince1970:60*60*24*daysToAdd];
        NSString *strSecondDate = [formatter stringFromDate:newDate1];
        NSDictionary *dictLastDate = [self GetFormattedDatefromString:strSecondDate];
        [dictDates setObject:dictLastDate forKey:@"SecondDay"];
        
    }
    

//    
//    NSMutableDictionary *dict2 = [[NSMutableDictionary alloc]init];
//    NSString *strSecondDate1 = [strSecondDate substringWithRange:NSMakeRange(0,2)];
//    NSInteger secondDate = [strSecondDate1 integerValue];
//    NSString *strSecondMonth1 = [strSecondDate substringWithRange:NSMakeRange(3,2)];
//    NSInteger secondMonth = [strSecondMonth1 integerValue];
//    
//    NSString *strSecondyear1 = [strSecondDate substringWithRange:NSMakeRange(6,4)];
//    NSInteger secondYear = [strSecondyear1 integerValue];
//    [dict2 setObject:[NSNumber numberWithInteger:secondDate] forKey:@"Date"];
//    [dict2 setObject:[NSNumber numberWithInteger:secondMonth] forKey:@"month"];
//    [dict2 setObject:[NSNumber numberWithInteger:secondYear] forKey:@"year"];
//    [dict2 setObject:newDate1 forKey:@"Day"];
//    
//    [dictDateSelected setObject:dict2 forKey:@"SecondDay"];
    
//    NSString *lastDate = [_dictYachtJourneyDetails valueForKey:@"end_date"];
//    NSDictionary *dictLastDate = [self GetFormattedDatefromString:lastDate];
//    [dictDates setObject:dictLastDate forKey:@"SecondDay"];
    
    [self updateUI];
    //range.length
    [self markDateBoundFromIndex:dictDates andDayCount:lastDay];
    //[self markDateBoundFromIndex:6 toIndex:20];
}



-(void)CustomNavigation{
    self.navigationController.navigationBar.hidden = false;
    self.title = @"Selected Dates";
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 10.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
    
}


-(BOOL)hidesBottomBarWhenPushed
{
    return YES;
}
-(void)BackButtonPressed{
    self.tabBarController.tabBar.hidden = false;
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)ButtonProceedToPaymentClicked:(id)sender {
    PaymentsTVC *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentsTVC"];
    controller.dictYachtDetail = _dictYachtJourneyDetails;
    [self.navigationController pushViewController:controller animated:true];
    
    
}

-(NSMutableDictionary *)GetFormattedDatefromString:(NSString *)strDate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *startDate = [formatter dateFromString:strDate];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    NSString *dayString = [df stringFromDate:startDate];
    [df setDateFormat:@"MM"];
    NSString *monthString = [df stringFromDate:startDate];
    
    [df setDateFormat:@"yyyy"];
    NSString  *yearString = [df stringFromDate:startDate];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:dayString forKey:@"day"];
    [dict setObject:monthString forKey:@"month"];
    [dict setObject:yearString forKey:@"year"];
    return dict;
}


- (void) updateUI{
    [self setCalenderForMonthNumber:monthNo andYear:yearNo];
}


- (void) setCalenderForMonthNumber:(int) month andYear:(int) year{
    [_labelMonth setText:[NSString stringWithFormat:@"%@, %d", [self monthNameForNumber:month], year]];
    NSNumber *number = _dictLocation[@"lat"];
    double latitude = 0, longitude = 0;
    latitude = number.doubleValue;
    number = _dictLocation[@"lng"];
    longitude = number.doubleValue;
    NSTimeZone *timeZone = [[CommonMethods sharedInstance]TimeZoneWithRespectToLatitiue:latitude andLongitude:longitude];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    calendar.timeZone = [NSTimeZone timeZoneWithAbbreviation:timeZone.abbreviation];
    [calendar setMinimumDaysInFirstWeek:1];
    NSDateComponents *dateComponent = [calendar components:(NSCalendarUnitYear | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitWeekday) fromDate:[NSDate date]];
    dateComponent.day = 1;
    dateComponent.month = month;
    dateComponent.year = year;
    NSDate *dayOneInCurrentMonth = [calendar dateFromComponents:dateComponent];
    NSString *dayName = [dateFormatter stringFromDate:dayOneInCurrentMonth];
    range = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:dayOneInCurrentMonth];
    [self setDatesOnButtons:[self dayNumberForString:dayName] andNumberOfDays:(int)range.length];
}
- (int) dayNumberForString:(NSString*) dayName{
    return (int)[[@"Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday" componentsSeparatedByString:@","] indexOfObject:dayName]+1;
}



- (void) setDatesOnButtons:(int) firstDay andNumberOfDays:(int) dayCount{
    for(UIButton *subview in [_containerView subviews]) {
        [subview removeFromSuperview];
    }
    int width;
    if (IS_IPHONE_5) {
        width = 245/7;
    }
    else if(IS_IPHONE_6 || IS_IPHONE_X){
        width = 300/7;
    }
    else{
        width = 339/7;
    }
    
    int j = 0;
    int l = firstDay-1;
    int m = 0;
    // int first = firstDay;
    //int  width = 300/7;
    int k = _containerView.frame.size.width/7;
    for(int i = 1; i <= dayCount; i++){
        
        [self addButtonForDatesWithFrame:CGRectMake(l*k, 0.0 + m*j+13, width, width) andTag:i];
        
        k = width;
        l++;
        if (l %7 == 0) {
            k = 0;
            j = width+7;
            l=0;
            m++;
        }
    }
    [self PreviousButtonClickAction:dictDates andDayCount:dayCount];
}
- (NSString*)monthNameForNumber:(int) monthNumber{
    if(monthNumber > 12)
        monthNumber -= 12;
    else if(monthNumber < 1)
        monthNumber = 12;
    return [[@"January,February,March,April,May,June,July,August,September,October,November,December" componentsSeparatedByString:@","] objectAtIndex:monthNumber-1];
}


- (UIButton *) addButtonForDatesWithFrame:(CGRect) frame1 andTag:(int) tag1{
    UIButton *btn = [[UIButton alloc] initWithFrame:frame1];
    [_containerView addSubview:btn];
    // [btn addTarget:self action:@selector(dateButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [btn setTag:tag1];
    [btn setTitle:[NSString stringWithFormat:@"%d",tag1] forState:UIControlStateNormal];
    btn.titleLabel.font = FontOpenSans(15);
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *strCurrentDate = [formatter stringFromDate:currentDate];
    NSString *strCurrentYear = [strCurrentDate substringWithRange:NSMakeRange(0, 4)];
    NSString *strCurrentMonth = [strCurrentDate substringWithRange:NSMakeRange(5,2)];
    if ([strCurrentYear intValue] == yearNo) {
        if ([strCurrentMonth intValue] == monthNo) {
            strCurrentDate = [strCurrentDate substringFromIndex:8];
            if ([strCurrentDate intValue] >= tag1) {
                btn.userInteractionEnabled = false;
                [btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            }
            if ([strCurrentDate intValue] == tag1) {
                btn.backgroundColor = [UIColor colorWithRed:149.0/255.0 green:135.0/255.0 blue:238.0/255.0 alpha:1.0];
                [[CommonMethods sharedInstance]CornerRadius:btn Radius:_containerView.frame.size.width/14 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
                [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
            }
        }
    }
    return btn;
}
#pragma mark - Button Clickes
- (void) btnPreviousOrNextClick:(UIButton*) sender{
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *strCurrentDate = [formatter stringFromDate:currentDate];
    NSString *strCurrentYear = [strCurrentDate substringWithRange:NSMakeRange(0, 4)];
    NSString *strCurrentMonth = [strCurrentDate substringWithRange:NSMakeRange(5,2)];
    if(sender.tag == 1){
        if ([strCurrentYear intValue] != yearNo) {
            if(monthNo == 1){
                monthNo = 12;
                yearNo --;
            }
            else{
                monthNo --;
            }
            [self setCalenderForMonthNumber:monthNo andYear:yearNo];
        }
        else if ([strCurrentMonth intValue] != monthNo) {
            if(monthNo == 1){
                monthNo = 12;
                yearNo --;
            }
            else{
                monthNo --;
            }
            [self setCalenderForMonthNumber:monthNo andYear:yearNo];
        }
    }
    else{
        if ([strCurrentYear intValue] != yearNo && [strCurrentMonth intValue] != monthNo ) {
        }
        if(monthNo == 12){
            monthNo = 1;
            yearNo ++;
        }
        else{
            monthNo++;
        }
        [self setCalenderForMonthNumber:monthNo andYear:yearNo];
    }
}

#pragma mark- Test
- (void) markDateBoundFromIndex:(NSMutableDictionary *)dictCalendarDate andDayCount:(NSInteger)lastday {
    NSInteger firstDay = [[[dictCalendarDate valueForKey:@"FirstDay"] valueForKey:@"day"] integerValue];
    NSInteger firstMonth = [[[dictCalendarDate valueForKey:@"FirstDay"] valueForKey:@"month"] integerValue];
    NSInteger firstYear = [[[dictCalendarDate valueForKey:@"FirstDay"]valueForKey:@"year"]integerValue];
    
    NSInteger secondDay  = [[[dictCalendarDate valueForKey:@"SecondDay"] valueForKey:@"day"] integerValue];
    NSInteger secondMonth = [[[dictCalendarDate valueForKey:@"SecondDay"] valueForKey:@"month"] integerValue];
    NSInteger secondYear = [[[dictCalendarDate valueForKey:@"SecondDay"] valueForKey:@"year"] integerValue];
    
    if (firstYear == yearNo) {
        if (firstMonth == monthNo && secondMonth == monthNo) {
            for (NSInteger i = firstDay; i <= secondDay; i++) {
                UIButton *dateDay = [_containerView viewWithTag:i];
                dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                dateDay.userInteractionEnabled = false;
                if (i == firstDay) {
                    [self LeftCornerRadius:dateDay];
                }
                if (i == secondDay) {
                    [self RightCornerradius:dateDay];
                }
            }
            
        }
        else if (firstMonth == monthNo){
            for (NSInteger i = firstDay; i <= lastday; i++) {
                UIButton *dateDay = [_containerView viewWithTag:i];
                dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                dateDay.userInteractionEnabled = false;
                if (i == firstDay) {
                    [self LeftCornerRadius:dateDay];
                }
            }
        }
        else if (secondMonth == monthNo){
            for (NSInteger i = 1; i <= secondDay; i++) {
                UIButton *dateDay = [_containerView viewWithTag:i];
                dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                dateDay.userInteractionEnabled = false;
                if (i == secondDay) {
                    [self LeftCornerRadius:dateDay];
                }
            }
            
        }
    }
}


-(void)PreviousButtonClickAction:(NSMutableDictionary *)dictCalendarDate andDayCount:(int)lastday{
    NSInteger firstDay = [[[dictCalendarDate valueForKey:@"FirstDay"] valueForKey:@"day"] integerValue];
    NSInteger firstMonth = [[[dictCalendarDate valueForKey:@"FirstDay"] valueForKey:@"month"] integerValue];
    NSInteger firstYear = [[[dictCalendarDate valueForKey:@"FirstDay"]valueForKey:@"year"]integerValue];
    
    if (dictCalendarDate.count == 1) {
        if (yearNo == firstYear) {
            if (monthNo == firstMonth) {
                for (NSInteger i =firstDay; i == firstDay ; i++) {
                    UIButton *btn = [_containerView viewWithTag:i];
                    btn.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                    [[CommonMethods sharedInstance]CornerRadius:btn Radius:_containerView.frame.size.width/14 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
                    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                }
            }
        }
    }
    else{
        NSInteger secondDay  = [[[dictCalendarDate valueForKey:@"SecondDay"] valueForKey:@"day"] integerValue];
        NSInteger secondMonth = [[[dictCalendarDate valueForKey:@"SecondDay"] valueForKey:@"month"] integerValue];
        NSInteger secondYear = [[[dictCalendarDate valueForKey:@"SecondDay"] valueForKey:@"year"] integerValue];
        
        if (yearNo == firstYear && yearNo == secondYear) {
            if (monthNo == firstMonth && monthNo == secondMonth) {
                for (NSInteger i =firstDay; i <= secondDay ; i++) {
                    UIButton *dateDay = [_containerView viewWithTag:i];
                    dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                    [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    dateDay.userInteractionEnabled = false;
                    dateDay.layer.cornerRadius = 0.0;
                    if (i == firstDay) {
                        dateDay.layer.cornerRadius = 0.0;
                        [self LeftCornerRadius:dateDay];
                    }
                    if (i == secondDay) {
                        dateDay.layer.cornerRadius = 0.0;
                        [self RightCornerradius:dateDay];
                    }
                }
            }
            else if (monthNo == firstMonth || monthNo == secondMonth) {
                if(monthNo == firstMonth){
                    for (NSInteger i = firstDay; i <= lastday; i++) {
                        UIButton *dateDay = [_containerView viewWithTag:i];
                        dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                        [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        dateDay.userInteractionEnabled = false;
                        dateDay.layer.cornerRadius = 0.0;
                        if (i == firstDay) {
                            dateDay.layer.cornerRadius = 0.0;
                            [self LeftCornerRadius:dateDay];
                        }
                    }
                    
                }
                else if(monthNo == secondMonth){
                    for (NSInteger i = 1; i <= secondDay; i++) {
                        UIButton *dateDay = [_containerView viewWithTag:i];
                        dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                        [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        dateDay.userInteractionEnabled = false;
                        dateDay.layer.cornerRadius = 0.0;
                        if (i == secondDay) {
                            dateDay.layer.cornerRadius = 0.0;
                            [self RightCornerradius:dateDay];
                        }
                    }
                }
            }
        }
        else if (yearNo == secondYear && yearNo != firstYear) {
            if (firstMonth > secondMonth && secondMonth == monthNo) {
                for (NSInteger i =1; i <= secondDay; i++) {
                    UIButton *dateDay = [_containerView viewWithTag:i];
                    dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                    [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    dateDay.userInteractionEnabled = false;
                    dateDay.layer.cornerRadius = 0.0;
                    if (i == secondDay) {
                        dateDay.layer.cornerRadius = 0.0;
                        [self RightCornerradius:dateDay];
                    }
                }
            }
        }
        else if (yearNo == firstYear && secondYear != yearNo){
            if (firstMonth > secondMonth && firstMonth == monthNo) {
                for (NSInteger i =firstDay; i <= lastday; i++) {
                    UIButton *dateDay = [_containerView viewWithTag:i];
                    dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                    [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    dateDay.userInteractionEnabled = false;
                    dateDay.layer.cornerRadius = 0.0;
                    if (i == firstDay) {
                        dateDay.layer.cornerRadius = 0.0;
                        [self LeftCornerRadius:dateDay];
                    }
                }
                
            }
            
        }
        NSString *strFirstDate = [NSString stringWithFormat:@"%d-%d-%d",01,monthNo,yearNo];
        NSString *strLastDate = [NSString stringWithFormat:@"%lu-%d-%d",(unsigned long)range.length,monthNo,yearNo];
        NSString *strStartDay = [ NSString stringWithFormat:@"%ld-%ld-%ld",(long)firstDay,(long)firstMonth,(long)firstYear];
        NSString *strEndDay = [ NSString stringWithFormat:@"%ld-%ld-%ld",(long)secondDay,(long)secondMonth,(long)secondYear];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"dd-MM-yyyy"];
        NSDate *firstDate = [formatter dateFromString:strFirstDate];
        NSDate *lastDate = [formatter dateFromString:strLastDate];
        NSDate *startDate = [formatter dateFromString:strStartDay];
        NSDate *endDate = [formatter dateFromString:strEndDay];
        NSMutableArray *arrDates = [[NSMutableArray alloc]init];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:startDate forKey:@"StartDay"];
        [dict setObject:endDate forKey:@"LastDay"];
        [arrDates addObject:dict];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"StartDay < %@ AND LastDay > %@", firstDate,lastDate];
        NSArray *arr = [arrDates filteredArrayUsingPredicate:predicate];
        if ([arr count ] >0 ) {
            for (NSInteger i = 1; i <= range.length; i++) {
                UIButton *dateDay = [_containerView viewWithTag:i];
                dateDay.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:194.0/255.0 blue:206.0/255.0 alpha:1.0];
                [dateDay setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                dateDay.userInteractionEnabled = false;
                dateDay.layer.cornerRadius = 0.0;
            }
        }
    }
}


-(void)LeftCornerRadius:(UIButton *)view{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(view.frame.size.width/2, view.frame.size.width/2)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    view.layer.mask = maskLayer;
}

-(void)RightCornerradius:(UIButton *)view{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(view.frame.size.width/2, view.frame.size.width/2)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    view.layer.mask = maskLayer;
}

-(void)ApiForGettingTotalAmount{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSString *strCurrency = [self.dictYachtJourneyDetails valueForKey:@"currency"];
        [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@add_stripe_fees?price_id=%@&currency=%@",kBaseUrlVersion2,[NSString stringWithFormat:@"%@",[_dictYachtJourneyDetails valueForKey:@"price_id"]],strCurrency] emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            //   [_dictYachtJourneyDetails valueForKey:@"total_price"]
            NSString *strPrice = [[result valueForKey:@"total_price"] stringValue];
            labelAmount.text = [NSString stringWithFormat:@"%@ %@",[_dictYachtJourneyDetails valueForKey:@"currency"], [[CommonMethods sharedInstance]  priceFormatting:strPrice]];
            [_dictYachtJourneyDetails setObject:[result valueForKey:@"total_price"] forKey:@"total_price"];
            [_dictYachtJourneyDetails setObject:[result valueForKey:@"stripe_fee"] forKey:@"stripe_fee"];
        } failure:^(NSString *msg, BOOL success) {
            
        }];
    }
}

@end


