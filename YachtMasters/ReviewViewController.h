//
//  ReviewViewController.h
//  YachtMasters
//
//  Created by Anvesh on 17/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *viewForInputReview;
@property (weak, nonatomic) IBOutlet UITextView *textViewReview;
@property (weak, nonatomic) IBOutlet UITableView *tableViewReview;
@property (weak, nonatomic) IBOutlet UIButton *buttonStar1;
@property (weak, nonatomic) IBOutlet UIButton *buttonStar2;
@property (weak, nonatomic) IBOutlet UIButton *buttonStar3;
@property (weak, nonatomic) IBOutlet UIButton *buttonStar4;
@property (weak, nonatomic) IBOutlet UIButton *buttonStar5;
@property (weak, nonatomic) NSString *strId;
@property BOOL isOwner;

@end
