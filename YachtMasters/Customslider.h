//
//  CustomUIslider.h
//  YachtMasters
//
//  Created by Anvesh on 24/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SliderValue <NSObject>
@required
- (void) ValueOnSlider:(NSString *)strValue;
@end
@class ToolTipPopupView;
@interface ToolTipPopupView : UIView
@property (nonatomic) int value;
@property (nonatomic, retain) UIFont *fontSize;
@property (nonatomic, strong) NSString *toolTipValue;
@property(nonatomic, strong) UILabel *lbl;
@end

@interface Customslider : UISlider
{
    
    float stepValue;
}
@property(nonatomic) CGRect trackFrame;
@property(nonatomic) int noOfTicks;
@property(nonatomic) BOOL isTickType;
@property(nonatomic) BOOL showTooltip;
@property(nonatomic, strong) ToolTipPopupView *toolTip;;

@property(nonatomic, strong) NSString *valueString;

@property (nonatomic,strong) id<SliderValue> delegate;

-(UIView*)addTickMarksView;
-(void)updateToolTipView;
@end
