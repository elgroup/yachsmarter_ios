//
//  LoginViewController.m
//  YachtMasters
//
//  Created by Anvesh on 09/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "LoginViewController.h"
#import "ForgotPasswordViewController.h"
#import "Validation.h"
#import "CommonMethods.h"
#import "WebServiceManager.h"
#import "Constants.h"
#import "ViewController.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "SearchYachtViewController.h"
#import "AppDelegate.h"
#import "YachtMasters-Swift.h"

#define MAXLENGTH 14
#define MIN_LENGTH 8

@interface LoginViewController ()<UITextFieldDelegate,MBProgressHUDDelegate>{
    __weak IBOutlet UITextField *textFieldEmail;
    __weak IBOutlet UITextField *textFieldpassword;
    __weak IBOutlet UIButton *buttonForgotPssword;
    __weak IBOutlet UIButton *buttonLogin;
    __weak IBOutlet UIButton *buttonSignUp;
    MBProgressHUD *hud;
    AppDelegate *appDelegate;
}
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = true;
    UIGraphicsBeginImageContext(self.view.frame.size);
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[UIImage imageNamed:@"gbpic"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    UIColor *color = [UIColor whiteColor];
    textFieldEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: color}];
    textFieldpassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    buttonLogin.layer.cornerRadius = 20.0;
    buttonLogin.clipsToBounds = true;
    _facebookLoginButton.layer.cornerRadius = 20.0;
    _facebookLoginButton.clipsToBounds = true;
    if ([FBSDKAccessToken currentAccessToken]) {
        // User is logged in, do work such as go to next view controller.
    }
    //    _facebookLoginButton.readPermissions =
    //        @[@"public_profile", @"email", @"user_friends"];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = true;
}

- (IBAction)FaceBookLogin:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    if ([FBSDKAccessToken currentAccessToken]){
        [FBSDKAccessToken setCurrentAccessToken:nil];
        [self FaceBookLogin:sender];
    }
    else {
        [login logInWithPermissions:@[@"email", @"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult * _Nullable result, NSError * _Nullable error) {
            if (error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    //   [[Loader sharedLoader]hideLoader];
                    [[CommonMethods sharedInstance]AlertMessage:@"Process Error" andViewController:self];
                });
            }
            else if (result.isCancelled){
                dispatch_async(dispatch_get_main_queue(), ^{
                    //   [[Loader sharedLoader]hideLoader];
                    [[CommonMethods sharedInstance]AlertMessage:@"You have terminated the authentication process!" andViewController:self];
                });
            }
            else{
                if ([result.grantedPermissions containsObject:@"email"]){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
                        
                    });
                    [self fetchUserInfo];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[LoaderNew sharedLoader]hideLoader];
                        [[CommonMethods sharedInstance]AlertMessage:@"Email error" andViewController:self];
                    });
                }
            }
        }];
    }
}

-(void)fetchUserInfo{
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
                                       parameters:@{@"fields": @"id, first_name, name, last_name, picture.type(large), email,gender,birthday"}]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            appDelegate.dictFacebookDetails = [[NSMutableDictionary alloc]init];
            [appDelegate.dictFacebookDetails setObject:result forKey:@"FacebookDetails"];
            [self loginWithFBID:result[@"id"] andEmailID:result[@"email"]];
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
            });
            NSLog(@"%@", [error localizedDescription]);
        }
    }];
}


-(void)loginWithFBID:(NSString *)strFBID andEmailID:(NSString *)strEmailId{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
#if TARGET_IPHONE_SIMULATOR
        appDelegate.strDeviceToken = @"dgfiuwfjwegfuiewgubcr7tcb8r7868736874326cb7bteuc723568732c";
#else
        
#endif
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:strFBID forKey:@"fb_id"];
        [dict setObject:strEmailId forKey:@"email"];
        [dict setObject:appDelegate.strDeviceToken forKey:@"device_id"];
        NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
        [dictResponse setObject:dict forKey:@"users"];
        
        [WebServiceManager PostRequest:[NSString stringWithFormat:@"%@users/social_signin",kBaseUrl] andParam:dictResponse andcompletionhandler:^(NSArray *returnArray, NSError *error) {
            if ([[returnArray valueForKey:@"status"] boolValue]) {
                NSLog(@"%@",returnArray);
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [[LoaderNew sharedLoader]hideLoader];
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setBool:true forKey:@"isLogIn"];
                    [defaults setBool:true forKey:@"isSignUp"];
                    [defaults setObject:[returnArray valueForKey:@"auth_token"] forKey:@"Auth_token"];
                    [defaults setObject:[returnArray valueForKey:@"role"] forKey:@"role"];
                    [defaults setObject:[[returnArray valueForKey:@"user"]valueForKey:@"name"] forKey:@"Name"];
                    [defaults setObject:[[returnArray valueForKey:@"user"]valueForKey:@"address"] forKey:@"Address"];
                    [defaults setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[returnArray valueForKey:@"user"]valueForKey:@"image"]]] forKey:@"Image"];
                    [defaults synchronize];
                    [appDelegate.socket initializeSocket];
                    BOOL islastUsed = [[NSUserDefaults standardUserDefaults]boolForKey:@"LastUsed"];
                    if (!islastUsed) {
                        [self RegisterForLastUse];
                    }
                    //                    if ([[returnArray valueForKey:@"role"] isEqualToString:@"owner"]) {
                    //                        //  UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    //                        SWRevealViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"]; //or the homeController
                    //                        // appDelegate.window.rootViewController = controller;
                    //                        [self.navigationController pushViewController:controller animated:true];
                    //                    }
                    //                    else{
                    //                        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    //                        SearchYachtViewController *controller = [story instantiateViewControllerWithIdentifier:@"SearchYacht"];
                    //                        [self.navigationController pushViewController:controller animated:true];
                    //                    }
                    if ([[returnArray valueForKey:@"role"] isEqualToString:@"owner"]) {
                        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        SWRevealViewController *controller = [mainStoryBoard instantiateViewControllerWithIdentifier:@"SwRootController"]; //or the homeController
                        appDelegate.window.rootViewController = controller;
                    }
                    else{
                        SearchYachtViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SearchYacht"]; //or the homeController
                        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                        appDelegate.window.rootViewController = navController;
                    }
                });
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[LoaderNew sharedLoader]hideLoader];
                    [[NSUserDefaults standardUserDefaults ]setBool:true forKey:@"FacebookLogin"];
                    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    ViewController  *controller = [story instantiateViewControllerWithIdentifier:@"ViewController"];
                    [self.navigationController pushViewController:controller animated:false];
                    
                });
                
            }
            
        }];
    }
}


- (IBAction)Tapped:(id)sender {
    [self.view endEditing:true];
    if (IS_IPAD) {
        [UIView animateWithDuration:0.45 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
        }];
        
    }
    
}

- (IBAction)ButtonForgotPrssed:(id)sender {
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ForgotPasswordViewController *controller = [story instantiateViewControllerWithIdentifier:@"ForgotPasswordViewController"];
    [self.navigationController pushViewController:controller animated:true];
    
}
- (IBAction)ButtonSignUpPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults ]setBool:false forKey:@"FacebookLogin"];
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController  *controller = [story instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:controller animated:false];
    
}
- (IBAction)ButtonLoginPressed:(id)sender {
    if(textFieldEmail.text.length == 0){
        [self AlertMessage:@"Enter the email id"];
        return;
    }
    else if (![Validation validateEmail:textFieldEmail.text]){
        [self AlertMessage:@"Enter valid email id"];
        return;
    }
    else if(![[CommonMethods sharedInstance] validateEmailForDots:textFieldEmail.text]){
        [[CommonMethods sharedInstance]AlertMessage:@"Enter valid email id" andViewController:self];
        return;
    }
    else if (textFieldpassword.text.length == 0){
        [self AlertMessage:@"Password field can't be empty"];
        return;
    }
    else if (textFieldpassword.text.length < 8){
        [self AlertMessage:@"Password must be between 8 to 14 characters"];
        return;
    }
    else{
        if ([[ CommonMethods sharedInstance] checkInternetConnection]) {
#if TARGET_IPHONE_SIMULATOR
            appDelegate.strDeviceToken = @"dgfiuwfjwegfuiewgubcr7tcb8r7868736874326cb7bteuc723568732c";
#else
            
#endif
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:textFieldEmail.text forKey:@"email"];
            [dict setObject:textFieldpassword.text forKey:@"password"];
            //[dict setObject:currentDeviceId forKey:@"device_id"];
            [dict setObject:appDelegate.strDeviceToken forKey:@"device_id"];
            NSMutableDictionary *dictRequest = [[NSMutableDictionary alloc]init];
            [dictRequest setObject:dict forKey:@"users"];
            
            dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
            dispatch_async(myQueue, ^{
                [WebServiceManager PostRequest:[NSString stringWithFormat:@"%@users/sign_in",kBaseUrl] andParam:dictRequest andcompletionhandler:^(NSArray *returArray, NSError *error){
                    if (!error) {
                        if (returArray.count == 0) {
                            [[CommonMethods sharedInstance]AlertMessage:[returArray valueForKey:@"message"] andViewController:self];
                        }
                        else if (![[returArray valueForKey:@"status"] boolValue]){
                            [[CommonMethods sharedInstance]AlertMessage:[returArray valueForKey:@"message"] andViewController:self];
                        }
                        else{
                            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                            [defaults setBool:true forKey:@"isLogIn"];
                            [defaults setBool:true forKey:@"isSignUp"];
                            [defaults setObject:[returArray valueForKey:@"auth_token"] forKey:@"Auth_token"];
                            [defaults setObject:[returArray valueForKey:@"role"] forKey:@"role"];
                            [defaults setObject:[[returArray valueForKey:@"user"]valueForKey:@"name"] forKey:@"Name"];
                            [defaults setObject:[[returArray valueForKey:@"user"]valueForKey:@"address"] forKey:@"Address"];
                            [defaults setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[returArray valueForKey:@"user"]valueForKey:@"image"]]] forKey:@"Image"];
                            [defaults synchronize];
                            [appDelegate.socket initializeSocket];
                            BOOL islastUsed = [[NSUserDefaults standardUserDefaults]boolForKey:@"LastUsed"];
                            if (!islastUsed) {
                                [self RegisterForLastUse];
                            }
                            if ([[returArray valueForKey:@"role"] isEqualToString:@"owner"]) {
                                UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                SWRevealViewController *controller = [mainStoryBoard instantiateViewControllerWithIdentifier:@"SwRootController"]; //or the homeController
                                appDelegate.window.rootViewController = controller;
                            }
                            else{
                                SearchYachtViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SearchYacht"]; //or the homeController
                                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                                appDelegate.window.rootViewController = navController;
                            }
                        }
                    }
                    else{
                        NSLog( @"%@",error);
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        [[LoaderNew sharedLoader]hideLoader];
                    });
                }];
                dispatch_async(dispatch_get_main_queue(), ^{
                    //                    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    //                    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
                    [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
                    
                });
            });
            
        }
        else{
            [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
        }
    }
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqual:@"SearchYachtToHome"]){
        SWRevealViewController *controller =  (SWRevealViewController*) [segue destinationViewController];
        appDelegate.window.rootViewController = controller;
    }
}


#pragma mark - TextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == textFieldEmail) {
        [textField becomeFirstResponder];
    }
    else if (textField == textFieldpassword){
        [textFieldEmail resignFirstResponder];
        [textField becomeFirstResponder];
        if (IS_IPAD) {
            [UIView animateWithDuration:0.45 animations:^{
                self.view.frame = CGRectMake(self.view.frame.origin.x, -100, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-100);
            }];
        }
    }
    else{
        [self.view endEditing:true];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == textFieldEmail) {
        [textField resignFirstResponder];
        [textFieldpassword becomeFirstResponder];
    }
    else if (textField == textFieldpassword){
        [textField resignFirstResponder];
    }
    else{
        [self.view endEditing:true];
    }
    return true;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == textFieldpassword) {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        return newLength <= MAXLENGTH || returnKey;
    }
    else{
        return true;
    }
}

#pragma mark - Alert Method
-(void)AlertMessage:(NSString *)strMessage{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Alert"
                                 message:strMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    //Add Buttons
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
        //Handle no, thanks button
    }];
    //Add your buttons to alert controller
    [alert addAction:noButton];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)RegisterForLastUse{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        
        NSDate *date = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"dd-MM-yyyy"];
        NSString *strDate = [formatter stringFromDate:date];
        [formatter setDateFormat:@"HH:mm"];
        NSString *strTime = [formatter stringFromDate:date];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:strDate forKey:@"last_used_date"];
        [dict setObject:strTime forKey:@"last_used_time"];
        [WebServiceManager putRequestWithUrlString:[NSString stringWithFormat:@"%@users/last_used",kBaseUrl] withPostString:dict emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            
            [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"LastUsed"];
        } failure:^(NSString *msg, BOOL success) {
            
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
