//
//  LoginViewController.h
//  YachtMasters
//
//  Created by Anvesh on 09/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *facebookLoginButton;
@end
