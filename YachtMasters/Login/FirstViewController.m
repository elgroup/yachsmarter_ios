//
//  FirstViewController.m
//  YachtMasters
//
//  Created by Anvesh on 02/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "FirstViewController.h"
#import "LoginViewController.h"
#import "ViewController.h"
#import "CommonMethods.h"
#import "AppDelegate.h"

@interface FirstViewController (){
    
    __weak IBOutlet UIButton *buttonLogin;
    __weak IBOutlet UIButton *buttonSignUp;
    AppDelegate *appDelegate;
}

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = true;
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"bgpic"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    [[CommonMethods sharedInstance]CornerRadius:buttonLogin Radius:20.0 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
    
    [[CommonMethods sharedInstance]CornerRadius:buttonSignUp Radius:20.0 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
    // Do any additional setup after loading the view.
}
- (IBAction)LoginButtonPressed:(id)sender {
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    LoginViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"]; //or the homeController
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
    appDelegate.window.rootViewController = navController;


}
- (IBAction)SignUpButtonPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults ]setBool:false forKey:@"FacebookLogin"];
    ViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:controller animated:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//-(void)RegisterForLastUse{
//    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
//        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
//        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
//        
//        NSDate *date = [NSDate date];
//        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
//        [formatter setDateFormat:@"dd-MM-yyyy"];
//        NSString *strDate = [formatter stringFromDate:date];
//        [formatter setDateFormat:@"HH:mm"];
//        NSString *strTime = [formatter stringFromDate:date];
//        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//        [dict setObject:strDate forKey:@"last_used_date"];
//        [dict setObject:strTime forKey:@""];
//        [WebServiceManager putRequestWithUrlString:[NSString stringWithFormat:@"%@users/last_used",kBaseUrl] withPostString:dict emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
//            
//        } failure:^(NSString *msg, BOOL success) {
//            
//        }];
//    }
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
