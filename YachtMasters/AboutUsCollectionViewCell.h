//
//  AboutUsCollectionViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 24/10/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutUsCollectionViewCell : UICollectionViewCell
@property IBOutlet UILabel *labelContent;
@property IBOutlet UILabel *labelHeading;
@end
