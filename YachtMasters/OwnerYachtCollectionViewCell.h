//
//  OwnerYachtCollectionViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 28/07/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OwnerYachtCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewYacht;
@property (weak, nonatomic) IBOutlet UIButton *buttonFavourite;
@property (weak, nonatomic) IBOutlet UIView *viewBaseYachDetail;
@property (weak, nonatomic) IBOutlet UILabel *labelYachtName;
@property (weak, nonatomic) IBOutlet UILabel *labelPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelNumberOfReviews;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewStar1;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewStar2;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewStar3;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewStar4;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewStar5;
@property (weak, nonatomic) IBOutlet UIView *viewBaseImageYacht;

@end
