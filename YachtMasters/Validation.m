//
//  Validation.m
//  GigGuideLive
//
//  Created by Mac-1 on 10/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Validation.h"

@implementation Validation

+ (BOOL) validateEmail: (NSString *) candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];

    return [emailTest evaluateWithObject:candidate];

    
//    if([emailString length]==0){
//        return NO;
//    }
//
//    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}";
//
//    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
//    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
//
//    NSLog(@"%lu", (unsigned long)regExMatches);
//    if (regExMatches == 0) {
//        return NO;
//    } else {
//        return YES;
//    }
}
+ (BOOL) textFieldLength: (NSString*) txtField
{
	if([txtField length] <= 0)
    {
		return NO;
	}
	return YES;
	
}
+ (BOOL) validatePhoneNumber: (NSString*) PhNo
{
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
	NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
	return [phoneTest evaluateWithObject:PhNo];
}
+ (BOOL) validateURL :(NSString*)URLAddress
{
	NSString *urlRegex = @"(?:(?:(?:^(?:ht|f)tps?://)|^)(?:(?:(?:(?:(?:[a-zA-Z0-9](?:(?:[a-zA-Z0-9]|-)*[a-zA-Z0-9])?)\\.)+(?:[a-zA-Z](?:(?:[a-zA-Z0-9]|-)*[a-zA-Z0-9])?))|(?:(?:[0-9]{1,3})(?:\\.(?:[0-9]{1,3})){3}))(?::(?:[0-9]{1,5}))?)(?:(?:/(?:(?:(?:(?:[a-zA-Z0-9$\\-_.+!*'(),]|(?:%[a-fA-F0-9]{2}))|[;:@&=])*)(?:/(?:(?:(?:[a-zA-Z0-9$\\-_.+!*'(),]|(?:%[a-fA-F0-9]{2}))|[;:@&=])*))*))?(?:\\?(?:(?:(?:[a-zA-Z0-9$\\-_.+!*'(),]|(?:%[a-fA-F0-9]{2}))|[;:@&=])*))?)?$)";
	NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegex];
	return [urlTest evaluateWithObject:URLAddress];
}

+(BOOL)validateTextField:(UITextField *)txtField
{
    if(txtField == nil)
    {
        return YES;
    }
    else if([txtField.text length]<=0)
    {
        return YES;
    }
    
    return NO;
}

+(BOOL) validateTextView:(UITextView *) textView
{
    if(textView == nil)
    {
        return YES;
    }
    else if([textView.text length]<=0)
    {
        return YES;
    }
    
    return NO;
}

+(BOOL)validatePassword:(UITextField *)txtPassword ConfirmPassword:(UITextField *)txtConfPass
{
    if ([txtPassword.text isEqualToString:txtConfPass.text])
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

+(BOOL)validatePasswordLength: (NSString*) passwordLength
{
    if([passwordLength length] >= 6)
    {
        return YES;
    }
    return NO;
}

+(BOOL)validatePhoneLength: (NSString*) passwordLength
{
    if([passwordLength length] == 10)
    {
        return YES;
    }
    return NO;
}

@end
