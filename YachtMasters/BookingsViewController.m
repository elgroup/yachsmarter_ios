//
//  BookingsViewController.m
//  YachtMasters
//
//  Created by Anvesh on 21/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "BookingsViewController.h"
#import "SWRevealViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UserHistoryTableViewCell.h"
#import "ActiveBookingTableViewCell.h"
#import "Constants.h"
#import "WebServiceManager.h"
#import "CommonMethods.h"
#import "MBProgressHUD.h"
#import "NewBookingVC.h"
#import "UIImageView+WebCache.h"
#import "OwnerBookingsTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreText/CTStringAttributes.h>
#import <CoreText/CoreText.h>
#import "YachtDetailTableViewController.h"
#import "ReviewViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "ChatViewController.h"
#import "YachtMasters-Swift.h"

@interface BookingsViewController ()<UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate>
{
    
    __strong IBOutlet UIButton *buttonActive;
    __weak IBOutlet UIButton *buttonHistory;
    __weak IBOutlet UIButton *buttonCancelled;
    NSMutableDictionary *dictBookings;
    NSMutableArray *arrHistory;
    NSMutableArray *arrActive;
    NSMutableArray *arrCancelled;
    MBProgressHUD *hud;
    NSString *strRole;
    UILabel *labelBackGroundView;
    NSMutableArray *arrNewBookings;
    UIButton *buttonNewBookings;
    BOOL isApi;
    NSString *strRoleForApi;
    CLLocationManager *locationManager;
    CLLocation *location;
    
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableViewBookings;

@end

@implementation BookingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    labelBackGroundView = [[UILabel alloc]init];
    [self.tableViewBookings addSubview:labelBackGroundView];
    labelBackGroundView.textAlignment = NSTextAlignmentCenter;
    labelBackGroundView.textColor = [UIColor lightGrayColor];
//    labelBackGroundView.backgroundColor = [UIColor redColor];
    
    self.tabBarController.tabBar.hidden = false;
    // Do any additional setup after loading the view.
    strRole = [[NSString alloc]init];
    strRole = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
    
    labelBackGroundView.hidden = true;
    arrActive =  [[NSMutableArray alloc]init];
    arrHistory = [[NSMutableArray alloc]init];
    arrCancelled = [[NSMutableArray alloc]init];
    if ([strRole isEqualToString:Owner]) {
        [self.tableViewBookings reloadData];
        [self ApiForNewBookings];
        [self ApiForActiveBookings];
    }
    else{
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
            [locationManager requestWhenInUseAuthorization];
        [locationManager startUpdatingLocation];
    }
    //    dictBookings = [[NSMutableDictionary alloc]init];
    [self.tableViewBookings reloadData];
    buttonActive.selected = true;
    buttonHistory.selected = false;
    buttonCancelled.selected = false;
    isApi = true;
    [self.tableViewBookings reloadData];
    [self CustomizingButton:buttonActive];
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    labelBackGroundView.frame = self.tableViewBookings.bounds;
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    labelBackGroundView.hidden = true;
    arrActive =  [[NSMutableArray alloc]init];
    arrHistory = [[NSMutableArray alloc]init];
    arrCancelled = [[NSMutableArray alloc]init];
    if ([strRole isEqualToString:Owner]) {
        [self.tableViewBookings reloadData];
        [self ApiForNewBookings];
        [self ApiForActiveBookings];
    }
    else{
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
            [locationManager requestWhenInUseAuthorization];
        [locationManager startUpdatingLocation];
    }
    [self.tableViewBookings reloadData];
    // [self CustomizingButton:];
    
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    //    buttonActive.layer.shadowOpacity = 0.0;
    //    buttonHistory.layer.shadowOpacity = 0.0;
    //    buttonCancelled.layer.shadowOpacity = 0.0;
    labelBackGroundView.hidden = true;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self CustomNavigationBar];
    //    self.tabBarController.tabBar.hidden = false;
    //    strRole = [[NSString alloc]init];
    //    strRole = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
    //
    //    [self CustomNavigationBar];
    //        if ([strRoleForApi isEqualToString:strRole]) {
    //            if (buttonActive.selected) {
    //                [self CustomizingButton:buttonActive];
    //                [self buttonBookingsAction:buttonActive];
    //                // [self.tableViewBookings reloadData];
    //            }
    //            else if(buttonHistory.selected){
    //                [self CustomizingButton:buttonHistory];
    //                [self buttonBookingsAction:buttonHistory];
    //
    //            }
    //            else if (buttonCancelled.selected){
    //                [self CustomizingButton:buttonCancelled];
    //                [self buttonBookingsAction:buttonCancelled];
    //            }
    //
    //    }
    //    else{
    //        labelBackGroundView.hidden = true;
    //        arrActive =  [[NSMutableArray alloc]init];
    //        arrHistory = [[NSMutableArray alloc]init];
    //        arrCancelled = [[NSMutableArray alloc]init];
    //        strRoleForApi = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
    //        if ([strRole isEqualToString:Owner]) {
    //            [self ApiForNewBookings];
    //        }
    //        [self ApiForActiveBookings];
    //        buttonActive.selected = true;
    //        buttonHistory.selected = false;
    //        buttonCancelled.selected = false;
    //        isApi = true;
    //        [self CustomizingButton:buttonActive];
    //        }
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            NSLog(@"User still thinking..");
            [self ApiForActiveBookings];
            
        } break;
        case kCLAuthorizationStatusDenied: {
            NSLog(@"User hates you");
            [self ApiForActiveBookings];
            
        } break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:{
            [locationManager startUpdatingLocation];
        }
            break;
        case kCLAuthorizationStatusAuthorizedAlways: {
            //Will update location immediately
        } break;
        default:
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    location = newLocation;
    
    if (location != nil) {
        NSString *strLongi =  [NSString stringWithFormat:@"%.8f", location.coordinate.longitude];
        NSString *strLAt = [NSString stringWithFormat:@"%.8f", location.coordinate.latitude];
        NSLog(@"LOngi =>%@   Lat =.%@", strLongi,strLAt);
    }
    [self ApiForActiveBookings];
    [locationManager stopUpdatingLocation];
}



#pragma mark - customization of Active, History and cancelled button

-(void)CustomizingButton:(id)sender{
    UIButton *btn = (UIButton *)sender;
    btn.tag = [sender tag];
    
    btn.layer.cornerRadius = 20.0;
    btn.clipsToBounds = false;
    btn.backgroundColor = [UIColor whiteColor];
    
    // drop shadow
    btn.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    btn.layer.shadowOffset = CGSizeMake(0, 0.0);
    btn.layer.shadowOpacity = 2.0;
    btn.layer.shadowRadius = 4.0;
}

#pragma mark - Method for Navigation Bar

-(void)CustomNavigationBar{
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    
    UIImage* image3 = [UIImage imageNamed:@"menu items.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    self.navigationItem.title = @"Bookings";
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    //[backButton setBackgroundColor:[UIColor redColor]];
    [backButton addTarget:self action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 5.0, 20.0, 20.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
    buttonNewBookings = [[UIButton alloc] initWithFrame:CGRectMake([[UIScreen mainScreen]bounds ].size.width - 20, 10.0,30.0, 30.0)];
    //[backButton setBackgroundColor:[UIColor redColor]];
    [[CommonMethods sharedInstance]CornerRadius:buttonNewBookings Radius:15 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
    buttonNewBookings.hidden = true;
    [buttonNewBookings setBackgroundImage:[UIImage imageNamed:@"circle"] forState:UIControlStateNormal];
    [buttonNewBookings addTarget:self action:@selector(ButtonNewBookingsPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:buttonNewBookings];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}


#pragma mark - New booking button action
-(void)ButtonNewBookingsPressed{
    NewBookingVC *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"NewBookingVC"];
    controller.arrNewBookings = arrNewBookings;
    [self.navigationController pushViewController:controller animated:true];

}

-(void)revealToggle:(id)sender{
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggleAnimated:YES];
}

#pragma mark- Action of Active, History and Cancelled Buttons
- (IBAction)buttonBookingsAction:(id)sender {
    switch ([sender tag]) {
        case 10:{
            [self buttonSelectedState:sender button:buttonCancelled andButton:buttonHistory];
        }
            break;
        case 11:{
            [self buttonSelectedState:sender button:buttonActive andButton:buttonCancelled];
            
        }
            break;
        case 12:{
            [self buttonSelectedState:sender button:buttonActive andButton:buttonHistory];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Selection State for Active, History and Cncelled Buttons
-(void)buttonSelectedState:(id)sender1 button:(id)sender2 andButton:(id)sender3{
    UIButton *btn = (UIButton *)sender1;
    if (!btn.selected) {
        [self CustomizingButton:sender1];
    }
    btn.selected = true;
    
    UIButton *btn2 =(UIButton *)sender2;
    btn2.layer.shadowOpacity = 0.0;
    btn2.selected = false;
    
    UIButton *btn3 = (UIButton *)sender3;
    btn3.layer.shadowOpacity = 0.0;
    btn3.selected = false;
    [self.tableViewBookings reloadData];
}

#pragma mark - Subscript in Date
-(NSString *)SubscriptToDate:(NSInteger )date{
    if (date == 1 || date == 21 || date == 31) {
        return @"st";
    }
    else if (date == 2 || date == 22){
        return @"nd";
    }
    else if (date == 3 || date == 23){
        return @"rd";
    }
    else{
        return @"th";
    }
    
}

#pragma mark - Attributed string for Price label
-(NSMutableAttributedString *)PlainStringToAttributedStringForPrice:(NSString *)strPrice andCurrency:(NSString *)strCurrency andNights:(NSString *)strNights normalString:(NSString *)normalString{
    
    // NSString *strPrice = [[[[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"yacht"]valueForKey:@"price"] stringValue];
    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@\n%@%@\nfor %@",normalString, strCurrency,strPrice,strNights]];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:attrText];
    strPrice  = [strPrice stringByAppendingString:[NSString stringWithFormat:@"You Paid\n%@",strCurrency]];
    NSRange range = [strPrice rangeOfString:strPrice];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:FontOpenSansSemiBold(12)} range:range];
    return attributedText;
}

#pragma mark - Formatted Date String in form of Day, Month and Year
-(NSMutableDictionary *)GetFormattedDatefromString:(NSString *)strDate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *startDate = [formatter dateFromString:strDate];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    NSString *dayString = [df stringFromDate:startDate];
    dayString = [NSString stringWithFormat:@"%@%@",dayString,[self SubscriptToDate:[dayString integerValue]]];
    [df setDateFormat:@"MMM"];
    NSString *monthString = [df stringFromDate:startDate];
    
    [df setDateFormat:@"yyyy"];
    NSString  *yearString = [df stringFromDate:startDate];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:dayString forKey:@"day"];
    [dict setObject:monthString forKey:@"month"];
    [dict setObject:yearString forKey:@"year"];
    return dict;
}



- (NSMutableAttributedString *)plainStringToAttributedUnits:(NSString *)string; {
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    UIFont *font = FontOpenSansBold(15);
    UIFont *smallFont = FontOpenSansLight(10);
    
    [attString beginEditing];
    [attString addAttribute:NSFontAttributeName value:(font) range:NSMakeRange(0, string.length - 2)];
    [attString addAttribute:NSFontAttributeName value:(smallFont) range:NSMakeRange(string.length-2, 2)];
    [attString addAttribute:(NSString*)kCTSuperscriptAttributeName value:@"1" range:NSMakeRange(string.length - 2, 2)];
    [attString addAttribute:(NSString*)kCTForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, string.length - 2)];
    [attString endEditing];
    return attString;
}

#pragma mark - Button Book Now action for the customer side
-(void)ButtonBookNowPressed:(id)sender{
    if (buttonActive.selected && [[[[dictBookings valueForKey:@"active"] objectAtIndex:[sender tag]] valueForKey:@"is_ongoing"] boolValue]){
        ReviewViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ReviewViewController"];
        controller.strId = [[[[[dictBookings valueForKey:@"active"]objectAtIndex:[sender tag]] valueForKey:@"yacht"] valueForKey:@"id"] stringValue];
        controller.isOwner = false;
        [self.navigationController pushViewController:controller animated:true];
    }
    
    else if (buttonCancelled.selected){
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        YachtDetailTableViewController *controller = [story instantiateViewControllerWithIdentifier:@"YachtDetailTableViewController"];
        controller.lang = location.coordinate.longitude;
        controller.lat = location.coordinate.latitude;
        controller.strId = [[[[[dictBookings valueForKey:@"cancelled"]objectAtIndex:[sender tag]] valueForKey:@"yacht"] valueForKey:@"id"] stringValue];
        [self.navigationController pushViewController:controller animated:true];
    }
    else if (buttonActive.selected){
        __block NSDictionary *dict = [[dictBookings valueForKey:@"active"]objectAtIndex:[sender tag]];
        __block NSString *strMessage =   [self StringforYachtCancelForDate:[dict valueForKey:@"end_date"]];
        if ([strMessage isEqualToString:@""]) {
            strMessage = @"Are you sure you want to cancel your booking? You will be partially charged for the booking.";
        }
        else{
            strMessage = @"Are you sure you want to cancel your booking?";
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Cancel trip?"
                                         message:strMessage
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"YES"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            [self ApiForCancelBooking:dict andForIndexPath:[sender tag]];
                                        }];
            //Add your buttons to alert controller
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"NO"
                                       style:UIAlertActionStyleDestructive
                                       handler:^(UIAlertAction * action) {
                                           //Handle no, thanks button
                                       }];
            [alert addAction:yesButton];
            [alert addAction:noButton];
            [self presentViewController:alert animated:YES completion:nil];
        });
        
    }
    else if (buttonHistory.selected){
        ReviewViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ReviewViewController"];
        controller.strId = [[[[[dictBookings valueForKey:@"history"]objectAtIndex:[sender tag]] valueForKey:@"yacht"] valueForKey:@"id"] stringValue];
        controller.isOwner = false;
        [self.navigationController pushViewController:controller animated:true];
    }
}


#pragma mark - Button Action For Book Again Button for Customer End
-(void)ButtonBookAgainPressed:(id)sender{
    if (buttonHistory.selected) {
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        YachtDetailTableViewController *controller = [story instantiateViewControllerWithIdentifier:@"YachtDetailTableViewController"];
        controller.strId = [[[[[dictBookings valueForKey:@"history"]objectAtIndex:[sender tag]] valueForKey:@"yacht"] valueForKey:@"id"] stringValue];
        controller.lat = location.coordinate.latitude;
        controller.lang = location.coordinate.longitude;
        [self.navigationController pushViewController:controller animated:true];
    }
}


#pragma mark - Button Pending  Pressed from Owner

-(void)ButtonPendinfClicked:(id)sender{
    if (buttonActive.selected) {
        [self ApprovingOfBooking:[[[[dictBookings valueForKey:@"active"] objectAtIndex:[sender tag]] valueForKey:@"id"] integerValue] forIndex:[sender tag]];
    }
    else if (buttonHistory.selected){
        ReviewViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ReviewViewController"];
        controller.strId = [[[[[dictBookings valueForKey:@"history"]objectAtIndex:[sender tag]] valueForKey:@"yacht"] valueForKey:@"id"] stringValue];
        controller.isOwner = true;
        [self.navigationController pushViewController:controller animated:true];
        
    }
    else if (buttonCancelled.selected){
        
    }
}

#pragma mark - Button Cancel pressed
-(void)ButtonDeclineClicked:(id)sender{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        __block NSInteger Tag = [sender tag];
        if (![[[[dictBookings valueForKey:@"active"] objectAtIndex:[sender tag]] valueForKey:@"is_ongoing"] boolValue]){
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:[[[dictBookings valueForKey:@"active"]objectAtIndex:[sender tag]] valueForKey:@"id"] forKey:@"id"];
            [dict setObject:@"declined" forKey:@"status"];
            NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
            [dictResponse setObject:dict forKey:@"bookings"];
            [WebServiceManager putRequestWithUrlString:[NSString stringWithFormat:@"%@bookings",kBaseUrl] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:Tag inSection:0];
                OwnerBookingsTableViewCell *cell = (OwnerBookingsTableViewCell*)[self.tableViewBookings cellForRowAtIndexPath:indexpath];
                [cell.buttonPending setTitle:@"Declined" forState:UIControlStateNormal];
                cell.buttonPending.userInteractionEnabled = false;
                [cell.buttonPending setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                cell.buttonDecline.hidden = true;
                [[[dictBookings valueForKey:@"active"] objectAtIndex:[sender tag]] setObject:@"declined" forKey:@"status"];
                //[cell.buttonPending setTitle:@"Approve" forState:UIControlStateNormal];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[LoaderNew sharedLoader]hideLoader];
                    [[CommonMethods sharedInstance]AlertMessage:[result valueForKey:@"message"] andViewController:self];
                });
                
            } failure:^(NSString *msg, BOOL success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[MBProgressHUD hideHUDForView:self.view animated:YES];
                    [[LoaderNew sharedLoader]hideLoader];
                    [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
                });
            }];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
            });
        }
        else{
            [[CommonMethods sharedInstance]AlertMessage:@"Can't cancel the booking as the trip is going on!" andViewController:self];
        }
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
        
    }
    
    
}

#pragma mark - Button Action for Approve
-(void)ApprovingOfBooking:(NSInteger)intId forIndex:(NSInteger)index{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:[NSNumber numberWithInteger:intId] forKey:@"id"];
        [dict setObject:@"approved" forKey:@"status"];
        NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
        [dictResponse setObject:dict forKey:@"bookings"];
        [WebServiceManager putRequestWithUrlString:[NSString stringWithFormat:@"%@bookings",kBaseUrl] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
                OwnerBookingsTableViewCell *cell = (OwnerBookingsTableViewCell*)[self.tableViewBookings cellForRowAtIndexPath:indexpath];
                [cell.buttonPending setTitle:@"Approved" forState:UIControlStateNormal];
                [cell.buttonDecline setTitle:@"Cancel" forState:UIControlStateNormal];
                [[[dictBookings valueForKey:@"active"] objectAtIndex:index] setObject:@"approved" forKey:@"status"];
                [[CommonMethods sharedInstance]AlertMessage:[result valueForKey:@"message"] andViewController:self];
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}

#pragma mark - Call Button Action
- (IBAction)ButtonCallPressed:(id)sender {
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",[[[[dictBookings valueForKey:@"active"] objectAtIndex:[sender tag]] valueForKey:@"user"] valueForKey:@"phone_number"]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        //        [[UIApplication sharedApplication] openURL:phoneUrl];
        NSDictionary *dict = [[NSDictionary alloc]init];
        [[UIApplication sharedApplication] openURL:phoneUrl options:dict completionHandler:nil];
    } else{
        [[CommonMethods sharedInstance]AlertMessage:@"Call facility is not available!!!" andViewController:self];
    }
    
}

#pragma mark - TableView Delegate and datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // if ([strRole isEqualToString:Owner]) {
    if (buttonActive.selected) {
        if ([[dictBookings valueForKey:@"active"] count] == 0) {
            labelBackGroundView.hidden = false;
            if (isApi) {
                labelBackGroundView.textColor = [UIColor clearColor];
            }
            else{
                labelBackGroundView.textColor = [UIColor lightGrayColor];
            }
            tableView.scrollEnabled = false;
            labelBackGroundView.text = @"No active yacht found";
            labelBackGroundView.font = FontOpenSans(15);
            return 0;
        }
        else{
            tableView.scrollEnabled = true;
            labelBackGroundView.hidden = true;
            return [[dictBookings valueForKey:@"active"] count];
        }
    }
    else if (buttonHistory.selected){
        if ([[dictBookings valueForKey:@"history"] count] == 0) {
            labelBackGroundView.hidden = false;
            if (isApi) {
                labelBackGroundView.textColor = [UIColor clearColor];
            }
            else{
                labelBackGroundView.textColor = [UIColor lightGrayColor];
            }
            tableView.scrollEnabled = false;
            labelBackGroundView.text = @"No history found";
            labelBackGroundView.font = FontOpenSans(15);
            
            return 0;
        }
        else{
            tableView.scrollEnabled = true;
            labelBackGroundView.hidden = true;
            return [[dictBookings valueForKey:@"history"] count];
        }
    }
    else if (buttonCancelled.selected){
        if ([[dictBookings valueForKey:@"cancelled"] count] == 0) {
            labelBackGroundView.hidden = false;
            if (isApi) {
                labelBackGroundView.textColor = [UIColor clearColor];
            }
            else{
                labelBackGroundView.textColor = [UIColor lightGrayColor];
            }
            tableView.scrollEnabled = false;
            labelBackGroundView.text = @"No cancelled yacht found";
            labelBackGroundView.font = FontOpenSans(15);
            
            return 0;
        }
        else{
            tableView.scrollEnabled = true;
            labelBackGroundView.hidden = true;
            return [[dictBookings valueForKey:@"cancelled"] count];
        }
    }
    else{
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([strRole isEqualToString:Owner]) {
        static NSString *cellIdentifier = @"OwnerBookingsTableViewCell";
        OwnerBookingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OwnerBookingsTableViewCell"];
        if (cell == nil) {
            NSArray *nib =[[NSBundle mainBundle]loadNibNamed:cellIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.buttonCall.hidden = true;
        cell.buttonChat.hidden = true;
        cell.buttonDecline.hidden = true;
        cell.buttonPending.hidden = true;
        cell.buttonPending.userInteractionEnabled = false;
        cell.buttonDecline.userInteractionEnabled = false;
        if (buttonActive.selected) {
            cell.viewBase.layer.shadowColor = [UIColor colorWithRed:0.0/255.0 green:196.0/255.0 blue:56.0/255.0 alpha:1].CGColor;
            cell.viewBase.layer.shadowOpacity = 0.3;
            cell.viewBase.backgroundColor = [UIColor whiteColor];
            cell.buttonPending.hidden = false;
            cell.buttonDecline.hidden = false;
            cell.buttonDecline.userInteractionEnabled = true;
            NSString *strStatus = [[[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row] valueForKey:@"status"] capitalizedString];
            if ([strStatus isEqualToString:@"Declined"]) {
                [cell.buttonPending setTitle:@"Declined" forState:UIControlStateNormal];
                [cell.buttonPending setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                cell.buttonDecline.hidden = true;
            }
            else if ([ strStatus isEqualToString:@"Pending"]){
                [cell.buttonPending setTitle:@"Approve" forState:UIControlStateNormal];
                [cell.buttonPending setTitleColor:[UIColor colorWithRed:0.0/255.0 green:196.0/255.0 blue:56.0/255.0 alpha:1] forState:UIControlStateNormal];
                cell.buttonPending.userInteractionEnabled = true;
                cell.buttonPending.tag = indexPath.row;
                [cell.buttonPending addTarget:self action:@selector(ButtonPendinfClicked:) forControlEvents:UIControlEventTouchUpInside];
            }
            else{
                [cell.buttonPending setTitle:@"Approved" forState:UIControlStateNormal];
                [cell.buttonPending setTitleColor:[UIColor colorWithRed:0.0/255.0 green:196.0/255.0 blue:56.0/255.0 alpha:1] forState:UIControlStateNormal];
                [cell.buttonDecline setTitle:@"Cancel" forState:UIControlStateNormal];
                cell.buttonDecline.hidden = false;
            }
            cell.buttonDecline.tag = indexPath.row;
            [cell.buttonDecline addTarget:self action:@selector(ButtonDeclineClicked:) forControlEvents:UIControlEventTouchUpInside];
            // [cell.buttonPending setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[[[dictBookings objectForKey:@"active"] objectAtIndex:indexPath.row] objectForKey:@"user"] valueForKey:@"image"]];
            NSURL *url = [NSURL URLWithString:strUrl];
            [cell.imageViewUser sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            cell.labelYachtName.text = [[[[dictBookings valueForKey:@"active"] objectAtIndex:indexPath.row] valueForKey:@"yacht"] valueForKey:@"name"];
            cell.labelUseName.text = [[[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"user"] valueForKey:@"name"];
           
            // Booking date Time and duration
            NSString *strStartDate = [[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"start_date"];
            NSString *strStartTime = [[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"start_time"];
            NSString *strTimeDuration = [[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"term"];
            int hours = [[[[dictBookings valueForKey:@"active"] objectAtIndex:indexPath.row]valueForKey:@"hours"] intValue];
            strStartTime = [self FormattedTime:strStartTime];
            strStartDate = [self FormattedDate:strStartDate];
            cell.labelDate.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Booking Date: %@",strStartDate] andRangeString:@"Booking Date:"];
            cell.labelDuration.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Booking Time: %@",strStartTime] andRangeString:@"Booking Time:"];
            if ([strTimeDuration isEqualToString:@"per_day"] || [strTimeDuration isEqualToString:@"half_day"]){
                cell.labelTimeDuration.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Duration: %@ (%d hours)",[self timeDurationforBookings:strTimeDuration],hours] andRangeString:@"Duration:"];
            }
            else{
                cell.labelTimeDuration.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Duration: %@",[self timeDurationforBookings:strTimeDuration]] andRangeString:@"Duration:"];
            }
            
            // Booking Price with formatted price
            cell.TextForAmount.text = @"Amount";
            NSString *strPrice = [[[dictBookings valueForKey:@"active"] objectAtIndex:indexPath.row]valueForKey:@"booking_price"];
            NSString *strCurrency = [[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"booking_currency_symbol"];
            NSString *stringBold = [NSString stringWithFormat:@"%@ %@",strCurrency,strPrice];
            cell.labelPrice.text =  stringBold; //[self AttributedTextForOwner:strAmountForOwner andRangeString:stringBold];
            
        
            cell.buttonCall.hidden = false;
            cell.buttonChat.hidden = false;
            cell.buttonCall.tag = indexPath.row;
            cell.buttonChat.tag = indexPath.row;
            [cell.buttonCall addTarget:self action:@selector(ButtonCallPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.buttonChat addTarget:self action:@selector(ButtonChatPressed:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        if (buttonHistory.selected) {
            cell.viewBase.layer.shadowColor = [UIColor grayColor].CGColor;
            cell.viewBase.backgroundColor = [UIColor whiteColor];
            cell.buttonPending.tag = indexPath.row;
            [cell.buttonPending setTitle:@"See Review" forState:UIControlStateNormal];
            [cell.buttonPending addTarget:self action:@selector(ButtonPendinfClicked:) forControlEvents:UIControlEventTouchUpInside];
            cell.buttonPending.hidden = false;
            [cell.buttonPending setTitleColor:[UIColor colorWithRed:40.0/255.0 green:186.0/255.0 blue:240.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.buttonPending.userInteractionEnabled = true;
            NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[[[dictBookings objectForKey:@"history"] objectAtIndex:indexPath.row] objectForKey:@"user"] valueForKey:@"image"]];
            NSURL *url = [NSURL URLWithString:strUrl];
            [cell.imageViewUser sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            cell.labelYachtName.text = [[[[dictBookings valueForKey:@"history"] objectAtIndex:indexPath.row] valueForKey:@"yacht"] valueForKey:@"name"];
            cell.labelUseName.text = [[[[dictBookings valueForKey:@"history"]objectAtIndex:indexPath.row]valueForKey:@"user"] valueForKey:@"name"];
           
            // Booking date Time and duration
            NSString *strStartDate = [[[dictBookings valueForKey:@"history"]objectAtIndex:indexPath.row]valueForKey:@"start_date"];
            NSString *strStartTime = [[[dictBookings valueForKey:@"history"]objectAtIndex:indexPath.row]valueForKey:@"start_time"];
            NSString *strTimeDuration = [[[dictBookings valueForKey:@"history"]objectAtIndex:indexPath.row]valueForKey:@"term"];
            int hours = [[[[dictBookings valueForKey:@"history"]objectAtIndex:indexPath.row]valueForKey:@"hours"] intValue];
            strStartTime = [self FormattedTime:strStartTime];
            strStartDate = [self FormattedDate:strStartDate];
            
            cell.labelDate.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Booking Date: %@",strStartDate] andRangeString:@"Booking Date:"];
            cell.labelDuration.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Booking Time: %@",strStartTime] andRangeString:@"Booking Time:"];;
            if ([strTimeDuration isEqualToString:@"per_day"] || [strTimeDuration isEqualToString:@"half_day"]){
                cell.labelTimeDuration.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Duration: %@ (%d hours)",[self timeDurationforBookings:strTimeDuration],hours] andRangeString:@"Duration:"];
            }
            else{
                cell.labelTimeDuration.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Duration: %@",[self timeDurationforBookings:strTimeDuration]] andRangeString:@"Duration:"];
            }
            
            // Booking Price with formatted price
            cell.TextForAmount.text = @"Amount";
            NSString *strPrice = [[[dictBookings valueForKey:@"history"] objectAtIndex:indexPath.row]valueForKey:@"booking_price"];
            NSString *strCurrency = [[[dictBookings valueForKey:@"history"]objectAtIndex:indexPath.row]valueForKey:@"booking_currency_symbol"];
          //  NSString *strAmountForOwner = [NSString stringWithFormat:@"You will get \n%@ %@ \nfor this booking.",strCurrency, strPrice];
            NSString *stringBold = [NSString stringWithFormat:@"%@ %@",strCurrency,strPrice];
            cell.labelPrice.text = stringBold;//[self AttributedTextForOwner:strAmountForOwner andRangeString:stringBold];
           
        }
        if (buttonCancelled.selected) {
            cell.viewBase.layer.shadowColor = [UIColor grayColor].CGColor;
            cell.viewBase.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0];
            NSString *strStatus = [[[[dictBookings valueForKey:@"cancelled"]objectAtIndex:indexPath.row] valueForKey:@"status"] capitalizedString];
            [cell.buttonPending setTitle:strStatus forState:UIControlStateNormal];
            cell.buttonPending.hidden = false;
            [cell.buttonPending setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[[[dictBookings objectForKey:@"cancelled"] objectAtIndex:indexPath.row] objectForKey:@"user"] valueForKey:@"image"]];
            NSURL *url = [NSURL URLWithString:strUrl];
            [cell.imageViewUser sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            cell.labelYachtName.text = [[[[dictBookings valueForKey:@"cancelled"] objectAtIndex:indexPath.row] valueForKey:@"yacht"] valueForKey:@"name"];
            cell.labelUseName.text = [[[[dictBookings valueForKey:@"cancelled"]objectAtIndex:indexPath.row]valueForKey:@"user"] valueForKey:@"name"];
          
            // Booking date Time and duration
            NSString *strStartDate = [[[dictBookings valueForKey:@"cancelled"]objectAtIndex:indexPath.row]valueForKey:@"start_date"];
            NSString *strStartTime = [[[dictBookings valueForKey:@"cancelled"]objectAtIndex:indexPath.row]valueForKey:@"start_time"];
            NSString *strTimeDuration = [[[dictBookings valueForKey:@"cancelled"]objectAtIndex:indexPath.row]valueForKey:@"term"];
            int hours = [[[[dictBookings valueForKey:@"cancelled"]objectAtIndex:indexPath.row]valueForKey:@"hours"] intValue];
           
            strStartTime = [self FormattedTime:strStartTime];
            strStartDate = [self FormattedDate:strStartDate];
            cell.labelDate.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Booking Date: %@",strStartDate] andRangeString:@"Booking Date:"];
            cell.labelDuration.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Booking Time: %@",strStartTime] andRangeString:@"Booking Time:"];;
            if ([strTimeDuration isEqualToString:@"per_day"] || [strTimeDuration isEqualToString:@"half_day"]){
                cell.labelTimeDuration.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Duration: %@ (%d hours)",[self timeDurationforBookings:strTimeDuration],hours] andRangeString:@"Duration:"];
            }
            else{
                cell.labelTimeDuration.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Duration: %@",[self timeDurationforBookings:strTimeDuration]] andRangeString:@"Duration:"];
            }

            
            // Booking Price with formatted price
            cell.TextForAmount.text = @"Cancellation Charge";
            NSString *strPrice = [[[dictBookings valueForKey:@"cancelled"] objectAtIndex:indexPath.row]valueForKey:@"booking_price"];
            NSString *strCurrency = [[[dictBookings valueForKey:@"cancelled"]objectAtIndex:indexPath.row]valueForKey:@"booking_currency_symbol"];
            NSString *stringBold = [NSString stringWithFormat:@"%@ %@",strCurrency,strPrice];
            cell.labelPrice.text = stringBold; //[self AttributedTextForOwner:strAmountForOwner andRangeString:stringBold];
        }
        return cell;
    }
    else{
        if (buttonActive.selected && [[[[dictBookings valueForKey:@"active"] objectAtIndex:indexPath.row] valueForKey:@"is_ongoing"] boolValue]) {
            static NSString *cellIdentifier = @"ActiveBookingTableViewCell";
            ActiveBookingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActiveBookingTableViewCell"];
            
            if (cell == nil) {
                NSArray *nib =[[NSBundle mainBundle]loadNibNamed:cellIdentifier owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            cell.labelYachtName.text = [[[[dictBookings valueForKey:@"active"] objectAtIndex:indexPath.row] valueForKey:@"yacht"] valueForKey:@"name"];
            NSString *strPrice = [[CommonMethods sharedInstance] priceFormatting:[[[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"yacht"]valueForKey:@"price"]];
            NSString *strCurrency = [[[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"yacht"]valueForKey:@"currency_symbol"];
            
            NSString *timeDuration = [[[dictBookings valueForKey:@"active"] objectAtIndex:indexPath.row] valueForKey:@"term"];
            timeDuration = [self timeDurationforBookings:timeDuration];
            cell.labelYachtPrice.attributedText = [self PlainStringToAttributedStringForPrice:strPrice andCurrency:strCurrency andNights:timeDuration normalString:@"You Paid"];
          
            NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[[[dictBookings objectForKey:@"active"] objectAtIndex:indexPath.row] objectForKey:@"yacht"] valueForKey:@"image"]];
            NSURL *url = [NSURL URLWithString:strUrl];
            [cell.imageViewRound sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            
            cell.labelYachtAddress.text = [[[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"yacht"]valueForKey:@"address"];
            cell.buttonMessageOwner.tag = indexPath.row;
            [cell.buttonMessageOwner addTarget:self action:@selector(ChatToOwner:) forControlEvents:UIControlEventTouchUpInside];
            cell.buttonReview.tag = indexPath.row;
            [cell.buttonReview addTarget:self action:@selector(ButtonBookNowPressed:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
        else{
            static NSString *cellIdentifier = @"UserHistoryTableViewCell";
            UserHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserHistoryTableViewCell"];
            
            if (cell == nil) {
                NSArray *nib =[[NSBundle mainBundle]loadNibNamed:cellIdentifier owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            cell.buttonBookNow.hidden = true;
            cell.buttonBookAgain.hidden = true;
            cell.labelCommentForYacht.hidden = true;
            if (buttonActive.selected) {
                cell.viewBackGround.backgroundColor = [UIColor whiteColor];
                NSString *strStartDate = [[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row] valueForKey:@"start_date"];
                NSMutableDictionary *dict = [self GetFormattedDatefromString:strStartDate];
                NSMutableAttributedString *str = [self plainStringToAttributedUnits:[dict valueForKey:@"day"]];
                cell.labelDate.attributedText = str;
                cell.labelMonth.text = [NSString stringWithFormat:@"%@ %@",[dict valueForKey:@"month"],[dict valueForKey:@"year"]];
                
                cell.labelYachtName.text = [[[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"yacht"] valueForKey:@"name"];
                cell.labelYachAddress.text = [[[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"yacht"]valueForKey:@"address"];
                
                NSString *strTime = [[[dictBookings valueForKey:@"active"] objectAtIndex:indexPath.row] valueForKey:@"start_time"];
                strTime = [self FormattedTime:strTime];
                cell.labelTime.hidden = false;

                cell.labelTime.text = strTime;
                /// attributed text for yacht price
                
                NSString *strPrice = [[CommonMethods sharedInstance] priceFormatting:[[[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"yacht"]valueForKey:@"price"]];
                NSString *strCurrency = [[[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"yacht"]valueForKey:@"currency_symbol"];
                NSString *timeDuration = [[[dictBookings valueForKey:@"active"] objectAtIndex:indexPath.row] valueForKey:@"term"];
                timeDuration = [self timeDurationforBookings:timeDuration];
                cell.labelYachtPrice.attributedText = [self PlainStringToAttributedStringForPrice:strPrice andCurrency:strCurrency andNights:timeDuration normalString:@"You Paid"];
                
                NSString *strStatus = [[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"status"];
                if ([strStatus isEqualToString:@"approved"]) {
                    cell.buttonBookNow.hidden = false;
                    [cell.buttonBookNow setTitle:@"Cancel" forState:UIControlStateNormal];
                    [cell.buttonBookNow setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                    cell.labelCommentForYacht.hidden = false;
                    cell.buttonBookNow.userInteractionEnabled = true;
                    cell.buttonBookNow.tag = indexPath.row;
                    [cell.buttonBookNow addTarget:self action:@selector(ButtonBookNowPressed:) forControlEvents:UIControlEventTouchUpInside];
                    cell.labelCommentForYacht.text = [self StringforYachtCancelForDate:strStartDate];
                }
                else if ([strStatus isEqualToString:@"declined"]){
                    cell.buttonBookNow.hidden = false;
                    [cell.buttonBookNow setTitle:@"Declined" forState:UIControlStateNormal];
                    [cell.buttonBookNow setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                    cell.labelCommentForYacht.hidden = true;
                    cell.buttonBookNow.userInteractionEnabled = false;
                    
                }
                else if ([strStatus isEqualToString:@"pending"]){
                    cell.buttonBookNow.hidden = false;
                    [cell.buttonBookNow setTitle:@"Pending" forState:UIControlStateNormal];
                    [cell.buttonBookNow setTitleColor:[UIColor colorWithRed:42.0/255.0 green:180.0/255.0 blue:238.0/255.0 alpha:1.0] forState:UIControlStateNormal];
                    cell.labelCommentForYacht.hidden = true;
                    cell.buttonBookNow.userInteractionEnabled = false;
                }
                else if ([strStatus isEqualToString:@"cancelled"]){
                    cell.buttonBookNow.hidden = false;
                    [cell.buttonBookNow setTitle:@"Cancelled" forState:UIControlStateNormal];
                    [cell.buttonBookNow setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                    cell.labelCommentForYacht.hidden = true;
                    cell.buttonBookNow.userInteractionEnabled = false;
                }
                NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[[[dictBookings objectForKey:@"active"] objectAtIndex:indexPath.row] objectForKey:@"yacht"] valueForKey:@"image"]];
                NSURL *url = [NSURL URLWithString:strUrl];
                [cell.imageViewRound sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            }
            else if (buttonCancelled.selected){
                cell.viewBackGround.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0];
                NSString *strStartDate = [[[dictBookings valueForKey:@"cancelled"]objectAtIndex:indexPath.row] valueForKey:@"start_date"];
                NSMutableDictionary *dict = [self GetFormattedDatefromString:strStartDate];
                NSMutableAttributedString *str = [self plainStringToAttributedUnits:[dict valueForKey:@"day"]];
                cell.labelDate.attributedText = str;
                cell.labelMonth.text = [NSString stringWithFormat:@"%@ %@",[dict valueForKey:@"month"],[dict valueForKey:@"year"]];
                
                cell.labelYachtName.text = [[[[dictBookings valueForKey:@"cancelled"]objectAtIndex:indexPath.row]valueForKey:@"yacht"] valueForKey:@"name"];
                cell.labelYachAddress.text = [[[[dictBookings valueForKey:@"cancelled"]objectAtIndex:indexPath.row]valueForKey:@"yacht"]valueForKey:@"address"];
                /// attributed text for yacht price
                
                NSString *strPrice = [[CommonMethods sharedInstance] priceFormatting:[[[dictBookings valueForKey:@"cancelled"]objectAtIndex:indexPath.row]valueForKey:@"booking_price"]];
                NSString *strCurrency = [[[dictBookings valueForKey:@"cancelled"]objectAtIndex:indexPath.row]valueForKey:@"booking_currency_symbol"];
                NSString *numberOfNight = [[[dictBookings valueForKey:@"cancelled"] objectAtIndex:indexPath.row] valueForKey:@"term"];
                numberOfNight = [self timeDurationforBookings:numberOfNight];
                cell.labelYachtPrice.attributedText = [self PlainStringToAttributedStringForPrice:strPrice andCurrency:strCurrency andNights:@"Cancellation" normalString:@"You get"];
                [cell.buttonBookNow setTitle:@"Book Now" forState:UIControlStateNormal];
                cell.buttonBookNow.hidden = false;
                [cell.buttonBookNow setTitleColor:[UIColor colorWithRed:42.0/255.0 green:180.0/255.0 blue:238.0/255.0 alpha:1.0] forState:UIControlStateNormal];
                cell.buttonBookNow.tag = indexPath.row;
                [cell.buttonBookNow addTarget:self action:@selector(ButtonBookNowPressed:) forControlEvents:UIControlEventTouchUpInside];
                cell.buttonBookNow.userInteractionEnabled = true;
                NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[[[dictBookings objectForKey:@"cancelled"] objectAtIndex:indexPath.row] objectForKey:@"yacht"] valueForKey:@"image"]];
                NSURL *url = [NSURL URLWithString:strUrl];
                [cell.imageViewRound sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
                cell.labelTime.hidden = true;
            }
            else if (buttonHistory.selected){
                cell.viewBackGround.backgroundColor = [UIColor whiteColor];
                NSString *strStartDate = [[[dictBookings valueForKey:@"history"]objectAtIndex:indexPath.row] valueForKey:@"start_date"];
                NSMutableDictionary *dict = [self GetFormattedDatefromString:strStartDate];
                NSMutableAttributedString *str = [self plainStringToAttributedUnits:[dict valueForKey:@"day"]];
                cell.labelDate.attributedText = str;
                cell.labelMonth.text = [NSString stringWithFormat:@"%@ %@",[dict valueForKey:@"month"],[dict valueForKey:@"year"]];
                
                cell.labelYachtName.text = [[[[dictBookings valueForKey:@"history"]objectAtIndex:indexPath.row]valueForKey:@"yacht"] valueForKey:@"name"];
                cell.labelYachAddress.text = [[[[dictBookings valueForKey:@"history"]objectAtIndex:indexPath.row]valueForKey:@"yacht"]valueForKey:@"address"];
                NSString *strTime = [[[dictBookings valueForKey:@"history"] objectAtIndex:indexPath.row] valueForKey:@"start_time"];
                strTime = [self FormattedTime:strTime];
                cell.labelTime.hidden = false;
                cell.labelTime.text = strTime;
                /// attributed text for yacht price
                
                NSString *strPrice = [[CommonMethods sharedInstance] priceFormatting:[[[dictBookings valueForKey:@"history"]objectAtIndex:indexPath.row]valueForKey:@"booking_price"]];
                NSString *strCurrency = [[[dictBookings valueForKey:@"history"]objectAtIndex:indexPath.row]valueForKey:@"booking_currency_symbol"];
                NSString *numberOfNight = [[[dictBookings valueForKey:@"history"] objectAtIndex:indexPath.row] valueForKey:@"term"];
                numberOfNight = [self timeDurationforBookings:numberOfNight];
                
                cell.labelYachtPrice.attributedText = [self PlainStringToAttributedStringForPrice:strPrice andCurrency:strCurrency andNights:numberOfNight normalString:@"You Paid"];
                cell.buttonBookNow.hidden  = false;
                [cell.buttonBookNow setTitle:@"Review" forState:UIControlStateNormal];
                [cell.buttonBookNow setTitleColor:[UIColor colorWithRed:46.0/255.0 green:180.0/255.0 blue:250.0/255.0 alpha:1.0] forState:UIControlStateNormal];
                [cell.buttonBookAgain setTitleColor:[UIColor colorWithRed:46.0/255.0 green:180.0/255.0 blue:250.0/255.0 alpha:1.0] forState:UIControlStateNormal];
                cell.buttonBookAgain.hidden = false;
                [cell.buttonBookAgain setTitle:@"Book Again" forState:UIControlStateNormal];
                [cell.buttonBookNow addTarget:self action:@selector(ButtonBookNowPressed:) forControlEvents:UIControlEventTouchUpInside];
                [cell.buttonBookAgain addTarget:self action:@selector(ButtonBookAgainPressed:) forControlEvents:UIControlEventTouchUpInside];
                cell.buttonBookNow.userInteractionEnabled = true;
                cell.buttonBookNow.tag = indexPath.row;
                cell.buttonBookAgain.tag = indexPath.row;
                NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[[[dictBookings objectForKey:@"history"] objectAtIndex:indexPath.row] objectForKey:@"yacht"] valueForKey:@"image"]];
                NSURL *url = [NSURL URLWithString:strUrl];
                [cell.imageViewRound sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
                
            }
            return cell;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([strRole isEqualToString:Owner]){
        return  240;
    }
    else if ([strRole isEqualToString:Customer]){
        if (buttonActive.selected && [[[[dictBookings valueForKey:@"active"] objectAtIndex:indexPath.row] valueForKey:@"is_ongoing"] boolValue]){
        return 260;
    }
    else{
        return 230;
    }
    }
        else{
        return 240;
        }
}

-(NSAttributedString *)AttributedTextForOwner:(NSString *)stringToShow andRangeString:(NSString *)boldString{
    
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor lightGrayColor],
                              NSFontAttributeName: FontOpenSans(12)
                              };
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:stringToShow attributes:attribs];
    
    
    UIFont *boldFont = FontOpenSansBold(12);
    
    NSRange range = [stringToShow rangeOfString:boldString];
    [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                    NSFontAttributeName:boldFont} range:range];
    return  attributedText;
}

-(NSString *)FormattedDate:(NSString *)startDate{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *strDate = [formatter dateFromString:startDate];
    [formatter setDateFormat:@"MMM dd yyyy"];
    startDate = [formatter stringFromDate:strDate];
    return startDate;
}

-(NSString *)FormattedTime:(NSString *)startTime{
   
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setTimeZone:sourceTimeZone];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *strTime = [formatter dateFromString:startTime];
    [formatter setDateFormat:@"hh:mm a"];
    startTime = [formatter stringFromDate:strTime];
    return startTime;
}

-(NSString *)timeDurationforBookings:(NSString *)strDuration{
    NSString *str;
    if ([strRole isEqualToString:Owner]){
    if ([strDuration isEqualToString:@"per_day"]){
        str = @"Full Day";
    }
    else if ([strDuration isEqualToString:@"half_day"]){
        str = @"Half Day";
    }
    else if ([strDuration isEqualToString:@"per_hour"]){
        str = @"Hour";
    }
    else if([strDuration isEqualToString:@"week"]) {
        str = @"Week";
    }
    else{
        str = @"";
    }
         return str;
    }
    else{
        if ([strDuration isEqualToString:@"per_day"]){
            str = @"Full Day";
        }
        else if ([strDuration isEqualToString:@"half_day"]){
            str = @"Half Day";
        }
        else if ([strDuration isEqualToString:@"per_hour"]){
            str = @"an Hour";
        }
        else if([strDuration isEqualToString:@"week"]) {
            str = @"a Week";
        }
        else{
            str = @"";
        }
         return str;
    }
   
}
#pragma mark - String For Label Comment
-(NSString *)StringforYachtCancelForDate:(NSString *)strDate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *startDate = [formatter dateFromString:strDate];
    NSDate *sevenDaysAgo = [startDate dateByAddingTimeInterval:-7*24*60*60];
    NSLog(@"7 days ago: %@", sevenDaysAgo);
    NSString *str;
    if ([sevenDaysAgo compare:[NSDate date]] == NSOrderedDescending) {
        NSLog(@"firstDate is later than secondDate");
        // NSTimeInterval timeDifference = [firstDate timeIntervalSinceDate:secondDate];
        NSString *strDateSevenDaysAgo = [formatter stringFromDate:sevenDaysAgo];
        NSDictionary *dict = [self GetFormattedDatefromString:strDateSevenDaysAgo];
        NSInteger intDate = [[dict valueForKey:@"day"] integerValue];
        NSString *strSubscriptDate = [NSString stringWithFormat:@"%ld%@",(long)intDate,[self SubscriptToDate:intDate]];
        str = [NSString stringWithFormat:@"Cancel before %@ %@", strSubscriptDate,[dict valueForKey:@"month"]];
    } else if ([sevenDaysAgo compare:[NSDate date]] == NSOrderedAscending) {
        NSLog(@"firstDate is earlier than secondDate");
        str = @"";
        //NSTimeInterval timeDifference = [secondDate timeIntervalSinceDate:firstDate];
    } else {
        NSLog(@"firstDate and secondDate are the same");
        str = @"";
    }
    return str;
}

#pragma mark - Api for Bookings

-(void)ApiForActiveBookings{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSString *strUrl = [NSString stringWithFormat:@"%@bookings/active",kBaseUrlVersion2];
        if (location.coordinate.latitude && location.coordinate.longitude) {
            strUrl = [NSString stringWithFormat:@"%@?latitude=%f&longitude=%f",strUrl,location.coordinate.latitude,location.coordinate.longitude];
        }
        [WebServiceManager getRequestUrlString:strUrl emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            dictBookings = [[NSMutableDictionary alloc]init];
            [dictBookings setObject:[[result valueForKey:@"bookings"] mutableCopy] forKey:@"active"];
            [self ApiForHistory];
        } failure:^(NSString *msg, BOOL success) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [self ApiForHistory];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
    
}

#pragma mark - Request & Response for History
-(void)ApiForHistory{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSString *strUrl = [NSString stringWithFormat:@"%@bookings/history",kBaseUrlVersion2];
        if (location.coordinate.latitude && location.coordinate.longitude) {
            strUrl = [NSString stringWithFormat:@"%@?latitude=%f&longitude=%f",strUrl,location.coordinate.latitude,location.coordinate.longitude];
        }
        [WebServiceManager getRequestUrlString:strUrl emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            [dictBookings setObject:[result valueForKey:@"bookings"] forKey:@"history"];
            [self ApiForCancelledBookings];
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [self ApiForCancelledBookings];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
    
}

#pragma mark - Request & Response for Cancelled Yacht
-(void)ApiForCancelledBookings{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSString *strUrl = [NSString stringWithFormat:@"%@bookings/cancelled",kBaseUrlVersion2];
        //    if ([strRole isEqualToString:Customer]) {
        if (location.coordinate.latitude && location.coordinate.longitude) {
            strUrl = [NSString stringWithFormat:@"%@?latitude=%f&longitude=%f",strUrl,location.coordinate.latitude,location.coordinate.longitude];
        }
        //    }
        
        [WebServiceManager getRequestUrlString:strUrl emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            [dictBookings setObject:[result valueForKey:@"bookings"] forKey:@"cancelled"];
            isApi = false;
            [_tableViewBookings reloadData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [[LoaderNew sharedLoader]hideLoader];
                [[LoaderNew sharedLoader]hideLoader];
            });
            
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
            
        }];
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
    
}

#pragma mark - Request and Response for New Bookings
-(void)ApiForNewBookings{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@get_new_bookings",kBaseUrl] emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            arrNewBookings = [[NSMutableArray alloc]init];
            arrNewBookings = [result valueForKey:@"bookings"];
            if ([arrNewBookings count] >0) {
                buttonNewBookings.hidden = false;
                [buttonNewBookings setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[arrNewBookings count]] forState:UIControlStateNormal];
                [buttonNewBookings setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                buttonNewBookings.titleLabel.font = FontOpenSansLight(11);
            }
            //arrNewBookings
            
        } failure:^(NSString *msg, BOOL success) {
            
        }];
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
    
}

#pragma park - Request and Response for Cancellation of Yacht
-(void)ApiForCancelBooking:(NSDictionary *)dict andForIndexPath:(NSInteger)indexpath{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSString *strId = [[dict valueForKey:@"id"] stringValue];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:strId forKey:@"id"];
        [dict setObject:@"cancelled" forKey:@"status"];
        NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
        [dictResponse setObject:dict forKey:@"bookings"];
        [WebServiceManager putRequestWithUrlString:[NSString stringWithFormat:@"%@bookings",kBaseUrl] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:[result valueForKey:@"message"] andViewController:self];
                NSIndexPath *indexpathforRow = [NSIndexPath indexPathForRow:indexpath inSection:0];
                UserHistoryTableViewCell *cell = (UserHistoryTableViewCell*)[self.tableViewBookings cellForRowAtIndexPath:indexpathforRow];
                // [cell.buttonBookNow setTitle:@"Approved" forState:UIControlStateNormal];
                // [cell.buttonBookAgain setTitle:@"Cancel" forState:UIControlStateNormal];
                cell.buttonBookNow.hidden = false;
                cell.buttonBookAgain.hidden = true;
                [cell.buttonBookNow setTitle:@"Cancelled" forState:UIControlStateNormal];
                [cell.buttonBookNow setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
                cell.labelCommentForYacht.hidden = true;
                cell.buttonBookNow.userInteractionEnabled = false;
                [[[dictBookings valueForKey:@"active"] objectAtIndex:indexpath] setObject:@"declined" forKey:@"status"];
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            //            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            //            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
    
}

#pragma mark - Button Chat Action for owner end/ Request for Chat to customer
-(void)ButtonChatPressed:(id)sender{
    NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
    NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
    NSString *strOwnerId = [[[[dictBookings valueForKey:@"active"] objectAtIndex:[sender tag]] valueForKey:@"user"] valueForKey:@"id"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:strOwnerId forKey:@"recipient_id"];
    NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
    [dictResponse setObject:dict forKey:@"conversations"];
    [WebServiceManager postRequestWithUrlString:[NSString stringWithFormat:@"%@conversations",kBaseUrl] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
        NSLog(@"%@",result);
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.socket joinChannel:@"ConversationChannel"];
        ChatViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
        controller.arrCurrentUser = [[[dictBookings valueForKey:@"active" ]objectAtIndex:[sender tag]] valueForKey:@"current_user"];
        controller.arrConversations = [[result valueForKey:@"conversation"] mutableCopy];
        [self.navigationController pushViewController:controller animated:true];
        
    } failure:^(NSString *msg, BOOL success) {
        [[CommonMethods sharedInstance]AlertMessage:@"Unable to connect to chat. Please try again." andViewController:self];
    }];
}

#pragma mark - Button Action for User End/ Request & Response for chatting to owner
-(void)ChatToOwner:(id)sender{
    NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
    NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
    NSString *strOwnerId = [[[[[dictBookings valueForKey:@"active"] objectAtIndex:[sender tag]] valueForKey:@"yacht"] valueForKey:@"user"] valueForKey:@"id"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:strOwnerId forKey:@"recipient_id"];
    NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
    [dictResponse setObject:dict forKey:@"conversations"];
    [WebServiceManager postRequestWithUrlString:[NSString stringWithFormat:@"%@conversations",kBaseUrl] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
        NSLog(@"%@",result);
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.socket joinChannel:@"ConversationChannel"];
        ChatViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
        controller.arrCurrentUser = [[[dictBookings valueForKey:@"active" ]objectAtIndex:[sender tag]] valueForKey:@"current_user"];
        controller.arrConversations = [[result valueForKey:@"conversation"] mutableCopy];
        [self.navigationController pushViewController:controller animated:true];
    } failure:^(NSString *msg, BOOL success) {
        [[CommonMethods sharedInstance]AlertMessage:@"Unable to connect to chat. Please try again." andViewController:self];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
