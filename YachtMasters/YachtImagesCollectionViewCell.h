//
//  YachtImagesCollectionViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 24/07/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YachtImagesCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageYacht;

@end
