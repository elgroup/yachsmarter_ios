//
//  AddStripeAccountViewController.m
//  YachtMasters
//
//  Created by Anvesh on 12/10/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "AddStripeAccountViewController.h"
#import "Constants.h"
#import "CommonMethods.h"
#import "MBProgressHUD.h"
#import "YachtMasters-Swift.h"
#import <WebKit/WebKit.h>

@interface AddStripeAccountViewController ()<WKUIDelegate,WKNavigationDelegate>{
    IBOutlet UIWebView *webView;
    __weak IBOutlet WKWebView *webKitView;
    MBProgressHUD *hud;
}
@end

@implementation AddStripeAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
  [self CustomNavigationBar];
   
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
    //ca_BybzPUyhAsDzkhsBilMSwdfVtPUF4APG
    NSString *str =  [NSString stringWithFormat:@"https://connect.stripe.com/oauth/authorize?response_type=code&client_id=%s&scope=read_write&state=%@","ca_BybzyGM0i2CTa78CMnIQzCWDbbEkq3KZ",strAuthToken];
    NSURL *url = [NSURL URLWithString:str];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [webKitView loadRequest:urlRequest];
    webKitView.navigationDelegate = self;
    [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];

}

// Delegate methods

-(void) webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[LoaderNew sharedLoader]hideLoader];
    });
}
-(void) webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[LoaderNew sharedLoader]hideLoader];
    });
}

-(void)CustomNavigationBar{
    self.tabBarController.tabBar.hidden =  true;
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    //[backButton setBackgroundColor:[UIColor redColor]];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 5.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    if (__isEdit) {
        UIButton *buttonChange = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonChange setTitle:@"Change" forState:UIControlStateNormal];
        buttonChange.titleLabel.font = FontOpenSansBold(12);
        buttonChange.frame = CGRectMake(SCREEN_WIDTH - 80, 10, 60, 30);
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:buttonChange];
        self.navigationItem.rightBarButtonItem = rightBarButtonItem;
        
    }
    self.title = @"Add Account";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}

-(void)BackButtonPressed{
    self.navigationController.navigationBar.hidden = true;
    self.tabBarController.tabBar.hidden = false;
    [self.navigationController popViewControllerAnimated:true];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
