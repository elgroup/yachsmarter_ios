//
//  ImageProfileTableViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 21/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageProfileTableViewCell : UITableViewCell
@property(nonatomic,weak)IBOutlet UIImageView *imageViewProfile;
@property(nonatomic,weak)IBOutlet UILabel *labelName;
@property(nonatomic,weak)IBOutlet UILabel *labelAddress;
@end
