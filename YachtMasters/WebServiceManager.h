//
//  WebServiceManager.h
//  WebServiceManager
//
//  Created by Shiv Kumar on 10/11/16.
//  Copyright © 2016 Shiv Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void (^Success) (NSDictionary *result, BOOL success);
typedef void (^Failure) (NSString *msg, BOOL success);


@interface WebServiceManager : NSObject
{
}

@property (nonatomic, strong) NSString *headerEmail;
@property (nonatomic, strong) NSString *accessTokens;

//+(BOOL)networkStatus;
//:: ClassType Method :://
+(void)postRequestWithUrlString:(NSString *)urlStr withPostString:(NSMutableDictionary *)postDict emailString:(NSString *)email accessToken:(NSString *)aToken completionSuccess:(Success)completionBlock failure:(Failure)failure;


+(void)getRequestUrlString:(NSString *)urlString emailString:(NSString *)email accessToken:(NSString *)aToken completionSuccess:(Success)completionBlock failure:(Failure)failure;


+(void)putRequestWithUrlString:(NSString *)urlStr withPostString:(NSMutableDictionary *)postDict emailString:(NSString *)email accessToken:(NSString *)aToken completionSuccess:(Success)completionBlock failure:(Failure)failure;


+(void)deleteRequestWithUrlString:(NSString *)urlStr emailString:(NSString *)email accessToken:(NSString *)aToken completionSuccess:(Success)completionBlock failure:(Failure)failure;

//:: For Messaging
+(void)recievedMSGRequestWithUrlString:(NSString *)urlStr mwithPostString:(NSString *)postStr completionSuccess:(Success)completion Blockfailure:(Failure)failure;
+(void)PostRequest:(NSString *)urlString andParam:(NSMutableDictionary *)dict andcompletionhandler:(void(^)(NSArray *returnArray, NSError *error)) completionBlock;
+(void)PutRequest:(NSString *)urlString andParam:(NSMutableDictionary *)dict andcompletionhandler:(void(^)(NSArray *returnArray, NSError *error)) completionBlock;



@end
