//
//  CommonMethods.h
//  YachtMasters
//
//  Created by Anvesh on 07/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface CommonMethods : NSObject
+(CommonMethods *) sharedInstance;
-(void)AlertMessage:(NSString *)strMessage andViewController:(UIViewController *)controller;
-(BOOL)checkInternetConnection;
-(UIImage*)ScaleImageWithImage:(UIImage*)image maxDimension:(CGFloat)maxDimension;
-(void)NavigationBarShadow :(UINavigationController *)navigationController;
-(void)setBorderOfButton:(UIButton *)btn borderColor:(UIColor *)borderColor borderWidth:(CGFloat)width cornerRadius:(CGFloat)cornerRadius;
-(void)setButtonShadow:(UIButton *)btn andShadowcolor:(UIColor *)color;
-(UIView *)CornerRadius:(UIView *)View Radius:(int)radius BorderWidth:(CGFloat )border andColorForBorder:(UIColor *)Color;
-(void)DropShadow:(UIView *)view UIColor:(UIColor *)color andShadowRadius:(float)radius;
- (CGSize)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font;
-(NSTimeZone *)TimeZoneWithRespectToLatitiue:(double)lat andLongitude:(double)lang;
-(void)RatingAndStar1:(UIImageView *)image1 Star2:(UIImageView *)image2 Star3:(UIImageView *)image3 Star4:(UIImageView *)image4 Star5:(UIImageView *)image5 andRating:(NSInteger )rating;
- (UIImage *)useColor:(UIColor *)color forImage:(UIImage *)image;
-(NSString *)priceFormatting:(NSString *)strPrice;
-(BOOL)validateEmailForDots:(NSString *)strEmail;
-(BOOL)validatePhoneNumber:(NSString *)phoneNumber;
@end
