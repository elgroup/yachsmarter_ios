//
//  CreditCard-Validator.m
//  CreditCard-Validator-ObjC
//
//  Created by Fernando Bass on 1/8/13.
//  Copyright (c) 2013 Fernando Bass. All rights reserved.
//

#pragma mark - Regex for CreditCard

//Finding or Verification Credit Card Numbers
//http://www.regular-expressions.info/creditcard.html

#define kVISA_TYPE          @"^4[0-9]{3}?"
#define kMASTER_CARD_TYPE   @"^5[1-5][0-9]{2}$"
#define kAMEX_TYPE          @"^3[47][0-9]{2}$"
#define kDINERS_CLUB_TYPE	@"^3(?:0[0-5]|[68][0-9])[0-9]$"
#define kDISCOVER_TYPE		@"^6(?:011|5[0-9]{2})$"

#import "CreditCard-Validator.h"

@implementation CreditCard_Validator

+ (CreditCardType)getCreditCardTypeFromString:(NSString *) string {
    CreditCardType type = CreditCardTypeUnknown;
    if (string == nil || string.length < 9) {
        type =  CreditCardTypeInvalid;
    }
    else
    {
        
        for(int counter = 0; counter<CreditCardTypeUnknown;counter++) {
            CreditCardType _type = (CreditCardType)counter;
            NSPredicate *predicate = [self predicateForType:_type];
            BOOL isCurrentType = [predicate evaluateWithObject:string];
            if (isCurrentType) {
                type = _type;
                break;
            }
        }
    }
    return type;
}

+(NSPredicate *) predicateForType:(CreditCardType) type {
    
    NSString *regex = nil;
    switch (type) {
        case CreditCardTypeAmex:
            regex = @"^3[47][0-9]{5,}$";
            break;
        case CreditCardTypeDinersClub:
            regex = @"^3(?:0[0-5]|[68][0-9])[0-9]{4,}$";
            break;
        case CreditCardTypeDiscover:
            regex = @"^6(?:011|5[0-9]{2})[0-9]{3,}$";
            break;
        case CreditCardTypeJCB:
            regex = @"^(?:2131|1800|35[0-9]{3})[0-9]{3,}$";
            break;
        case CreditCardTypeMasterCard:
            regex = @"^5[1-5][0-9]{5,}$";
            break;
        case CreditCardTypeVisa:
            regex = @"^4[0-9]{6,}$";
            break;
        case CreditCardTypeMaestroCard:
            regex = @"^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)[0-9]$";
            break;
        default:
            break;
    }
    return [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
}
+(NSString*)getCreditCard:(CreditCardType) cardType{
    
    switch (cardType) {
        case CreditCardTypeAmex:
            return @"AmericanExpress";
            
        case CreditCardTypeDinersClub:
            return @"DinersClub";
            
        case CreditCardTypeDiscover:
            return @"Discover";
            
        case CreditCardTypeMasterCard:
            return @"Master";
            
        case CreditCardTypeMaestroCard:
            return @"Maestro";
            
        case CreditCardTypeVisa:
            
            return @"Visa";
        case CreditCardTypeJCB:
            
            return @"JCB";
        case CreditCardTypeUnknown:
            
            return @"Unknown";
            
        default:
            break;
    }
    return @"Unknown";
}

+ (BOOL)checkCreditCardNumber:(NSString *)cardNumber
{
    NSInteger len = [cardNumber length];
    NSInteger oddDigits = 0;
    NSInteger evenDigits = 0;
    BOOL isOdd = YES;
    
    for (NSInteger i = len - 1; i >= 0; i--) {
        
        NSInteger number = [cardNumber substringWithRange:NSMakeRange(i, 1)].integerValue;
        if (isOdd) {
            oddDigits += number;
        }else{
            number = number * 2;
            if (number > 9) {
                number = number - 9;
            }
            evenDigits += number;
        }
        isOdd = !isOdd;
    }
    
    return ((oddDigits + evenDigits) % 10 == 0);
}

@end
