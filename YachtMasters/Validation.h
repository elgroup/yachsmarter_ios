//
//  Validation.h
//  GonzApp
//
//  Created by Chandresh on 8/2/16.
//  Copyright © 2016 Chandresh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Validation : NSObject
{

}
+(BOOL) validateEmail: (NSString *) candidate;
+(BOOL) textFieldLength: (NSString*) txtField;
+(BOOL) validatePhoneNumber: (NSString*) PhNo;
+(BOOL) validateURL :(NSString*)URLAddress;
+(BOOL) validateTextField:(UITextField *) txtField;
+(BOOL) validateTextView:(UITextView *) textView;
+(BOOL) validatePassword:(UITextField *)txtPassword ConfirmPassword:(UITextField *)txtConfPass;
+(BOOL) validatePasswordLength: (NSString*) passwordLength;
+(BOOL) validatePhoneLength: (NSString*) passwordLength;
@end
