//
//  SignUpTVC.m
//  YachtMasters
//
//  Created by Anvesh on 30/07/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "SignUpTVC.h"
#import "Constants.h"
#import "Validation.h"
#import "CommonMethods.h"
#import "WebServiceManager.h"
#import "LoginViewController.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "SearchYachtViewController.h"
#import "AppDelegate.h"
#import "YachtMasters-Swift.h"
#define MAX_LENGTH 14
#define MIN_LENGTH 8


@interface SignUpTVC ()<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>{
    
    __weak IBOutlet UILabel *label3;
    __weak IBOutlet UIView *View3;
    __weak IBOutlet UIView *View2;
    __weak IBOutlet UILabel *label2;
    __weak IBOutlet UIView *View1;
    __weak IBOutlet UILabel *label1;
    __weak IBOutlet UIButton *buttonRentaYacht;
    __weak IBOutlet UIButton *buttonRentMyYacht;
    __weak IBOutlet UIButton *buttonSignUp;
    __weak IBOutlet UITextField *textFieldFullName;
    __weak IBOutlet UITextField *textFieldEmail;
    __weak IBOutlet UITextField *textFieldPassword;
    __weak IBOutlet UITextField *textFieldConfirmPassword;
    __weak IBOutlet UITextField *textFieldCountry;
    __weak IBOutlet UITextField *textFieldDateOfBirth;
    UIDatePicker *datePickerDateOfBirth;
    UIPickerView *pickerViewCountry;
    UIBarButtonItem *doneButton;
    UIToolbar *toolBar;
    BOOL isRent;
    BOOL isForMakingRent;
    MBProgressHUD *hud;
    int timestamp;
    NSMutableArray *arrCountry;
    BOOL isFacebookLogin;
    UIImage *FacebookProfileImage;
    NSString *strFBID;
    AppDelegate *appDelegate;
    IBOutlet NSLayoutConstraint *heightforRentMyYacht;
    IBOutlet NSLayoutConstraint *lcHeightOfSignUp;
}


@end

@implementation SignUpTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem=nil;
    self.navigationItem.hidesBackButton=YES;
    isFacebookLogin = [[NSUserDefaults standardUserDefaults]boolForKey:@"FacebookLogin"];
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:
                                     [UIImage imageNamed:@"gbpic"]];
    label1.layer.cornerRadius = 13;
    label1.clipsToBounds = true;
    
    View2.layer.cornerRadius = 14;
    View2.clipsToBounds = true;
    label2.layer.cornerRadius = 13;
    label2.clipsToBounds = true;
    
    View3.layer.cornerRadius = 18;
    View3.clipsToBounds = true;
    label3.layer.cornerRadius = 13;
    label3.clipsToBounds = true;
    
    isRent = true;
    
    buttonRentaYacht.layer.cornerRadius = 17.5;
    buttonRentaYacht.clipsToBounds = true;
    buttonRentaYacht.layer.borderColor = [UIColor whiteColor].CGColor;
    buttonRentaYacht.layer.borderWidth = 1.0;
    buttonRentaYacht.selected = true;
    
    buttonSignUp.layer.cornerRadius = 22.5;
    buttonSignUp.clipsToBounds = true;
    
    buttonRentMyYacht.layer.cornerRadius = 17.5;
    buttonRentMyYacht.clipsToBounds = true;
    buttonRentMyYacht.layer.borderColor = [UIColor whiteColor].CGColor;
    buttonRentMyYacht.layer.borderWidth = 1.0;
    
    UIColor *color = [UIColor whiteColor];
    textFieldEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: color}];
    textFieldFullName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Full Name" attributes:@{NSForegroundColorAttributeName: color}];
    textFieldPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    textFieldConfirmPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: color}];
    textFieldCountry.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Country" attributes:@{NSForegroundColorAttributeName: color}];
    textFieldDateOfBirth.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Birthday" attributes:@{NSForegroundColorAttributeName: color}];
    
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.tableView addGestureRecognizer:gestureRecognizer];
    
    doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(hideKeyboard)];
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 50)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton, nil];
    [toolBar setItems:toolbarItems];
    doneButton.tintColor = [UIColor lightGrayColor];
    textFieldDateOfBirth.inputAccessoryView = toolBar;
    
    
    datePickerDateOfBirth = [[UIDatePicker alloc]init];
    [datePickerDateOfBirth addTarget:self action:@selector(date)   forControlEvents:UIControlEventValueChanged];
    [self.tableView.superview addSubview:datePickerDateOfBirth];
    datePickerDateOfBirth.backgroundColor = [UIColor whiteColor];
    datePickerDateOfBirth.datePickerMode = UIDatePickerModeDate;
    textFieldDateOfBirth.inputView = datePickerDateOfBirth;
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:-12];
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    [comps setYear:-80];
    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    
    [datePickerDateOfBirth setMaximumDate:maxDate];
    [datePickerDateOfBirth setMinimumDate:minDate];
    
    buttonRentMyYacht.selected = false;
    buttonRentMyYacht.backgroundColor = [UIColor clearColor];
    buttonRentaYacht.selected = true;
    buttonRentaYacht.titleLabel.font = FontOpenSansBold(14);
    buttonRentMyYacht.titleLabel.font = FontOpenSans(14);
    buttonRentaYacht.backgroundColor = [UIColor whiteColor];
    [buttonSignUp setTitle:@"SIGN UP" forState:UIControlStateNormal];
    isRent = true;
    isForMakingRent = false;
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Country" ofType:@"json"];
    NSData *content = [[NSData alloc] initWithContentsOfFile:filePath];
    NSDictionary *dictCountryCode = [NSJSONSerialization JSONObjectWithData:content options:kNilOptions error:nil];
    NSLog(@"%@",dictCountryCode);
    arrCountry = [[NSMutableArray alloc]init];
    arrCountry = [[dictCountryCode valueForKey:@"array"]valueForKey:@"country_name"];
    NSLog(@"%@",arrCountry);
    
    textFieldCountry.inputAccessoryView = toolBar;
    
    pickerViewCountry = [[UIPickerView alloc]init];
    [self.tableView.superview addSubview:datePickerDateOfBirth];
    pickerViewCountry.backgroundColor = [UIColor whiteColor];
    textFieldCountry.inputView = pickerViewCountry;
    pickerViewCountry.delegate = self;
    
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSDictionary *dict = [app.dictFacebookDetails valueForKey:@"FacebookDetails"]; //[[NSUserDefaults standardUserDefaults]objectForKey:@"FacebookDetails"];
    NSString *strFullName = [dict objectForKey:@"name"];
    NSString *strEmail = [dict objectForKey:@"email"];
    NSString *strDateOfBirth = [dict objectForKey:@"birthday"];
    strFBID = [dict objectForKey:@"id"];
    if (![strEmail isKindOfClass:[NSNull class]]) {
        textFieldEmail.text = strEmail;
    }
    if (![strFullName isKindOfClass:[NSNull class]]) {
        textFieldFullName.text = strFullName;
    }
    if (![strDateOfBirth isKindOfClass:[NSNull class]]) {
        textFieldDateOfBirth.text = strDateOfBirth;
    }
    NSString  *strImageURLString = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"picture"] objectForKey:@"data"]valueForKey:@"url"]];
    NSData  *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:strImageURLString]];
    FacebookProfileImage  = [UIImage imageWithData:data];
    if (IS_IPHONE_6P || IS_IPHONE_XSMAX) {
        heightforRentMyYacht.constant = 40;
        lcHeightOfSignUp.constant = 50.0;
        buttonSignUp.titleLabel.font = FontOpenSansBold(16);
        
    }
    
}

-(void)date
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    // dateFormat.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormat setDateFormat:@"MMMM dd yyyy"];
    NSString *dateString = [dateFormat stringFromDate:[datePickerDateOfBirth date]];
    //  dateDOB = [datePickerDateOfBirth date];
    NSString *strtimeStamp = [NSString stringWithFormat:@"%.0f",[[datePickerDateOfBirth date] timeIntervalSince1970] * 1000];
    timestamp = [strtimeStamp intValue];
    textFieldDateOfBirth.text = dateString;
}
- (void) hideKeyboard {
    [self.view endEditing:true];
    [UIView animateWithDuration:0.25 animations:^{
        toolBar.frame = CGRectMake(0.0, SCREEN_HEIGHT, SCREEN_WIDTH, 50);
        // datePickerDateOfBirth.frame = CGRectMake(0.0, SCREEN_HEIGHT - 50, SCREEN_WIDTH, 150);
    }];
}
- (IBAction)ButtonLoginPressed:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:true];
}

#pragma mark - Button Rent my Yacht Clicked
- (IBAction)ButtonRentMyYachtClicked:(id)sender {
    if (!isForMakingRent && isRent) {
        buttonRentMyYacht.selected = true;
        buttonRentMyYacht.backgroundColor = [UIColor whiteColor];
        buttonRentaYacht.selected = false;
        buttonRentaYacht.backgroundColor = [UIColor clearColor];
        buttonRentMyYacht.titleLabel.font = FontOpenSansBold(14);
        buttonRentaYacht.titleLabel.font = FontOpenSans(14);
        [buttonSignUp setTitle:@"SIGN UP & ADD YACHT" forState:UIControlStateNormal];
        isRent = false;
        isForMakingRent = true;
    }
}

#pragma mark - Button Rent a Yacht Clicked
- (IBAction)ButtonRentAYachtClicked:(id)sender {
    if (isForMakingRent && !isRent) {
        buttonRentMyYacht.selected = false;
        buttonRentMyYacht.backgroundColor = [UIColor clearColor];
        buttonRentaYacht.selected = true;
        buttonRentaYacht.titleLabel.font = FontOpenSansBold(14);
        buttonRentMyYacht.titleLabel.font = FontOpenSans(14);
        buttonRentaYacht.backgroundColor = [UIColor whiteColor];
        [buttonSignUp setTitle:@"SIGN UP" forState:UIControlStateNormal];
        isRent = true;
        isForMakingRent = false;
    }
    
}

#pragma mark - PickerView delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return arrCountry.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [arrCountry  objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    textFieldCountry.text = [arrCountry objectAtIndex:row];
}

#pragma mark - Sign Up Clicked
- (IBAction)ButtonSignUpClicked:(id)sender {
    if (isFacebookLogin) {
        [self SignUpAsFacebook];
    }
    else{
        [self SignupApi];
    }
}

-(void)SignupApi{
    if (textFieldFullName.text.length == 0) {
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter full name" andViewController:self];
        return;
    }
    else if (textFieldCountry.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please select your country" andViewController:self];
    }
    //    else if (textFieldDateOfBirth.text.length == 0){
    //        [[CommonMethods sharedInstance]AlertMessage:@"Please select your birthday" andViewController:self];
    //    }
    else if (textFieldEmail.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter the email id" andViewController:self];
        return;
    }
    else if (![Validation validateEmail:textFieldEmail.text]){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter valid email id" andViewController:self];
        return;
    }
    else if(![[CommonMethods sharedInstance] validateEmailForDots:textFieldEmail.text]){
        [[CommonMethods sharedInstance]AlertMessage:@"Enter valid email id" andViewController:self];
        return;
    }
    else if (textFieldPassword.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter the password" andViewController:self];
        return;
    }
    else if (![Validation validatePasswordLength:textFieldPassword.text]){
        [[CommonMethods sharedInstance]AlertMessage:@"Password must be between 8 to 15 characters" andViewController:self];
        return;
    }
    else if (textFieldConfirmPassword.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please confirm your password" andViewController:self];
        return;
    }
    else if (![Validation validatePassword:textFieldPassword ConfirmPassword:textFieldConfirmPassword]){
        [[CommonMethods sharedInstance]AlertMessage:@"Password is not same" andViewController:self];
        return;
    }
    
    else{
        BOOL isInternet = [[CommonMethods sharedInstance]checkInternetConnection];
        if (isInternet) {
            NSString *strPhoneId = [[NSUserDefaults standardUserDefaults]objectForKey:@"phone_id"];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:textFieldFullName.text forKey:@"name"];
            [dict setObject:textFieldEmail.text forKey:@"email"];
            [dict setObject:textFieldPassword.text forKey:@"password"];
            [dict setObject:[NSString stringWithFormat:@"%@",strPhoneId] forKey:@"phone_id"];
            [dict setObject:textFieldCountry.text forKey:@"country"];
            // [dict setObject:[NSNumber numberWithInt:timestamp] forKey:@"dob"];
            if (isRent) {
                [dict setObject:@"customer" forKey:@"role"];
            }
            else{
                [dict setObject:@"owner" forKey:@"role"];
            }
            
            NSMutableDictionary *dictRequest = [[NSMutableDictionary alloc]init];
            [dictRequest setObject:dict forKey:@"users"];
            
            [WebServiceManager PostRequest:[NSString stringWithFormat:@"%@users",kBaseUrl] andParam:dictRequest andcompletionhandler:^(NSArray *returArray, NSError *error){
                if (!error) {
                    NSLog(@"%@",returArray);
                    if ([[returArray valueForKey:@"status"] boolValue]) {
                        
                        
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setBool:true forKey:@"isSignUp"];
                        [defaults setBool:false forKey:@"isLogIn"];
                        [defaults synchronize];
                        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        LoginViewController *controller = [story instantiateViewControllerWithIdentifier:@"LoginViewController"];
                        [self.navigationController pushViewController:controller animated:true];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[LoaderNew sharedLoader]hideLoader];
                            [[ CommonMethods sharedInstance] AlertMessage:[returArray valueForKey:@"message"] andViewController:self];
                        });
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[LoaderNew sharedLoader]hideLoader];
                            [[ CommonMethods sharedInstance] AlertMessage:[returArray valueForKey:@"message"] andViewController:self];
                        });
                    }
                }
                else{
                    NSLog( @"%@",error);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [[LoaderNew sharedLoader]hideLoader];
                        [[ CommonMethods sharedInstance] AlertMessage:[returArray valueForKey:@"message"] andViewController:self];
                        
                    });
                    
                    
                }
                
            }];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
                
            });
            
            
        }
        else{
            [[CommonMethods sharedInstance]AlertMessage:@"No internet is connection" andViewController:self];
        }
    }
    
    
}

-(void)SignUpAsFacebook{
    if (textFieldFullName.text.length == 0) {
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter full name" andViewController:self];
        return;
    }
    else if (textFieldCountry.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please select your country" andViewController:self];
    }
    //    else if (textFieldDateOfBirth.text.length == 0){
    //        [[CommonMethods sharedInstance]AlertMessage:@"Please select your birthday" andViewController:self];
    //    }
    else if (textFieldEmail.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter the email id" andViewController:self];
        return;
    }
    else if (![Validation validateEmail:textFieldEmail.text]){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter valid email id" andViewController:self];
        return;
    }
    else{
        BOOL isInternet = [[CommonMethods sharedInstance]checkInternetConnection];
        if (isInternet) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
            });
            NSString *strPhoneId = [[NSUserDefaults standardUserDefaults]objectForKey:@"phone_id"];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:textFieldFullName.text forKey:@"name"];
            [dict setObject:textFieldEmail.text forKey:@"email"];
            //[dict setObject:textFieldPassword.text forKey:@"password"];
            [dict setObject:[NSString stringWithFormat:@"%@",strPhoneId] forKey:@"phone_id"];
            [dict setObject:textFieldCountry.text forKey:@"country"];
            [dict setObject:[NSNumber numberWithInt:timestamp] forKey:@"dob"];
            [dict setObject:@"facebook" forKey:@"provider"];
            [dict setObject:strFBID forKey:@"fb_id"];
            // UIImage *obj = _imageViewProfilePic.image;
            NSData *data = UIImageJPEGRepresentation(FacebookProfileImage, .8);// UIImagePNGRepresentation(obj);
            NSString *imageData = [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
            imageData = [NSString stringWithFormat:@"data:image/jpeg;base64,%@",imageData];
            [dict setObject:imageData forKey:@"image"];
            
            if (isRent) {
                [dict setObject:@"customer" forKey:@"role"];
            }
            else{
                [dict setObject:@"owner" forKey:@"role"];
            }
            
            NSMutableDictionary *dictRequest = [[NSMutableDictionary alloc]init];
            [dictRequest setObject:dict forKey:@"users"];
            
            [WebServiceManager PostRequest:[NSString stringWithFormat:@"%@users/social_signin",kBaseUrl] andParam:dictRequest andcompletionhandler:^(NSArray *returnArray, NSError *error){
                if (!error) {
                    NSLog(@"%@",returnArray);
                    if ([[returnArray valueForKey:@"status"] boolValue]) {
                        
                        AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                        app.dictFacebookDetails = [[NSMutableDictionary alloc]init];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[LoaderNew sharedLoader]hideLoader];
                        });
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setBool:true forKey:@"isSignUp"];
                        [defaults setBool:false forKey:@"isLogIn"];
                        [defaults setBool:true forKey:@"isLogIn"];
                        [defaults setBool:true forKey:@"isSignUp"];
                        [defaults setObject:[returnArray valueForKey:@"auth_token"] forKey:@"Auth_token"];
                        [defaults setObject:[returnArray valueForKey:@"role"] forKey:@"role"];
                        [defaults setObject:[[returnArray valueForKey:@"user"]valueForKey:@"name"] forKey:@"Name"];
                        [defaults setObject:[[returnArray valueForKey:@"user"]valueForKey:@"address"] forKey:@"Address"];
                        [defaults setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[returnArray valueForKey:@"user"]valueForKey:@"image"]]] forKey:@"Image"];
                        [defaults synchronize];
                        //                        if ([[returnArray valueForKey:@"role"] isEqualToString:@"owner"]) {
                        //                            UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        //                            SWRevealViewController *controller = [mainStoryBoard instantiateViewControllerWithIdentifier:@"SwRootController"]; //or the homeController
                        //                            appDelegate.window.rootViewController = controller;
                        //                            [appDelegate.window makeKeyAndVisible];
                        //                        }
                        //                        else{
                        //
                        //                            SearchYachtViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SearchYacht"]; //or the homeController
                        //                            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                        //                            appDelegate.window.rootViewController = navController;
                        //                            [appDelegate.window makeKeyAndVisible];
                        //                        }
                        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        LoginViewController *controller = [story instantiateViewControllerWithIdentifier:@"LoginViewController"];
                        [self.navigationController pushViewController:controller animated:true];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[LoaderNew sharedLoader]hideLoader];
                            //[[ CommonMethods sharedInstance] AlertMessage:[returnArray valueForKey:@"message"] andViewController:self];
                        });
                        
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[LoaderNew sharedLoader]hideLoader];
                            [[ CommonMethods sharedInstance] AlertMessage:[returnArray valueForKey:@"message"] andViewController:self];
                            
                        });
                    }
                }
                else{
                    NSLog( @"%@",error);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[LoaderNew sharedLoader]hideLoader];
                        [[ CommonMethods sharedInstance] AlertMessage:[returnArray valueForKey:@"message"] andViewController:self];
                        
                    });
                    
                    
                }
                
            }];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
            });
            
            
        }
        else{
            [[CommonMethods sharedInstance]AlertMessage:@"No internet is connection" andViewController:self];
        }
    }
    
    
}

#pragma mark - Delegates of TextField

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField == textFieldFullName) {
        [textField becomeFirstResponder];
    }
    else if (textField == textFieldCountry){
        [textFieldFullName resignFirstResponder];
        [textField becomeFirstResponder];
    }
    else if (textField == textFieldDateOfBirth){
        [textFieldCountry resignFirstResponder];
        
        [UIView animateWithDuration:0.25 animations:^{
            toolBar.frame = CGRectMake(0.0, SCREEN_HEIGHT - 200, SCREEN_WIDTH, 50);
            //   datePickerDateOfBirth.frame = CGRectMake(0.0, SCREEN_HEIGHT - 150, SCREEN_WIDTH, 150);
        }];
        
    }
    else if (textField == textFieldEmail) {
        [textFieldCountry resignFirstResponder];
        if (!isFacebookLogin) {
            [textFieldEmail  becomeFirstResponder];
        }
        
    }
    else if (textField == textFieldPassword){
        if (!isFacebookLogin) {
            [textFieldEmail resignFirstResponder];
            [textFieldPassword becomeFirstResponder];
        }
        
    }
    else if (textField == textFieldConfirmPassword){
        if (!isFacebookLogin) {
            [textFieldPassword resignFirstResponder];
            [textField becomeFirstResponder];
        }
        
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == textFieldFullName) {
        [textField resignFirstResponder];
        [textFieldCountry becomeFirstResponder];
    }
    else if (textField == textFieldCountry){
        [textField resignFirstResponder];
        [UIView animateWithDuration:0.25 animations:^{
            toolBar.frame = CGRectMake(0.0, SCREEN_HEIGHT - 200, SCREEN_WIDTH, 50);
            // datePickerDateOfBirth.frame = CGRectMake(0.0, SCREEN_HEIGHT - 150, SCREEN_WIDTH, 150);
        }];
        
    }
    else if (textField == textFieldDateOfBirth){
        [textField resignFirstResponder];
        [textFieldEmail becomeFirstResponder];
    }
    else if (textField == textFieldEmail){
        [textField resignFirstResponder];
        if (!isFacebookLogin) {
            [textFieldPassword becomeFirstResponder];
        }
        
        
    }
    else if (textField == textFieldPassword){
        [textField resignFirstResponder];
        //[textFieldConfirmPassword becomeFirstResponder];
    }
    
    else if (textField == textFieldConfirmPassword){
        [self.view endEditing:true];
    }
    return true;
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == textFieldPassword || textField == textFieldConfirmPassword) {
        BOOL digit = false;
        if([textField.text length] >= 8 && [textField.text length] <= 14)
        {
            for (int i = 0; i < [textField.text length]; i++)
            {
                unichar c = [textField.text characterAtIndex:i];
                if(!digit)
                {
                    digit = [[NSCharacterSet alphanumericCharacterSet] characterIsMember:c];
                }
            }
            
            if(digit)
            {
                //do what u want
            }
            else{
                [[CommonMethods sharedInstance]AlertMessage:@"Please Ensure that you have alphanumeric character" andViewController:self];
                [textField becomeFirstResponder];
            }
        }
        else
        {
            [[CommonMethods sharedInstance]AlertMessage:@"Password must be between 8 to 14 characters" andViewController:self];
            [textField becomeFirstResponder];
        }
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField == textFieldFullName)
    {
        NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    
    else if (textField == textFieldPassword){
        if (textField.text.length >= MAX_LENGTH && range.length == 0)
        {
            return NO; // return NO to not change text
        }
        else
        {
            return YES;
        }
    }
    else{
        return YES;
    }
}

//- (IBAction)TappedAction:(id)sender {
//    [self.view endEditing:true];
//    [UIView animateWithDuration:0.25 animations:^{
//        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
//    }];
//
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 7;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  //  if (isFacebookLogin) {
//        if  (indexPath.row == 4 || indexPath.row == 5){
//            return 0;
//        }
//        else{
            return [super tableView:tableView heightForRowAtIndexPath:indexPath];
//        }
//    }
//    else{
//        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
//    }
}
/*
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
 
 // Configure the cell...
 
 return cell;
 }
 */

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
