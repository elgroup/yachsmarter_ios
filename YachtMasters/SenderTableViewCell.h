//
//  SenderTableViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 31/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SenderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewText;
@property (weak, nonatomic) IBOutlet UILabel *labelText;
@property CAShapeLayer *borderLayer;
@property CAShapeLayer *maskLayer;
@end
