//
//  MenuListTableViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 21/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelMenuListName;
@property (weak, nonatomic) IBOutlet UILabel *labelNotificationCount;
@property (weak, nonatomic) IBOutlet UILabel *labelLine;

@end
