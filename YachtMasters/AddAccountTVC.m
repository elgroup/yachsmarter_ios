//
//  AddAccountTVC.m
//  YachtMasters
//
//  Created by Anvesh on 29/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "AddAccountTVC.h"
#import "Constants.h"
#import "CommonMethods.h"
#import "WebServiceManager.h"
#import "MBProgressHUD.h"
#import "YachtMasters-Swift.h"

@interface AddAccountTVC ()<UIPickerViewDelegate,UITextFieldDelegate>
{
    __weak IBOutlet UITableViewCell *tableViewCellSelectAccountType;
    __weak IBOutlet UITableViewCell *tableViewCellAddressLine1;
    __weak IBOutlet UITableViewCell *tableViewCellAddressLine2;
    __weak IBOutlet UITableViewCell *tableViewCellCity;
    __weak IBOutlet UITableViewCell *tableViewCellCountry;
    __weak IBOutlet UITableViewCell *tableViewCellOwnerName;
    __weak IBOutlet UITableViewCell *tableViewCellIBAN;
    __weak IBOutlet UITableViewCell *tableViewCellAccountNumber;
    __weak IBOutlet UITableViewCell *tableViewCellABA;
    __weak IBOutlet UITableViewCell *tableViewCellBranchCode;
    __weak IBOutlet UITableViewCell *tableViewCellInstitutionName;
    __weak IBOutlet UITableViewCell *tableViewCellBankName;
    __weak IBOutlet UITableViewCell *tableViewCellSortCode;
    __weak IBOutlet UITableViewCell *tableViewCellBIC;
    __weak IBOutlet UITableViewCell *tableViewCellButtonAddAccount;
    __weak IBOutlet UITableViewCell *tableViewCellBankCountry;
    __weak IBOutlet UITextField *tfAddressLine1;
    __weak IBOutlet UITableViewCell *tableViewCellDeleteAccount;
    __weak IBOutlet UITextField *tfAddressline2;
    __weak IBOutlet UITextField *tfRegion;
    __weak IBOutlet UITextField *tfBIC;
    __weak IBOutlet UITextField *tfSortCode;
    __weak IBOutlet UITextField *tfbankName;
    __weak IBOutlet UITextField *tfInstitutionNumber;
    __weak IBOutlet UITextField *tfBranchCode;
    __weak IBOutlet UITextField *tfABA;
    __weak IBOutlet UITextField *tfAccountNumber;
    __weak IBOutlet UITextField *tfIBAN;
    __weak IBOutlet UITextField *tfOwnerName;
    __weak IBOutlet UITextField *tfPostalCode;
    __weak IBOutlet UITextField *tfCountry;
    __weak IBOutlet UITextField *tfCity;
    __weak IBOutlet UIButton *buttonAddAccount;
    __weak IBOutlet UITextField *textFieldBankAccountType;
    __weak IBOutlet UITextField *tfBankCountry;
    __weak IBOutlet UIButton *buttonDeleteAccount;
    //IBOutlet NSLayoutConstraint *lccontraints
    NSArray *arrAccountType;
    UIBarButtonItem *doneButton;
    UIToolbar *toolBar;
    UIPickerView *pickerBankAccount;
    NSInteger selectedAccountType;
    NSMutableDictionary *dictTableViewCells;
    NSMutableArray *_cellArr;
    NSMutableDictionary *dictResponse;
    UITextField *textFieldSelected;
    NSMutableArray *arrCountry;
    MBProgressHUD *hud;
    BOOL isDelete;
}
@end

@implementation AddAccountTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Country" ofType:@"json"];
    NSData *content = [[NSData alloc] initWithContentsOfFile:filePath];
    NSDictionary *dictCountryCode = [NSJSONSerialization JSONObjectWithData:content options:kNilOptions error:nil];
    NSLog(@"%@",dictCountryCode);
    arrCountry = [[NSMutableArray alloc]init];
    arrCountry = [[dictCountryCode valueForKey:@"array"]valueForKey:@"country_name"];
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.tableView addGestureRecognizer:gestureRecognizer];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self CustomNavigationBar];
    dictResponse = [[NSMutableDictionary alloc]init];
    selectedAccountType = 0;
    arrAccountType = @[@"IBAN Bank Account",@"US Bank Account",@"CA Bank Account",@"GB Bank Account",@"Other Bank Account"];
    
    doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(hideKeyboard)];
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 50)];
    toolBar.translucent=NO;
    toolBar.barTintColor = [UIColor colorWithRed:40.0/255.0 green:107.0/255.0 blue:240.0/255.0 alpha:1.0];
    
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton, nil];
    [toolBar setItems:toolbarItems];
    doneButton.tintColor = [UIColor whiteColor];
    
    
    textFieldBankAccountType.inputAccessoryView = toolBar;
    tfSortCode.inputAccessoryView = toolBar;
    tfAccountNumber.inputAccessoryView = toolBar;
    tfSortCode.inputAccessoryView = toolBar;
    tfABA.inputAccessoryView = toolBar;
    tfInstitutionNumber.inputAccessoryView = toolBar;
    tfBranchCode.inputAccessoryView = toolBar;
    tfPostalCode.inputAccessoryView = toolBar;
    tfBankCountry.inputAccessoryView = toolBar;
    tfCountry.inputAccessoryView = toolBar;
    
    pickerBankAccount = [[UIPickerView alloc]init];
    pickerBankAccount.backgroundColor = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1.0];
    pickerBankAccount.delegate = self;
    textFieldBankAccountType.inputView = pickerBankAccount;
    tfBankCountry.inputView = pickerBankAccount;
    tfCountry.inputView = pickerBankAccount;
    [self CustomCells];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[CommonMethods sharedInstance]DropShadow:buttonAddAccount UIColor:[UIColor colorWithRed:90.0/255.0 green:190.0/255.0 blue:238.0/255.0 alpha:1.0] andShadowRadius:4.0];
    if (_isRevealView) {
        self.tableView.frame = CGRectMake(0.0, 44.0, SCREEN_WIDTH, SCREEN_HEIGHT);
    }
}
-(BOOL)hidesBottomBarWhenPushed{
    return YES;
}

-(void)hideKeyboard{
    [self.view endEditing:true];
}

-(void)CustomNavigationBar{
    self.tabBarController.tabBar.hidden =  true;
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    //[backButton setBackgroundColor:[UIColor redColor]];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 5.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    if (__isEdit) {
        UIButton *buttonChange = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonChange setTitle:@"Change" forState:UIControlStateNormal];
        buttonChange.titleLabel.font = FontOpenSansBold(12);
        buttonChange.frame = CGRectMake(SCREEN_WIDTH - 80, 10, 60, 30);
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:buttonChange];
        self.navigationItem.rightBarButtonItem = rightBarButtonItem;
        
    }
    self.title = @"Add Account";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}

-(void)BackButtonPressed{
    self.navigationController.navigationBar.hidden = true;
    self.tabBarController.tabBar.hidden = false;
    [self.navigationController popViewControllerAnimated:true];
}


-(void)CustomCells{
    dictTableViewCells = [[NSMutableDictionary alloc]init];
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    [arr addObject:tableViewCellSelectAccountType];
    [arr addObject:tableViewCellAddressLine1];
    [arr addObject:tableViewCellAddressLine2];
    [arr addObject:tableViewCellCity];
    [arr addObject:tableViewCellCountry];
    [arr addObject:tableViewCellOwnerName];
    [arr addObject:tableViewCellBankCountry];
    [arr addObject:tableViewCellIBAN];
    [arr addObject:tableViewCellBIC];
    [arr addObject:tableViewCellAccountNumber];
    
    if (__isEdit) {
        [arr addObject:tableViewCellDeleteAccount];
    }
    else{
        [arr addObject:tableViewCellButtonAddAccount];
    }
    [dictTableViewCells setObject:arr forKey:@"1"];
    arr = [NSMutableArray new];
    [arr addObject:tableViewCellSelectAccountType];
    [arr addObject:tableViewCellAddressLine1];
    [arr addObject:tableViewCellAddressLine2];
    [arr addObject:tableViewCellCity];
    [arr addObject:tableViewCellCountry];
    
    [arr addObject:tableViewCellOwnerName];
    [arr addObject:tableViewCellBankCountry];
    [arr addObject:tableViewCellAccountNumber];
    [arr addObject:tableViewCellABA];
    [arr addObject:tableViewCellBIC];
    if (__isEdit) {
        [arr addObject:tableViewCellDeleteAccount];
    }
    else{
        [arr addObject:tableViewCellButtonAddAccount];
    }
    [dictTableViewCells setObject:arr forKey:@"2"];
    
    arr = [NSMutableArray new];
    [arr addObject:tableViewCellSelectAccountType];
    [arr addObject:tableViewCellAddressLine1];
    [arr addObject:tableViewCellAddressLine2];
    [arr addObject:tableViewCellCity];
    [arr addObject:tableViewCellCountry];
    
    [arr addObject:tableViewCellOwnerName];
    [arr addObject:tableViewCellBankCountry];
    [arr addObject:tableViewCellAccountNumber];
    [arr addObject:tableViewCellBankName];
    [arr addObject:tableViewCellBranchCode];
    [arr addObject:tableViewCellInstitutionName];
    if (__isEdit) {
        [arr addObject:tableViewCellDeleteAccount];
    }
    else{
        [arr addObject:tableViewCellButtonAddAccount];
    }
    [dictTableViewCells setObject:arr forKey:@"3"];
    
    arr = [NSMutableArray new];
    [arr addObject:tableViewCellSelectAccountType];
    [arr addObject:tableViewCellAddressLine1];
    [arr addObject:tableViewCellAddressLine2];
    [arr addObject:tableViewCellCity];
    [arr addObject:tableViewCellCountry];
    
    [arr addObject:tableViewCellOwnerName];
    [arr addObject:tableViewCellBankCountry];
    [arr addObject:tableViewCellAccountNumber];
    [arr addObject:tableViewCellSortCode];
    if (__isEdit) {
        [arr addObject:tableViewCellDeleteAccount];
    }
    else{
        [arr addObject:tableViewCellButtonAddAccount];
    }
    [dictTableViewCells setObject:arr forKey:@"4"];
    
    arr = [NSMutableArray new];
    [arr addObject:tableViewCellSelectAccountType];
    [arr addObject:tableViewCellAddressLine1];
    [arr addObject:tableViewCellAddressLine2];
    [arr addObject:tableViewCellCity];
    [arr addObject:tableViewCellCountry];
    
    [arr addObject:tableViewCellOwnerName];
    [arr addObject:tableViewCellBankCountry];
    [arr addObject:tableViewCellBIC];
    [arr addObject:tableViewCellAccountNumber];
    if (__isEdit) {
        [arr addObject:tableViewCellDeleteAccount];
    }
    else{
        [arr addObject:tableViewCellButtonAddAccount];
    }
    [dictTableViewCells setObject:arr forKey:@"5"];
    
    
    _cellArr = [[NSMutableArray alloc]init];
    [_cellArr addObject:tableViewCellSelectAccountType];
    if (__isEdit) {
        NSString *strBankAccountType = [_arrAccountDetail valueForKey:@"account_type"];
        if ([strBankAccountType isEqualToString:@"IBAN"]) {
            selectedAccountType = 1;
        }
        else if ([strBankAccountType isEqualToString:@"US"]){
            selectedAccountType = 2;
        }
        else if ([strBankAccountType isEqualToString:@"CA"]){
            selectedAccountType = 3;
        }
        else if ([strBankAccountType isEqualToString:@"GB"]){
            selectedAccountType = 4;
        }
        else if ([strBankAccountType isEqualToString:@"OTHER"]){
            selectedAccountType = 5;
        }
        _cellArr = [dictTableViewCells valueForKey:[NSString stringWithFormat:@"%ld",(long)selectedAccountType]];
        [self.tableView reloadData];
        
    }
    
}
- (IBAction)AddAccount:(id)sender {
    
    if (tfAddressLine1.text.length == 0) {
        [[CommonMethods sharedInstance]AlertMessage:@"Enter the address" andViewController:self];
    }
    else if (tfAddressline2.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Enter the address" andViewController:self];
    }
    else if (tfCity.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Enter the city" andViewController:self];
    }
    else if (tfRegion.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Enter the region" andViewController:self];
    }
    else if (tfCountry.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Enter the country" andViewController:self];
    }
    else if (tfPostalCode.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Enter the postal code" andViewController:self];
    }
    else if (tfBankCountry.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Enter the country of the bank" andViewController:self];
        
    }
    else if (selectedAccountType == 1){
        if (tfOwnerName.text.length == 0) {
            [[CommonMethods sharedInstance]AlertMessage:@"Enter the owner name" andViewController:self];
        }
        else if (tfIBAN.text.length == 0){
            [[CommonMethods sharedInstance]AlertMessage:@"Enter the IBAN" andViewController:self];
        }
        else{
            [self ApiForAccountAddition];
        }
    }
    else if (selectedAccountType == 2){
        if (tfOwnerName.text.length == 0) {
            [[CommonMethods sharedInstance]AlertMessage:@"Enter the owner name" andViewController:self];
        }
        else if (tfAccountNumber.text.length == 0){
            [[CommonMethods sharedInstance]AlertMessage:@"Enter the account number" andViewController:self];
        }
        else if (tfABA.text.length == 0){
            [[CommonMethods sharedInstance]AlertMessage:@"Enter the ABA" andViewController:self];
        }
        else if (tfBIC.text.length == 0){
            [[CommonMethods sharedInstance]AlertMessage:@"Enter the BIC" andViewController:self];
        }
        else{
            [self ApiForAccountAddition];
        }
    }
    else if (selectedAccountType == 3){
        if (tfOwnerName.text.length == 0) {
            [[CommonMethods sharedInstance]AlertMessage:@"Enter the owner name" andViewController:self];
        }
        else if (tfAccountNumber.text.length == 0){
            [[CommonMethods sharedInstance]AlertMessage:@"Enter the account number" andViewController:self];
        }
        else if (tfbankName.text.length == 0){
            [[CommonMethods sharedInstance]AlertMessage:@"Enter the bank name" andViewController:self];
        }
        else if (tfBranchCode.text.length == 0){
            [[CommonMethods sharedInstance]AlertMessage:@"Enter the branch code" andViewController:self];
        }
        else if (tfInstitutionNumber.text.length == 0){
            [[CommonMethods sharedInstance]AlertMessage:@"Enter the institution number" andViewController:self];
        }
        else{
            [self ApiForAccountAddition];
        }
    }
    else if (selectedAccountType == 4){
        if (tfOwnerName.text.length == 0) {
            [[CommonMethods sharedInstance]AlertMessage:@"Enter the owner name" andViewController:self];
        }
        else if (tfAccountNumber.text.length == 0){
            [[CommonMethods sharedInstance]AlertMessage:@"Enter the account number" andViewController:self];
        }
        else if (tfSortCode.text.length == 0){
            [[CommonMethods sharedInstance]AlertMessage:@"Enter the sort code" andViewController:self];
        }
        else{
            [self ApiForAccountAddition];
        }
        
    }
    else if (selectedAccountType == 5){
        if (tfOwnerName.text.length == 0) {
            [[CommonMethods sharedInstance]AlertMessage:@"Enter the owner name" andViewController:self];
        }
        else if (tfBIC.text.length == 0){
            [[CommonMethods sharedInstance]AlertMessage:@"Enter the BIC" andViewController:self];
        }
        else if (tfAccountNumber.text.length == 0){
            [[CommonMethods sharedInstance]AlertMessage:@"Enter the account number" andViewController:self];
        }
        else{
            [self ApiForAccountAddition];
        }
    }
    
    
    
}
- (IBAction)ButtonDeleteAccountPressed:(id)sender {
    
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSString *strId = [[_arrAccountDetail valueForKey:@"id"] stringValue];
        [WebServiceManager deleteRequestWithUrlString:[NSString stringWithFormat:@"%@bank_accounts/%@",kBaseUrl,strId] emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                __isEdit =false;
                _cellArr = [[NSMutableArray alloc]init];
                [_cellArr addObject:tableViewCellSelectAccountType];
                isDelete = true;
                [self CustomCells];
                [self.tableView reloadData];            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
//            hud = [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
//            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
            
        });
    }else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}


-(void)ApiForAccountAddition{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:dictResponse forKey:@"bank_accounts"];
        [WebServiceManager postRequestWithUrlString:[NSString stringWithFormat:@"%@bank_account",kBaseUrlVersion2] withPostString:dict emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:[result valueForKey:@"message"] andViewController:self];
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
//            hud = [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
//            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
            
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}




#pragma mark - PickerView delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (textFieldSelected == textFieldBankAccountType) {
        return  [arrAccountType count];
    }
    else if (textFieldSelected == tfBankCountry){
        return [arrCountry count];
    }
    else if (textFieldSelected == tfCountry){
        return [arrCountry count];
    }
    else{
        return 0;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (textFieldSelected == textFieldBankAccountType) {
        return [arrAccountType objectAtIndex:row];
    }
    else if (textFieldSelected == tfBankCountry){
        return [arrCountry objectAtIndex:row];
    }
    else if (textFieldSelected == tfCountry){
        return [arrCountry objectAtIndex:row];
    }
    else{
        return 0;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (textFieldSelected == textFieldBankAccountType) {
        textFieldBankAccountType.text = [arrAccountType objectAtIndex:row];
        textFieldBankAccountType.textColor = [UIColor blackColor];
        selectedAccountType = row+1;
        _cellArr = [NSMutableArray new];
        _cellArr = [dictTableViewCells valueForKey:[NSString stringWithFormat:@"%ld",(long)selectedAccountType]];
        [self.tableView reloadData];
    }
    else if (textFieldSelected == tfBankCountry){
        tfBankCountry.text = [arrCountry objectAtIndex:row];
        
    }
    else if (textFieldSelected == tfCountry){
        tfCountry.text = [arrCountry objectAtIndex:row];
        
    }
    
}


#pragma mark - TextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == textFieldBankAccountType) {
        textFieldSelected = textField;
        [pickerBankAccount reloadAllComponents];
    }
    else if (textField == tfBankCountry){
        textFieldSelected = textField;
        [pickerBankAccount reloadAllComponents];
    }
    else if (textField == tfCountry){
        textFieldSelected = textField;
        [pickerBankAccount reloadAllComponents];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == tfAddressLine1) {
        [textField resignFirstResponder];
        [tfAddressline2 becomeFirstResponder];
    }
    else if (textField == tfAddressline2){
        [textField resignFirstResponder];
        [tfCity becomeFirstResponder];
    }
    else if (textField == tfCity){
        [textField resignFirstResponder];
        [tfRegion becomeFirstResponder];
    }
    else if (textField == tfRegion){
        [textField resignFirstResponder];
        [tfCountry becomeFirstResponder];
    }
    else if (textField == tfCountry){
        [textField resignFirstResponder];
        [tfPostalCode becomeFirstResponder];
    }
    else if (textField == tfOwnerName){
        [textField resignFirstResponder];
    }
    else if (textField == tfBranchCode){
        [textField resignFirstResponder];
    }
    return true;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == textFieldBankAccountType) {
        switch (selectedAccountType) {
            case 1:
                [dictResponse setObject:@"IBAN" forKey:@"account_type"];
                break;
            case 2:
                [dictResponse setObject:@"US" forKey:@"account_type"];
                break;
            case 3:
                [dictResponse setObject:@"CA" forKey:@"account_type"];
                break;
            case 4:
                [dictResponse setObject:@"GB" forKey:@"account_type"];
                break;
            case 5:
                [dictResponse setObject:@"OTHER" forKey:@"account_type"];
                break;
                
            default:
                break;
        }
        
    }
    else if (textField == tfAddressLine1){
        [dictResponse setObject:tfAddressLine1.text forKey:@"address_line_1"];
    }
    else if (textField == tfAddressline2){
        [dictResponse setObject:tfAddressline2.text forKey:@"address_line_2"];
    }
    else if (textField == tfCity) {
        [dictResponse setObject:tfCity.text forKey:@"city"];
    }
    else if (textField == tfRegion){
        [dictResponse setObject:tfRegion.text forKey:@"region"];
    }
    else if (textField == tfPostalCode){
        [dictResponse setObject:tfPostalCode.text forKey:@"postal_code"];
    }
    else if (textField == tfCountry){
        [dictResponse setObject:tfCountry.text forKey:@"country"];
        
    }
    else if (textField == tfOwnerName){
        [dictResponse setObject:tfOwnerName.text forKey:@"holder_name"];
    }
    else if (textField == tfIBAN) {
        [dictResponse setObject:tfIBAN.text forKey:@"iban"];
    }
    else if (textField == tfABA){
        [dictResponse setObject:tfABA.text forKey:@"aba"];
    }
    else if (textField == tfAccountNumber){
        [dictResponse setObject:tfAccountNumber.text forKey:@"number"];
    }
    else if (textField == tfBranchCode){
        [dictResponse setObject:tfBranchCode.text forKey:@"branch_code"];
    }
    else if (textField == tfInstitutionNumber){
        [dictResponse setObject:tfInstitutionNumber.text forKey:@"institute_number"];
    }
    else if (textField == tfbankName) {
        [dictResponse setObject:tfbankName.text forKey:@"bank_name"];
    }
    else if (textField == tfSortCode){
        [dictResponse setObject:tfSortCode.text forKey:@"sort_code"];
    }
    else if (textField == tfBIC){
        [dictResponse setObject:tfBIC.text forKey:@"bic"];
    }
    else if (textField == tfBankCountry){
        
        [dictResponse setObject:tfBankCountry.text forKey:@"bank_country"];
    }
    
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1 ;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [_cellArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [_cellArr objectAtIndex:indexPath.row];
    if (__isEdit) {
        textFieldBankAccountType.text = [arrAccountType objectAtIndex:selectedAccountType - 1];
        textFieldBankAccountType.enabled = false;
        tfAddressLine1.text = [_arrAccountDetail valueForKey:@"address_line_1"];
        tfAddressLine1.enabled = false;
        tfAddressline2.text = [_arrAccountDetail valueForKey:@"address_line_2"];
        tfAddressline2.enabled = false;
        tfCity.text = [_arrAccountDetail valueForKey:@"city"];
        tfCity.enabled = false;
        tfRegion.text = [_arrAccountDetail valueForKey:@"region"];
        tfRegion.enabled = false;
        tfCountry.text = [_arrAccountDetail valueForKey:@"country"];
        tfCountry.enabled = false;
        tfPostalCode.text = [_arrAccountDetail valueForKey:@"postal_code"];
        tfPostalCode.enabled = false;
        tfAccountNumber.text = [_arrAccountDetail valueForKey:@"number"];
        tfAccountNumber.enabled = false;
        tfOwnerName.text = [_arrAccountDetail valueForKey:@"holder_name"];
        tfOwnerName.enabled = false;
        tfBankCountry.text = [_arrAccountDetail valueForKey:@"bank_country"];
        tfBankCountry.enabled = false;
        NSString *strAccountype = [_arrAccountDetail valueForKey:@"account_type"];
        if ([strAccountype isEqualToString:@"IBAN"]) {
            tfIBAN.text = [_arrAccountDetail valueForKey:@"iban"];
            tfIBAN.enabled = false;
            tfBIC.text = [_arrAccountDetail valueForKey:@"bic"];
            tfBIC.enabled = false;
        }
        else if ([strAccountype isEqualToString:@"US"]){
            tfABA.text = [_arrAccountDetail valueForKey:@"aba"];
            tfABA.enabled = false;
            tfBIC.text = [_arrAccountDetail valueForKey:@"bic"];
            tfBIC.enabled = false;
        }
        else if ([strAccountype isEqualToString:@"CA"]){
            tfbankName.text = [_arrAccountDetail valueForKey:@"bank_name"];
            tfbankName.enabled = false;
            tfBranchCode.text = [_arrAccountDetail valueForKey:@"branch_code"];
            tfBranchCode.enabled = false;
            tfInstitutionNumber.text = [_arrAccountDetail valueForKey:@"institute_number"];
            tfInstitutionNumber.enabled = false;
            
        }
        else if ([strAccountype isEqualToString:@"GB"]){
            tfSortCode.text = [_arrAccountDetail valueForKey:@"sort_code"];
            tfSortCode.enabled = false;
        }
        else{
            tfBIC.text = [_arrAccountDetail valueForKey:@"bic"];
            tfBIC.enabled = false;
        }
    }
    else if (isDelete){
        textFieldBankAccountType.text =@"Select type of account";
        textFieldBankAccountType.enabled = true;
        isDelete = false;
        
        // textFieldBankAccountType.text = @"Select type of account";
        textFieldBankAccountType.enabled = true;
        tfAddressLine1.text = @"";
        tfAddressLine1.enabled = true;
        tfAddressline2.text = @"";
        tfAddressline2.enabled = true;
        tfCity.text = @"";
        tfCity.enabled = true;
        tfRegion.text = @"";
        tfRegion.enabled = true;
        tfCountry.text =@"";
        tfCountry.enabled = true;
        tfPostalCode.text = @"";
        tfPostalCode.enabled = true;
        tfAccountNumber.text = @"";
        tfAccountNumber.enabled = true;
        tfOwnerName.text = @"";
        tfOwnerName.enabled = true;
        tfBankCountry.text = @"";
        NSString *strAccountype = [_arrAccountDetail valueForKey:@"account_type"];
        if ([strAccountype isEqualToString:@"IBAN"]) {
            tfIBAN.text = @"";
            tfIBAN.enabled = true;
            tfBIC.text = @"";
            tfBIC.enabled = true;
        }
        else if ([strAccountype isEqualToString:@"US"]){
            tfABA.text = @"";
            tfABA.enabled = true;
            tfBIC.text = @"";
            tfBIC.enabled = true;
        }
        else if ([strAccountype isEqualToString:@"CA"]){
            tfbankName.text = @"";
            tfbankName.enabled = true;
            tfBranchCode.text =@"";
            tfBranchCode.enabled = true;
            tfInstitutionNumber.text = @"";
            tfInstitutionNumber.enabled = true;
            
        }
        else if ([strAccountype isEqualToString:@"GB"]){
            tfSortCode.text = @"";
            tfSortCode.enabled = true;
        }
        else{
            tfBIC.text = @"";
            tfBIC.enabled = true;
        }
        
    }
//    else{
//        // textFieldBankAccountType.text = @"Select type of account";
//        textFieldBankAccountType.enabled = true;
//        tfAddressLine1.text = @"";
//        tfAddressLine1.enabled = true;
//        tfAddressline2.text = @"";
//        tfAddressline2.enabled = true;
//        tfCity.text = @"";
//        tfCity.enabled = true;
//        tfRegion.text = @"";
//        tfRegion.enabled = true;
//        tfCountry.text =@"";
//        tfCountry.enabled = true;
//        tfPostalCode.text = @"";
//        tfPostalCode.enabled = true;
//        tfAccountNumber.text = @"";
//        tfAccountNumber.enabled = true;
//        tfOwnerName.text = @"";
//        tfOwnerName.enabled = true;
//        tfBankCountry.text = @"";
//        NSString *strAccountype = [_arrAccountDetail valueForKey:@"account_type"];
//        if ([strAccountype isEqualToString:@"IBAN"]) {
//            tfIBAN.text = @"";
//            tfIBAN.enabled = true;
//            tfBIC.text = @"";
//            tfBIC.enabled = true;
//        }
//        else if ([strAccountype isEqualToString:@"US"]){
//            tfABA.text = @"";
//            tfABA.enabled = true;
//        }
//        else if ([strAccountype isEqualToString:@"CA"]){
//            tfbankName.text = @"";
//            tfbankName.enabled = true;
//            tfBranchCode.text =@"";
//            tfBranchCode.enabled = true;
//            tfInstitutionNumber.text = @"";
//            tfInstitutionNumber.enabled = true;
//            
//        }
//        else if ([strAccountype isEqualToString:@"GB"]){
//            tfSortCode.text = @"";
//            tfSortCode.enabled = true;
//        }
//        else{
//            tfBIC.text = @"";
//            tfBIC.enabled = true;
//        }
//    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [_cellArr objectAtIndex:indexPath.row];
    return cell.frame.size.height;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
