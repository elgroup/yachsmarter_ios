//
//  MapViewController.h
//  YachtMasters
//
//  Created by Anvesh on 14/06/17.
//  Copyright © 2017 EL. All rights reserved.
//


#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
@protocol LocationDelegate <NSObject>
@required
- (void) AddressOfYacht:(NSMutableArray *)arrCompleteAddress;
@end


@interface MapViewController : UIViewController<CLLocationManagerDelegate>
{
}

@property (nonatomic,strong) id<LocationDelegate> delegate;
@property BOOL isEditProfile;
@end
