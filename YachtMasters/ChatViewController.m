//
//  ChatViewController.m
//  YachtMasters
//
//  Created by Anvesh on 31/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "ChatViewController.h"
#import "Constants.h"
#import "CustomNavigationViewController.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "CommonMethods.h"
#import "ReviewTableViewCell.h"
#import "RecieverTableViewCell.h"
#import "SenderTableViewCell.h"
#import "AppDelegate.h"
#import "WebServiceManager.h"
#import "UIImageView+WebCache.h"
#import "AppSocket.h"

@interface ChatViewController ()<UITextViewDelegate,AppSocketDelegate>{
    
    __weak IBOutlet UITableView *tableViewChat;
    __weak IBOutlet UIView *viewForTextView;
    __weak IBOutlet UIButton *buttonSendMessage;
    __weak IBOutlet UITextView *textViewChat;
    __weak IBOutlet NSLayoutConstraint *lcHeightForView;
    NSInteger numberofRows;
    AppDelegate *delegate;
    NSMutableArray *arrMessages;
    NSString *strUrl;
    IBOutlet NSLayoutConstraint *lcBottom;
    AppDelegate *appDelegate;
    UIButton *buttonOnline;
}

@end

@implementation ChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    textViewChat.text = @"Send message...";
    textViewChat.textColor = [UIColor lightGrayColor];
    self.tabBarController.tabBar.hidden = true;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    UITapGestureRecognizer *tapRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tableTapped:)];
    [tableViewChat addGestureRecognizer:tapRec];
    tableViewChat.rowHeight = UITableViewAutomaticDimension;
    tableViewChat.estimatedRowHeight = 60;
 //   [self setStatusBarBackgroundColor:[UIColor whiteColor]];
    numberofRows = 10;
    //    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [tableViewChat reloadData];
    if ([arrMessages count] > 0) {
        [tableViewChat scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[arrMessages count] -1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
    appDelegate =(AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.socket.delegate = self;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"is_read = 0 AND recipient_id = %@",[_arrCurrentUser valueForKey:@"id"]]];
    NSArray *arrUnRead = [arrMessages filteredArrayUsingPredicate:predicate]; //[arrMessages filterUsingPredicate:predicate];
    NSArray *arrUnreadListIds = [arrUnRead valueForKeyPath:@"@distinctUnionOfObjects.id" ];
    NSLog(@"%@", arrUnreadListIds);
    NSLog(@"%@", arrUnRead);
    [self updateTheUnreadStatus:arrUnreadListIds];
    // [self scrollToBottom];
}

-(BOOL)hidesBottomBarWhenPushed{
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    arrMessages = [[NSMutableArray alloc]init];
    arrMessages = [[self.arrConversations valueForKey:@"messages"] mutableCopy];
    [self CustomNavigationBar];
    [tableViewChat reloadData];
    if ([arrMessages count] > 0) {
        [tableViewChat scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[arrMessages count] -1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object: nil];
    
}
-(void)CustomNavigationBar{
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    CGRect frameimg = CGRectMake(15,10, 30,30);
    if ([[_arrCurrentUser valueForKey:@"id"] intValue] == [[[self.arrConversations valueForKey:@"recipient"] valueForKey:@"id"] intValue]) {
        self.navigationItem.title = [[self.arrConversations valueForKey:@"sender"] valueForKey:@"name"];
        strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[self.arrConversations valueForKey:@"sender"] valueForKey:@"image"]];
    }
    else{
        self.navigationItem.title = [[self.arrConversations valueForKey:@"recipient"] valueForKey:@"name"];
        strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[self.arrConversations valueForKey:@"recipient"] valueForKey:@"image"]];
        
    }
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.layer.shadowOpacity = 0.0;
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    //[backButton setBackgroundColor:[UIColor redColor]];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    CGRect frame = CGRectMake([[UIScreen mainScreen]bounds ].size.width - 20, 15.0,10.0, 10.0);
    buttonOnline = [[UIButton alloc] initWithFrame:frame];
    //[backButton setBackgroundColor:[UIColor redColor]];
    UIBarButtonItem *rightBarButton =[[UIBarButtonItem alloc] initWithCustomView:buttonOnline];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    buttonOnline.layer.cornerRadius = 5;
    
    if ([[_arrCurrentUser valueForKey:@"id"] intValue] == [[[self.arrConversations valueForKey:@"sender"] valueForKey:@"id"] intValue] ){
        BOOL isOnline = [[[self.arrConversations valueForKey:@"recipient"] valueForKey:@"is_online"] boolValue];
        if (isOnline){
            [buttonOnline setImage:[UIImage imageNamed:@"online"] forState:UIControlStateNormal];
        }else{
            [buttonOnline setImage:[UIImage imageNamed:@"offline"] forState:UIControlStateNormal];
        }
    }
    else{
        BOOL isOnline = [[[self.arrConversations valueForKey:@"sender"] valueForKey:@"is_online"] boolValue];
        if (isOnline){
            [buttonOnline setImage:[UIImage imageNamed:@"online"] forState:UIControlStateNormal];
        }else{
            [buttonOnline setImage:[UIImage imageNamed:@"offline"] forState:UIControlStateNormal];
        }
    }
    
    
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 5.0, 20.0, 20.0)];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    UIImage *image = [[CommonMethods sharedInstance] useColor:[UIColor colorWithRed:65.0/255.0 green:141.0/255.0 blue:240.0/255.0 alpha:1.0] forImage:[UIImage imageNamed:@"back arrow.png"]];
    imageView.image = image;
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0.0, 64.0, SCREEN_WIDTH, 1.0)];
    [self.view addSubview:view];
    view.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor darkGrayColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
    
//    self.navigationController.navigationBar.layer.shadowColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0].CGColor;
//    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
//    self.navigationController.navigationBar.layer.shadowRadius = 5.0f;
//    self.navigationController.navigationBar.layer.shadowOpacity = 0.3f;
//    self.navigationController.navigationBar.layer.masksToBounds=NO;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor colorWithRed:40.0/255.0 green:107.0/255.0 blue:240.0/255.0 alpha:1.0].CGColor;
//    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
//    self.navigationController.navigationBar.layer.shadowRadius = 7.0f;
//    self.navigationController.navigationBar.layer.shadowOpacity = 0.7f;
//    self.navigationController.navigationBar.layer.masksToBounds=NO;
}

//- (void)setStatusBarBackgroundColor:(UIColor *)color {
//
//    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
//
//    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
//        statusBar.backgroundColor = color;
//    }
//}

- (void)tableTapped:(UITapGestureRecognizer *)tap
{
    [self.view endEditing:true];
    textViewChat.textColor = [UIColor lightGrayColor];
    textViewChat.text = @"Send message";
    [textViewChat resignFirstResponder];
}



#pragma mark - Back Button pressed

-(void)BackButtonPressed{
    [textViewChat resignFirstResponder];
    sleep(0.5);
    self.tabBarController.tabBar.hidden = true;
    [appDelegate.socket unSubscribeChannel:@"ConversationChannel"];
   // [self setStatusBarBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"topstrip3.png"] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.layer.shadowOpacity = 1.0;
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - TextView Delegate

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    textViewChat.text = @"";
    textViewChat.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    
    //    if(textViewChat.text.length == 0){
    //        textViewChat.textColor = [UIColor lightGrayColor];
    //        textViewChat.text = @"Your message...";
    //        [textViewChat resignFirstResponder];
    //    }
}

#pragma mark- Keyboard
- (void)keyboardWasShown:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    lcBottom.constant = keyboardSize.height;
   
    [UIView animateWithDuration:0.1
                     animations:^{
                         
                         tableViewChat.contentOffset = CGPointMake(0, tableViewChat.contentOffset.y+keyboardSize.height);
//                         [self scrollToBottom];
                         [self.view layoutIfNeeded];
                     }];
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    lcBottom.constant = 0;
    [UIView animateWithDuration:0.2
                     animations:^{
                         
                         tableViewChat.contentOffset = CGPointMake(0, tableViewChat.contentOffset.y-keyboardSize.height);
                         [self.view layoutIfNeeded];
                     }];
}



#pragma mark - Method for Scrolling table to bottom

- (void)scrollToBottom{
    CGFloat yOffset = 0;
    if (tableViewChat.contentSize.height > tableViewChat.bounds.size.height) {
        yOffset = tableViewChat.contentSize.height - tableViewChat.bounds.size.height;
    }
    [tableViewChat setContentOffset:CGPointMake(0, yOffset) animated:NO];
}


#pragma mark - TableView Delegates and datasource

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([[_arrCurrentUser valueForKey:@"id"] intValue] == [[[arrMessages objectAtIndex:indexPath.row] valueForKey:@"sender_id"] intValue]) {
        static NSString *simpleTableIdentifier = @"SenderTableViewCell";
        SenderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil) {
            NSArray *nib =[[NSBundle mainBundle]loadNibNamed:simpleTableIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.labelText.text = [[arrMessages objectAtIndex:indexPath.row] valueForKey:@"body"];
        cell.viewText.layer.cornerRadius = 10.0;
        cell.viewText.layer.masksToBounds = true;
        return cell;
        
    }
    else{
        static NSString *simpleTableIdentifier = @"RecieverTableViewCell";
        RecieverTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil) {
            NSArray *nib =[[NSBundle mainBundle]loadNibNamed:simpleTableIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.labelText.text = [[arrMessages objectAtIndex:indexPath.row] valueForKey:@"body"];
        
        NSURL *url = [NSURL URLWithString:strUrl];
        [cell.imageViewReciever sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        NSString *strDate = [[arrMessages objectAtIndex:indexPath.row]valueForKey:@"sent_at"];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
        [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
        NSDate  *Date = [dateFormat dateFromString:strDate];
        NSTimeZone *outputTimeZone = [NSTimeZone systemTimeZone];
        NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
        [outputDateFormatter setTimeZone:outputTimeZone];
        [outputDateFormatter setDateFormat:@"hh:mm a"];
        NSString *outputString = [outputDateFormatter stringFromDate:Date];
        cell.labelTime.text = outputString;
        cell.viewText.layer.cornerRadius = 10.0;
        cell.viewText.layer.masksToBounds = true;
        
        return cell;
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrMessages count];
}

-(IBAction)SendMessgaeToRecipient:(id)sender{
    if (textViewChat.text.length > 0 && ![textViewChat.text isEqualToString:@"Send message..."]) {
        if ([[CommonMethods sharedInstance]checkInternetConnection]) {
            NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
            NSString *strAuthToken =[[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:[NSNumber numberWithInt:[[self.arrConversations valueForKey:@"id"] intValue]] forKey:@"conversation_id"];
            if ([[_arrCurrentUser valueForKey:@"id"] intValue] == [[[self.arrConversations valueForKey:@"sender"] valueForKey:@"id"] intValue]) {
                [dict setObject:[NSNumber numberWithInt:[[[self.arrConversations valueForKey:@"recipient"] valueForKey:@"id"] intValue]] forKey:@"recipient_id"];
            }
            else{
                [dict setObject:[NSNumber numberWithInt:[[[self.arrConversations valueForKey:@"sender"] valueForKey:@"id"] intValue]] forKey:@"recipient_id"];
            }
            [dict setObject:textViewChat.text forKey:@"body"];
            NSDate *date = [NSDate date];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
            [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
            NSString  *strDate = [dateFormat stringFromDate:date];
            NSLog(@"Date %@",date);
            [dict setObject:strDate forKey:@"sent_at"];
            NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
            [dictResponse setObject:dict forKey:@"messages"];
            [dictResponse setObject:[NSNumber numberWithBool:0] forKey:@"is_read"];
            buttonSendMessage.userInteractionEnabled = false;
            [WebServiceManager postRequestWithUrlString:[NSString stringWithFormat:@"%@messages",kBaseUrl] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
                textViewChat.text = @"";
                buttonSendMessage.userInteractionEnabled = true;
                NSLog(@"%@",result);
            } failure:^(NSString *msg, BOOL success) {
                buttonSendMessage.userInteractionEnabled = true;
            }];
            
        }
        else{
            [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
        }
    }
}

-(void)socketDidReceiveMessage:(NSDictionary *)messageInfo{
    NSDictionary *dictMessage = messageInfo[@"message"];
    textViewChat.text = @"";
    buttonSendMessage.userInteractionEnabled = true;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
    [dict setObject:[dictMessage valueForKey:@"body"] forKey:@"body"];
    [dict setObject:[dictMessage valueForKey:@"sender_id"] forKey:@"sender_id"];
    [dict setObject:[dictMessage valueForKey:@"id"] forKey:@"id"];
    [dict setObject:[dictMessage valueForKey:@"recipient_id"] forKey:@"recipient_id"];
    [dict setObject:[dictMessage valueForKey:@"sent_at"] forKey:@"sent_at"];
    [dict setObject:[dictMessage valueForKey:@"is_read"] forKey:@"is_read"];
    
    [arrMessages addObject:dict];
    NSArray *paths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:[arrMessages count]-1 inSection:0]];
    [tableViewChat insertRowsAtIndexPaths:paths withRowAnimation:(UITableViewRowAnimationFade)];  //or a rowAnimation of your choice
    [tableViewChat endUpdates];
    // [self scrollToBottom];
    [tableViewChat scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[arrMessages count] -1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"is_read = 0 AND recipient_id = %@",[_arrCurrentUser valueForKey:@"id"]]];
    NSArray *arrUnRead = [arrMessages filteredArrayUsingPredicate:predicate]; //[arrMessages filterUsingPredicate:predicate];
    NSArray *arrUnreadListIds = [arrUnRead valueForKeyPath:@"@distinctUnionOfObjects.id" ];
    NSLog(@"%@", arrUnreadListIds);
    NSLog(@"%@", arrUnRead);
    [self updateTheUnreadStatus:arrUnreadListIds];
}

-(void)updateTheUnreadStatus:(NSArray *)arrUnreadIDs{
    NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
    [dictResponse setObject:[NSNumber numberWithInt:[[self.arrConversations valueForKey:@"id"] intValue]] forKey:@"conversation_id"];
    [dictResponse setObject: arrUnreadIDs forKey:@"message_ids"];
    NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
    NSString *strAuthToken =[[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
    [WebServiceManager putRequestWithUrlString:[NSString stringWithFormat:@"%@messages",kBaseUrl] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
        
    } failure:^(NSString *msg, BOOL success) {
        
    }];
}

-(void)userStatusDidChangeWithData:(NSDictionary *)userInfo {
    if ([[_arrCurrentUser valueForKey:@"id"] intValue] == [[[self.arrConversations valueForKey:@"sender"] valueForKey:@"id"] intValue] ){
        if ([[userInfo valueForKey:@"user"] integerValue] == [[[self.arrConversations valueForKey:@"recipient"] valueForKey:@"id"]intValue]){
            if ([[userInfo valueForKey:@"online"] isEqualToString:@"on"]){
                [buttonOnline setImage:[UIImage imageNamed:@"online"] forState:UIControlStateNormal];
                
            }
            else{
                [buttonOnline setImage:[UIImage imageNamed:@"offline"] forState:UIControlStateNormal];
            }
        }
    }
    else{
        if ([[userInfo valueForKey:@"user"] integerValue] == [[[self.arrConversations valueForKey:@"sender"] valueForKey:@"id"]intValue]){
            if ([[userInfo valueForKey:@"online"] isEqualToString:@"on"]){
                [buttonOnline setImage:[UIImage imageNamed:@"online"] forState:UIControlStateNormal];
                
            }
            else{
                [buttonOnline setImage:[UIImage imageNamed:@"offline"] forState:UIControlStateNormal];
            }
        }
    }
}


@end
