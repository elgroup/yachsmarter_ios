/*
 * Copyright 2016 Google Inc. All rights reserved.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

/**
 * To use GooglePlacesDemos, please register an API Key for your application and set it here. Your
 * API Key should be kept private.
 *
 * See documentation on getting an API Key for your API Project here:
 * https://developers.google.com/places/ios-api/start#get-key
 */

//#error Register your API key and insert here, then delete this line.
//static NSString *const kAPIKey = @"AIzaSyAZlt28-5-0KPrKpmPXtuQzY7rj4bESwbY";
//AIzaSyAZlt28-5-0KPrKpmPXtuQzY7rj4bESwbY
//AIzaSyC6o1_WYAYkc13Fcc6mVfZ0yIObOM4mGAM
//Pictium
//AIzaSyDiuSGYNI2AXmL77lPjTpfvDakQYrgHQ6s
//StyleUp
//AIzaSyBvxPNaAyoeGfhhbiU53riAQZQjR5OME8o
//AIzaSyBvINYOcbAiz0ieyu7Ur_zfdbFoDVF8NyU
//AIzaSyCZxMQj4vJcai4JlpSBHhfL22E8vmw1I4Y

static NSString *const kAPIKey = @"AIzaSyBvINYOcbAiz0ieyu7Ur_zfdbFoDVF8NyU";

//AIzaSyDQ5quO-pab7qy27XfY1UrRxt17hXuakIs
