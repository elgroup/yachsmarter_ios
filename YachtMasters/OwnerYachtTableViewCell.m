//
//  OwnerYachtTableViewCell.m
//  YachtMasters
//
//  Created by Anvesh on 05/07/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "OwnerYachtTableViewCell.h"

@implementation OwnerYachtTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _viewBackGround.layer.cornerRadius  = 10.0;
    _viewBackGround.clipsToBounds = false;
    
    // Shadow
    _viewBackGround.layer.shadowColor = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1] .CGColor;
    _viewBackGround.layer.shadowOffset = CGSizeMake(1.0, 1.0);
    _viewBackGround.layer.shadowOpacity = 3.0;
    _viewBackGround.layer.shadowRadius = 5.0;
    _viewBackGround.layer.masksToBounds = false;
    // Initialization code
}
- (void)layoutSubviews {
    [super layoutSubviews];
    dispatch_async(dispatch_get_main_queue(), ^{
        _imageViewShip = (UIImageView *)[self roundCornersOnView:_imageViewShip onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10.0];
    });
    
    
}


- (UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius
{
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0;
        if (tl) corner = corner | UIRectCornerTopLeft;
        if (tr) corner = corner | UIRectCornerTopRight;
        if (bl) corner = corner | UIRectCornerBottomLeft;
        if (br) corner = corner | UIRectCornerBottomRight;
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    }
    return view;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
