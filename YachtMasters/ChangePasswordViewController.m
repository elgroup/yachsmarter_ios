//
//  ChangePasswordViewController.m
//  YachtMasters
//
//  Created by Anvesh on 12/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "Constants.h"
#import "WebServiceManager.h"
#import "Validation.h"
#import "CommonMethods.h"
#import "LoginViewController.h"
#import "MBProgressHUD.h"
#import "YachtMasters-Swift.h"


@interface ChangePasswordViewController ()<UITextFieldDelegate>{
    __weak IBOutlet UITextField *textFieldPassword;
    __weak IBOutlet UITextField *textFieldConfirmPassword;
    __weak IBOutlet UIButton *buttonSave;
    __weak IBOutlet UIButton *buttonSignUp;
    __weak IBOutlet UITextField *textFieldCode;
    MBProgressHUD *hud;
}
@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = true;
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"gbpic"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    UIColor *color = [UIColor whiteColor];
    textFieldPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    textFieldConfirmPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: color}];
    textFieldCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Code" attributes:@{NSForegroundColorAttributeName: color}];
    
    buttonSave.layer.cornerRadius = 20.0;
    buttonSave.clipsToBounds = true;
    
    // Do any additional setup after loading the view.
}
- (IBAction)buttonSavePressed:(id)sender {
    if (textFieldPassword.text == 0) {
        [[CommonMethods sharedInstance]AlertMessage:@"Enter the password" andViewController:self];
        return;
    }
    else if (textFieldPassword.text.length <8){
        [[CommonMethods sharedInstance]AlertMessage:@"Password must be between 8 to 15 characters" andViewController:self];
        return;
    }
    else if (textFieldConfirmPassword.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Enter the Confirm password" andViewController:self];
        return;
    }
    else if (![Validation validatePassword:textFieldPassword ConfirmPassword:textFieldConfirmPassword]){
        [[CommonMethods sharedInstance]AlertMessage:@"Password is not same" andViewController:self];
        return;
    }
    else if (textFieldCode.text == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Enter the code recieved in your mail id" andViewController:self];
        return;
    }
    else if (textFieldCode.text.length < 5){
        [[CommonMethods sharedInstance]AlertMessage:@"Enter the valid code" andViewController:self];
        return;
    }
    else{
        if ([[CommonMethods sharedInstance]checkInternetConnection]) {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:textFieldCode.text forKey:@"otp"];
            [dict setObject:textFieldPassword.text forKey:@"password"];
            NSMutableDictionary *dictRequest = [[NSMutableDictionary alloc]init];
            [dictRequest setObject:dict forKey:@"users"];
            dispatch_async(dispatch_get_main_queue(), ^{
                //                hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                //                hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
                [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
            });
            [WebServiceManager PutRequest:[NSString stringWithFormat:@"%@users/password",kBaseUrl] andParam:dictRequest andcompletionhandler:^(NSArray *returArray, NSError *error){
                if (!error) {
                    //                       [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [[LoaderNew sharedLoader]hideLoader];
                    if ([[returArray valueForKey:@"status"] boolValue]){
                        //
                        UIAlertController * alert = [UIAlertController
                                                     alertControllerWithTitle:@""
                                                     message:@"Password successfully changed"
                                                     preferredStyle:UIAlertControllerStyleAlert];
                        
                        //Add Buttons
                        
                        UIAlertAction* yesButton = [UIAlertAction
                                                    actionWithTitle:@"Done"
                                                    style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action) {
                                                        [self.navigationController popToRootViewControllerAnimated:true];
                                                    }];
                        [alert addAction:yesButton];
                        [self presentViewController:alert animated:YES completion:nil];
                    }else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                            [[LoaderNew sharedLoader]hideLoader];
                            [[CommonMethods sharedInstance]AlertMessage:[returArray valueForKey:@"message"] andViewController:self];
                        });
                        
                    }
                }
                else{
                    NSLog( @"%@",error);
                }
                
            }];
            
        }
    }
    
}
- (IBAction)buttonBackPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
- (IBAction)buttonSignUpPressed:(id)sender {
}
- (IBAction)Tapped:(id)sender {
    [self.view endEditing:true];
    if (IS_IPHONE_5 ) {
        [UIView animateWithDuration:0.25 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, 0.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
        }];
    }
    
}


#pragma mark - textfield delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == textFieldPassword) {
        [textField becomeFirstResponder];
        if (IS_IPHONE_5 ) {
            [UIView animateWithDuration:0.25 animations:^{
                self.view.frame = CGRectMake(self.view.frame.origin.x, 0.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
            }];
        }
    }
    else if (textField == textFieldConfirmPassword){
        [textFieldPassword resignFirstResponder];
        [textFieldConfirmPassword becomeFirstResponder];
        if (IS_IPHONE_5 ) {
            [UIView animateWithDuration:0.25 animations:^{
                self.view.frame = CGRectMake(self.view.frame.origin.x, -50, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-50);
            }];
        }
        
    }
    else if (textField == textFieldCode){
        [textFieldConfirmPassword resignFirstResponder];
        [textField becomeFirstResponder];
        if (IS_IPHONE_5 ) {
            [UIView animateWithDuration:0.25 animations:^{
                self.view.frame = CGRectMake(self.view.frame.origin.x, -50, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-50);
            }];
        }
        
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == textFieldPassword) {
        [textField resignFirstResponder];
        [textFieldConfirmPassword becomeFirstResponder];
        if (IS_IPHONE_5 ) {
            [UIView animateWithDuration:0.25 animations:^{
                self.view.frame = CGRectMake(self.view.frame.origin.x, -50, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-50);
            }];
        }
        
    }
    else if (textField == textFieldConfirmPassword){
        [textField resignFirstResponder];
        [textFieldCode becomeFirstResponder];
        if (IS_IPHONE_5 ) {
            [UIView animateWithDuration:0.25 animations:^{
                self.view.frame = CGRectMake(self.view.frame.origin.x, -50, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-50);
            }];
        }
        
    }
    else if (textField == textFieldCode){
        [textField resignFirstResponder];
        if (IS_IPHONE_5 ) {
            [UIView animateWithDuration:0.25 animations:^{
                self.view.frame = CGRectMake(self.view.frame.origin.x, 0.0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
            }];
        }
        
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
