//
//  OwnerDetailTVC.h
//  YachtMasters
//
//  Created by Anvesh on 27/07/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OwnerDetailTVC : UITableViewController{
    
    __weak IBOutlet UIButton *buttonBack;
    __weak IBOutlet UIImageView *imageViewProfilePicOwner;
    __weak IBOutlet UIButton *buttonFollow;
    __weak IBOutlet UIButton *buttonTick;
    __weak IBOutlet UIButton *buttonCall;
    __weak IBOutlet UIButton *buttonMessage;
    __weak IBOutlet UILabel *labelAddress;
    __weak IBOutlet UILabel *labelOwnerName;
    __weak IBOutlet UILabel *labelDescription;
    __weak IBOutlet UILabel *labelReview;
    __weak IBOutlet UIImageView *imageViewStar5;
    __weak IBOutlet UIImageView *imageViewStar4;
    __weak IBOutlet UIImageView *imageViewStar3;
    __weak IBOutlet UIImageView *imageViewStar2;
    __weak IBOutlet UIImageView *imageViewStar1;
    __weak IBOutlet UICollectionView *collectionViewYachts;
    __weak IBOutlet UIButton *buttonReadMore;
    
}
@property(nonatomic,strong)NSString *strId;
@property(nonatomic)float lat;
@property(nonatomic)float lang;

@end
