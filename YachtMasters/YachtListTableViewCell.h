//
//  YachtListTableViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 29/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YachtListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewBackGround;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewShip;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewRound;
@property (weak, nonatomic) IBOutlet UILabel *labelYachtName;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewGroup;
@property (weak, nonatomic) IBOutlet UILabel *labelNumberOfGroups;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewStar1;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewStar2;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewStar3;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewStar4;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewStar5;
@property (weak, nonatomic) IBOutlet UILabel *labelPricing;
@property (weak, nonatomic) IBOutlet UIButton *buttonGetYachtOnMap;
@property (weak, nonatomic) IBOutlet UIButton *buttonFavorite;

@end
