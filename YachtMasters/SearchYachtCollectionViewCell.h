//
//  SearchYachtCollectionViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 08/09/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchYachtCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelCityname;
@property (weak, nonatomic) IBOutlet UILabel *labelSubDistrict;
@property (weak, nonatomic) IBOutlet UILabel *labelNumberOfBookings;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewBackgrund;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewAnchor;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewShip;
@end
