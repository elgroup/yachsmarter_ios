//
//  SearchYachtViewController.h
//  YachtMasters
//
//  Created by Anvesh on 27/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchDelegate <NSObject>

@required

- (void) CityList:(NSMutableArray *)arrCityList;

@end

@interface SearchYachtViewController : UIViewController

@property (nonatomic,strong) id<SearchDelegate> delegate;

-(void)ApiForCities;

@property(nonatomic,strong)NSMutableArray *arrResponse;

@property (nonatomic, assign) BOOL isLoading;

@property (assign, nonatomic) BOOL hasNextPage;

@property (assign, nonatomic) int currentPage;

@end
