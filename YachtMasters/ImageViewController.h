//
//  ImageViewController.h
//  Sho N Go
//
//  Created by Navyug on 01/04/15.
//  Copyright (c) 2015 otpapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController

@property(nonatomic, retain) NSMutableArray *selectedImages;
@property(nonatomic, assign) int index;
@property(nonatomic, assign) BOOL deletionNotAllowed;
//@property (strong, nonatomic) Product * product;
//@property (strong, nonatomic) Vendor  * vendor;

@end
