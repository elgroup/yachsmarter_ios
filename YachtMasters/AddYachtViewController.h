//
//  AddYachtViewController.h
//  YachtMasters
//
//  Created by Anvesh on 13/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface AddYachtViewController : UIViewController
{
    __weak IBOutlet UIView *viewDeleteImage;
    
    __weak IBOutlet UICollectionView *collectionViewDeleteImage;
    __weak IBOutlet UIView *buttonCancel;
}
@property BOOL isEdit;
@property (nonatomic,strong) NSMutableArray *arrYachtDetail;
@end
