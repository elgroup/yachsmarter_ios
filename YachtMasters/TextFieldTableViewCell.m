//
//  TextFieldTableViewCell.m
//  YachtMasters
//
//  Created by Anvesh on 13/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "TextFieldTableViewCell.h"
#import "RPFloatingPlaceholderTextField.h"

@implementation TextFieldTableViewCell
{
    __weak IBOutlet RPFloatingPlaceholderTextField *textField;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
