//
//  AboutUsViewController.m
//  YachtMasters
//
//  Created by Anvesh on 24/10/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "AboutUsViewController.h"
#import "Constants.h"
#import "AboutUsCollectionViewCell.h"
#import <CoreText/CoreText.h>
#import "CustomNavigationViewController.h"

@interface AboutUsViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    IBOutlet UICollectionView *collectionViewAboutUs;
    IBOutlet UIPageControl *pcAboutUs;
}
@end

@implementation AboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBarController.hidesBottomBarWhenPushed = true;
    self.tabBarController.tabBar.hidden = true;
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.isSWReveal) {
        [self CustomNavigationBarForSWReveal];
    }
    else
        [self CustomNavigationBar];
}


-(void)CustomNavigationBar{
    
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"topstrip3.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor colorWithRed:40.0/255.0 green:107.0/255.0 blue:240.0/255.0 alpha:1.0].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    self.navigationController.navigationBar.layer.shadowRadius = 7.0f;
    self.navigationController.navigationBar.layer.shadowOpacity = 1.0f;
    self.navigationController.navigationBar.layer.masksToBounds=NO;
    
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    //[backButton setBackgroundColor:[UIColor redColor]];
    [backButton addTarget:self action:@selector(ButtonBackPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 10.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    self.title  = @"About";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}

-(void)CustomNavigationBarForSWReveal{
    self.tabBarController.tabBar.hidden =  true;
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    UIImage* image3;
    
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    if (self.isSWReveal) {
        if(IS_IPHONE_X){
            self.navigationController.navigationBar.frame = CGRectMake(0.0, 45.0, SCREEN_WIDTH, 94);
        }
        else{
            self.navigationController.navigationBar.frame = CGRectMake(0.0, 20.0, SCREEN_WIDTH, 44);
        }
        
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"topstrip3"] forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.navigationBar setShadowImage:[UIImage new]];
        self.navigationController.navigationBar.layer.shadowColor = [UIColor colorWithRed:40.0/255.0 green:107.0/255.0 blue:240.0/255.0 alpha:1.0].CGColor;
        self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
        self.navigationController.navigationBar.layer.shadowRadius = 7.0f;
        self.navigationController.navigationBar.layer.shadowOpacity = 1.0f;
        self.navigationController.navigationBar.layer.masksToBounds=NO;
    }
    image3  = [UIImage imageNamed:@"back arrow.png"];
    [backButton addTarget:self action:@selector(ButtonBackPressed) forControlEvents:UIControlEventTouchUpInside];
    //   }
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 5.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    self.navigationController.navigationBar.topItem.leftBarButtonItem = leftBarButton;
    self.title  = @"About";
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //imageViewNav.image = nil;
    
}
-(void)ButtonBackPressed{
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = true;
    if (_isAboutUs) {
        self.tabBarController.tabBar.hidden = false;
    }
    [self.navigationController popViewControllerAnimated:true];
}


#pragma mark- Collection View Delegate and Data Source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 3;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    AboutUsCollectionViewCell *cell = [collectionViewAboutUs dequeueReusableCellWithReuseIdentifier:@"AboutUsCollectionViewCell" forIndexPath:indexPath];
    if (cell == nil)    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"AboutUsCollectionViewCell" owner:self options:nil];
        cell = nibArray[0];
    }
    
    if (indexPath.row == 0) {
        cell.labelHeading.text = @"Who Are We!";
        cell.labelContent.text = @"Unique app for  all of the world's A-listers who demand nothing but the finest yachting experience. One stop shop for global access to ultimate offshore luxury. Indulge in the breathtaking scenery, romantic getaways, glamorous parties and blue open water. Yachtsmarter is the perfect partner and planner which can⁝ find book and customize a perfect offshore vacation experience.";
    }
    else if (indexPath.row == 1){
        
        cell.labelHeading.text = @"For Charterers\n\n";
        NSString *yourString = @"With Yachtsmarter booking a yacht is as easy as booking a hotel. It works on peer-to-peer yacht charter system. You can find a yacht to charter, or offer your yacht for charter and earn extra income. We provide multiple options around the globe at most exotic locations and through our in-app chat feature connect you directly with yacht owners to answer all possible queries, ensuring a smooth mesmerizing trip of a lifetime.";
        
        cell.labelContent.text = yourString;
    }
    else if (indexPath.row == 2){
        cell.labelHeading.text = @"For Yacht Owners\n\n";
        NSString *yourString = @"Yacht owners can earn big bucks from their underused yachts as YatchSmarter will connect prospect clients to the owners directly. All transactions are 100% secure and earnings for the boat owners are deposited directly to their own bank accounts. With Yachtsmarter possibilities are endless.";
        cell.labelContent.text = yourString;
    }
    else{
        cell.labelContent.text = @"";
    }
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(collectionViewAboutUs.frame.size.width, collectionViewAboutUs.frame.size.height);
    // return CGSizeMake(100, 60);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //    [self ApiforGettingYachtList];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    int count = (scrollView.contentOffset.x/scrollView.frame.size.width);
    pcAboutUs.currentPage = count;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)twitterButtonPressed:(id)sender {
    [self openScheme:@"https://twitter.com/yachtsmarter"];
    
}
- (IBAction)faceBookButtonPressed:(id)sender {
     [self openScheme:@"https://www.facebook.com/yachtsmarter/"];
    
}
- (IBAction)instaGramButtonPressed:(id)sender {
    [self openScheme:@"https://instagram.com/yachtsmarterapp"];
}

- (void)openScheme:(NSString *)scheme {
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:scheme];
    
    if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [application openURL:URL options:@{}
           completionHandler:^(BOOL success) {
               NSLog(@"Open %@: %d",scheme,success);
           }];
    } else {
        //
//        NSDictionary *options = @{UIApplicationOpenURLOptionUniversalLinksOnly : @YES};
//        [application openURL:URL options:options completionHandler:nil];
        BOOL success = [application openURL:URL];
        NSLog(@"Open %@: %d",scheme,success);
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
