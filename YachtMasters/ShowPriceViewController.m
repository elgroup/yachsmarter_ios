//
//  ShowPriceViewController.m
//  YachtMasters
//
//  Created by Anvesh on 18/09/18.
//  Copyright © 2018 EL. All rights reserved.
//

#import "ShowPriceViewController.h"
#import "PriceListTableViewCell.h"
#import <CoreGraphics/CoreGraphics.h>

@interface ShowPriceViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableViewPriceList;


@end

@implementation ShowPriceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.viewBar.layer.cornerRadius = 2.5;
    self.viewBar.clipsToBounds = true;
    self.viewBottom.layer.cornerRadius = 20.0;
    self.viewPrices.layer.cornerRadius = 20.0;
    self.viewPrices.clipsToBounds = true;
    
}


- (IBAction)buttonBackPressed:(id)sender {
    //    [UIView animateWithDuration:0.4
    //                          delay:0.0
    //                        options:UIViewAnimationOptionCurveEaseIn
    //                     animations:^{
    //                         self.viewBottom.frame = CGRectMake(0.0, self.view.frame.size.height, self.view.frame.size.width, 270);
    //                     } completion:^(BOOL finished){
    //                         [self willMoveToParentViewController:nil];
    //                         [self.view removeFromSuperview];
    //                         [self removeFromParentViewController];
    //                     }];
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

#pragma mark - Table View Delegate and data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrPrices.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"PriceListTableViewCell";
    PriceListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        NSArray *nib =[[NSBundle mainBundle]loadNibNamed:simpleTableIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSString *strTerm = [[self.arrPrices objectAtIndex:indexPath.row] valueForKey:@"term"];
    if ([strTerm isEqualToString:@"per_hour"]){
        strTerm = @"Per Hour";
    }
    else if ([strTerm isEqualToString:@"per_day"]){
        strTerm = [NSString stringWithFormat:@"Per Day(%ld hours)",[[[self.arrPrices objectAtIndex:indexPath.row]valueForKey:@"hours"]integerValue]];
    }
    else if ([strTerm isEqualToString:@"week"]){
        strTerm = @"Per Week";
    }
    else if ([strTerm isEqualToString:@"half_day"]){
        strTerm = [NSString stringWithFormat:@"Half Day (%ld Hours)",[[[self.arrPrices objectAtIndex:indexPath.row]valueForKey:@"hours"]integerValue]];
    }
    cell.labelDuration.text = strTerm;
    cell.labelPrice.text =  [NSString stringWithFormat:@"%@ %0.2f",self.strCurrencySymbol, [[[self.arrPrices objectAtIndex:indexPath.row] valueForKey:@"price"] floatValue]];
    return cell;
} 

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
