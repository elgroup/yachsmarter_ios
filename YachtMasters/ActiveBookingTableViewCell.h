//
//  ActiveBookingTableViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 28/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActiveBookingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewBackGround;
@property (weak, nonatomic) IBOutlet UIView *viewRound;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewRound;
@property (weak, nonatomic) IBOutlet UILabel *labelEnjoyYachting;
@property (weak, nonatomic) IBOutlet UILabel *labelActiveNow;
@property (weak, nonatomic) IBOutlet UILabel *labelYachtPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelYachtName;
@property (weak, nonatomic) IBOutlet UILabel *labelYachtAddress;
@property (weak, nonatomic) IBOutlet UIButton *buttonMessageOwner;
@property (weak, nonatomic) IBOutlet UIButton *buttonReview;

@end
