//
//  CreditCard-Validator.h
//  CreditCard-Validator-ObjC
//
//  Created by Fernando Bass on 1/8/13.
//  Copyright (c) 2013 Fernando Bass. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum CardType{
    
    CreditCardTypeInvalid = 0 ,
    CreditCardTypeMasterCard,
    CreditCardTypeMaestroCard,
    CreditCardTypeAmex,
    CreditCardTypeDinersClub,
    CreditCardTypeDiscover,
    CreditCardTypeVisa,
    CreditCardTypeJCB,
    CreditCardTypeUnknown
    
}CreditCardType;

@interface CreditCard_Validator : NSObject

+ (CreditCardType)getCreditCardTypeFromString:(NSString *) string;
+ (NSPredicate *) predicateForType:(CreditCardType) type;
+ (NSString*)getCreditCard:(CreditCardType) cardType;
+ (BOOL)checkCreditCardNumber:(NSString *)cardNumber;

@end
