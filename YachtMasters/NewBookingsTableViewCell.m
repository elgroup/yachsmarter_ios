//
//  NewBookingsTableViewCell.m
//  YachtMasters
//
//  Created by Anvesh on 17/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "NewBookingsTableViewCell.h"
#import "CommonMethods.h"

@implementation NewBookingsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _viewBackGround.layer.shadowColor = [UIColor colorWithRed:0.0/255.0 green:196.0/255.0 blue:56.0/255.0 alpha:1].CGColor;
    _viewBackGround.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    _viewBackGround.layer.shadowOpacity = 0.3;
    _viewBackGround.layer.shadowRadius = 5;
    _viewBackGround.layer.masksToBounds = false;
    _viewBackGround.layer.cornerRadius = 5.0;
    _viewImageBackGround.layer.cornerRadius = _viewImageBackGround.frame.size.height/2;
    _imageViewUser.layer.cornerRadius = _imageViewUser.frame.size.height/2;
    _imageViewUser.clipsToBounds = true;
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
