//
//  WalletViewController.h
//  YachtMasters
//
//  Created by Anvesh on 30/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletViewController : UIViewController
@property NSString *strAvailableWalletAmount;
@property NSString *strPendingAmount;
@property BOOL isNotTransferable;
@property BOOL isAccountCreated;
@end
