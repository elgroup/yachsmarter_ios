//
//  YachtListViewController.m
//  YachtMasters
//
//  Created by Anvesh on 21/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "YachtListViewController.h"
#import "CityListCollectionViewCell.h"
#import "YachtListTableViewCell.h"
#import "YachtDetailTableViewController.h"
#import "Constants.h"
#import "OwnerYachtTableViewCell.h"
#import "AddYachtViewController.h"
#import "MBProgressHUD.h"
#import "WebServiceManager.h"
#import "Constants.h"
#import "CommonMethods.h"
#import "SearchYachtViewController.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>
#import "YachtBookingsTableViewController.h"
#import "FilterViewController.h"
#import "CustomNavigationViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "AppDelegate.h"
#import "HomeTabBarController.h"
#import "UIImageView+WebCache.h"
#import "YachtMasters-Swift.h"



@interface YachtListViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegateFlowLayout,SearchDelegate,CLLocationManagerDelegate>
{
    NSInteger selectedButton;
    NSString *strRole;
    UIButton *buttonAdd;
    MBProgressHUD *hud;
    __weak IBOutlet UIView *viewCollectionView;
    NSMutableArray *arrResponse;
    IBOutlet NSLayoutConstraint *_lcCitiesCollection;
    NSIndexPath *selectedIndexPath;
    UIButton *buttonFilter;
    BOOL isApi;
    CustomNavigationViewController *customNavigation;
    CLLocationManager *locationManager;
    CLLocation *location;
    AppDelegate *appDelegate;
    BOOL isViewWillAppear;
    //    Loader *loader;
    LoaderNew *loader;
    UIButton  *btnHome;
    float minPrice;
    float maxPrice;
}

@property NSMutableArray *arrCityList;
@property (weak, nonatomic) IBOutlet UITableView *tableViewYachtList;
@property (strong, nonatomic)IBOutlet UICollectionView *collectionViewCityList;
@property (nonatomic) IBOutlet UIBarButtonItem* revealButtonItem;
@property (nonatomic,weak) IBOutlet UIView *viewBackCollectionView;
@end

@implementation YachtListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.originalOrigin = 424.0;
    
    [self ButtonAddYacht];
    selectedButton = 0;
    SearchYachtViewController *controller = [[SearchYachtViewController alloc]init];
    controller.delegate = self;
    selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    isApi = true;
    UINib *cellNib = [UINib nibWithNibName:@"CityListCollectionViewCell" bundle:nil];
    [self.collectionViewCityList registerNib:cellNib forCellWithReuseIdentifier:@"CityListCollectionViewCell"];
    self.automaticallyAdjustsScrollViewInsets = NO;
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self CustomNavigationBar];
    [self setNeedsStatusBarAppearanceUpdate];
    //self.tabBarController.tabBar.hidden = false;
    isViewWillAppear = true;
    isApi = true;
    arrResponse = [[NSMutableArray alloc]init];
    [self.collectionViewCityList reloadData];
    [self.tableViewYachtList reloadData];
    strRole = [[NSUserDefaults standardUserDefaults] valueForKey:@"role"];
    if ([strRole isEqualToString:Customer]) {
        self.navigationItem.title = @"Available Yacht";
        self.currentPage = 1;
        _lcCitiesCollection.constant = 60;
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
            [locationManager requestWhenInUseAuthorization];
        [locationManager startUpdatingLocation];
        buttonAdd.hidden = true;
        buttonFilter.hidden = false;
        btnHome.hidden = false;
        //[self.collectionViewCityList setItemSize:CGSizeMake(170, 300)];
    }
    else{
        self.navigationItem.title = @"My Yachts";
        _lcCitiesCollection.constant = 0;
        buttonAdd.hidden = false;
        buttonFilter.hidden = true;
        btnHome.hidden = true;
        [self ApiforGettingYachtList];
    }
    // [self.tableViewYachtList reloadData];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.tabBarController.tabBar.hidden = false;
    if (!isViewWillAppear) {
        [self setNeedsStatusBarAppearanceUpdate];
        [self CustomNavigationBar];
        isApi = true;
        //self.tabBarController.tabBar.hidden = false;
        arrResponse = [[NSMutableArray alloc]init];
        [self.tableViewYachtList reloadData];
        [_collectionViewCityList reloadData];
        //    [_collectionViewCityList.collectionViewLayout invalidateLayout];
        strRole = [[NSUserDefaults standardUserDefaults] valueForKey:@"role"];
        if ([strRole isEqualToString:Customer]) {
            self.navigationItem.title = @"Available Yacht";
            _lcCitiesCollection.constant = 60;
            locationManager = [[CLLocationManager alloc] init];
            locationManager.delegate = self;
            locationManager.distanceFilter = kCLDistanceFilterNone;
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
                [locationManager requestWhenInUseAuthorization];
            [locationManager startUpdatingLocation];
            buttonAdd.hidden = true;
            buttonFilter.hidden = false;
            btnHome.hidden = false;
            self.currentPage = 1;
        }
        else{
            self.navigationItem.title = @"My Yachts";
            _lcCitiesCollection.constant = 0;
            buttonAdd.hidden = false;
            buttonFilter.hidden = true;
            btnHome.hidden = true;
            [self ApiforGettingYachtList];
        }
    }
    
    if ([strRole isEqualToString:Customer]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // [self AddCollectionView];
        });
        
    }
    
    isViewWillAppear = false;
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    // [_collectionViewCityList removeFromSuperview];
    arrResponse = [[NSMutableArray alloc]init];
    [_tableViewYachtList reloadData];
    buttonAdd.hidden = true;
    if (hud) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[LoaderNew sharedLoader] hideLoader];
            //      [loader showLoaderOnScreenWithVc:<#(UIView * _Nonnull)#>];
        });
    }
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    // self.navigationController.navigationBar.frame = CGRectMake(0,0, [UIScreen mainScreen].bounds.size.width, 200);
    if (!isApi && [strRole isEqualToString:Customer] && [arrResponse count] > 0) {
        selectedButton = [[[NSUserDefaults standardUserDefaults]valueForKey:@"selectedIndex"] integerValue];
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        [_collectionViewCityList scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}

-(void)CityList:(NSMutableArray *)arrCityList{
    arrCityList = [[NSMutableArray alloc]init];
    arrCityList = [arrCityList mutableCopy];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            NSLog(@"User still thinking..");
            [self ApiforGettingYachtList];
            
        } break;
        case kCLAuthorizationStatusDenied: {
            NSLog(@"User hates you");
            [self ApiforGettingYachtList];
            
        } break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:{
            [locationManager startUpdatingLocation];
        }
            break;
        case kCLAuthorizationStatusAuthorizedAlways: {
            //Will update location immediately
        } break;
        default:
            break;
    }
}



#pragma mark - CLLocationManagerDelegate


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    location = newLocation;
    appDelegate.currentLocation = newLocation;
    
    if (location != nil) {
        NSString *strLongi =  [NSString stringWithFormat:@"%.8f", location.coordinate.longitude];
        NSString *strLAt = [NSString stringWithFormat:@"%.8f", location.coordinate.latitude];
        NSLog(@"LOngi =>%@   Lat =.%@", strLongi,strLAt);
    }
    [self ApiforGettingYachtList];
    [locationManager stopUpdatingLocation];
}

#pragma mark- ButtonAddYacht
-(void)ButtonAddYacht{
    buttonAdd = [UIButton buttonWithType:UIButtonTypeCustom];
    //[[UIScreen mainScreen]bounds].size.height - 130
    buttonAdd.frame = CGRectMake([[UIScreen mainScreen]bounds].size.width - 90, [[UIScreen mainScreen] bounds].size.height - self.tabBarController.tabBar.frame.size.height - 100 , 64.0, 64.0);
    buttonAdd.layer.cornerRadius = 32.0;
    buttonAdd.clipsToBounds = true;
    
    buttonAdd.layer.shadowColor = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1] .CGColor;
    buttonAdd.layer.shadowOffset = CGSizeMake(0, 1.0);
    buttonAdd.layer.shadowOpacity = 0.7;
    buttonAdd.layer.shadowRadius = 3.0;
    
    [buttonAdd setTitle:@"+" forState:UIControlStateNormal];
    buttonAdd.titleLabel.font = FontOpenSans(38);
    [buttonAdd setBackgroundColor:[UIColor colorWithRed:64.0/255.0 green:140.0/255.0 blue:240.0/255.0 alpha:1]];
    [buttonAdd addTarget:self action:@selector(ButtonAddPressed) forControlEvents:UIControlEventTouchUpInside];
    [[CommonMethods sharedInstance]setButtonShadow:buttonAdd andShadowcolor:[UIColor colorWithRed:57.0/255.0 green:130.0/255.0 blue:240.0/255.0 alpha:1.0]];
    [self.navigationController.view addSubview:buttonAdd];
    
    // buttonAdd.hidden = true;
}

-(void)ButtonAddPressed{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddYachtViewController *controller = [story instantiateViewControllerWithIdentifier:@"AddYachtViewController"];
    buttonAdd.hidden = true;
    [self.navigationController pushViewController:controller animated:true];
}
#pragma mark- Collection View

-(void)AddCollectionView{
    UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    _collectionViewCityList = [[UICollectionView alloc]initWithFrame:CGRectMake(_viewBackCollectionView.frame.origin.x,0.0, [[UIScreen mainScreen]bounds].size.width, 60.0) collectionViewLayout:flowLayout];
    [_collectionViewCityList setDataSource:self];
    [_collectionViewCityList setDelegate:self];
    UINib *cellNib1 = [UINib nibWithNibName:@"CityListCollectionViewCell" bundle:nil];
    [_collectionViewCityList registerNib:cellNib1 forCellWithReuseIdentifier:@"CityListCollectionViewCell"];
    [_collectionViewCityList setBackgroundColor:[UIColor whiteColor]];
    _collectionViewCityList.showsHorizontalScrollIndicator = NO;
    _collectionViewCityList.pagingEnabled = false;
    [_viewBackCollectionView addSubview:_collectionViewCityList];
}



#pragma mark - Custom Navigation BAr
-(void)CustomNavigationBar{
    
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    
    UIImage* image3 = [UIImage imageNamed:@"Three_Lines"];//[UIImage imageNamed:@"menu items.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton setImage:image3 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    buttonFilter = [[UIButton alloc] initWithFrame:CGRectMake([[UIScreen mainScreen]bounds ].size.width - 20, 10.0,30.0, 30.0)];
    [buttonFilter setImage:[UIImage imageNamed:@"levels-adjusment"] forState:UIControlStateNormal];
    [buttonFilter addTarget:self action:@selector(ButtonFilterPressed) forControlEvents:UIControlEventTouchUpInside];
    
    btnHome = [[UIButton alloc]initWithFrame:CGRectMake([[UIScreen mainScreen]bounds].size.width - 70, 10.0, 30.0, 30.0)];
    [btnHome setImage:[UIImage imageNamed:@"globe"] forState:UIControlStateNormal];
    [btnHome addTarget:self action:@selector(ButtonHomePressed) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightBarButtonItem1 =[[UIBarButtonItem alloc] initWithCustomView:buttonFilter];
    UIBarButtonItem *rightBarButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView: btnHome];
    self.navigationItem.rightBarButtonItems = @[rightBarButtonItem1,rightBarButtonItem2];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
    
    self.navigationController.navigationBar.layer.shadowColor = [UIColor colorWithRed:40.0/255.0 green:107.0/255.0 blue:240.0/255.0 alpha:1.0].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    self.navigationController.navigationBar.layer.shadowRadius = 7.0f;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.7f;
    self.navigationController.navigationBar.layer.masksToBounds=NO;
    // [[CommonMethods sharedInstance] NavigationBarShadow:self.navigationController];
}

#pragma mark - Button Filter Action

-(void)ButtonHomePressed{
    //[self.navigationController popViewControllerAnimated:true];
    appDelegate.dictFilterItems = [[NSMutableDictionary alloc]init];
    [((UINavigationController *)((UITabBarController *)((SWRevealViewController *)((UINavigationController *)((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController)))) popToRootViewControllerAnimated:true];
}
-(void)ButtonFilterPressed{
    FilterViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterViewController"];
    controller.location = location;
    controller.minPrice = minPrice;
    controller.maxPrice = maxPrice;
    [self.navigationController pushViewController:controller animated:true];
}


#pragma mark- Custom setup for reveal view

- (void)customSetup{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController ){
        [self.revealButtonItem setTarget: revealViewController];
        [self.revealButtonItem setAction: @selector(revealToggle:)];
        [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
}

-(void)revealToggle:(id)sender{
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggleAnimated:YES];
}

#pragma mark state preservation / restoration

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // Save what you need here
    [super encodeRestorableStateWithCoder:coder];
}


- (void)decodeRestorableStateWithCoder:(NSCoder *)coder{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // Restore what you need here
    [super decodeRestorableStateWithCoder:coder];
}


- (void)applicationFinishedRestoringState{
    NSLog(@"%s", __PRETTY_FUNCTION__);
    // Call whatever function you need to visually restore
    [self customSetup];
}

#pragma mark - Api for Getting Yacht List

-(void)ApiforGettingYachtList{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSInteger countryId = [[NSUserDefaults standardUserDefaults]integerForKey:@"CountryId"];
        NSString *strUrl = [[NSString alloc]init];
        if ([str isEqualToString:@"customer"]) {
            NSDictionary *dictPriceRange = [appDelegate.dictFilterItems valueForKey:@"PriceRange"];
            NSDictionary *dictGuests = [appDelegate.dictFilterItems valueForKey:@"NumberOfGuests"];
            NSDictionary *dictAmenities = [appDelegate.dictFilterItems valueForKey:@"SelectedAmenities"];
            strUrl = [NSString stringWithFormat:@"%@countries/%ld?latitude=%f&longitude=%f",kBaseUrlVersion2,countryId,location.coordinate.latitude,location.coordinate.longitude];
            //strUrl = [NSString stringWithFormat:@"%@countries/%ld?latitude=%s&longitude=%s",kBaseUrlVersion2,countryId,"4.570868","-74.297333"]; //Colombia
            // strUrl = [NSString stringWithFormat:@"%@countries/%ld?latitude=%s&longitude=%s",kBaseUrlVersion2,countryId,"25.034281","-77.396278"]; //Bahamas
            // strUrl = [NSString stringWithFormat:@"%@countries/%ld?latitude=%s&longitude=%s",kBaseUrlVersion2,countryId,"23.634501","-102.552788"]; //Mexico
            // strUrl = [NSString stringWithFormat:@"%@countries/%ld?latitude=%s&longitude=%s",kBaseUrlVersion2,countryId,"18.735693","-70.162651"]; //DR
            // strUrl = [NSString stringWithFormat:@"%@countries/%ld?latitude=%s&longitude=%s",kBaseUrlVersion2,countryId,"23.424076","53.847816"]; //UAE
            //strUrl = [NSString stringWithFormat:@"%@countries/%ld?latitude=%s&longitude=%s",kBaseUrlVersion2,countryId,"51.507351","-0.127758"]; //UK
            // strUrl = [NSString stringWithFormat:@"%@countries/%ld?latitude=%s&longitude=%s",kBaseUrlVersion2,countryId,"33.6088149","-83.170507"];
            if (!([dictPriceRange count] == 0)) {
                strUrl = [NSString stringWithFormat:@"%@&min_price=%@&max_price=%@",strUrl,[dictPriceRange valueForKey:@"min_price"],[dictPriceRange valueForKey:@"max_price"]];
                selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            }
            if (!([dictGuests count] == 0)) {
                strUrl = [NSString stringWithFormat:@"%@&max_guests=%@",strUrl,[dictGuests valueForKey:@"max_guests"]];
                selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            }
            if (!([dictAmenities count] == 0)) {
                if (!([[dictAmenities valueForKey:@"amenity_ids"] count] == 0)) {
                    NSArray *arr = dictAmenities[@"amenity_ids"];
                    arr = [arr valueForKeyPath:@"description"];
                    NSString *str = [arr componentsJoinedByString:@","];
                    str = [NSString stringWithFormat:@"[%@]",str];
                    strUrl = [NSString stringWithFormat:@"%@&amenity_ids=%@",strUrl,str];
                    selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                }
            }
        }
        else{
            strUrl =[NSString stringWithFormat:@"%@yachts",kBaseUrl];
        }
        buttonAdd.userInteractionEnabled = false;
        self.tabBarController.tabBar.userInteractionEnabled = false;
        [WebServiceManager getRequestUrlString:strUrl emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            NSLog(@"%@",result);
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
                if ([str isEqualToString:Customer]) {
                    arrResponse = [result valueForKey:@"cities"];
                    minPrice = [[result valueForKey:@"min_price"] floatValue];
                    maxPrice = [[result valueForKey:@"max_price"] floatValue];
                    if (!appDelegate.isAppLaunch) {
                        for (NSDictionary *dict in [arrResponse valueForKey:@"yachts"]) {
                            for (int i = 0; i < [dict count]; i++) {
                                [[SDImageCache sharedImageCache]removeImageForKey:[NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[dict valueForKey:@"owner_image"] objectAtIndex:i]] withCompletion:^{
                                }];
                            }
                        }
                        appDelegate.isAppLaunch = true;
                    }
                    isApi = false;
                    
                    if([[result valueForKey:@"total_pages"] integerValue] == self.currentPage){
                        self.hasNextPage = false;
                    }
                    else{
                        self.hasNextPage = true;
                    }
                    [_collectionViewCityList reloadData];
                    [self.tableViewYachtList reloadData];
                    //[self viewDidLayoutSubviews];
                }
                else{
                    arrResponse = [result valueForKey:@"yachts"];
                    isApi = false;
                    [self.tableViewYachtList reloadData];
                }
                [[LoaderNew sharedLoader]hideLoader];
                buttonAdd.userInteractionEnabled = true;
                self.tabBarController.tabBar.userInteractionEnabled = true;
                
            });
            
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@""
                                             message:@"Problem in loading the data. Want to try again?"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                //Add Buttons
                
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"TRYAGAIN"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action) {
                    [self ApiforGettingYachtList];
                }];
                
                //Add your buttons to alert controller
                UIAlertAction* noButton = [UIAlertAction
                                           actionWithTitle:@"CANCEL"
                                           style:UIAlertActionStyleDestructive
                                           handler:^(UIAlertAction * action) {
                    buttonAdd.userInteractionEnabled = true;
                    self.tabBarController.tabBar.userInteractionEnabled = true;
                    
                }];
                
                
                [alert addAction:noButton];
                [alert addAction:yesButton];
                
                [self presentViewController:alert animated:YES completion:nil];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}
#pragma mark - Button Favorite action

-(void)ButtonFavoriteClicked:(id)sender{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSString *strYachtId = [[[[[arrResponse objectAtIndex:selectedIndexPath.row] valueForKey:@"yachts"]objectAtIndex:[sender tag]]valueForKey:@"id"] stringValue];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:strYachtId forKey:@"yacht_id"];
        NSMutableDictionary *dictPost = [[NSMutableDictionary alloc]init];
        [dictPost setObject:dict forKey:@"favorites"];
        [WebServiceManager postRequestWithUrlString:[NSString stringWithFormat:@"%@favorites",kBaseUrl] withPostString:dictPost emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success){
            [self ApiforGettingYachtList];
        }
                                            failure:^(NSString *msg, BOOL success) {
            [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
        }];
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
    
}



#pragma mark- Collection View Delegate and Data Source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrResponse.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CityListCollectionViewCell *cell = [self.collectionViewCityList dequeueReusableCellWithReuseIdentifier:@"CityListCollectionViewCell" forIndexPath:indexPath];
    if (cell == nil)    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"CityListCollectionViewCell" owner:self options:nil];
        cell = nibArray[0];
    }
    
    
    if (indexPath.row == selectedIndexPath.row ) {
        cell.viewLabelCityName.layer.cornerRadius = 20;
        cell.viewLabelCityName.clipsToBounds = false;
        cell.viewLabelCityName.backgroundColor = [UIColor whiteColor];
        
        //drop shadow
        cell.viewLabelCityName.layer.shadowColor = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1] .CGColor;
        cell.viewLabelCityName.layer.shadowOffset = CGSizeMake(0, 1.0);
        cell.viewLabelCityName.layer.shadowOpacity = 1.0;
        cell.viewLabelCityName.layer.shadowRadius = 5.0;
        cell.viewLabelCityName.layer.masksToBounds = false;
        cell.labelCityName.text = [[[arrResponse objectAtIndex:indexPath.row] valueForKey:@"city"] uppercaseString];
        cell.labelCityName.textColor = [UIColor blackColor];
    }
    else{
        cell.viewLabelCityName.layer.shadowOpacity = 0.0;
        cell.labelCityName.text = [[[arrResponse objectAtIndex:indexPath.row] valueForKey:@"city"] capitalizedString];
        cell.labelCityName.textColor = [UIColor lightGrayColor];
        
    }
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    UILabel *label = [[UILabel alloc]init];
    label.font = FontOpenSansSemiBold(13);
    label.text = [[[arrResponse objectAtIndex:indexPath.row] valueForKey:@"city"] uppercaseString];
    CGSize size = label.intrinsicContentSize;
    
    //return CGSizeMake([[UIScreen mainScreen] bounds].size.width/3, 60);
    return  CGSizeMake(size.width + 40, 60);
    
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    selectedIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:0];;
    [collectionView reloadData];
    [_tableViewYachtList reloadData];
    //    [self ApiforGettingYachtList];
}


- (void)loadNextPage:(int)pageNumber {
    if (!self.hasNextPage) return;
    [self ApiForPagination];
    self.isLoading = YES;
}


-(void)ApiForPagination{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSString *strUrl = [[NSString alloc]init];
        if ([str isEqualToString:@"customer"]) {
            NSDictionary *dictPriceRange = [appDelegate.dictFilterItems valueForKey:@"PriceRange"];
            NSDictionary *dictGuests = [appDelegate.dictFilterItems valueForKey:@"NumberOfGuests"];
            NSDictionary *dictAmenities = [appDelegate.dictFilterItems valueForKey:@"SelectedAmenities"];
            strUrl = [NSString stringWithFormat:@"%@search/all_yachts?latitude=%f&longitude=%f",kBaseUrl,location.coordinate.latitude,location.coordinate.longitude];
            if (!([dictPriceRange count] == 0)) {
                strUrl = [NSString stringWithFormat:@"%@&min_price=%@&max_price=%@",strUrl,[dictPriceRange valueForKey:@"min_price"],[dictPriceRange valueForKey:@"max_price"]];
                selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            }
            if (!([dictGuests count] == 0)) {
                strUrl = [NSString stringWithFormat:@"%@&max_guests=%@",strUrl,[dictGuests valueForKey:@"max_guests"]];
                selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            }
            if (!([dictAmenities count] == 0)) {
                if (!([[dictAmenities valueForKey:@"amenity_ids"] count] == 0)) {
                    NSArray *arr = dictAmenities[@"amenity_ids"];
                    arr = [arr valueForKeyPath:@"description"];
                    NSString *str = [arr componentsJoinedByString:@","];
                    str = [NSString stringWithFormat:@"[%@]",str];
                    strUrl = [NSString stringWithFormat:@"%@&amenity_ids=%@",strUrl,str];
                    selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                }
            }
        }
        strUrl = [NSString stringWithFormat:@"%@&paginate=%d&page=%d&per_page=%d",strUrl,true,_currentPage,3];
        [WebServiceManager getRequestUrlString:strUrl emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            NSLog(@"%@",result);
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
                if ([str isEqualToString:Customer]) {
                    //arrResponse = [result valueForKey:@"cities"];
                    [arrResponse addObjectsFromArray:[result valueForKey:@"cities"]];
                    self.isLoading = NO;
                    if([[result valueForKey:@"total_pages"] integerValue] == self.currentPage){
                        self.hasNextPage = false;
                    }
                    else{
                        self.hasNextPage = true;
                    }
                    isApi = false;
                    [_collectionViewCityList reloadData];
                    [self.tableViewYachtList reloadData];
                    //   [self viewDidLayoutSubviews];
                }
                [[LoaderNew sharedLoader]hideLoader];
                buttonAdd.userInteractionEnabled = true;
                self.tabBarController.tabBar.userInteractionEnabled = true;
                
            });
        }
                                       failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                buttonAdd.userInteractionEnabled = true;
                self.tabBarController.tabBar.userInteractionEnabled = true;
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
    }
    
}
#pragma mark - tableView Delegate & data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger rowCount = 0;
    if (arrResponse.count >  0) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        if ([str isEqualToString:Owner]) {
            rowCount = [arrResponse count];
        }
        else{
            rowCount = [[[arrResponse objectAtIndex:selectedIndexPath.row] valueForKey:@"yachts"]count];
        }
        [self TableViewHelper:@"" andtableView:self.tableViewYachtList];
    }
    else{
        if (!isApi) {
            [self TableViewHelper:@"No yacht found" andtableView:self.tableViewYachtList];
        }
    }
    
    return rowCount;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([strRole isEqualToString:Customer]) {
        static NSString *simpleTableIdentifier = @"YachtListTableViewCell";
        YachtListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YachtListTableViewCell"];
        
        if (cell == nil) {
            NSArray *nib =[[NSBundle mainBundle]loadNibNamed:simpleTableIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        NSString *str = [[[[arrResponse objectAtIndex:selectedIndexPath.row] valueForKey:@"yachts"] objectAtIndex:indexPath.row]valueForKey:@"name"];
        cell.labelYachtName.text = str;
        cell.labelNumberOfGroups.text = [[[[[arrResponse objectAtIndex:selectedIndexPath.row] valueForKey:@"yachts"] objectAtIndex:indexPath.row]valueForKey:@"max_guests"] stringValue];
        NSDictionary *dictPrice = [[[[arrResponse objectAtIndex:selectedIndexPath.row]valueForKey:@"yachts"] objectAtIndex:indexPath.row] valueForKey:@"price"];
        NSString *strPrice = [NSString stringWithFormat:@"%.02f",[[dictPrice valueForKey:@"price"]floatValue]];
        NSString *strCurrency = [[[[arrResponse objectAtIndex:selectedIndexPath.row]valueForKey:@"yachts"] objectAtIndex:indexPath.row]valueForKey:@"currency_symbol"];
        NSString *strTerm = [dictPrice valueForKey:@"term"];
        if ([strTerm isEqualToString:@"per_day"]){
            strTerm = @"/Full Day";
        }
        else if ([strTerm isEqualToString:@"per_hour"]){
            strTerm = @"/Hour";
        }
        else if ([strTerm isEqualToString:@"half_day"]){
            strTerm = @"/Half Day";
        }
        else if ([strTerm isEqualToString:@"week"]){
            strTerm = @"/Week";
        }
        
        cell.labelPricing.attributedText = [self PlainStringToAttributedStringForPrice:strPrice Currency:strCurrency andTerm:strTerm];
        BOOL isfavorite = [[[[[arrResponse objectAtIndex:selectedIndexPath.row] valueForKey:@"yachts"] objectAtIndex:indexPath.row]valueForKey:@"is_favoritre"] boolValue];
        if (isfavorite) {
            if(cell.buttonFavorite){
                [cell.buttonFavorite setImage:[UIImage imageNamed:@"heart fill.png"] forState:UIControlStateNormal];
            }
        }
        else{
            if(cell.buttonFavorite){
                [cell.buttonFavorite setImage:[UIImage imageNamed:@"heart.png"] forState:UIControlStateNormal];
            }
        }
        if(cell.buttonFavorite){
            cell.buttonFavorite.tag = indexPath.row;
            [cell.buttonFavorite addTarget:self action:@selector(ButtonFavoriteClicked:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[[[arrResponse objectAtIndex:selectedIndexPath.row] valueForKey:@"yachts"] objectAtIndex:indexPath.row]valueForKey:@"image"]];
        NSURL *url = [NSURL URLWithString:strUrl];
        [cell.imageViewShip sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        
        //owner_image
        NSString *strUrlOwner = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[[[arrResponse objectAtIndex:selectedIndexPath.row] valueForKey:@"yachts"] objectAtIndex:indexPath.row]valueForKey:@"owner_image"]];
        NSURL *urlOwner = [NSURL URLWithString:strUrlOwner];
        [cell.imageViewRound sd_setImageWithURL:urlOwner placeholderImage:[UIImage imageNamed:@"lower strip"]];
        NSInteger intRating = [[[[[arrResponse objectAtIndex:selectedIndexPath.row] valueForKey:@"yachts"] objectAtIndex:indexPath.row]valueForKey:@"rating"] integerValue];
        [[CommonMethods sharedInstance]RatingAndStar1:cell.imageViewStar1 Star2:cell.imageViewStar2 Star3:cell.imageViewStar3 Star4:cell.imageViewStar4 Star5:cell.imageViewStar5 andRating:intRating];
        [[CommonMethods sharedInstance]DropShadow:cell.buttonFavorite UIColor:[UIColor darkGrayColor] andShadowRadius:3.0];
        return cell;
    }
    else{
        static NSString *simpleTableIdentifier = @"OwnerYachtTableViewCell";
        OwnerYachtTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OwnerYachtTableViewCell"];
        if (cell == nil) {
            NSArray *nib =[[NSBundle mainBundle]loadNibNamed:simpleTableIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.labelYachtName.text = [[arrResponse objectAtIndex:indexPath.row]valueForKey:@"name"];
        cell.labelNumberOfGroups.text = [[[arrResponse objectAtIndex:indexPath.row]valueForKey:@"max_guests"] stringValue];
        NSDictionary *dictPrice = [[arrResponse valueForKey:@"price"] objectAtIndex:indexPath.row];
        NSString *strPrice = [NSString stringWithFormat:@"%.02f",[[dictPrice valueForKey:@"price"]floatValue]];
        NSString *strCurrency = [[arrResponse valueForKey:@"currency_symbol"] objectAtIndex:indexPath.row];
        NSString *strTerm = [dictPrice valueForKey:@"term"];
        if ([strTerm isEqualToString:@"per_day"]){
            strTerm = @"/Full Day";
        }
        else if ([strTerm isEqualToString:@"per_hour"]){
            strTerm = @"/Hour";
        }
        else if ([strTerm isEqualToString:@"half_day"]){
            strTerm = @"/Half Day";
        }
        else if ([strTerm isEqualToString:@"week"]){
            strTerm = @"/Week";
        }
        cell.labelPricing.attributedText = [self PlainStringToAttributedStringForPrice:strPrice Currency:strCurrency andTerm:strTerm];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[arrResponse objectAtIndex:indexPath.row]valueForKey:@"image"]];
        NSURL *url = [NSURL URLWithString:strUrl];
        [cell.imageViewShip sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"lower strip"]];
        
        //self.attributedText = attributedText;
        NSInteger intRating = [[[arrResponse objectAtIndex:indexPath.row]valueForKey:@"rating"] integerValue];
        [[CommonMethods sharedInstance]RatingAndStar1:cell.imageViewStar1 Star2:cell.imageViewStar2 Star3:cell.imageViewStar3 Star4:cell.imageViewStar4 Star5:cell.imageViewStar5 andRating:intRating];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (IS_IPHONE_6P) {
        return 250;
    }
    else if (IS_IPHONE_6){
        return 235;
    }
    else{
        return 220;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
    if ([str isEqualToString:Customer]) {
        NSString *strID = [[[[[arrResponse objectAtIndex:selectedIndexPath.row]valueForKey:@"yachts"]objectAtIndex:indexPath.row] valueForKey:@"id"] stringValue];
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        YachtDetailTableViewController *controller = [story instantiateViewControllerWithIdentifier:@"YachtDetailTableViewController"];
        controller.strId = strID;
        controller.lat = location.coordinate.latitude;
        controller.lang = location.coordinate.longitude;
        [self.navigationController pushViewController:controller animated:true];
    }
    else{
        YachtBookingsTableViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"YachtBookingsTableViewController"];
        controller.strID = [[[arrResponse objectAtIndex:indexPath.row]valueForKey:@"id"] stringValue];
        [self.navigationController pushViewController:controller animated:true];
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSInteger numOfSections = 0;
    if (arrResponse)   {
        numOfSections  = 1;
        self.tableViewYachtList.backgroundView = nil;
    }
    else{
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableViewYachtList.bounds.size.width, self.tableViewYachtList.bounds.size.height)];
        noDataLabel.text             = @"No data available";
        noDataLabel.textColor        = [UIColor lightGrayColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        self.tableViewYachtList.backgroundView = noDataLabel;
        self.tableViewYachtList.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return numOfSections;
}

-(void)TableViewHelper:(NSString *)strMessage andtableView:(UITableView *)tableView{
    UILabel *label = [[UILabel alloc]init];
    label.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, self.tableViewYachtList.frame.size.height);
    label.text = strMessage;
    label.numberOfLines = 0;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = FontOpenSans(15);
    label.textColor = [UIColor lightGrayColor];
    tableView.backgroundView = label;
}

-(NSMutableAttributedString *)PlainStringToAttributedStringForPrice:(NSString *)strPrice Currency:(NSString *)strCurrency andTerm:(NSString *)strTerm{
    
    // NSString *strPrice = [[[[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"yacht"]valueForKey:@"price"] stringValue];
    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@%@ %@",strCurrency,strPrice,strTerm]];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:attrText];
    strPrice  = [strPrice stringByAppendingString:strCurrency];
    NSRange range = [strPrice rangeOfString:strPrice];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:FontOpenSansSemiBold(12)} range:range];
    return attributedText;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end


