//
//  AmenitiesViewController.h
//  YachtMasters
//
//  Created by Anvesh on 15/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol AmenitiesDelagate <NSObject>
@required
- (void) amenitiesRecieved:(NSMutableArray *)arrAmenities;
@end

@interface AmenitiesViewController : UIViewController
@property (nonatomic,strong) id<AmenitiesDelagate> delegate;
@property(nonatomic,strong) NSMutableArray *arrSelectedAmenities;

@end
