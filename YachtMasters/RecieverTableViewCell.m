//
//  RecieverTableViewCell.m
//  YachtMasters
//
//  Created by Anvesh on 31/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "RecieverTableViewCell.h"
#import "Constants.h"
#import "CommonMethods.h"

@implementation RecieverTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [[CommonMethods sharedInstance]CornerRadius:_imageViewReciever Radius:_imageViewReciever.frame.size.height/2 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
