//
//  FilterViewController.h
//  YachtMasters
//
//  Created by Anvesh on 30/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NMRangeSlider.h"
#import <CoreLocation/CoreLocation.h>

@interface FilterViewController : UIViewController{

}
@property (weak, nonatomic) IBOutlet NMRangeSlider *labelSlider;
@property (weak, nonatomic) IBOutlet UILabel *lowerLabel;
@property (weak, nonatomic) IBOutlet UILabel *upperLabel;

@property (weak, nonatomic) IBOutlet NMRangeSlider *labelSliderPrice;
@property (weak, nonatomic) IBOutlet UILabel *lowerLabelPrice;
@property (weak, nonatomic) IBOutlet UILabel *upperLabelPrice;
@property (nonatomic) CLLocation *location;
@property (nonatomic) float minPrice;
@property (nonatomic) float maxPrice;
@end
