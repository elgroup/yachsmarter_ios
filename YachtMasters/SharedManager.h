//
//  SharedManager.h
//  KoonApp
//
//  Created by Maurya on 02/11/15.
//  Copyright (c) 2015 i-verve infoWeb. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"


@interface SharedManager : NSObject

@property(assign,readwrite)  BOOL isNetAvailable;

@property(retain, nonatomic) NSUserDefaults *userDefaults;


@property (retain, nonatomic) NSString *deviceToken;

@property(retain,nonatomic) NSString *deviceType;
@property(retain,nonatomic) NSString *latitude;
@property(retain,nonatomic) NSString *longitude;






+(SharedManager *)sharedInstance;



@end
