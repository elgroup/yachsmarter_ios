//
//  ExistingCardListTableViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 14/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExistingCardListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelCardNumber;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCArdType;
@property (weak, nonatomic) IBOutlet UIButton *buttonSeletion;
@property (weak, nonatomic) IBOutlet UILabel *labelCard;

@end
