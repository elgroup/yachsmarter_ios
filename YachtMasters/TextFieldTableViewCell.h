//
//  TextFieldTableViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 13/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPFloatingPlaceholderTextField.h"

@interface TextFieldTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextField *textField;

@end
