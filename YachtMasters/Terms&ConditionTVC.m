//
//  Terms&ConditionTVC.m
//  YachtMasters
//
//  Created by Anvesh on 24/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "Terms&ConditionTVC.h"
#import "Constants.h"
#import "TermsAndConditionsTableViewCell.h"


@interface Terms_ConditionTVC ()
{
    IBOutlet UIView *viewTermsConditions;
    IBOutlet UILabel *labeltermsAndConditions;
    IBOutlet NSLayoutConstraint *lcTopConstraint;
    UIImageView *imageViewNav;
}
@end

@implementation Terms_ConditionTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 97;
    if (_isAboutUs) {
        labeltermsAndConditions.text = [NSString stringWithFormat:@"Who Are We!"];
        self.tabBarController.hidesBottomBarWhenPushed = true;
        self.tabBarController.tabBar.hidden = true;
        
    }
    else{
        labeltermsAndConditions.text = [NSString stringWithFormat:@"Yacht Smarter\nTerms & Conditions"];
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (_isSWReveal) {
        [self CustomNavigationBarForSWReveal];
    }
    else
    [self CustomNavigationBar];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (_isSWReveal)
    self.navigationController.navigationBar.topItem.title = @"About";

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (_isSWReveal) {
        self.tableView.frame = CGRectMake(0.0, 44.0, SCREEN_WIDTH, SCREEN_HEIGHT);
    }
}
-(void)CustomNavigationBar{
    
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"topstrip3.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor colorWithRed:40.0/255.0 green:107.0/255.0 blue:240.0/255.0 alpha:1.0].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    self.navigationController.navigationBar.layer.shadowRadius = 7.0f;
    self.navigationController.navigationBar.layer.shadowOpacity = 1.0f;
    self.navigationController.navigationBar.layer.masksToBounds=NO;
    
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    //[backButton setBackgroundColor:[UIColor redColor]];
    [backButton addTarget:self action:@selector(ButtonBackPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 10.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    if (_isAboutUs) {
        self.title  = @"About";
    }
    else{
        self.title = @"Term & Conditions";
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}

-(void)CustomNavigationBarForSWReveal{
    self.tabBarController.tabBar.hidden =  true;
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    UIImage* image3;
    
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    if (_isSWReveal) {
        self.navigationController.navigationBar.frame = CGRectMake(0.0, 20.0, SCREEN_WIDTH, 44);
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"topstrip3.png"] forBarMetrics:UIBarMetricsDefault];
        [self.navigationController.navigationBar setShadowImage:[UIImage new]];
        self.navigationController.navigationBar.layer.shadowColor = [UIColor colorWithRed:40.0/255.0 green:107.0/255.0 blue:240.0/255.0 alpha:1.0].CGColor;
        self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
        self.navigationController.navigationBar.layer.shadowRadius = 7.0f;
        self.navigationController.navigationBar.layer.shadowOpacity = 1.0f;
        self.navigationController.navigationBar.layer.masksToBounds=NO;
    }
//        image3  = [UIImage imageNamed:@"back arrow.png"];
//        [backButton addTarget:self action:@selector(ButtonBackPressed) forControlEvents:UIControlEventTouchUpInside];
//        //   }
//        UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
//        self.navigationItem.leftBarButtonItem = leftBarButton;
//        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 5.0, 15.0, 15.0)];
//        imageView.image = image3;
//        imageView.contentMode = UIViewContentModeScaleAspectFit;
//        [backButton addSubview:imageView];
//
    
//        [self.navigationController.navigationBar setTitleTextAttributes:
//         @{NSForegroundColorAttributeName:[UIColor whiteColor],
//           NSFontAttributeName:FontOpenSansSemiBold(13)}];
//        lcTopConstraint.constant = 44;
//    }
//    else{
        image3  = [UIImage imageNamed:@"back arrow.png"];
        [backButton addTarget:self action:@selector(ButtonBackPressed) forControlEvents:UIControlEventTouchUpInside];
        //   }
        UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.leftBarButtonItem = leftBarButton;
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 5.0, 15.0, 15.0)];
        imageView.image = image3;
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [backButton addSubview:imageView];
    self.navigationController.navigationBar.topItem.leftBarButtonItem = leftBarButton;
   
    [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor],
           NSFontAttributeName:FontOpenSansSemiBold(13)}];
   // }
}



-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //imageViewNav.image = nil;
    
}
-(void)ButtonBackPressed{
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = true;
    if (_isAboutUs) {
        
        self.tabBarController.tabBar.hidden = false;
    }
    
    [self.navigationController popViewControllerAnimated:true];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return viewTermsConditions;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"TermsAndConditionsTableViewCell";
    TermsAndConditionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TermsAndConditionsTableViewCell"];
    
    if (cell == nil) {
        NSArray *nib =[[NSBundle mainBundle]loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (_isAboutUs) {
        return 70;
    }
    else{
        return 115;
    }
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
