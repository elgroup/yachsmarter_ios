//
//  QuestionTableViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 25/09/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UILabel *labelQuestion;
@property (nonatomic,weak) IBOutlet UILabel *labelAnswer;
@end
