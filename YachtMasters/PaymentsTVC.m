//
//  PaymentsTVC.m
//  YachtMasters
//
//  Created by Anvesh on 14/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "PaymentsTVC.h"
#import "CardDetailsTableViewCell.h"
#import "ExistingCardListTableViewCell.h"
#import "Constants.h"
#import "WebServiceManager.h"
#import "CommonMethods.h"
#import "MBProgressHUD.h"
#import "CreditCard-Validator.h"
#import "BookingConfirmationViewController.h"
#import "YachtMasters-Swift.h"
@import Stripe;
//#import <STPAPIClient + STPCardParams>

@interface PaymentsTVC ()<UITextFieldDelegate,UIPickerViewDelegate>{
    UIBarButtonItem *doneButton;
    UIToolbar *toolBar;
    UITextField *txtField;
    UIPickerView *pickerViewCountry;
    NSMutableArray *arrCountry;
    NSMutableArray *arrNumberOfCards;
    UIButton *btnBookNow;
    NSMutableDictionary *dictCardDetails;
    BOOL isCardSelected;
    UIView *viewForButtonBookNow;
    NSString *cardId;
    NSArray *arrCardCurrency;
    NSInteger buttonTag;
    NSIndexPath *indexPathForButton;
    UIImageView *imageCardType;
    MBProgressHUD *hud;
    UIImageView *imageViewCard;
}
@property (strong, nonatomic) IBOutlet UIView *viewFooter;
@property (weak, nonatomic) IBOutlet UIButton *buttonBookNow;
//@property (nonatomic, strong) MPAPIClient *mangopayClient;

@end

@implementation PaymentsTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    dictCardDetails = [[NSMutableDictionary alloc]init];
    arrNumberOfCards = [[NSMutableArray alloc]init];
    self.tabBarController.hidesBottomBarWhenPushed = true;
    self.tabBarController.tabBar.hidden = true;
    if(!self.isAddAccount){
        [self ApiForgettingCardList];
    }
    else{
    arrCardCurrency = @[@"EUR",@"AUD",@"USD",@"GBP",@"PLN",@"CHF",@"NOK",@"SEK",@"DKK",@"CAD"];
    }
    doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(hideKeyboard)];
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 50)];
    toolBar.translucent=NO;
    toolBar.barTintColor = [UIColor colorWithRed:40.0/255.0 green:107.0/255.0 blue:240.0/255.0 alpha:1.0];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton, nil];
    [toolBar setItems:toolbarItems];
    doneButton.tintColor = [UIColor whiteColor];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Country" ofType:@"json"];
    NSData *content = [[NSData alloc] initWithContentsOfFile:filePath];
    NSDictionary *dictCountryCode = [NSJSONSerialization JSONObjectWithData:content options:kNilOptions error:nil];
    NSLog(@"%@",dictCountryCode);
    arrCountry = [[NSMutableArray alloc]init];
    arrCountry = [[dictCountryCode valueForKey:@"array"]valueForKey:@"country_name"];
    NSLog(@"%@",arrCountry);
    pickerViewCountry = [[UIPickerView alloc]init];
    pickerViewCountry.backgroundColor = [UIColor whiteColor];
    pickerViewCountry.delegate = self;
    
    
    viewForButtonBookNow = [[UIView alloc]initWithFrame:CGRectMake(0.0, SCREEN_HEIGHT - 75, SCREEN_WIDTH, 75)];
    viewForButtonBookNow.backgroundColor = [UIColor whiteColor];
    [self.navigationController.view addSubview:viewForButtonBookNow];
    
    btnBookNow = [UIButton buttonWithType:UIButtonTypeCustom];
    if(IS_IPHONE_X || IS_IPHONE_6P){
        btnBookNow.frame = CGRectMake(45.0, 0, SCREEN_WIDTH - 90, 55);
    }
    else{
        btnBookNow.frame = CGRectMake(45.0, 10, SCREEN_WIDTH - 90, 48);
    }
    [viewForButtonBookNow addSubview:btnBookNow];
    [btnBookNow setBackgroundImage:[UIImage imageNamed:@"add shape.png"] forState:UIControlStateNormal];
    [btnBookNow setTitle:@"BOOK NOW" forState:UIControlStateNormal];
    btnBookNow.titleLabel.font = FontOpenSansBold(15);
    [btnBookNow addTarget:self action:@selector(ButtonBookNowPressed) forControlEvents:UIControlEventTouchUpInside];
    
    
    // buttonPhone = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 12)];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.isAddAccount){
        [btnBookNow setTitle:@"ADD CARD" forState:UIControlStateNormal];
    }
    else{
        [btnBookNow setTitle:@"BOOK NOW" forState:UIControlStateNormal];
    }
    [self CustomNavigation];
}

-(void)CustomNavigation{
    self.navigationController.navigationBar.hidden = false;
    self.title = @"Payment";
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 10.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
    
}

-(BOOL)hidesBottomBarWhenPushed{
    return YES;
}

-(void)BackButtonPressed{
    [viewForButtonBookNow removeFromSuperview];
    self.tabBarController.tabBar.hidden = false;
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
     [viewForButtonBookNow removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) hideKeyboard {
    [self.view endEditing:true];
    [UIView animateWithDuration:0.25 animations:^{
        toolBar.frame = CGRectMake(0.0, SCREEN_HEIGHT, SCREEN_WIDTH, 50);
        // datePickerDateOfBirth.frame = CGRectMake(0.0, SCREEN_HEIGHT - 50, SCREEN_WIDTH, 150);
    }];
}

#pragma mark - Button card selected

-(void)ButtonCardSelected:(id )sender{

    if (indexPathForButton) {
        ExistingCardListTableViewCell *cell = (ExistingCardListTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPathForButton];
        cell.buttonSeletion.selected = false;
        isCardSelected = false;
        
    }
    
    indexPathForButton = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    ExistingCardListTableViewCell *cell = (ExistingCardListTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPathForButton];
    cell.buttonSeletion.selected = true;
    isCardSelected = true;
    cardId = [[arrNumberOfCards objectAtIndex:[sender tag]]valueForKey:@"id"];
}


#pragma mark - Button Book now Pressed

-(void)ButtonBookNowPressed{
    if (isCardSelected) {
        [self ApiForBookingWithExistingCard];
    }
    else{
        [self ApiForBooking];
    }
}

#pragma mark - PickerView delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    NSInteger tag = txtField.tag;
    if (tag == 5) {
        return arrCountry.count;
    }else if (tag == 6){
        return arrCardCurrency.count;
    }
    else{
        return 0;
    }
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSInteger tag = txtField.tag;
    if (tag == 5) {
        return [arrCountry objectAtIndex:row];
    }else if (tag == 6){
        return [arrCardCurrency objectAtIndex:row];
    }
    else{
        return nil;
    }
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSInteger tag = txtField.tag;
    if (tag == 5) {
        txtField.text = [arrCountry objectAtIndex:row];
    }
    else if (tag == 6){
        txtField.text = [arrCardCurrency objectAtIndex:row];
    }
    //    else{
    //        return 0;
    //    }
}


#pragma mark - UItextfield Delegate and datasource

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    NSInteger tag = textField.tag;
    if (tag == 1) {
        isCardSelected = false;
        txtField = textField;
    }
    else if (tag == 2){
        isCardSelected = false;
        txtField = textField;
    }
    else if (tag == 3){
        isCardSelected = false;
        txtField = textField;
    }
    else if (tag == 4){
        isCardSelected = false;
        txtField = textField;
    }
    else{
        
    }
    
    if (isCardSelected) {
        ExistingCardListTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ExistingCardListTableViewCell"];
        cell.buttonSeletion.selected = false;
        isCardSelected = false;
        NSRange range = NSMakeRange(0, 1);
        NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
        [self.tableView reloadSections:section withRowAnimation:UITableViewRowAnimationNone];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSInteger tag = textField.tag;
    
    if (tag == 1) {
        [dictCardDetails setObject:textField.text forKey:@"holder_name"];
    }
    else if (tag == 2){
        NSString *str = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        [dictCardDetails setObject:str forKey:@"number"];
    }
    else if (tag == 3){
        
        [dictCardDetails setObject:textField.text forKey:@"expiry_date"];
    }
    else if (tag == 4){
        [dictCardDetails setObject:textField.text forKey:@"cvv"];
    }
    //    else if (tag == 5){
    //        [dictCardDetails setObject:textField.text forKey:@"country"];
    //    }
    //    else if (tag == 6){
    //        [dictCardDetails setObject:textField.text forKey:@"currency"];
    //    }
    else{
        
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSInteger tag = textField.tag;
    if (tag == 1) {
        [textField resignFirstResponder];
    }
    return true;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if (textField.tag == 2) {
        __block NSString *text = [textField text];
        
        NSCharacterSet *characterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789\b"];
        string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
        if ([string rangeOfCharacterFromSet:[characterSet invertedSet]].location != NSNotFound) {
            return NO;
        }
        
        text = [text stringByReplacingCharactersInRange:range withString:string];
        text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        NSString *newString = @"";
        while (text.length > 0) {
            NSString *subString = [text substringToIndex:MIN(text.length, 4)];
            newString = [newString stringByAppendingString:subString];
            if (subString.length == 4) {
                newString = [newString stringByAppendingString:@" "];
            }
            text = [text substringFromIndex:MIN(text.length, 4)];
        }
        
        newString = [newString stringByTrimmingCharactersInSet:[characterSet invertedSet]];
        
        if (newString.length >= 20) {
            return NO;
        }
        
        [textField setText:newString];
        NSString *strCardType = [newString stringByReplacingOccurrencesOfString:@" " withString:@""];
        CreditCardType cardType = [CreditCard_Validator getCreditCardTypeFromString:strCardType];
        
        imageViewCard = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,30, 20)];
        textField.rightViewMode = UITextFieldViewModeAlways;
        textField.rightView = imageViewCard;
        switch (cardType) {
                
            case CreditCardTypeAmex:
                imageViewCard.image = [UIImage imageNamed:@"amex"];
                return NO;
            case CreditCardTypeDinersClub:
                imageViewCard.image = [UIImage imageNamed:@"diner"];
                return  NO;
            case CreditCardTypeDiscover:
                imageViewCard.image = [UIImage imageNamed:@"discover"];
                return  NO;
                
            case CreditCardTypeMasterCard:
                imageViewCard.image = [UIImage imageNamed:@"master"];
                return  NO;
                
            case CreditCardTypeMaestroCard:
                imageViewCard.image = [UIImage imageNamed:@""];
                return  NO;
            case CreditCardTypeVisa:
                imageViewCard.image = [UIImage imageNamed:@"visa"];
                return  NO;
            case CreditCardTypeJCB:
                imageViewCard.image = [UIImage imageNamed:@""];
                return  NO;
            case CreditCardTypeUnknown:
                imageViewCard.image = [UIImage imageNamed:@""];
                return  NO;
                
            default:
                break;
        }
        return NO;
    }
    else if (textField.tag == 3) {
        if ([string isEqualToString:@""]){
            return true;
        }
        NSString *text = textField.text;
        
        // If we're trying to add more than the max amount of characters, don't allow it
        if ([text length] == 7 && range.location > 6) {
            return NO;
        }
        
        // First lets add the whole string we're going for
        text = [text stringByReplacingCharactersInRange:range withString:string];
        
        // We need to use an NSMutableString to do insertString calls in a moment
        NSMutableString *mutableText = [text mutableCopy];
        
        // If the text is more than 11 characters, we also want to insert a '/' at the 11th character index
        if (mutableText.length == 2) {
            [mutableText insertString:@"/" atIndex:2];
        }
        // lets set text to our new string
        text = mutableText;
        
        // Now, lets check if we need to cut off extra characters (like if the person pasted a too-long string)
        if (text.length > 7) {
            text = [text stringByReplacingCharactersInRange:NSMakeRange(7, mutableText.length-7) withString:@""];
        }
        
        // Finally, set the textfield to our newly modified string!
        textField.text = text;
        
        return NO;
    }
    else if (textField.tag == 4){
        if(range.length + range.location > textField.text.length){
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 3;
    }
    else{
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 50;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (arrNumberOfCards.count >0) {
        return 2;
    }
    else{
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (arrNumberOfCards.count > 0) {
        if (section==0) {
            return [arrNumberOfCards count];
        }
        else{
            return 1;
        }
    }
    else{
        return 1;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (arrNumberOfCards.count == 0) {
        
        static NSString *simpleTableIdentifier = @"CardDetailsTableViewCell";
        CardDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CardDetailsTableViewCell"];
        if (cell == nil) {
            NSArray *nib =[[NSBundle mainBundle]loadNibNamed:simpleTableIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.textFieldCardCvv.inputAccessoryView = toolBar;
        cell.textFieldCardNumber.inputAccessoryView = toolBar;
        cell.textFieldCardExpiryDate.inputAccessoryView = toolBar;
        //cell.textFieldCardCountry.inputAccessoryView = toolBar;
        // cell.textFieldCardCurrency.inputAccessoryView = toolBar;
        // cell.textFieldCardCountry.inputView = pickerViewCountry;
        //cell.textFieldCardCurrency.inputView = pickerViewCountry;
        
        return cell;
        
    }
    else{
        if (indexPath.section == 0) {
            static NSString *simpleTableIdentifier = @"ExistingCardListTableViewCell";
            ExistingCardListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExistingCardListTableViewCell"];
            if (cell == nil) {
                NSArray *nib =[[NSBundle mainBundle]loadNibNamed:simpleTableIdentifier owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            cell.labelCardNumber.text = [NSString stringWithFormat:@"**** **** **** %@",[[arrNumberOfCards valueForKey:@"number"] objectAtIndex:indexPath.row]];
            cell.buttonSeletion.tag = indexPath.row;
            [cell.buttonSeletion addTarget:self action:@selector(ButtonCardSelected:) forControlEvents:UIControlEventTouchUpInside];
            cell.labelCard.text = [NSString stringWithFormat:@"CARD %ld",indexPath.row + 1];
            
            
            return cell;
        }
        else{
            static NSString *simpleTableIdentifier = @"CardDetailsTableViewCell";
            CardDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CardDetailsTableViewCell"];
            if (cell == nil) {
                NSArray *nib =[[NSBundle mainBundle]loadNibNamed:simpleTableIdentifier owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            imageCardType = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
            
            cell.textFieldCardNumber.rightView = imageCardType;
            cell.textFieldCardCvv.inputAccessoryView = toolBar;
            cell.textFieldCardNumber.inputAccessoryView = toolBar;
            cell.textFieldCardExpiryDate.inputAccessoryView = toolBar;
            return cell;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (arrNumberOfCards.count > 0) {
        if (indexPath.section == 0) {
            return 50;
        }
        else{
            return 325;
        }
    }
    else{
        return 325;
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, SCREEN_WIDTH, 70)];
    view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(33.0, 40.0, SCREEN_WIDTH - 66, 15.0)];
    [view addSubview:label];
    label.font = FontOpenSansSemiBold(15);
    label.textColor = [UIColor grayColor];
    if (arrNumberOfCards.count >0) {
        if (section == 0) {
            label.text = @"USE EXISTING CARD";
        }
        else{
            label.text = @"ADD NEW CREDIT CARD";
        }
    }
    else{
        label.text = @"ADD NEW CREDIT CARD";
    }
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 70;
}



#pragma mark - Api for getting card list

-(void)ApiForgettingCardList{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@cards",kBaseUrl] emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                arrNumberOfCards = [result valueForKey:@"cards"];
                [self.tableView reloadData];
                
            });
        } failure:^(NSString *msg, BOOL success) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
                [[CommonMethods sharedInstance]AlertMessage:@"Unable to fetch your saved cards" andViewController:self];
            });
            
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
            
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}


#pragma mark -Api For bookings

-(void)ApiForBooking{
    if ([[dictCardDetails valueForKey:@"holder_name"] length] == 0) {
        [[CommonMethods sharedInstance]AlertMessage:@"Please provide card holder name" andViewController:self];
    }
    else if ([[dictCardDetails valueForKey:@"number"] length] == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please provide card number" andViewController:self];
    }
    else if ([[dictCardDetails valueForKey:@"number"] length] <16){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter correct card number" andViewController:self];
    }
    else if (![CreditCard_Validator checkCreditCardNumber:[dictCardDetails valueForKey:@"number"]]){
        [[CommonMethods sharedInstance]AlertMessage:@"Please provide valid card number" andViewController:self];
    }
    else if ([[dictCardDetails valueForKey:@"expiry_date"] length] == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please provide expiry date" andViewController:self];
        
    }
    else if ([[dictCardDetails valueForKey:@"expiry_date"] length] <4){
        [[CommonMethods sharedInstance]AlertMessage:@"Please provide valid expiry date" andViewController:self];
    }
    else if ([[dictCardDetails valueForKey:@"cvv"] length] == 0 ){
        [[CommonMethods sharedInstance]AlertMessage:@"Please provide cvv" andViewController:self];
    }
    else if ([[dictCardDetails valueForKey:@"cvv"] length] <3){
        [[CommonMethods sharedInstance]AlertMessage:@"Please provide valid cvv" andViewController:self];
    }
    else{
        
        if([[CommonMethods sharedInstance]checkInternetConnection]){
            if (self.isAddAccount){
                [self CreateCardToken];
            }
            else{
                [self CreateBooking];
            }
        }
        else{
            [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
        }
    }
    
}

-(void)CreateBooking{
    STPCardParams *cardParams = [[STPCardParams alloc] init];
    NSString *strDate = [dictCardDetails valueForKey:@"expiry_date"];
    NSString *strMonth = [strDate substringToIndex:2];
    NSString *strYear = [strDate substringFromIndex:3];
    cardParams.number =  [dictCardDetails valueForKey:@"number"];
    cardParams.expMonth = [strMonth integerValue];
    cardParams.expYear = [strYear integerValue];
    cardParams.cvc = [dictCardDetails valueForKey:@"cvv"];
    [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view.superview];
    [[STPAPIClient sharedClient] createTokenWithCard:cardParams completion:^(STPToken *token, NSError *error) {
        if (token == nil || error != nil) {
            // Present error to user...
            [[LoaderNew sharedLoader]hideLoader];
            [[CommonMethods sharedInstance]AlertMessage:@"Problem in making the payment. Please try again!!" andViewController:self];
            return;
        }
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
        [_dictYachtDetail setObject:[NSString stringWithFormat:@"%@",token] forKey:@"card_token"];
        [dictResponse setObject:_dictYachtDetail forKey:@"bookings"];
        [WebServiceManager postRequestWithUrlString:[NSString stringWithFormat:@"%@bookings",kBaseUrlVersion2] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            //   NSInteger cardId = [[result  valueForKey:@"card_id"] integerValue];
            // NSString *strPreRegistrationData = [[result valueForKey:@"card_data"] valueForKey:@"cardPreregistrationId"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                BookingConfirmationViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"BookingConfirmationViewController"];
                [viewForButtonBookNow removeFromSuperview];
                [self.navigationController pushViewController:controller animated:NO];
                // [[CommonMethods sharedInstance]AlertMessage:[result valueForKey:@"message"] andViewController:self];
            });
            
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
    }];
}

-(void)CreateCardToken{
    STPCardParams *cardParams = [[STPCardParams alloc] init];
    NSString *strDate = [dictCardDetails valueForKey:@"expiry_date"];
    NSString *strMonth = [strDate substringToIndex:2];
    NSString *strYear = [strDate substringFromIndex:3];
    cardParams.number =  [dictCardDetails valueForKey:@"number"];
    cardParams.expMonth = [strMonth integerValue];
    cardParams.expYear = [strYear integerValue];
    cardParams.cvc = [dictCardDetails valueForKey:@"cvv"];
    [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view.superview];
    [[STPAPIClient sharedClient] createTokenWithCard:cardParams completion:^(STPToken *token, NSError *error) {
        if (token == nil || error != nil) {
            // Present error to user...
            [[LoaderNew sharedLoader]hideLoader];
            [[CommonMethods sharedInstance]AlertMessage:@"Problem in making the payment. Please try again!!" andViewController:self];
            return;
        }
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
        [dictResponse setObject:[NSString stringWithFormat:@"%@",token] forKey:@"card_token"];
        [WebServiceManager postRequestWithUrlString:[NSString stringWithFormat:@"%@cards",kBaseUrlVersion2] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:@"" message:[result valueForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    [self.navigationController popViewControllerAnimated:true];
                }];
                [alertcontroller addAction:yesButton];
                [self presentViewController:alertcontroller animated:YES completion:nil];
            });
            
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
    }];
}

-(void)ApiForBookingWithExistingCard{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        [_dictYachtDetail setObject:cardId forKey:@"card_id"];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:_dictYachtDetail forKey:@"bookings"];
        [WebServiceManager postRequestWithUrlString:[NSString stringWithFormat:@"%@bookings",kBaseUrlVersion2] withPostString:dict emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                BookingConfirmationViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"BookingConfirmationViewController"];
                [viewForButtonBookNow removeFromSuperview];
                [self.navigationController pushViewController:controller animated:NO];
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
            
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}

@end
