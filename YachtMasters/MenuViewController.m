//
//  MenuViewController.m
//  RevealControllerStoryboardExample
//
//  Created by Nick Hodapp on 1/9/13.
//  Copyright (c) 2013 CoDeveloper. All rights reserved.
//

#import "MenuViewController.h"
#import "AppDelegate.h"
#import "ImageProfileTableViewCell.h"
#import "HomeTabBarViewController.h"
#import "MenuListTableViewCell.h"
#import "SWRevealViewController.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "CommonMethods.h"
#import "WebServiceManager.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"
//#import "SettingsViewController.h"
#import "SettingViewController.h"
#import "Terms&ConditionTVC.h"
#import "AboutUsViewController.h"
#import "YachtMasters-Swift.h"

@implementation SWUITableViewCell
@end

@implementation MenuViewController{
    NSArray *arrMenuListName ;
    NSInteger index;
    MBProgressHUD *hud;
    IBOutlet UITableView *tableViewMenu;
    AppDelegate *appDelegate;
}
-(void)viewDidLoad{
    [super viewDidLoad];
    arrMenuListName = @[
                        @"HOME",
                        @"MESSAGES",
                        @"BOOKINGS",
                        @"PROFILE",
                        @"SETTINGS",
                        @"ABOUT",
                        @"CONTACT",
                        @"LOGOUT"
                        ];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIViewController *rootController = (UIViewController*)[[(AppDelegate*)
                                                            
                                                            [[UIApplication sharedApplication]delegate] window] rootViewController];
    UIViewController *childController =nil;
    HomeTabBarViewController * tabcontroller    = nil;
    UINavigationController *navController =nil;
    if([rootController isKindOfClass:[UINavigationController class]]){
        navController = (UINavigationController*)rootController;
        for (UIViewController *obj in navController.childViewControllers) {
            if([obj isKindOfClass:[SWRevealViewController class]]){
                childController = obj;
            }
        }
        
    }
    else{
        childController = rootController;
    }
    if(childController){
        for (UIViewController *controller in childController.childViewControllers) {
            
            if([controller isKindOfClass:[HomeTabBarViewController class]]){
                tabcontroller = (HomeTabBarViewController*)controller;
                break;
            }
        }
    }
    index = [tabcontroller selectedIndex];
    [tableViewMenu reloadData];
    NSLog(@"%ld",(long)index);
}

//- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender{
//    // configure the destination view controller:
//    if ( [sender isKindOfClass:[UITableViewCell class]] ){
//        UILabel* c = [(SWUITableViewCell *)sender label];
//        UINavigationController *navController = segue.destinationViewController;
//        ColorViewController* cvc = [navController childViewControllers].firstObject;
//        if ( [cvc isKindOfClass:[ColorViewController class]] ){
//            cvc.color = c.textColor;
//            cvc.text = c.text;
//        }
//    }
//}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrMenuListName.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row==0) {
        static NSString *cellIdentifier = @"ImageProfileTableViewCell";
        ImageProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ImageProfileTableViewCell"];
        
        if (cell == nil) {
            
            NSArray *nib =[[NSBundle mainBundle]loadNibNamed:cellIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.imageViewProfile.layer.cornerRadius = cell.imageViewProfile.frame.size.width/2;
        cell.imageViewProfile.clipsToBounds = true;
        NSURL *url = [[NSUserDefaults standardUserDefaults]URLForKey:@"Image"];
        [cell.imageViewProfile sd_setImageWithURL:url placeholderImage:nil];
        //        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        //        dispatch_async(q, ^{
        //            NSData *data = [NSData dataWithContentsOfURL:url];
        //            UIImage *img = [[UIImage alloc] initWithData:data];
        //            dispatch_async(dispatch_get_main_queue(), ^{
        //                cell.imageViewProfile.image = img;
        //            });
        //        });
        
        
        cell.labelName.text = [[NSUserDefaults standardUserDefaults]valueForKey:@"Name"];
        cell.labelAddress.text = [[NSUserDefaults standardUserDefaults]valueForKey:@"Address"];
        
        
        return cell;
    }
    else{
        static NSString *cellIdentifier = @"MenuListTableViewCell";
        MenuListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MenuListTableViewCell"];
        if (cell == nil) {
            NSArray *nib =[[NSBundle mainBundle]loadNibNamed:cellIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.labelMenuListName.text = [arrMenuListName objectAtIndex:indexPath.row - 1];
        cell.labelMenuListName.textColor = [UIColor blackColor];
        if (indexPath.row == index+1) {
            cell.labelMenuListName.textColor = [UIColor colorWithRed:103.0/255.0 green:203.0/255.0 blue:243.0/255.0 alpha:1];
        }
        
        if (indexPath.row == 7) {
            cell.labelLine.backgroundColor = [UIColor whiteColor];
        }
        if (indexPath.row == 8) {
            cell.labelLine.backgroundColor = [UIColor whiteColor];
            cell.labelMenuListName.font = FontOpenSansSemiBold(12);
            cell.labelMenuListName.textColor = [UIColor colorWithRed:100.0/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:1];
        }
        cell.labelNotificationCount.layer.cornerRadius = cell.labelNotificationCount.frame.size.width/2;
        cell.labelNotificationCount.clipsToBounds = true;
        cell.labelNotificationCount.hidden = true;
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 110;
    }
    else{
        return  60;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIViewController *rootController = (UIViewController*)[[(AppDelegate*)[[UIApplication sharedApplication]delegate] window] rootViewController];
    UIViewController *childController =nil;
    HomeTabBarViewController * tabcontroller    = nil;
    UINavigationController *navController =nil;
    if([rootController isKindOfClass:[UINavigationController class]]){
        navController = (UINavigationController*)rootController;
        for (UIViewController *obj in navController.childViewControllers) {
            if([obj isKindOfClass:[SWRevealViewController class]]){
                childController = obj;
            }
        }
    }
    else{
        childController = rootController;
    }
    if(childController){
        for (UIViewController *controller in childController.childViewControllers) {
            
            if([controller isKindOfClass:[HomeTabBarViewController class]]){
                tabcontroller = (HomeTabBarViewController*)controller;
                break;
            }
        }
    }
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    switch (indexPath.row) {
        case 1:
            tabcontroller.selectedIndex = 0;
            [revealViewController revealToggleAnimated:YES];
            break;
        case 2:
            tabcontroller.selectedIndex = 1;
            [revealViewController revealToggleAnimated:YES];
            break;
        case 3:
            tabcontroller.selectedIndex = 2;
            [revealViewController revealToggleAnimated:YES];
            break;
        case 4:
            tabcontroller.selectedIndex = 3;
            [revealViewController revealToggleAnimated:YES];
            break;
        case 5:{
            [revealViewController revealToggleAnimated:YES];
            SettingViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
            controller.isSwrevealView = true;
            [((UINavigationController *)((UITabBarController *)((SWRevealViewController *)((UINavigationController *)((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController).topViewController).frontViewController).selectedViewController) pushViewController:controller animated:true];
        }
            break;
        case 6:{
            [revealViewController revealToggleAnimated:YES];
            AboutUsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsViewController"];
            controller.isAboutUs = true;
            controller.isSWReveal = true;
            [((UINavigationController *)((UITabBarController *)((SWRevealViewController *)((UINavigationController *)((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController).topViewController).frontViewController).selectedViewController) pushViewController:controller animated:true];
            break;
        }
        case 7:
            [self ButtonCallPressed];
            break;
        case 8:
            [self CustomAlert];
            break;
        default:
            break;
    }
    
}

- (void)ButtonCallPressed {
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",@"+13476588944"]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        //        [[UIApplication sharedApplication] openURL:phoneUrl];
        NSDictionary *dict = [[NSDictionary alloc]init];
        [[UIApplication sharedApplication] openURL:phoneUrl options:dict completionHandler:nil];
    } else{
        [[CommonMethods sharedInstance]AlertMessage:@"Call facility is not available!!!" andViewController:self];
    }
    
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    // configure the destination view controller:
    if ( [sender isKindOfClass:[UIViewController class]] )
    {
        
        //UINavigationController *navController = segue.destinationViewController;
        SettingViewController* cvc = [sender childViewControllers].firstObject;
        if ( [cvc isKindOfClass:[SettingViewController class]] )
        {
            
        }
    }
}




-(void)CustomAlert{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@""
                                     message:@"Want to logout from YachtSmarter?"
                                     preferredStyle:UIAlertControllerStyleAlert];
        //Add Buttons
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"LOGOUT"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        [self ApiForLogOut];
                                    }];
        //Add your buttons to alert controller
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"CANCEL"
                                   style:UIAlertActionStyleDestructive
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                   }];
        
        [alert addAction:noButton];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    });
}


-(void)ApiForLogOut{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *strRole = @"";//[[NSUserDefaults standardUserDefaults]valueForKey:@"role"];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:@"Auth_token"];
        
        [WebServiceManager deleteRequestWithUrlString:[NSString stringWithFormat:@"%@users/sign_out",kBaseUrl] emailString:strRole accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setBool:false forKey:@"isLogIn"];
            [defaults setObject:@"" forKey:@"Auth_token"];
            [defaults setObject:@"" forKey:@"role"];
            [defaults synchronize];
            appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate.socket unSubscribeChannel:@"AppearanceChannel"];
            LoginViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"]; //or the homeController
            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
            appDelegate.window.rootViewController = navController;
            //[self.navigationController pushViewController:navController animated:YES];
            dispatch_async(dispatch_get_main_queue(), ^{
                //                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [self.navigationController pushViewController:loginController animated:true];
            });
            NSLog(@"%@",result);
            
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //                [MBProgressHUD hideHUDForView:self.view.superview animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            //            hud = [MBProgressHUD showHUDAddedTo:self.view.superview animated:YES];
            //            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
        
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}




#pragma mark state preservation / restoration
- (void)encodeRestorableStateWithCoder:(NSCoder *)coder {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO save what you need here
    
    [super encodeRestorableStateWithCoder:coder];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO restore what you need here
    
    [super decodeRestorableStateWithCoder:coder];
}

- (void)applicationFinishedRestoringState {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // TODO call whatever function you need to visually restore
}

@end
