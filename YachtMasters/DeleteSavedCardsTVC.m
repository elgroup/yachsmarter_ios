//
//  DeleteSavedCardsTVC.m
//  YachtMasters
//
//  Created by Anvesh on 30/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "DeleteSavedCardsTVC.h"
#import "MBProgressHUD.h"
#import "Constants.h"
#import "WebServiceManager.h"
#import "CommonMethods.h"
#import "YachtMasters-Swift.h"
@interface DeleteSavedCardsTVC (){
    
    __weak IBOutlet UILabel *labelCardNumber;
    __weak IBOutlet UILabel *labelCardHolderName;
    __weak IBOutlet UILabel *labelExpiryDate;
    __weak IBOutlet UIButton *buttonDelete;
    MBProgressHUD *hud;
}

@end

@implementation DeleteSavedCardsTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self CustomNavigationBar];
    labelCardNumber.text = [NSString stringWithFormat:@"**** **** **** %@",[_arrCardDetail valueForKey:@"number"]];
    labelCardHolderName.text = [[_arrCardDetail valueForKey:@"card_type"] capitalizedString];
    labelExpiryDate.text = [NSString stringWithFormat:@"%@/%@",[_arrCardDetail valueForKey:@"exp_month"],[_arrCardDetail valueForKey:@"exp_year"]];
    [[CommonMethods sharedInstance]DropShadow:buttonDelete UIColor:[UIColor redColor] andShadowRadius:3.0];
}

-(void)CustomNavigationBar{
    self.tabBarController.tabBar.hidden =  true;
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    //[backButton setBackgroundColor:[UIColor redColor]];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 5.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    self.title = @"Card Details";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}

-(void)BackButtonPressed{
    [self.navigationController popViewControllerAnimated:true];
}
- (IBAction)DeleteCard:(id)sender {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Alert"
                                 message:@"Are you sure to delete this card?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    //Add Buttons
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    [self ApiToDeleteCard];
                                }];
    
    //Add your buttons to alert controller
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"NO"
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


#pragma mark- Api for deleting the card

-(void)ApiToDeleteCard{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];

        [WebServiceManager deleteRequestWithUrlString:[NSString stringWithFormat:@"%@cards/%@",kBaseUrl,[_arrCardDetail valueForKey:@"id"]] emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                labelCardHolderName.text = @"";
                labelExpiryDate.text = @"";
                labelCardNumber.text = @"";
                buttonDelete.hidden = true;
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Info"
                                             message:@"Your card has been successfully deleted"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                //Add Buttons
                
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"OK"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action) {
                                                [self.navigationController popViewControllerAnimated:true];
                                            }];
                
                //Add your buttons to alert controller
                [alert addAction:yesButton];
                [self presentViewController:alert animated:YES completion:nil];
            });
            
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
//            hud = [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
//            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
            
        });
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
