//
//  ActiveBookingTableViewCell.m
//  YachtMasters
//
//  Created by Anvesh on 28/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "ActiveBookingTableViewCell.h"

@implementation ActiveBookingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _viewBackGround.layer.cornerRadius = 5.0;
    _viewBackGround.clipsToBounds = false;
    _viewBackGround.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1];
    
    // drop shadow
    _viewBackGround.layer.shadowColor = [UIColor colorWithRed:231.0/255.0 green:238.0/255.0 blue:253.0/255.0 alpha:1] .CGColor;
    _viewBackGround.layer.shadowOffset = CGSizeMake(0, 1.0);
    _viewBackGround.layer.shadowOpacity = 3.0;
    _viewBackGround.layer.shadowRadius = 10.0;
    
    
    _viewRound.layer.cornerRadius = 35.0;
    _viewRound.clipsToBounds = true;
    
    _imageViewRound.layer.cornerRadius = 30.0;
    _imageViewRound.clipsToBounds = true;

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
