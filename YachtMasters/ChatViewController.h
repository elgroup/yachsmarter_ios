//
//  ChatViewController.h
//  YachtMasters
//
//  Created by Anvesh on 31/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "PSWebSocket.h"

@interface ChatViewController : UIViewController ///<>
//@property (nonatomic, strong) PSWebSocket *socket;
@property (nonatomic,strong) NSMutableArray *arrConversations;
@property (nonatomic,strong) NSArray *arrCurrentUser;

@end
