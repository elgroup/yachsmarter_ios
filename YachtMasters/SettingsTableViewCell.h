//
//  SettingsTableViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 30/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UILabel *labelText;
@property (nonatomic,weak) IBOutlet UIImageView *imageTick;

@end
