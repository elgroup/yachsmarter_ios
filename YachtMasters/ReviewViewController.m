//
//  ReviewViewController.m
//  YachtMasters
//
//  Created by Anvesh on 17/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "ReviewViewController.h"
#import "Constants.h"
#import "ReviewTableViewCell.h"
#import "CommonMethods.h"
#import "WebServiceManager.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "YachtMasters-Swift.h"


@interface ReviewViewController ()<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource>
{
    MBProgressHUD *hud;
    NSInteger intRating;
    NSMutableArray *arrReview;
    BOOL isApi;
    NSString *strMessage;
    IBOutlet NSLayoutConstraint *lcBtmConatraint;
    CGFloat _currentKeyboardHeight;
}
@end

@implementation ReviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isApi = false;
    [self ApiForGettingReviews];
    intRating = 0;
    _currentKeyboardHeight = 0.0f;
    self.tabBarController.hidesBottomBarWhenPushed = true;
    self.tabBarController.tabBar.hidden = true;
    arrReview = [[NSMutableArray alloc]init];
    _textViewReview.text = @"Add review";
    _textViewReview.textColor = [UIColor lightGrayColor];
    // _textViewReview.delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    UITapGestureRecognizer *tapRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tableTapped:)];
    [_tableViewReview addGestureRecognizer:tapRec];
    _tableViewReview.delegate = self;
    _tableViewReview.dataSource = self;
    _tableViewReview.rowHeight = UITableViewAutomaticDimension;
    _tableViewReview.estimatedRowHeight = 97;
    [self scrollToBottom];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self CustomNavigationBar];
    if (_isOwner) {
        _viewForInputReview.hidden = true;
        strMessage = @"No reviews available for your Yacht";
        lcBtmConatraint.constant = -111;
    }
    else{
        _viewForInputReview.hidden = false;
        strMessage = @"Be the first person to review this Yacht";
        lcBtmConatraint.constant = 0;
    }
    
}

-(void)TableViewHelper:(NSString *)strMessage andtableView:(UITableView *)tableView{
    UILabel *label = [[UILabel alloc]init];
    label.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH, self.tableViewReview.frame.size.height);
    label.text = strMessage;
    label.numberOfLines = 0;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = FontOpenSans(15);
    label.textColor = [UIColor lightGrayColor];
    tableView.backgroundView = label;
}

-(void)CustomNavigationBar{
    self.navigationController.navigationBar.hidden = false;
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 10.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    self.title =@"Review";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}


-(void)BackButtonPressed{
    self.tabBarController.tabBar.hidden = false;
    [self.navigationController popViewControllerAnimated:true];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self scrollToBottom];
}
- (void)scrollToBottom{
    CGFloat yOffset = -64;
    if (_tableViewReview.contentSize.height > _tableViewReview.bounds.size.height) {
        yOffset = _tableViewReview.contentSize.height - _tableViewReview.bounds.size.height;
    }
    
    [_tableViewReview setContentOffset:CGPointMake(0, yOffset) animated:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableTapped:(UITapGestureRecognizer *)tap
{
    [self.view endEditing:true];
    if(_textViewReview.text.length == 0){
        _textViewReview.textColor = [UIColor lightGrayColor];
        _textViewReview.text = @"Add review";
        [_textViewReview resignFirstResponder];
    }
    
}
- (IBAction)ButtonStarPressed:(id)sender {
    
    switch ([sender tag]) {
        case 1:
            _buttonStar1.selected = true;
            _buttonStar2.selected = false;
            _buttonStar3.selected = false;
            _buttonStar4.selected = false;
            _buttonStar5.selected = false;
            intRating = 1;
            break;
        case 2:
            _buttonStar1.selected = true;
            _buttonStar2.selected = true;
            _buttonStar3.selected = false;
            _buttonStar4.selected = false;
            _buttonStar5.selected = false;
            intRating = 2;
            break;
        case 3:
            _buttonStar1.selected = true;
            _buttonStar2.selected = true;
            _buttonStar3.selected = true;
            _buttonStar4.selected = false;
            _buttonStar5.selected = false;
            intRating = 3;
            break;
        case 4:
            _buttonStar1.selected = true;
            _buttonStar2.selected = true;
            _buttonStar3.selected = true;
            _buttonStar4.selected = true;
            _buttonStar5.selected = false;
            intRating = 4;
            break;
        case 5:
            _buttonStar1.selected = true;
            _buttonStar2.selected = true;
            _buttonStar3.selected = true;
            _buttonStar4.selected = true;
            _buttonStar5.selected = true;
            intRating = 5;
            break;
            
        default:
            break;
    }
}

- (void)keyboardWasShown:(NSNotification *)notification{
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGFloat deltaHeight = kbSize.height - _currentKeyboardHeight;
    _currentKeyboardHeight = kbSize.height;
    [UIView animateWithDuration:0.25 animations:^{
        _tableViewReview.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH,SCREEN_HEIGHT - _currentKeyboardHeight - 111 );
        lcBtmConatraint.constant = _currentKeyboardHeight + 111;
       // _viewForInputReview.frame = CGRectMake(0.0,SCREEN_HEIGHT - _currentKeyboardHeight-111, SCREEN_WIDTH, _viewForInputReview.frame.size.height);
        [self scrollToBottom];
    }];
}

-(IBAction)ButtonReviewSendPressed:(id)sender{
    if (_textViewReview.text.length == 0 || [_textViewReview.text isEqualToString:@"Add review"]) {
        [[CommonMethods sharedInstance]AlertMessage:@"Please add review" andViewController:self];
    }
    else{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSInteger intId = [_strId integerValue];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:_textViewReview.text forKey:@"comment"];
        [dict setObject:[NSNumber numberWithInteger:intId] forKey:@"yacht_id"];
        [dict setObject:[NSNumber numberWithInteger:intRating] forKey:@"rating"];
        
        NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
        [dictResponse setObject:dict forKey:@"reviews"];
        [WebServiceManager postRequestWithUrlString:[NSString stringWithFormat:@"%@reviews",kBaseUrl] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            self.textViewReview.text = @"";
            self.textViewReview.userInteractionEnabled = false;
            self.buttonStar1.userInteractionEnabled = false;
            self.buttonStar2.userInteractionEnabled = false;
            self.buttonStar3.userInteractionEnabled = false;
            self.buttonStar4.userInteractionEnabled = false;
            self.buttonStar5.userInteractionEnabled = false;
            self.buttonStar5.selected = false;
            self.buttonStar4.selected = false;
            self.buttonStar3.selected = false;
            self.buttonStar2.selected = false;
            self.buttonStar1.selected = false;
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
            });
            [self ApiForGettingReviews];
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
//            hud = [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
//            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
    }
}

-(void)keyboardWillBeHidden:(NSNotification *)notification{
    [UIView animateWithDuration:0.25 animations:^{
        _tableViewReview.frame = CGRectMake(0.0, 0.0, SCREEN_WIDTH,SCREEN_HEIGHT - 111 );
        _viewForInputReview.frame = CGRectMake(0.0,SCREEN_HEIGHT - 111 , SCREEN_WIDTH, _viewForInputReview.frame.size.height);
        [self scrollToBottom];
    }];
    
}

#pragma mark - textview delegate
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView{
    _textViewReview.text = @"";
    _textViewReview.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView{
    if(_textViewReview.text.length == 0){
        _textViewReview.textColor = [UIColor lightGrayColor];
        _textViewReview.text = @"Add review";
        [_textViewReview resignFirstResponder];
    }
}
#pragma mark - tableView Delegate and Datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if ([arrReview count] >0) {
        [self TableViewHelper:@"" andtableView:self.tableViewReview];
        return 1;
    }
    else{
        if (isApi) {
        [self TableViewHelper:strMessage andtableView:self.tableViewReview];
        }
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"ReviewTableViewCell";
    ReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReviewTableViewCell"];
    
    if (cell == nil) {
        NSArray *nib =[[NSBundle mainBundle]loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.labelName.text = [[arrReview objectAtIndex:indexPath.row]valueForKey:@"user_name"];
    cell.labelReview.text = [[arrReview objectAtIndex:indexPath.row]valueForKey:@"comment"];
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[arrReview objectAtIndex:indexPath.row] valueForKey:@"user_image"]];
    NSURL *url = [NSURL URLWithString:strUrl];
    [cell.imageViewUser sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    float floatRating = [[[arrReview objectAtIndex:indexPath.row]valueForKey:@"rating"] floatValue];
    NSInteger intRatin = (NSInteger) roundf(floatRating);;
    if (floatRating) {
        
    }
    
    
    [[CommonMethods sharedInstance]RatingAndStar1:cell.imageViewStar1 Star2:cell.imageViewStar2 Star3:cell.imageViewStar3 Star4:cell.imageViewStar4 Star5:cell.imageViewStar5 andRating:intRatin];
    return cell;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrReview count];
}



#pragma mark - Api For getting reviews

-(void)ApiForGettingReviews{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        
        [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@reviews/%@",kBaseUrl,_strId] emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            arrReview = [[result valueForKey:@"reviews"]mutableCopy];
            [self.tableViewReview reloadData];
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                isApi = true;
                [self.tableViewReview reloadData];

            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
//            hud = [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
//            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
            
        });

    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
