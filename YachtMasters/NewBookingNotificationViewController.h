//
//  NewBookingNotificationViewController.h
//  YachtMasters
//
//  Created by Anvesh on 27/04/18.
//  Copyright © 2018 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewBookingNotificationViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *yachtNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIButton *approveButton;
@property (weak, nonatomic) IBOutlet UIButton *declineButton;
@property (weak, nonatomic) IBOutlet UILabel *startDatelabel;
@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;
@property (weak, nonatomic) IBOutlet UIButton *homeButton;
@property (weak, nonatomic) NSString *strYachtName;
@property (weak, nonatomic) NSString *strStartDate;
@property (weak, nonatomic) NSString *strEndDate;
@property (weak, nonatomic) NSString *strAmount;
@property (strong, nonatomic) NSMutableDictionary *dictYacht;

@end
