//
//  UserProfileTVC.m
//  YachtMasters
//
//  Created by Anvesh on 21/09/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "UserProfileTVC.h"
#import "Constants.h"
#import "WebServiceManager.h"
#import "CommonMethods.h"
#import "UIImageView+WebCache.h"
#import "MBProgressHUD.h"
#import "YachtMasters-Swift.h"

@interface UserProfileTVC (){
    IBOutlet UIButton *buttonReadMore;
    IBOutlet UILabel *labelDescription;
    IBOutlet UILabel *labelName;
    IBOutlet UILabel *labelAddress;
    IBOutlet UIImageView *imageProfile;
    IBOutlet UIButton *buttonBack;
    MBProgressHUD *hud;
    NSMutableArray *arrResponse;
    NSString *strText;
    CGSize heightText;
    BOOL isReadMore;
}

@end

@implementation UserProfileTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    strText = [[NSString alloc]init];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.hidesBottomBarWhenPushed = true;
    self.tabBarController.tabBar.hidden = true;
    self.navigationItem.leftBarButtonItem=nil;
    self.navigationItem.hidesBackButton=YES;
    self.navigationController.navigationBar.hidden = true;
    [self ApiForOwnerProfile];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Button Clicks
// Button Back Action Method
- (IBAction)ButtonBackPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

// Button Read More Method
- (IBAction)ButtonReadMorePressed:(UIButton *)sender {
    
    if (isReadMore) {
        isReadMore = false;
    }
    else{
        isReadMore = true;
    }
   // sender.selected = !sender.selected;
    [self.tableView reloadData];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 2) {
        
        if (isReadMore) {
            NSLog(@"bigger");
            labelDescription.numberOfLines = 0;
            CGSize Height = [[CommonMethods sharedInstance]findHeightForText:strText havingWidth:SCREEN_WIDTH - 50 andFont:FontOpenSans(13)];
            labelDescription.text = @"";
            labelDescription.text = [arrResponse valueForKey:@"about"];
            buttonReadMore.hidden = false;
            return 90 + Height.height -30;
        }
        else {
            labelDescription.numberOfLines = 0;
            if (heightText.height > 70.0) {
                buttonReadMore.selected = false;
                buttonReadMore.hidden = true;
                return 90;
            }
            else{
                return heightText.height+10;
            }
            
        }
    }
    else{
        if (!isReadMore) {
            [self addReadMoreStringToUILabel:labelDescription];
        }
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
    
}

#pragma mark - Api For Owner profile
-(void)ApiForOwnerProfile{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        
        NSString *strUrl =[NSString stringWithFormat:@"%@users/get_profile/%@?role=%@",kBaseUrl,_strId,Owner];
        [WebServiceManager getRequestUrlString:strUrl emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            arrResponse = [result valueForKey:@"user"];
            strText = [arrResponse valueForKey:@"about"];
//            NSMutableAttributedString* attrString = [[NSMutableAttributedString  alloc] initWithString:strText];
//            NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
//            [style setLineSpacing:4];
//            [attrString addAttribute:NSParagraphStyleAttributeName
//                               value:style
//                               range:NSMakeRange(0, strText.length)];
            [self addReadMoreStringToUILabel:labelDescription];
         //   labelDescription.text = strText;
            
            NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[arrResponse valueForKey:@"image"]];
            NSURL *url = [NSURL URLWithString:strUrl];
            [imageProfile sd_setImageWithURL:url placeholderImage:nil];
           
            labelName.text = [arrResponse valueForKey:@"name"];
            labelAddress.text =  [NSString stringWithFormat:@"%@",[arrResponse valueForKey:@"address"]];
             heightText = [[CommonMethods sharedInstance]findHeightForText:strText havingWidth:SCREEN_WIDTH - 50 andFont:FontOpenSans(13)];
            if (heightText.height > 70.0) {
                buttonReadMore.hidden = false;
            }
            else{
                buttonReadMore.hidden = true;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
               // [MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
            });
            
            
            [self.tableView reloadData];
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
               // [MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
            
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
//            hud = [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
//            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
            
        });
    }
}


- (void)addReadMoreStringToUILabel:(UILabel*)label
{
    NSString *readMoreText = @" ...Read More";
    NSInteger lengthForString = label.text.length;
    if (lengthForString >= 30){
        NSInteger lengthForVisibleString = [self fitString:label.text intoLabel:label];
        NSMutableString *mutableString = [[NSMutableString alloc] initWithString:label.text];
        NSString *trimmedString = [mutableString stringByReplacingCharactersInRange:NSMakeRange(lengthForVisibleString, (label.text.length - lengthForVisibleString)) withString:@""];
        NSInteger readMoreLength = readMoreText.length;
        NSString *trimmedForReadMore = [trimmedString stringByReplacingCharactersInRange:NSMakeRange((trimmedString.length - readMoreLength), readMoreLength) withString:@""];
        NSMutableAttributedString *answerAttributed = [[NSMutableAttributedString alloc] initWithString:trimmedForReadMore attributes:@{
                                                                                                                                        NSFontAttributeName : label.font
                                                                                                                                        }];
        
        NSMutableAttributedString *readMoreAttributed = [[NSMutableAttributedString alloc] initWithString:readMoreText attributes:@{NSFontAttributeName : FontOpenSans(13),NSForegroundColorAttributeName : [UIColor colorWithRed:93.0/255.0 green:193.0/255.0 blue:241.0/255.0 alpha:1.0] }];
        
        [answerAttributed appendAttributedString:readMoreAttributed];
        label.attributedText = answerAttributed;
        
        UITapGestureRecognizer *readMoreGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ButtonReadMorePressed:)];
//        readMoreGesture.tag = 1;
        readMoreGesture.numberOfTapsRequired = 1;
        [label addGestureRecognizer:readMoreGesture];
        
        label.userInteractionEnabled = YES;
    }
    else {
        
        NSLog(@"No need for 'Read More'...");
        
    }
}

- (NSUInteger)fitString:(NSString *)string intoLabel:(UILabel *)label
{
    UIFont *font           = label.font;
    NSLineBreakMode mode   = label.lineBreakMode;
    
    CGFloat labelWidth     = label.frame.size.width;
    CGFloat labelHeight    = label.frame.size.height;
    CGSize  sizeConstraint = CGSizeMake(labelWidth, CGFLOAT_MAX);
    
//    if (SYSTEM_VERSION_GREATER_THAN(iOS_7))
//    {
        NSDictionary *attributes = @{ NSFontAttributeName : font };
        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:string attributes:attributes];
        CGRect boundingRect = [attributedText boundingRectWithSize:sizeConstraint options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        {
            if (boundingRect.size.height > labelHeight)
            {
                NSUInteger index = 0;
                NSUInteger prev;
                NSCharacterSet *characterSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
                
                do
                {
                    prev = index;
                    if (mode == NSLineBreakByCharWrapping)
                        index++;
                    else
                        index = [string rangeOfCharacterFromSet:characterSet options:0 range:NSMakeRange(index + 1, [string length] - index - 1)].location;
                }
                
                while (index != NSNotFound && index < [string length] && [[string substringToIndex:index] boundingRectWithSize:sizeConstraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.height <= labelHeight);
                
                return prev;
            }
        }
//    }
//    else
//    {
//        if ([string sizeWithFont:font constrainedToSize:sizeConstraint lineBreakMode:mode].height > labelHeight)
//        {
//            NSUInteger index = 0;
//            NSUInteger prev;
//            NSCharacterSet *characterSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
//
//            do
//            {
//                prev = index;
//                if (mode == NSLineBreakByCharWrapping)
//                    index++;
//                else
//                    index = [string rangeOfCharacterFromSet:characterSet options:0 range:NSMakeRange(index + 1, [string length] - index - 1)].location;
//            }
//
//            while (index != NSNotFound && index < [string length] && [[string substringToIndex:index] sizeWithFont:font constrainedToSize:sizeConstraint lineBreakMode:mode].height <= labelHeight);
//
//            return prev;
//        }
//    }
    
    return [string length];
}

@end
