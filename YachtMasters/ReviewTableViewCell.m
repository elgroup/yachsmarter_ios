//
//  ReviewTableViewCell.m
//  YachtMasters
//
//  Created by Anvesh on 17/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "ReviewTableViewCell.h"
#import "CommonMethods.h"

@implementation ReviewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [[CommonMethods sharedInstance]CornerRadius:_imageViewUser Radius:20 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
