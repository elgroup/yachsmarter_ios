//
//  ProfileViewController.m
//  YachtMasters
//
//  Created by Anvesh on 21/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "ProfileViewController.h"
#import "AboutUsViewController.h"
#import "SettingViewController.h"
#import "YachtMasters-Swift.h"
#import "PrivacyPolicyViewController.h"
#import "SearchYachtViewController.h"
#import "SWRevealViewController.h"

@interface ProfileViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
{
    NSArray *arrName;
    NSArray *arrImages;
    MBProgressHUD *hud;
    AppDelegate *appDelegate;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableViewProfile;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    arrName = @[@"Settings",@"Invite Friends",@"Help",@"About us",@"Switch to Owner", @"Terms & Conditions", @"Privacy Policy",@"Logout"];
    arrImages = @[@"profilesettings",@"invitefriends",@"help",@"aboutus",@"switchtoowner",@"Terms-and-Conditions", @"PrivacyPolicy", @"logout"];
    [self setNeedsStatusBarAppearanceUpdate];
    //  [self setStatusBarHidden:NO];
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    self.tabBarController.tabBar.hidden = false;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [self.tableViewProfile reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}

-(void)CustomNavigationBar{
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    
    UIImage *image = [UIImage imageNamed:@"top strip.png"];
    UIImageView *imageViewNav = [[UIImageView alloc] initWithImage:image];
    imageViewNav.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 64);
    [self.navigationController.navigationBar addSubview:imageViewNav];
    self.navigationItem.title = @"MESSAGES";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:13]}];
    UIImage* image3 = [UIImage imageNamed:@"menu items.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    //[backButton setBackgroundColor:[UIColor redColor]];
    [backButton addTarget:self action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 5.0, 20.0, 20.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    
}

-(void)revealToggle:(id)sender{
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggleAnimated:YES];
}



#pragma mark - TableView Delegate and Datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        static NSString *simpleTableIdentifier = @"EditProfileTableViewCell";
        EditProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EditProfileTableViewCell"];
        
        if (cell == nil) {
            NSArray *nib =[[NSBundle mainBundle]loadNibNamed:simpleTableIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.buttonEdit addTarget:self action:@selector(ButtonEditPressed) forControlEvents:UIControlEventTouchUpInside];
        cell.imageViewProfilePicture.image = nil;
        NSURL *url = [[NSUserDefaults standardUserDefaults]URLForKey:@"Image"];
        [cell.imageViewProfilePicture sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        cell.labelName.text = [[NSUserDefaults standardUserDefaults]valueForKey:@"Name"];
        cell.labelAddress.text = [[NSUserDefaults standardUserDefaults]valueForKey:@"Address"];
        return cell;
    }
    else{
        static NSString *simpleTableIdentifier = @"ProfileTableViewCell";
        ProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileTableViewCell"];
        
        if (cell == nil) {
            NSArray *nib =[[NSBundle mainBundle]loadNibNamed:simpleTableIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.labelNames.text = [arrName objectAtIndex:indexPath.row - 1];
        cell.imageViewSettingsName.image = [UIImage imageNamed:[arrImages objectAtIndex:indexPath.row - 1]];
        if (indexPath.row == 5) {
            NSString *strRole = [[NSUserDefaults standardUserDefaults]valueForKey:@"role"];
            
            if ([strRole isEqualToString:Customer]) {
                strRole = @"Owner";
            }
            else{
                strRole = @"Customer";
            }
            //strRole =[NSString stringWithFormat:@"%@%@",[[strRole substringToIndex:1] uppercaseString],[strRole substringFromIndex:1]];
            cell.labelNames.text = [NSString stringWithFormat:@"Switch to %@",strRole];
        }
        if (indexPath.row == 8) {
            cell.labelNames.textColor = [UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:1];
        }
        
        
        return cell;
    }
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 9;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    float height = 0.0;
    if (IS_IPHONE_5) {
        height = 100.0;
    }
    else if (IS_IPHONE_6){
        height = 120.0;
    }
    else if (IS_IPHONE_6P){
        height = 130.0;
    }
    else if (IS_IPHONE_X){
        height = 140.0;
    }
    else if (IS_IPHONE_XSMAX){
        height = 150.0;
    }
    
    if (indexPath.row == 0) {
        return height;
    }
    else{
        return (self.tableViewProfile.frame.size.height - height)/8; //([[UIScreen mainScreen]bounds].size.height - 115 -20 - 49)/6;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 1) {
        SettingViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
        controller.isSwrevealView = false;
        [self.navigationController pushViewController:controller animated:true];
    }
    
    else if (indexPath.row == 2) {
        [self InviteFriends];
    }
    else if (indexPath.row == 3){
        HelpViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
        [self.navigationController pushViewController:controller animated:true];
        
    }
    else if (indexPath.row == 4){
        [self AboutUs];
    }
    else if (indexPath.row == 5) {
        NSString *strRole = [[NSUserDefaults standardUserDefaults]valueForKey:@"role"];
        NSString *strMessage;
        if ([strRole isEqualToString:Customer]){
            strMessage = @"Want to switch the role to Owner?";
        }
        else{
            strMessage = @"Want to switch the role to Customer?";
        }
        [self CustomAlert:strMessage yesButtonText:@"SWITCH" cancelButtonText:@"CANCEL" andID:1];
    }
    else if (indexPath.row == 6){
        PrivacyPolicyViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicyViewController"];
        controller.isTerms = true;
        [self.navigationController pushViewController:controller animated:true];
        
    }
    else if (indexPath.row == 7){
        PrivacyPolicyViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicyViewController"];
        controller.isTerms = false;
        [self.navigationController pushViewController:controller animated:true];
        
    }
    else if (indexPath.row == 8){
        //        [self CustomAlert];
        [self CustomAlert:@"Want to logout from YachtSmarter?" yesButtonText:@"LOGOUT" cancelButtonText:@"CANCEL" andID:2];
    }
}

-(void)InviteFriends{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSString *url=@"https://itunes.apple.com/us/app/yachtsmarter-yacht-rentals/id1378189484?ls=1&mt=8";
        NSString * title =[NSString stringWithFormat:@"You have been invited to join YACHTSMARTER by your friend %@\nclick on the below link to download the app\n%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"Name"],url];
        NSArray* dataToShare = @[title];
        UIActivityViewController* activityViewController =[[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
        activityViewController.excludedActivityTypes = @[UIActivityTypeAirDrop];
        [self presentViewController:activityViewController animated:YES completion:^{}];
    });
}


-(void)CustomAlert:(NSString *)strMessage yesButtonText:(NSString *)buttonText cancelButtonText:(NSString *)NoButtonText andID:(NSInteger)tag{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@""
                                     message:strMessage
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        //Add Buttons
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:buttonText
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        if (tag == 1){
                                            [self ApitoSwitchUser];
                                        }
                                        else{
                                            [self ApiForLogOut];
                                        }
                                    }];
        
        //Add your buttons to alert controller
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:NoButtonText
                                   style:UIAlertActionStyleDestructive
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                   }];
        
        
        [alert addAction:noButton];
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    });
}

#pragma mark - About us 

-(void)AboutUs{
    AboutUsViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsViewController"];
    controller.isAboutUs = true;
    [self.navigationController pushViewController:controller animated:true];
}


#pragma mark - Button   Edit Pressed
-(void)ButtonEditPressed{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EditProfileTVC *controller = [story instantiateViewControllerWithIdentifier:@"EditProfileTVC"];
    [self.navigationController pushViewController:controller animated:true];
}

#pragma mark - Api For  Switch   User
-(void)ApitoSwitchUser{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *strRole = [[NSUserDefaults standardUserDefaults]valueForKey:@"role"];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:@"Auth_token"];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        if ([strRole isEqualToString:@"owner"]) {
            [dict setObject:Customer forKey:@"role"];
        }
        else{
            [dict setObject:Owner forKey:@"role"];
        }
        [WebServiceManager postRequestWithUrlString:[NSString stringWithFormat:@"%@users/switch_user",kBaseUrl] withPostString:dict emailString:strRole accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            
            NSLog(@"%@",result);
            [[NSUserDefaults standardUserDefaults]setObject:[dict valueForKey:@"role"] forKey:@"role"] ;
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self.tableViewProfile reloadData];
            NSString *strRole = [[NSUserDefaults standardUserDefaults]stringForKey:@"role"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                if ([strRole isEqualToString:Customer]){
                    SearchYachtViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SearchYacht"]; //or the homeController
                    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                    appDelegate.window.rootViewController = navController;
                    //                    appDelegate.window.rootViewController = [loginController instantiateInitialViewController];
                    // [[CommonMethods sharedInstance] AlertMessage:@"You have switched to Customer Role" andViewController:self];
                    
                }
                else{
                    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    SWRevealViewController *controller = [mainStoryBoard instantiateViewControllerWithIdentifier:@"SwRootController"]; //or the homeController
                    appDelegate.window.rootViewController = controller;
                    // [[CommonMethods sharedInstance] AlertMessage:@"You have switched to Owner Role" andViewController:self];
                    
                }
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
                [[LoaderNew sharedLoader]hideLoader];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}

#pragma mark - Api Request for Logout
-(void)ApiForLogOut{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *strRole = @"";//[[NSUserDefaults standardUserDefaults]valueForKey:@"role"];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:@"Auth_token"];
        
        [WebServiceManager deleteRequestWithUrlString:[NSString stringWithFormat:@"%@users/sign_out",kBaseUrl] emailString:strRole accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setBool:false forKey:@"isLogIn"];
            [defaults setObject:@"" forKey:@"Auth_token"];
            [defaults setObject:@"" forKey:@"role"];
            [defaults synchronize];
            
            //[self.navigationController pushViewController:navController animated:YES];
            dispatch_async(dispatch_get_main_queue(), ^{
                // [MBProgressHUD hideHUDForView:self.view animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[NSUserDefaults standardUserDefaults]setBool:false forKey:@"LastUsed"];
                [appDelegate.socket unSubscribeChannel:@"AppearanceChannel"];
                LoginViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"]; //or the homeController
                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                appDelegate.window.rootViewController = navController;
                //   [self.navigationController pushViewController:loginController animated:true];
            });
            NSLog(@"%@",result);
            
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
        
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
