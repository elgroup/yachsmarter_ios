//
//  ImageTableViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 03/07/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;

@end
