//
//  YachtBookingsTableViewController.h
//  YachtMasters
//
//  Created by Anvesh on 24/07/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YachtBookingsTableViewController : UITableViewController{
    
    __weak IBOutlet UIButton *buttonEdit;
    __weak IBOutlet UIButton *buttonDelete;
    UIButton *buttonBack;
}
@property (strong, nonatomic) IBOutlet UIView *ViewForHeader;
@property (weak, nonatomic) IBOutlet UILabel *labelYachtName;
@property (weak, nonatomic) IBOutlet UILabel *labelPriceOfYacht;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewYachtImage;
@property (strong, nonatomic) IBOutlet UIView *viewForFooter;
@property (strong,nonatomic) NSString *strID;
@property (weak, nonatomic) IBOutlet UIButton *btnPrice;

@end
