//
//  FilterAmenitiesCollectionViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 29/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterAmenitiesCollectionViewCell : UICollectionViewCell
@property (nonatomic,weak)IBOutlet UIImageView *imageViewAmenities;
@property (nonatomic,weak)IBOutlet UILabel *labelAmenitiesName;
@end
