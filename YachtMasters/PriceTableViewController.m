//
//  PriceTableViewController.m
//  YachtMasters
//
//  Created by Anvesh on 03/09/18.
//  Copyright © 2018 EL. All rights reserved.
//

#import "PriceTableViewController.h"
#import "Constants.h"
#import "RPFloatingPlaceholderTextField.h"
#import "CommonMethods.h"

@interface PriceTableViewController ()<UITextFieldDelegate>{
    
    __weak IBOutlet UILabel *labelCurrecnyText;
    __weak IBOutlet RPFloatingPlaceholderTextField *textFieldPricePerHour;
    __weak IBOutlet RPFloatingPlaceholderTextField *textFieldHourFullDay;
    __weak IBOutlet RPFloatingPlaceholderTextField *textFieldPriceFullDay;
    __weak IBOutlet RPFloatingPlaceholderTextField *textFieldHourHalfDay;
    __weak IBOutlet RPFloatingPlaceholderTextField *textFieldPriceHalfDay;
    __weak IBOutlet RPFloatingPlaceholderTextField *textFieldPricePerWeek;
}

@end

@implementation PriceTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self CustomNavigationBar];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     NSDictionary *dictCurrencyList = [[NSUserDefaults standardUserDefaults]valueForKey:@"SelectedCurrency"];
     if (dictCurrencyList.count > 0) {
    labelCurrecnyText.text = [NSString stringWithFormat:@"Currently the selected Currency is %@. To change the Currency please go to Settings.",[dictCurrencyList valueForKey:@"Country"]];
     }
     else{
         labelCurrecnyText.text = [NSString stringWithFormat:@"Currently the selected Currency is USD. To change the Currency please go to Settings."];
     }
    
    if (_arrPrice.count > 0){
        for (NSDictionary *dict in _arrPrice) {
            if([[dict valueForKey:@"term"] isEqualToString:@"per_hour"]){
                textFieldPricePerHour.text = [[dict valueForKey:@"price"] stringValue];
            }
            else if ([[dict valueForKey:@"term"] isEqualToString:@"half_day"]){
                textFieldHourHalfDay.text = [[dict valueForKey:@"hours"] stringValue];
                textFieldPriceHalfDay.text = [[dict valueForKey:@"price"] stringValue];
            }
            else if ([[dict valueForKey:@"term"] isEqualToString:@"per_day"]){
                textFieldHourFullDay.text = [[dict valueForKey:@"hours"] stringValue];
                textFieldPriceFullDay.text = [[dict valueForKey:@"price"] stringValue];
            }
            else if ([[dict valueForKey:@"term"] isEqualToString:@"week"]){
                textFieldPricePerWeek.text = [[dict valueForKey:@"price"] stringValue];
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)CustomNavigationBar{
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 10.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    
    CGRect frame = CGRectMake(UIScreen.mainScreen.bounds.size.width - 40, 10, 30,30);
    UIButton *doneButton = [[UIButton alloc] initWithFrame:frame];
    [doneButton setImage:[UIImage imageNamed:@"check-mark"] forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(buttonTickClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButton =[[UIBarButtonItem alloc] initWithCustomView:doneButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    self.title = @"Add Price for Yacht";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}

-(void)BackButtonPressed{
    [self.navigationController popViewControllerAnimated:true];
}

-(void)buttonTickClicked{
    NSMutableArray *arrPrices = [[NSMutableArray alloc]init];
    BOOL isChecked = [self checkForThePrices];
    if (isChecked ) {
        if (textFieldPricePerHour.text.length > 0){
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            float price = [textFieldPricePerHour.text floatValue];
            [dict setObject:[NSNumber numberWithFloat:price] forKey:@"price"];
            [dict setObject:[NSNumber numberWithInteger:1] forKey:@"hours"];
            [dict setObject:@"per_hour" forKey:@"term"];
            [arrPrices addObject:dict];
        }
        if (textFieldHourHalfDay.text.length > 0 && textFieldPriceHalfDay.text.length > 0){
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            float price = [textFieldPriceHalfDay.text floatValue];
            NSInteger hours = [textFieldHourHalfDay.text integerValue];
            [dict setObject:[NSNumber numberWithFloat:price] forKey:@"price"];
            [dict setObject:[NSNumber numberWithInteger:hours] forKey:@"hours"];
            [dict setObject:@"half_day" forKey:@"term"];
            [arrPrices addObject:dict];
        }
        if (textFieldPriceFullDay.text.length > 0 && textFieldHourFullDay.text.length > 0){
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            float price = [textFieldPriceFullDay.text floatValue];
            [dict setObject:[NSNumber numberWithFloat:price] forKey:@"price"];
            [dict setObject:[NSNumber numberWithInteger:[textFieldHourFullDay.text integerValue]] forKey:@"hours"];
            [dict setObject:@"per_day" forKey:@"term"];
            [arrPrices addObject:dict];
        }
        if (textFieldPricePerWeek.text.length > 0){
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:[NSNumber numberWithFloat:[textFieldPricePerWeek.text floatValue]] forKey:@"price"];
            [dict setObject:[NSNumber numberWithInteger:0] forKey:@"hours"];
            [dict setObject:@"week" forKey:@"term"];
            [arrPrices addObject:dict];
        }
        [self.delegate priceForYacht:arrPrices];
        [self.navigationController popViewControllerAnimated:true];
    }
    else{
        NSLog(@"not checked");
    }
}


-(BOOL)checkForThePrices{
    
    if (textFieldPricePerHour.text.length == 0 && textFieldPriceHalfDay.text.length == 0 && textFieldPriceFullDay.text.length == 0 && textFieldPricePerWeek.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter the prices for a Yacht" andViewController:self];
        return false;
    }
    
    else if (textFieldHourHalfDay.text.length == 0 && textFieldPriceHalfDay.text.length > 0 ){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter hours for Half Day" andViewController:self];
        return false;
    }
    else if ([textFieldHourHalfDay.text isEqualToString:@"0"] && textFieldPriceHalfDay.text.length > 0 ){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter valid hours for Half Day" andViewController:self];
        return false;
    }
    else if (textFieldHourFullDay.text.length == 0 && textFieldPriceFullDay.text.length > 0 ){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter hours for Full Day" andViewController:self];
        return false;
    }
    else if ([textFieldHourFullDay.text isEqualToString:@"0"] && textFieldPriceFullDay.text.length > 0 ){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter valid hours for Full Day" andViewController:self];
        return false;
    }
    else if (textFieldHourHalfDay.text.length > 0 && textFieldPriceHalfDay.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter the price for Half Day" andViewController:self];
        return false;
    }
    else if (textFieldHourFullDay.text.length > 0 && textFieldPriceFullDay.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter the price for Full Day" andViewController:self];
        return false;
    }
    else{
        return true;
    }
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == textFieldHourFullDay){
        NSString *strText = [NSString stringWithFormat:@"%@%@",textField.text,string];
        int text = [strText intValue];
        if (text < 25) {
            return true;
        }
        else{
            return false;
        }
    }
    else if (textField == textFieldHourHalfDay){
        NSString *strText = [NSString stringWithFormat:@"%@%@",textField.text,string];
        int text = [strText intValue];
        if (text < 13) {
            return true;
        }
        else{
            if (textField.text.length > 1 ){
                return true;
            }
            else{
            return false;
            }
        }
    }
    else{
        return true;
    }
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0){
        return 2;
    }
    else if (section == 1){
        return 2;
    }
    else if (section == 2){
        return 2;
    }
    else{
        return 1;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }
    else if (section == 1){
        return 2;
    }
    else if(section == 2){
        return 2;
    }
    else if (section == 3){
        return 2;
    }
    else{
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor lightGrayColor];
    return view;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView endEditing:true];
}


/*
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
 
 // Configure the cell...
 
 return cell;
 }
 */

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
