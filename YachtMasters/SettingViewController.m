//
//  SettingViewController.m
//  YachtMasters
//
//  Created by Anvesh on 06/01/18.
//  Copyright © 2018 EL. All rights reserved.
//

#import "SettingViewController.h"
#import "Constants.h"
#import "AddAccountViewController.h"
#import "AddAccountTVC.h"
#import "CommonMethods.h"
#import "WebServiceManager.h"
#import "MBProgressHUD.h"
#import "SettingsTableViewCell.h"
#import "DeleteSavedCardsTVC.h"
#import "WalletViewController.h"
#import "SWRevealViewController.h"
#import "CustomNavigationViewController.h"
#import "AddStripeAccountViewController.h"
#import "YachtMasters-Swift.h"
#import "PaymentsTVC.h"

@interface SettingViewController ()<UITableViewDelegate,UITableViewDataSource>{
    __weak IBOutlet UITableView *tableViewSettings;
    NSMutableArray *arrNumberOfCards;
    MBProgressHUD *hud;
    BOOL isApi;
    IBOutlet UITableView *_tblSettings;
    NSString *strRole;
    NSIndexPath *_ipSelected;
    NSMutableArray *arrCurrencyList;
    NSMutableIndexSet *expandedSections;
    BOOL isExpand;
    IBOutlet NSLayoutConstraint *lcTopConstraint;
}


@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isExpand = NO;
    NSMutableArray *array = [[NSUserDefaults standardUserDefaults]valueForKey:@"CurrencyList"];
    if ([array count] > 0) {
        arrCurrencyList = [[NSMutableArray alloc]init];
        arrCurrencyList = [array mutableCopy];
    } else {
        [self createDictionaryOfCurrency];
    }
    
    
    
    // Do any additional setup after loading the view.
}

-(void)createDictionaryOfCurrency {
    arrCurrencyList = [[NSMutableArray alloc] init];
    NSMutableDictionary *dictionary = [[ NSMutableDictionary alloc] init];
    [ dictionary setObject:@"USD" forKey:@"Country"];
    [ dictionary setObject: @"1" forKey:@"isSelected"];
    [arrCurrencyList addObject:dictionary];
    
    NSMutableDictionary *dictionary1 = [[ NSMutableDictionary alloc] init];
    [ dictionary1 setObject:@"EUR" forKey:@"Country"];
    [ dictionary1 setObject: @"0" forKey:@"isSelected"];
    [arrCurrencyList addObject:dictionary1];
    
    NSMutableDictionary *dictionary2 = [[ NSMutableDictionary alloc] init];
    [ dictionary2 setObject:@"GBP" forKey:@"Country"];
    [ dictionary2 setObject: @"0" forKey:@"isSelected"];
    [arrCurrencyList addObject:dictionary2];
    
    NSMutableDictionary *dictionary3 = [[ NSMutableDictionary alloc] init];
    [ dictionary3 setObject:@"AUD" forKey:@"Country"];
    [ dictionary3 setObject: @"0" forKey:@"isSelected"];
    [arrCurrencyList addObject:dictionary3];
    
    NSMutableDictionary *dictionary4 = [[ NSMutableDictionary alloc] init];
    [ dictionary4 setObject:@"PLN" forKey:@"Country"];
    [ dictionary4 setObject: @"0" forKey:@"isSelected"];
    [arrCurrencyList addObject:dictionary4];
    
    NSMutableDictionary *dictionary5 = [[ NSMutableDictionary alloc] init];
    [ dictionary5 setObject:@"CHF" forKey:@"Country"];
    [ dictionary5 setObject: @"0" forKey:@"isSelected"];
    [arrCurrencyList addObject:dictionary5];
    
    NSMutableDictionary *dictionary6 = [[ NSMutableDictionary alloc] init];
    [ dictionary6 setObject:@"NOK" forKey:@"Country"];
    [ dictionary6 setObject: @"0" forKey:@"isSelected"];
    [arrCurrencyList addObject:dictionary6];
    
    NSMutableDictionary *dictionary7 = [[ NSMutableDictionary alloc] init];
    [ dictionary7 setObject:@"SEK" forKey:@"Country"];
    [ dictionary7 setObject: @"0"  forKey:@"isSelected"];
    [arrCurrencyList addObject:dictionary7];
    
    NSMutableDictionary *dictionary8 = [[ NSMutableDictionary alloc] init];
    [ dictionary8 setObject:@"DKK" forKey:@"Country"];
    [ dictionary8 setObject: @"0" forKey:@"isSelected"];
    [arrCurrencyList addObject:dictionary8];
    
    NSMutableDictionary *dictionary9 = [[ NSMutableDictionary alloc] init];
    [ dictionary9 setObject:@"CAD" forKey:@"Country"];
    [ dictionary9 setObject: @"0"  forKey:@"isSelected"];
    [arrCurrencyList addObject:dictionary9];
    
    NSMutableDictionary *dictionary10 = [[ NSMutableDictionary alloc] init];
    [ dictionary10 setObject:@"BSD" forKey:@"Country"];
    [ dictionary10 setObject: @"0"  forKey:@"isSelected"];
    [arrCurrencyList addObject:dictionary10];
    
//    NSMutableDictionary *dictionary11 = [[ NSMutableDictionary alloc] init];
//    [ dictionary11 setObject:@"DOP" forKey:@"Country"];
//    [ dictionary11 setObject: @"0"  forKey:@"isSelected"];
//    [arrCurrencyList addObject:dictionary11];
    
    NSMutableDictionary *dictionary12 = [[ NSMutableDictionary alloc] init];
    [ dictionary12 setObject:@"MXN" forKey:@"Country"];
    [ dictionary12 setObject: @"0"  forKey:@"isSelected"];
    [arrCurrencyList addObject:dictionary12];
    
    NSMutableDictionary *dictionary13 = [[ NSMutableDictionary alloc] init];
    [ dictionary13 setObject:@"AED" forKey:@"Country"];
    [ dictionary13 setObject: @"0"  forKey:@"isSelected"];
    [arrCurrencyList addObject:dictionary13];
    
    NSMutableDictionary *dictionary14 = [[ NSMutableDictionary alloc] init];
    [ dictionary14 setObject:@"COP" forKey:@"Country"];
    [ dictionary14 setObject: @"0"  forKey:@"isSelected"];
    [arrCurrencyList addObject:dictionary14];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    strRole = [[NSUserDefaults standardUserDefaults] valueForKey:@"role"];
    if ([strRole isEqualToString:Customer]) {
        [self ApiForgettingCardList];
    }
    else{
        [self ApiForAccountDetails];
    }
    [self CustomNavigationBar];
    
    isApi = true;
    tableViewSettings.allowsMultipleSelection = NO;
    // _ipSelected = [NSIndexPath indexPathForRow:0 inSection:2];
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
}

-(void)CustomNavigationBar{
    self.tabBarController.tabBar.hidden =  true;
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    UIImage* image3;
    
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    if (_isSwrevealView) {
        
        self.navigationController.navigationBar.layer.shadowColor = [UIColor colorWithRed:40.0/255.0 green:107.0/255.0 blue:240.0/255.0 alpha:1.0].CGColor;
        self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
        self.navigationController.navigationBar.layer.shadowRadius = 7.0f;
        self.navigationController.navigationBar.layer.shadowOpacity = 1.0f;
        self.navigationController.navigationBar.layer.masksToBounds=NO;
    }
    
    image3  = [UIImage imageNamed:@"back arrow.png"];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    //   }
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 10.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    if (_isSwrevealView) {
        self.navigationController.navigationBar.topItem.leftBarButtonItem = leftBarButton;
    }
    else{
        self.navigationItem.title = @"Settings";
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
    // }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (_isSwrevealView) {
        self.navigationController.navigationBar.topItem.title = @"Settings";
    }
}

-(void)BackButtonPressed{
    if (isExpand) {
        //        NSString *str = [arrCurrencyList objectAtIndex:_ipSelected.row];
        //        [arrCurrencyList removeObjectAtIndex:_ipSelected.row];
        //        [arrCurrencyList insertObject:str atIndex:0];
        //        _ipSelected = [NSIndexPath indexPathForRow:0 inSection:2];
        //        [[NSUserDefaults standardUserDefaults]setObject:arrCurrencyList forKey:@"CurrencyList"];
    }
    self.navigationController.navigationBar.hidden = true;
    self.tabBarController.tabBar.hidden = false;
    [self.navigationController popViewControllerAnimated:true];
}

-(void)revealToggle:(id)sender{
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggleAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)ButtonPressedForAccount:(id)sender{
//    BOOL isNotTransferable = [arrNumberOfCards valueForKey:@"is_not_transferable"];
//    if (isNotTransferable){
//        AddAccountTVC *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AddAccountTVC"];
//        [self.navigationController pushViewController:controller animated:true];
//    }
//    else{
        if ([sender tag] == 201){
            PaymentsTVC *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentsTVC"];
            controller.isAddAccount = true;
            [self.navigationController pushViewController:controller animated:true];
        }
        else{
            AddStripeAccountViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AddStripeAccountViewController"];
            controller._isEdit = false;
            if (_isSwrevealView) {
                controller.isRevealView = true;
            }
            else{
                controller.isRevealView = false;
            }
            [self.navigationController pushViewController:controller animated:true];
        }
   // }
}

#pragma mark - Api For Getting Card list

-(void)ApiForgettingCardList{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@cards",kBaseUrl] emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                arrNumberOfCards = [result valueForKey:@"cards"];
                if ([arrNumberOfCards count] == 0){
                    [[CommonMethods sharedInstance]AlertMessage:@"No cards added yet. Please add a new card!!!" andViewController:self];
                }
                isApi = false;
                [tableViewSettings reloadData];
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
                [[CommonMethods sharedInstance]AlertMessage:@"Unable to fetch your saved cards!!!" andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}

#pragma mark - Api for getting Bank Account details

-(void)ApiForAccountDetails{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@bank_accounts",kBaseUrl] emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                arrNumberOfCards = [result mutableCopy];
                isApi = false;
                [self changeCurrencyDefalutValue: [result valueForKey:@"currency"] ];
                //[tableViewSettings reloadData];
                //      for (NSMutableDictionary *dict in arrCurrencyList) {
                //                    if ([[dict valueForKey:@"Country"] isEqualToString:[arrNumberOfCards valueForKey:@"currency"]]) {
                //                        [dict setObject: @"1" forKey:@"isSelected"];
                //                    }
                //                    for (int i = 0; i < arrCurrencyList.count; i++) {
                //                        if ([[[arrCurrencyList objectAtIndex:i] valueForKey:@"Country"] isEqualToString:[arrNumberOfCards valueForKey:@"currency"]]) {
                //                            //[[arrCurrencyList objectAtIndex:i] valueForKey:@"isSelected"];
                //                            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
                //                            dict = [arrCurrencyList objectAtIndex:i];
                //                            [dict setObject:@"1" forKey:@"isSelected"];
                //                            [arrCurrencyList removeObjectAtIndex:i];
                //                            [arrCurrencyList addObject:dict];
                //                        }
                //  }
                // }
                
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@""
                                             message:@"Unable to fetch data. Want to try again?"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                //Add Buttons
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"TRYAGAIN"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action) {
                                                [self ApiForAccountDetails];
                                            }];
                
                //Add your buttons to alert controller
                UIAlertAction* noButton = [UIAlertAction
                                           actionWithTitle:@"CANCEL"
                                           style:UIAlertActionStyleDestructive
                                           handler:^(UIAlertAction * action) {
                                               [self.navigationController popViewControllerAnimated:true];
                                           }];
                
                
                [alert addAction:noButton];
                [alert addAction:yesButton];
                [self presentViewController:alert animated:YES completion:nil];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}



#pragma mark - Table view delegates and data source
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"SettingsTableViewCell";
    SettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        NSArray *nib =[[NSBundle mainBundle]loadNibNamed:simpleTableIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if ([strRole isEqualToString:Customer]) {
        //        if ([arrNumberOfCards count] == 0) {
        //            if (indexPath.row == 1){
        //                cell.labelText.text = @"Add New Card";
        //                cell.imageTick.hidden = true;
        //            }
        //            else if (indexPath.row == 0){
        //                cell.labelText.text = @"No Card Found";
        //                cell.imageTick.hidden = true;
        //            }
        //        }
        //        else
        cell.labelText.text = [NSString stringWithFormat:@"**** **** **** %@",[[arrNumberOfCards objectAtIndex:indexPath.row] valueForKey:@"number"]];
        cell.imageTick.hidden = true;
    }
    else {
        if (indexPath.section == 0) {
            if (arrNumberOfCards){
                cell.labelText.text = [NSString stringWithFormat:@"%@",[arrNumberOfCards valueForKey:@"stripe_account_id"]];
            }
            else{
                cell.labelText.text = @"";
            }
            cell.imageTick.hidden = true;
        }
        else if (indexPath.section == 1){
            if ([arrNumberOfCards count]== 0) {
                cell.labelText.text = @"";
                
            }
            else {
                float totalWalletAmount = [[arrNumberOfCards valueForKey:@"availble_amount"] floatValue]+ [[arrNumberOfCards valueForKey:@"pending_amount"] floatValue];
                cell.labelText.text = [NSString stringWithFormat:@"%@%0.2f",[arrNumberOfCards valueForKey:@"symbol"],totalWalletAmount];
            }
            cell.imageTick.hidden = true;
        }
        else if (indexPath.section == 2 ){
            if (!isExpand) {
                cell.labelText.text = [[arrCurrencyList valueForKey:@"Country"]objectAtIndex:indexPath.row];
                cell.labelText.textColor = [UIColor blackColor];
                NSString *isSelected = [[arrCurrencyList objectAtIndex:indexPath.row] valueForKey:@"isSelected"];
                
                if (indexPath.row == 2) {
                    cell.labelText.text = @"Show more currencies";
                    cell.labelText.textColor = [UIColor colorWithRed:90.0/255.0 green:190.0/255.0 blue:238.0/255.0 alpha:1.0];
                    cell.imageTick.hidden = true;
                } else {
                    if ([isSelected isEqualToString:@"1"]) {
                        cell.imageTick.hidden = false;
                    } else {
                        cell.imageTick.hidden = true;
                    }
                }
            }
            else {
                
                //NSInteger totalRow = [tableView numberOfRowsInSection:indexPath.section];//first get total rows in that section by current indexPath.
                if(indexPath.row == arrCurrencyList.count) {
                    cell.labelText.text = @"Show less currencies";
                    cell.labelText.textColor = [UIColor colorWithRed:90.0/255.0 green:190.0/255.0 blue:238.0/255.0 alpha:1.0];
                    cell.imageTick.hidden = true;
                    
                }
                else{
                    cell.labelText.text = [[arrCurrencyList valueForKey:@"Country"]objectAtIndex:indexPath.row];
                    cell.labelText.textColor = [UIColor blackColor];
                    
                    NSString *isSelected = [[arrCurrencyList objectAtIndex:indexPath.row] valueForKey:@"isSelected"];
                    
                    if ([isSelected isEqualToString:@"1"]) {
                        cell.imageTick.hidden = false;
                    } else {
                        cell.imageTick.hidden = true;
                    }
                }
                
            }
        }
    }
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, SCREEN_WIDTH,50.0)];
    // view.backgroundColor = [UIColor redColor];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(20.0, 5.0, SCREEN_WIDTH - 40 , 40);
    [view addSubview:button];
    
    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    if ([strRole isEqualToString:Customer]) {
        if( [arrNumberOfCards count] > 0){
            [button setTitle:@"Saved cards" forState:UIControlStateNormal];
        }
        else{
            [button setTitle:@"Add New Card" forState:UIControlStateNormal];
            button.tag = 201;
            [button addTarget:self action:@selector(ButtonPressedForAccount:) forControlEvents:UIControlEventTouchUpInside];
        }
        
    }
    else{
        if (section == 0) {
            if ([[arrNumberOfCards valueForKey:@"stripe_account_id"]  isEqualToString:@""]) {
                BOOL isNotTransferable = [[arrNumberOfCards valueForKey:@"is_not_transferable"] boolValue];
                if (!isNotTransferable){
                    [button setTitle:@"Add stripe account" forState:UIControlStateNormal];
                }
                else{
                    [button setTitle:@"Add Bank account" forState:UIControlStateNormal];
                }
                
                button.tag = 301;
                [button addTarget:self action:@selector(ButtonPressedForAccount:) forControlEvents:UIControlEventTouchUpInside];
            }
            else{
                [button setTitle:@"Bank account" forState:UIControlStateNormal];
                
            }
        }
        else if (section == 1){
            [button setTitle:@"Wallet" forState:UIControlStateNormal];
        }
        else if (section == 2){
            [button setTitle:@"Currency" forState:UIControlStateNormal];
        }
        
    }
    button.titleLabel.font = FontOpenSans(13);
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([strRole isEqualToString:Customer]) {
        if ([arrNumberOfCards count] == 0) {
            if (isApi) {
                return 0;
            }
            else{
                return 0;
            }
        }
        else{
            return [arrNumberOfCards count];
        }
    }
    else{
        if (section == 0) {
            if ([[arrNumberOfCards valueForKey:@"stripe_account_id"] isEqualToString:@""]) {
                return 0;
            }
            else{
                return 1;
            }
        }
        else if(section == 1){
            return 1;
        }
        else{
            if (!isExpand) {
                return 3;
            }
            else{
                return [arrCurrencyList count] + 1;
            }
        }
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if ([strRole isEqualToString:Customer]) {
        return 1;
    }
    else{
        return 3;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([strRole isEqualToString:Customer]) {
        if ([arrNumberOfCards count]>0) {
            DeleteSavedCardsTVC *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DeleteSavedCardsTVC"];
            controller.arrCardDetail = [arrNumberOfCards objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:controller animated:true];
        }
        else{
            
        }
    }
    else {
        if (indexPath.section == 0) {
            //            AddAccountTVC *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AddAccountTVC"];
            //            controller.arrAccountDetail = [arrNumberOfCards valueForKey:@"stripe_account_id"];
            //            controller._isEdit = true;
            //            [self.navigationController pushViewController:controller animated:true];
        }
        
        else if (indexPath.section == 1) {
            WalletViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"WalletViewController"];
            NSString *strPendingAmount = [NSString stringWithFormat:@"%@%@",[arrNumberOfCards valueForKey:@"symbol"],[arrNumberOfCards valueForKey:@"pending_amount"] ];
            NSString *strAvailableAmount = [NSString stringWithFormat:@"%@%@",[arrNumberOfCards valueForKey:@"symbol"],[arrNumberOfCards valueForKey:@"availble_amount"]];
            BOOL isAccountCreated = [[arrNumberOfCards valueForKey:@"account_created"]boolValue];
            BOOL isNotTransferable = [[arrNumberOfCards valueForKey:@"is_not_transferable"] boolValue];
            controller.strAvailableWalletAmount = strAvailableAmount;
            controller.strPendingAmount = strPendingAmount;
            controller.isNotTransferable = isNotTransferable;
            controller.isAccountCreated = isAccountCreated;
            [self.navigationController pushViewController:controller animated:true];
        }
        else {
            if(isExpand) {
                
                if (indexPath.row == arrCurrencyList.count) {
                    isExpand = NO;
                    [tableView reloadSections:[NSIndexSet indexSetWithIndex:[indexPath section]]  withRowAnimation:UITableViewRowAnimationNone];
                } else {
                    [self updateCurrencyAndReloadTableView:(int)indexPath.row];
                }
                
            } else {
                if (indexPath.row == 2) {
                    isExpand = YES;
                    [tableView reloadSections:[NSIndexSet indexSetWithIndex:[indexPath section]]  withRowAnimation:UITableViewRowAnimationNone];
                } else {
                    [self updateCurrencyAndReloadTableView:(int)indexPath.row];
                }
            }
        }
    }
}


-(void)ApiForCurrencyChange{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:[[arrCurrencyList objectAtIndex:0] valueForKey:@"Country"] forKey:@"currency"];
        NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
        [dictResponse setObject:dict forKey:@"settings"];
        [WebServiceManager putRequestWithUrlString:[NSString stringWithFormat:@"%@settings",kBaseUrl] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            [self ApiForAccountDetails];
        } failure:^(NSString *msg, BOOL success) {
            
        }];
    }
}

-(void)updateCurrencyAndReloadTableView:(int)rowNumber {
    NSString *str = [[arrCurrencyList valueForKey:@"Country"] objectAtIndex:rowNumber];
    for (id item in arrCurrencyList) {
        NSString *str = [item valueForKey:@"Country"];
        NSString *isSelected = [item valueForKey:@"isSelected"];
        if ([isSelected isEqualToString:@"1"]) {
            NSMutableDictionary *dictionary = [[ NSMutableDictionary alloc] init];
            [ dictionary setObject:str forKey:@"Country"];
            [ dictionary setObject: @"0" forKey:@"isSelected"];
            [arrCurrencyList  replaceObjectAtIndex:0 withObject:dictionary];
            break;
        }
    }
    NSMutableDictionary *dictionary = [[ NSMutableDictionary alloc] init];
    [ dictionary setObject:str forKey:@"Country"];
    [ dictionary setObject: @"1" forKey:@"isSelected"];
    [arrCurrencyList  replaceObjectAtIndex:rowNumber withObject:dictionary];
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"isSelected"
                                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
    NSArray *sortedArray = [arrCurrencyList sortedArrayUsingDescriptors:sortDescriptors];
    arrCurrencyList = [NSMutableArray arrayWithArray:sortedArray];
    //    [tableViewSettings reloadData];
    //    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"CurrencyList"];
    //    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"SelectedCurrency"];
    [[NSUserDefaults standardUserDefaults]setObject:arrCurrencyList forKey:@"CurrencyList"];
    [[NSUserDefaults standardUserDefaults]setObject:[arrCurrencyList objectAtIndex:0] forKey:@"SelectedCurrency"];
    [tableViewSettings reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self ApiForCurrencyChange];
}
-(void)changeCurrencyDefalutValue:(NSString *)currencyString {
    for (int i=0;i<[arrCurrencyList count];i++) {
        NSString *currencyStr = [[arrCurrencyList objectAtIndex:i] valueForKey:@"Country"];
        if ([currencyStr isEqualToString:currencyString]) {
            NSMutableDictionary *dictionary = [[ NSMutableDictionary alloc] init];
            [ dictionary setObject:currencyStr forKey:@"Country"];
            [ dictionary setObject: @"1" forKey:@"isSelected"];
            [arrCurrencyList  replaceObjectAtIndex:i withObject:dictionary];
        }
        NSString *selectedVlaue = [[arrCurrencyList objectAtIndex:i] valueForKey:@"isSelected"];
        if ([selectedVlaue isEqualToString:@"1"] && ![currencyStr isEqualToString:currencyString]) {
            NSMutableDictionary *dictionary = [[ NSMutableDictionary alloc] init];
            [ dictionary setObject:currencyStr forKey:@"Country"];
            [ dictionary setObject: @"0" forKey:@"isSelected"];
            [arrCurrencyList  replaceObjectAtIndex:i withObject:dictionary];
        }
    }
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"isSelected"
                                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
    NSArray *sortedArray = [arrCurrencyList sortedArrayUsingDescriptors:sortDescriptors];
    arrCurrencyList = [NSMutableArray arrayWithArray:sortedArray];
    //    [tableViewSettings reloadData];
    //  [[NSUserDefaults standardUserDefaults]setObject:arrCurrencyList forKey:@"CurrencyList"];
    //  [[NSUserDefaults standardUserDefaults]setObject:[arrCurrencyList objectAtIndex:0] forKey:@"SelectedCurrency"];
    // [tableViewSettings reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
    [tableViewSettings reloadData];
    
}
@end
