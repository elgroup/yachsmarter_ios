//
//  CardDetailsTableViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 14/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPFloatingPlaceholderTextField.h"

@interface CardDetailsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextField *textFieldCardname;
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextField *textFieldCardNumber;
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextField *textFieldCardExpiryDate;
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextField *textFieldCardCvv;
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextField *textFieldCardCountry;
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextField *textFieldCardCurrency;
@property (strong, nonatomic) UIImageView *imageViewCard;
@end
