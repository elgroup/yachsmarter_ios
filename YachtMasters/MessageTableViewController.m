//
//  MessageTableViewController.m
//  YachtMasters
//
//  Created by Anju Singh Yadav on 8/26/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "MessageTableViewController.h"
#import "MessageTableViewCell.h"
#import "Constants.h"
#import "SWRevealViewController.h"
#import "ChatViewController.h"
#import "WebServiceManager.h"
#import "CommonMethods.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
#import "YachtMasters-Swift.h"

@interface MessageTableViewController ()
{
    NSMutableArray *arrChat;
    NSMutableArray *arrChatResponse;
    MBProgressHUD *hud;
    AppDelegate *appDelegate;
    BOOL isApi;
}
@end

@implementation MessageTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isApi = false;
    self.tabBarController.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBarController.tabBar.barStyle = UIBarStyleDefault;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    arrChatResponse = [[NSMutableArray alloc]init];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    isApi = false;
   
    [self CustomNavigationBar];
    [self GetListOfChats];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   //  self.tabBarController.tabBar.hidden = false;
    isApi = false;
    [self GetListOfChats];
}

-(void)CustomNavigationBar{
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    UIImage* image3 = [UIImage imageNamed:@"menu items.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    self.navigationItem.title = @"Messages";
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    //[backButton setBackgroundColor:[UIColor redColor]];
    [backButton addTarget:self action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 5.0, 20.0, 20.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}

-(void)revealToggle:(id)sender{
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggleAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (isApi) {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        if ([arrChatResponse count] ==0) {
            noDataLabel.hidden = false;
            button.hidden = false;
            noDataLabel.text             = @"No conversations found";
            noDataLabel.textColor        = [UIColor lightGrayColor];
            noDataLabel.textAlignment    = NSTextAlignmentCenter;
            tableView.backgroundView = noDataLabel;
            noDataLabel.userInteractionEnabled = true;
            noDataLabel.font = FontOpenSans(15);
            return 1;
        }
        else{
            tableView.backgroundView.hidden = true;
            return 1;
        }
    }
    else{
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([arrChatResponse count] >0) {
        return [arrChatResponse count];
    }
    else{
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"MessageTableViewCell";
    MessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageTableViewCell"];
    
    if (cell == nil) {
        NSArray *nib =[[NSBundle mainBundle]loadNibNamed:simpleTableIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[[arrChatResponse objectAtIndex:indexPath.row]valueForKey:@"recipient"] valueForKey:@"image"]];
    NSURL *url = [NSURL URLWithString:strUrl];
    [cell.imageViewProfilePicture sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    cell.labelName.text = [[[arrChatResponse objectAtIndex:indexPath.row] valueForKey:@"recipient"] valueForKey:@"name"];
    cell.labelMessage.text = [[[arrChatResponse objectAtIndex:indexPath.row] valueForKey:@"message"]valueForKey:@"body"];
    NSString *strDate = [[[arrChatResponse objectAtIndex:indexPath.row] valueForKey:@"message"]valueForKey:@"sent_at"];
    NSString *strUnreadMessage = [[[[arrChatResponse objectAtIndex:indexPath.row] valueForKey:@"sender"]valueForKey:@"unread_count"] stringValue];
    cell.labelMessageNotification.text = strUnreadMessage;
    cell.labelMessageNotification.hidden = false;
    if ([strUnreadMessage isEqualToString:@"0"]){
        cell.labelMessageNotification.hidden = true;
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSDate  *Date = [dateFormat dateFromString:strDate];
    NSTimeZone *outputTimeZone = [NSTimeZone systemTimeZone];
    NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
    [outputDateFormatter setTimeZone:outputTimeZone];
    [outputDateFormatter setDateFormat:@"hh:mm a"];
    NSString *outputString = [outputDateFormatter stringFromDate:Date];
    cell.labelTime.text = outputString;
    cell.labelTime.text = outputString;
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 90;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self GetConversationsForParticularUser:[[[arrChatResponse objectAtIndex:indexPath.row] valueForKey:@"id"] intValue] andArrayCurrentUser:[[arrChatResponse objectAtIndex:indexPath.row] valueForKey:@"current_user"]];
}


-(void)GetListOfChats{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@conversations",kBaseUrl] emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            arrChatResponse = [result valueForKey:@"conversations"];
            dispatch_async(dispatch_get_main_queue(), ^{
                // [MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                isApi = true;
                [self.tableView reloadData];
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                isApi = false;
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!hud) {
                isApi = false;
                //                hud = [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
                //                hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
                [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view.window];
            }
        });
    }
}


-(void)GetConversationsForParticularUser:(int)conversationID andArrayCurrentUser:(NSArray *)ArrUser{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@conversations/%d",kBaseUrl,conversationID] emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // [MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [appDelegate.socket joinChannel:@"ConversationChannel"];
                
                ChatViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
                controller.arrConversations = [result valueForKey:@"conversation"];
                controller.arrCurrentUser = ArrUser;//[[result valueForKey:@"conversation"] valueForKey:@""];
                [self.navigationController pushViewController:controller animated:true];
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // [MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:@"Unable to fetch conversation" andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //            hud = [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
            //            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view.window];
            
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
    
}


-(void)StatusOnline:(NSNotification *)notification{
    NSLog(@"%@",notification.object);
}
@end
