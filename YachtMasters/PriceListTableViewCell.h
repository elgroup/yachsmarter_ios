//
//  PriceListTableViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 18/09/18.
//  Copyright © 2018 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PriceListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelDuration;
@property (weak, nonatomic) IBOutlet UILabel *labelPrice;

@end
