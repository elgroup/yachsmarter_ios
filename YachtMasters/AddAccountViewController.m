//
//  AddAccountViewController.m
//  YachtMasters
//
//  Created by Anvesh on 29/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "AddAccountViewController.h"
#import "Constants.h"

@interface AddAccountViewController ()<UIPickerViewDelegate,UIPickerViewDataSource>
{
    UIBarButtonItem *doneButton;
     UIToolbar *toolBar;
    UIPickerView *pickerBankAccount;
    __weak IBOutlet UITextField *textFieldBankAccountType;
    NSArray *arrAccountType;
    
}
@end

@implementation AddAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self CustomNavigationBar];
    arrAccountType = @[@"IBAN Bank Account",@"US Bank Account",@"CA Bank Account",@"GB Bank Account",@"Other Bank Account"];
    doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(hideKeyboard)];
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 50)];
    //[toolBar setBarStyle:UIBarStyleBlackOpaque];
    toolBar.translucent=NO;
    toolBar.barTintColor = [UIColor colorWithRed:40.0/255.0 green:107.0/255.0 blue:240.0/255.0 alpha:1.0];
    
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton, nil];
    [toolBar setItems:toolbarItems];
    doneButton.tintColor = [UIColor whiteColor];
    textFieldBankAccountType.inputAccessoryView = toolBar;
    
    pickerBankAccount = [[UIPickerView alloc]init];
    pickerBankAccount.backgroundColor = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1.0];
    pickerBankAccount.delegate = self;
    textFieldBankAccountType.inputView = pickerBankAccount;

}

-(void)hideKeyboard{
    [self.view endEditing:true];
}
-(void)CustomNavigationBar{
    self.tabBarController.tabBar.hidden =  true;
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    //[backButton setBackgroundColor:[UIColor redColor]];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 5.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    self.title = @"Add Account";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}

-(void)BackButtonPressed{
    self.navigationController.navigationBar.hidden = true;
    self.tabBarController.tabBar.hidden = false;
    [self.navigationController popViewControllerAnimated:true];
}


#pragma mark - PickerView delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return  [arrAccountType count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [arrAccountType objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
   
        textFieldBankAccountType.text = [arrAccountType objectAtIndex:row];
    textFieldBankAccountType.textColor = [UIColor blackColor];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
