//
//  MakePaymentViewController.h
//  YachtMasters
//
//  Created by Anvesh on 05/09/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MakePaymentViewController : UIViewController
@property (nonatomic,strong) NSMutableDictionary *dictYachtJourneyDetails;
@property (weak, nonatomic) IBOutlet UIView *CalendarView;
@property (weak, nonatomic) IBOutlet UILabel *labelMonth;
@property (weak, nonatomic) IBOutlet UIButton *buttonNext;
@property (weak, nonatomic) IBOutlet UIButton *buttonPreviuos;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property(nonatomic,strong) NSDictionary *dictLocation;

@end
