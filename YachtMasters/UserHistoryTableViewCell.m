//
//  UserHistoryTableViewCell.m
//  YachtMasters
//
//  Created by Anvesh on 28/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "UserHistoryTableViewCell.h"

@implementation UserHistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _viewBackGround.layer.cornerRadius = 5.0;
    _viewBackGround.clipsToBounds = false;
    _viewBackGround.backgroundColor = [UIColor whiteColor];
    
    // drop shadow
    _viewBackGround.layer.shadowColor = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1] .CGColor;
    _viewBackGround.layer.shadowOffset = CGSizeMake(0, 1.0);
    _viewBackGround.layer.shadowOpacity = 1.0;
    _viewBackGround.layer.shadowRadius = 5.0;

    
    _viewROund.layer.cornerRadius = 35.0;
    _viewROund.clipsToBounds = true;
    _imageViewRound.layer.cornerRadius = 30.0;
    _imageViewRound.clipsToBounds = true;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
