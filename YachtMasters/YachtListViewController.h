//
//  YachtListViewController.h
//  YachtMasters
//
//  Created by Anvesh on 21/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface YachtListViewController : UIViewController
@property (nonatomic) float originalOrigin;

@property (nonatomic, assign) BOOL isLoading;

@property (assign, nonatomic) BOOL hasNextPage;

@property (assign, nonatomic) int currentPage;
@property int countryID;
@end
