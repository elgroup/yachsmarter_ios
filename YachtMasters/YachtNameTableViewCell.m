//
//  YachtNameTableViewCell.m
//  YachtMasters
//
//  Created by Anvesh on 03/07/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "YachtNameTableViewCell.h"

@implementation YachtNameTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _viewDeparture.layer.masksToBounds = NO;
    _viewDeparture.layer.shadowOffset = CGSizeMake(0, 0);
    _viewDeparture.layer.shadowRadius = 1;
    _viewDeparture.layer.shadowOpacity = 0.7;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    // Start at the Top Left Corner
    [path moveToPoint:CGPointMake(0.0, 0.0)];
    
    // Move to the Top Right Corner
    [path addLineToPoint:CGPointMake(CGRectGetWidth(_viewDeparture.frame), 0.0)];
    
    // Move to the Bottom Right Corner
    [path addLineToPoint:CGPointMake(CGRectGetWidth(_viewDeparture.frame), CGRectGetHeight(_viewDeparture.frame))];
    
    // This is the extra point in the middle :) Its the secret sauce.
    [path addLineToPoint:CGPointMake(CGRectGetWidth(_viewDeparture.frame) / 2.0, CGRectGetHeight(_viewDeparture.frame) / 2.0)];
    
    // Move to the Bottom Left Corner
    [path addLineToPoint:CGPointMake(0.0, CGRectGetHeight(_viewDeparture.frame))];
    
    // Move to the Close the Path
    [path closePath];
    
    _viewDeparture.layer.shadowPath = path.CGPath;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
