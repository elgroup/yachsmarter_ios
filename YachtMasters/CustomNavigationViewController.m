//
//  CustomNavigationViewController.m
//  YachtMasters
//
//  Created by Anvesh on 25/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "CustomNavigationViewController.h"
#import "AppDelegate.h"
#import "Constants.h"

@interface CustomNavigationViewController ()

@end

@implementation CustomNavigationViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewWillAppear:(BOOL)animated{
    _imageViewNav.backgroundColor = [UIColor whiteColor];
    [self CustomNav];
}

-(void)CustomNav{
    
    if (_imageViewNav) {
        _imageNav = [UIImage imageNamed:@"topstrip3"];
        _imageViewNav = [[UIImageView alloc] initWithImage:_imageNav];
        if (IS_IPHONE_X) {
            _imageViewNav.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 88);
        }
        else{
        _imageViewNav.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 64);
        }
        [self.navigationController.navigationBar.superview insertSubview:_imageViewNav atIndex:1];
        
    }
    [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"topstrip3"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationBar setShadowImage:[UIImage new]];
    self.navigationBar.layer.shadowColor = [UIColor colorWithRed:40.0/255.0 green:107.0/255.0 blue:240.0/255.0 alpha:1.0].CGColor;
    self.navigationBar.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    self.navigationBar.layer.shadowRadius = 7.0f;
    self.navigationBar.layer.shadowOpacity = 1.0f;
    self.navigationBar.layer.masksToBounds=NO;

    
}

- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)viewDidLayoutSubviews{
//    [super viewDidLayoutSubviews];
//    self.navigationBar.frame = CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, 500);
////    _imageViewNav.backgroundColor = [UIColor whiteColor];
////    [self CustomNav];
//}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
