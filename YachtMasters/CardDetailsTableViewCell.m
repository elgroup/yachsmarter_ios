//
//  CardDetailsTableViewCell.m
//  YachtMasters
//
//  Created by Anvesh on 14/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "CardDetailsTableViewCell.h"

@implementation CardDetailsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
