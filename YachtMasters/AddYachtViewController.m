
//
//  AddYachtViewController.m
//  YachtMasters
//
//  Created by Anvesh on 13/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "AddYachtViewController.h"
#import "RPFloatingPlaceholderTextField.h"
#import "TextFieldTableViewCell.h"
#import "RPFloatingPlaceholderTextView.h"
#import "PhotosCollectionViewCell.h"
#import "CommonMethods.h"
#import "MapViewController.h"
#import "AmenitiesViewController.h"
#import "MBProgressHUD.h"
#import "WebServiceManager.h"
#import "Constants.h"
#import "SWRevealViewController.h"
#import "DeleteImagesCollectionViewCell.h"
#import "ImageViewController.h"
#import <QBImagePickerController/QBImagePickerController.h>
#import "YachtMasters-Swift.h"
#import "PriceTableViewController.h"

@interface AddYachtViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AmenitiesDelagate,LocationDelegate,UITextViewDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIGestureRecognizerDelegate,QBImagePickerControllerDelegate,PriceDelegate>
{
    UIImagePickerController *ipc;
    __weak IBOutlet UITableView *tableViewAddYacht;
    NSMutableArray *arrImages;
    UICollectionView *collectionViewImages;
    NSMutableArray *arrAmenities;
    NSMutableDictionary *dictData;
    UITextField *textFieldPlace;
    UITextField *textFieldPrice;
    UITextField *textFieldYachtName;
    RPFloatingPlaceholderTextView *flTextView;
    UITextField *textFieldCountry;
    UITextField *textFieldState;
    UITextField *textFieldCity;
    UITextField *textFieldZipCode;
    UITextField *textFieldMaxMembers;
    UIScrollView *scrollView;
    BOOL isPresent;
    MBProgressHUD *hud;
    RPFloatingPlaceholderTextField *activeField;
    RPFloatingPlaceholderTextView *activeView;
    BOOL isTextView;
    //NSMutableArray *arrYachtDetail;
    UIPickerView *pickerViewCountry;
    UIPickerView *pickerViewState;
    NSMutableArray *arrCountry;
    NSMutableArray *arrayState;
    NSMutableArray *arr;
    NSString *str;
    QBImagePickerController *multipleImagePicker;
    CGSize sViewContentSize;
    CGSize keyboardSize;
    NSMutableArray *arrPrices;
    UISwitch *switchCaptain;
}

@end

@implementation AddYachtViewController
@synthesize arrYachtDetail;

- (void)viewDidLoad {
    [super viewDidLoad];
    arrImages = [[NSMutableArray alloc]init];
    [self CustomNavigationBar];
    str = [[NSString alloc]init];
    if (_isEdit) {
        arrAmenities = [[NSMutableArray alloc]init];
        arrPrices = [[NSMutableArray alloc]init];
        arrPrices = [[arrYachtDetail valueForKey:@"prices"] mutableCopy];
        
        NSMutableArray *_arrAmenities = [[NSMutableArray alloc]init];
        _arrAmenities = [[arrYachtDetail valueForKey:@"amenities"] mutableCopy];
        for (int i=0; i<[_arrAmenities count]; i++) {
            NSMutableDictionary *dict = [_arrAmenities objectAtIndex:i];
            NSString *urlStr = [dict valueForKey:@"icon"];
            NSString *idStr = [dict valueForKey:@"id"];
            NSString *nameStr = [dict valueForKey:@"name"];
            NSMutableDictionary *dict1 = [[NSMutableDictionary alloc] init];
            [dict1 setObject:urlStr forKey:@"url"];
            [dict setObject:dict1 forKey:@"icon"];
            [dict setObject:nameStr forKey:@"name"];
            [dict setObject:idStr forKey:@"id"];
            [arrAmenities addObject:dict];
        }
        arrImages = [[NSMutableArray alloc]init];
        arrImages = [arrYachtDetail valueForKey:@"yacht_images"];
        self.navigationItem.hidesBackButton = true;
        self.navigationController.navigationBar.hidden = false;
    }
}

-(void)CustomNavigationBar{
    
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 10.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    if (_isEdit) {
        self.title = @"Edit Yacht";
    }
    else{
        self.title = @"Add Yacht";
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}

-(void)BackButtonPressed{
    self.tabBarController.tabBar.hidden = false;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}
-(void)viewWillAppear:(BOOL)animated{
    self.tabBarController.hidesBottomBarWhenPushed = true;
    self.tabBarController.tabBar.hidden = true;
    
    for(UIView *subview in [self.view subviews]) {
        [subview removeFromSuperview];
    }
    
    [super viewWillAppear:animated];
    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0.0, 0.0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
    [self.view addSubview:scrollView];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonClicked:)];
    UIToolbar *toolBarPhoneNumber = [[UIToolbar alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-200, self.view.frame.size.width, 44)];
    toolBarPhoneNumber.translucent=NO;
    toolBarPhoneNumber.barTintColor = [UIColor colorWithRed:40.0/255.0 green:107.0/255.0 blue:240.0/255.0 alpha:1.0];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton, nil];
    [toolBarPhoneNumber setItems:toolbarItems];
    doneButton.tintColor = [UIColor whiteColor];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TapGesture:)];
    [scrollView addGestureRecognizer:tapRecognizer];
    
    textFieldYachtName = [self TextFieldCall:CGRectMake(30.0, 30.0, [[UIScreen mainScreen] bounds].size.width - 60, 30) andPlaceHolder:@"YACHT NAME"];
    [scrollView addSubview:textFieldYachtName];
    textFieldYachtName.keyboardType = UIKeyboardTypeAlphabet;
    
    textFieldPlace =  [self TextFieldCall:CGRectMake(30.0, 90.0, [[UIScreen mainScreen] bounds].size.width - 60, 30) andPlaceHolder:@"PLACE"];
    UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [addButton setImage:[UIImage imageNamed:@"target"] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(SwitchToMap) forControlEvents:UIControlEventTouchUpInside];
    textFieldPlace.rightViewMode = UITextFieldViewModeAlways;
    textFieldPlace.rightView = addButton;
    textFieldPlace.keyboardType = UIKeyboardTypeAlphabet;
    [scrollView addSubview:textFieldPlace];
    
    
    textFieldCountry = [self TextFieldCall:CGRectMake(30, 150.0, [[UIScreen mainScreen]bounds].size.width/2 - 30, 30) andPlaceHolder:@"COUNTRY"];
    [scrollView addSubview:textFieldCountry];
    
    textFieldState = [self TextFieldCall:CGRectMake([[UIScreen mainScreen]bounds].size.width /2, 150.0, [[UIScreen mainScreen] bounds].size.width/2, 30) andPlaceHolder:@"STATE"];
    [scrollView addSubview:textFieldState];
    // textFieldState.keyboardType = UIKeyboardTypeAlphabet;
    
    textFieldCity = [self TextFieldCall:CGRectMake(30.0, 210, [[UIScreen mainScreen] bounds].size.width/2 - 30, 30) andPlaceHolder:@"CITY"];
    [scrollView addSubview:textFieldCity];
    textFieldCity.keyboardType = UIKeyboardTypeAlphabet;
    
    textFieldZipCode = [self TextFieldCall:CGRectMake([[UIScreen mainScreen] bounds].size.width/2.0, 210, [[UIScreen mainScreen] bounds].size.width/2, 30) andPlaceHolder:@"ZIPCODE"];
    [scrollView addSubview:textFieldZipCode];
    textFieldZipCode.keyboardType = UIKeyboardTypeNumberPad;
    textFieldZipCode.inputAccessoryView = toolBarPhoneNumber;
    
    
    textFieldMaxMembers = [self TextFieldCall:CGRectMake(30.0, 270.0, [[UIScreen mainScreen] bounds].size.width - 60, 30) andPlaceHolder:@"MAXIMUM MEMBERS"];
    [scrollView addSubview:textFieldMaxMembers];
    textFieldMaxMembers.keyboardType = UIKeyboardTypeNumberPad;
    textFieldMaxMembers.inputAccessoryView = toolBarPhoneNumber;
    
    
    
    NSDictionary *dictCurrencyList = [[NSUserDefaults standardUserDefaults]valueForKey:@"SelectedCurrency"];
    if (dictCurrencyList.count > 0) {
        NSString *strPlaceHolder = [NSString stringWithFormat:@"PRICE FOR YACHT (%@)",[dictCurrencyList objectForKey:@"Country"]];
        textFieldPrice = [self TextFieldCall:CGRectMake(30.0, textFieldMaxMembers.frame.origin.y + textFieldMaxMembers.frame.size.height + 30, [[UIScreen mainScreen] bounds].size.width - 60, 30) andPlaceHolder:strPlaceHolder ];
    }
    else{
        NSString *strPlaceHolder = [NSString stringWithFormat:@"PRICE FOR YACHT (USD)"];
        textFieldPrice = [self TextFieldCall:CGRectMake(30.0, textFieldMaxMembers.frame.origin.y + textFieldMaxMembers.frame.size.height + 30, [[UIScreen mainScreen] bounds].size.width - 60, 30) andPlaceHolder:strPlaceHolder ];
    }
    
    [scrollView addSubview:textFieldPrice];
    
    UIView *viewPriceSection = [[UIView alloc]init];
    viewPriceSection.frame = CGRectMake(30.0, textFieldPrice.frame.origin.y + 30, [[UIScreen mainScreen] bounds].size.width - 60, 0);
    
    if (arrPrices.count > 0){
        int heightView = 0;
        for (NSDictionary *dict in arrPrices) {
            if ([[dict valueForKey:@"term"] isEqualToString:@"per_hour"]){
                UILabel *labelPerHourPrice = [[UILabel alloc]init];
                labelPerHourPrice.frame = CGRectMake(5.0, heightView, viewPriceSection.frame.size.width/2, 30);
                labelPerHourPrice.text = [NSString stringWithFormat:@"Per Hour Price: %0.0f", [[dict valueForKey:@"price"]floatValue]];
                labelPerHourPrice.textAlignment = NSTextAlignmentLeft;
                labelPerHourPrice.font = FontOpenSans(11);
                [viewPriceSection addSubview:labelPerHourPrice];
                heightView = heightView + 30;
                
            }
            if ([[dict valueForKey:@"term"] isEqualToString:@"per_day"]) {
                UILabel *labelHour = [[UILabel alloc]init];
                labelHour.frame = CGRectMake(5.0, heightView, viewPriceSection.frame.size.width/2, 30);
                labelHour.text = [NSString stringWithFormat:@"Full Day Hours: %ld", [[dict valueForKey:@"hours"]integerValue]];
                labelHour.textAlignment = NSTextAlignmentLeft;
                labelHour.font = FontOpenSans(11);
                [viewPriceSection addSubview:labelHour];
                UILabel *labelFullDayPrice = [[UILabel alloc]init];
                labelFullDayPrice.frame = CGRectMake(viewPriceSection.frame.size.width/2, heightView, viewPriceSection.frame.size.width/2, 30);
                labelFullDayPrice.text = [NSString stringWithFormat:@"Full Day Price: %0.0f", [[dict valueForKey:@"price"]floatValue] ];
                labelFullDayPrice.textAlignment = NSTextAlignmentLeft;
                labelFullDayPrice.font = FontOpenSans(11);
                [viewPriceSection addSubview:labelFullDayPrice];
                heightView = heightView + 30;
                
                
            }
            if ([[dict valueForKey:@"term"] isEqualToString:@"half_day"]) {
                
                UILabel *labelHour = [[UILabel alloc]init];
                labelHour.frame = CGRectMake(5.0, heightView, viewPriceSection.frame.size.width/2, 30);
                labelHour.text = [NSString stringWithFormat:@"Half Day Hours: %ld", [[dict valueForKey:@"hours"]integerValue]];
                labelHour.textAlignment = NSTextAlignmentLeft;
                labelHour.font = FontOpenSans(11);
                [viewPriceSection addSubview:labelHour];
                UILabel *labelFullDayPrice = [[UILabel alloc]init];
                labelFullDayPrice.frame = CGRectMake(viewPriceSection.frame.size.width/2, heightView, viewPriceSection.frame.size.width/2, 30);
                labelFullDayPrice.text = [NSString stringWithFormat:@"Half Day Price: %0.0f", [[dict valueForKey:@"price"]floatValue]];
                labelFullDayPrice.textAlignment = NSTextAlignmentLeft;
                labelFullDayPrice.font = FontOpenSans(11);
                [viewPriceSection addSubview:labelFullDayPrice];
                heightView = heightView + 30;
                
            }
            if ([[dict valueForKey:@"term"] isEqualToString:@"week"]){
                UILabel *labelPerHourPrice = [[UILabel alloc]init];
                labelPerHourPrice.frame = CGRectMake(5.0, heightView, viewPriceSection.frame.size.width/2, 30);
                labelPerHourPrice.text = [NSString stringWithFormat:@"Per Week Price: %0.0f", [[dict valueForKey:@"price"]floatValue]];
                labelPerHourPrice.textAlignment = NSTextAlignmentLeft;
                labelPerHourPrice.font = FontOpenSans(11);
                [viewPriceSection addSubview:labelPerHourPrice];
                heightView = heightView + 30;
            }
        }
        viewPriceSection.frame = CGRectMake(30.0, textFieldPrice.frame.origin.y + 30, [[UIScreen mainScreen] bounds].size.width - 60, heightView);
        [scrollView addSubview:viewPriceSection];
    }
    
    
    
    UIButton *btnAmenities = [UIButton buttonWithType:UIButtonTypeCustom];
    if (arrPrices.count > 0){
        btnAmenities.frame = CGRectMake(35.0, viewPriceSection.frame.origin.y + viewPriceSection.frame.size.height + 30 , [[UIScreen mainScreen] bounds].size.width - 60, 30.0);
    }
    else{
        btnAmenities.frame = CGRectMake(35.0, viewPriceSection.frame.origin.y + viewPriceSection.frame.size.height + 20 , [[UIScreen mainScreen] bounds].size.width - 60, 30.0);
    }
    [scrollView addSubview:btnAmenities];
    
    if (arrAmenities.count > 0) {
        [btnAmenities setTitle:[NSString stringWithFormat:@"AMENITIES %lu",(unsigned long)arrAmenities.count] forState:UIControlStateNormal];
        btnAmenities.titleLabel.font = FontOpenSansLight(12);//[UIFont fontWithName:@"Helvetica-SemiBold" size:10.0];
        [btnAmenities setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        
    }
    else{
        btnAmenities.titleLabel.font = FontOpenSans(12);//[UIFont fontWithName:@"Helvetica" size:12.0];
        [btnAmenities setTitle:@"AMENITIES" forState:UIControlStateNormal];
    }
    
    
    [btnAmenities setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    btnAmenities.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btnAmenities addTarget:self action:@selector(ButtonAminitiesClicked) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *viewAmenities = [[UIView alloc]init];
    viewAmenities.frame = CGRectMake(30.0, btnAmenities.frame.origin.y + 30, [[UIScreen mainScreen]bounds].size.width - 60,  30);
    
    CGFloat  k = 0.0,j = 0.0;
    for (int i = 1; i<= arrAmenities.count; i++) {
        UILabel *label = [[UILabel alloc]init];
        label.frame = CGRectMake(0.0 + k, 0.0 + j, viewAmenities.frame.size.width/2, 25);
        label.text = [NSString stringWithFormat:@"- %@",[[arrAmenities objectAtIndex:i -1] valueForKey:@"name"]];
        [viewAmenities addSubview:label];
        label.font = FontOpenSans(11);//[UIFont fontWithName:@"Helvetica" size:11];
        // label.backgroundColor = [UIColor redColor];
        if (i %2 == 0) {
            j = j + 25;
            k= 0.0;
        }
        if (i%2 == 1) {
            k = viewAmenities.frame.size.width/2;
        }
    }
    
    viewAmenities.frame = CGRectMake(30.0, btnAmenities.frame.origin.y + 30, [[UIScreen mainScreen]bounds].size.width - 60, j + 30);
    [scrollView addSubview:viewAmenities];
    
    
    CGRect frame2 = CGRectMake(30.f, viewAmenities.frame.origin.y + viewAmenities.frame.size.height + 20 , [[UIScreen mainScreen] bounds].size.width - 60, 95.f);
    flTextView = [[RPFloatingPlaceholderTextView alloc] initWithFrame:frame2];
    flTextView.tintColor = [UIColor blackColor];
    flTextView.defaultPlaceholderColor = [UIColor lightGrayColor];
    flTextView.placeholder = @"DESCRIPTION";
    flTextView.font = FontOpenSans(12);//[UIFont fontWithName:@"Helvetica" size:12.f];
    flTextView.autocorrectionType = UITextAutocorrectionTypeNo;
    flTextView.delegate = self;
    flTextView.floatingLabel.font = FontOpenSansLight(12);
    flTextView.floatingLabel.textColor = [UIColor darkGrayColor];
    [scrollView addSubview:flTextView];
    
    UILabel *labelAddPhotos = [[UILabel alloc]initWithFrame:CGRectMake(35.0, flTextView.frame.origin.y + 95 + 30, [[UIScreen mainScreen] bounds].size.width - 60, 20)];
    labelAddPhotos.textColor = [UIColor lightGrayColor];
    labelAddPhotos.text = @"ADD PHOTOS";
    [scrollView addSubview:labelAddPhotos];
    labelAddPhotos.font = FontOpenSansLight(12);//[UIFont fontWithName:@"Helvetica" size:10];
    flTextView.inputAccessoryView = toolBarPhoneNumber;
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    collectionViewImages = [[UICollectionView alloc]initWithFrame:CGRectMake(35.0, labelAddPhotos.frame.origin.y+35, [[UIScreen mainScreen] bounds].size.width - 60, 50.0) collectionViewLayout:layout];
    [collectionViewImages setDataSource:self];
    [collectionViewImages setDelegate:self];
    UINib *cellNib1 = [UINib nibWithNibName:@"PhotosCollectionViewCell" bundle:nil];
    [collectionViewImages registerNib:cellNib1 forCellWithReuseIdentifier:@"PhotosCollectionViewCell"];
    [collectionViewImages setBackgroundColor:[UIColor clearColor]];
    [scrollView addSubview:collectionViewImages];
    
    
    
    
    UILabel *labelCaptain = [[UILabel alloc]initWithFrame:CGRectMake(30.0, collectionViewImages.frame.origin.y + collectionViewImages.frame.size.height + 30, 200 , 30)];
    labelCaptain.text = @"Does Yacht has Captain?";
    labelCaptain.font = FontOpenSans(12);
    [scrollView addSubview:labelCaptain];
    
    switchCaptain = [[UISwitch alloc]initWithFrame:CGRectMake(210,labelCaptain.frame.origin.y , 30, 30)];
    //UISwitch *switchCaptain = [[UISwitch alloc]initWithFrame:];
    [scrollView addSubview:switchCaptain];
    [switchCaptain addTarget:self action:@selector(switchToggled:) forControlEvents: UIControlEventTouchUpInside];
    if ([[arrYachtDetail valueForKey:@"is_captain"] boolValue]){
        [switchCaptain setOn:true];
    }
    else{
        [switchCaptain setOn:false];
    }
    
    UIButton *buttonAdd = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonAdd.frame = CGRectMake(40.0, labelCaptain.frame.size.height + labelCaptain.frame.origin.y + 30, [[UIScreen mainScreen] bounds].size.width - 80, 50.0);
    [scrollView addSubview:buttonAdd];
    [buttonAdd setBackgroundImage:[UIImage imageNamed:@"add shape.png"] forState:UIControlStateNormal];
    if (_isEdit) {
        [buttonAdd setTitle:@"SAVE" forState:UIControlStateNormal];
    }
    else{
        [buttonAdd setTitle:@"ADD" forState:UIControlStateNormal];
    }
    [buttonAdd setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    buttonAdd.titleLabel.font = FontOpenSansBold(13);//[UIFont fontWithName:@"Helvetica" size:12];
    [buttonAdd addTarget:self action:@selector(ApiForAddingYacht) forControlEvents:UIControlEventTouchUpInside];
    [[CommonMethods sharedInstance]DropShadow:buttonAdd UIColor:[UIColor colorWithRed:90.0/255.0 green:190.0/255.0 blue:238.0/255.0 alpha:1.0] andShadowRadius:3.0];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Country" ofType:@"json"];
    NSData *content = [[NSData alloc] initWithContentsOfFile:filePath];
    NSDictionary *dictCountryCode = [NSJSONSerialization JSONObjectWithData:content options:kNilOptions error:nil];
    NSLog(@"%@",dictCountryCode);
    arrCountry = [[NSMutableArray alloc]init];
    arrCountry = [dictCountryCode valueForKey:@"array"];
    NSLog(@"%@",arrCountry);
    
    
    
    pickerViewCountry = [[UIPickerView alloc]init];
    pickerViewCountry.backgroundColor = [UIColor lightGrayColor];
    pickerViewCountry.delegate = self;
    
    textFieldCountry.inputView = pickerViewCountry;
    textFieldCountry.inputAccessoryView = toolBarPhoneNumber;
    
    pickerViewState = [[UIPickerView alloc]init];
    pickerViewState.backgroundColor = [UIColor lightGrayColor];
    pickerViewState.delegate = self;
    textFieldState.inputView = pickerViewState;
    textFieldState.inputAccessoryView = toolBarPhoneNumber;
    
    if (dictData.count > 0) {
        if ([[dictData valueForKey:@"YachtName"]  length] > 0) {
            textFieldYachtName.text = [[dictData valueForKey:@"YachtName"] capitalizedString];
        }
        //        if ([[dictData valueForKey:@"Price"]  length] > 0) {
        //            textFieldPrice.text = [dictData valueForKey:@"Price"];
        //        }
        if ([[dictData valueForKey:@"Place"] length]>0) {
            textFieldPlace.text = [[dictData valueForKey:@"Place"] capitalizedString];
        }
        
        if ([[dictData valueForKey:@"Photos"] count] > 0) {
            arrImages = [[dictData valueForKey:@"Photos"] mutableCopy];
            [collectionViewImages reloadData];
        }
        if ([[dictData valueForKey:@"Description"] length] >0) {
            flTextView.text = [dictData valueForKey:@"Description"];
        }
        if ([[dictData valueForKey:@"MaxMembers"] length] > 0 ) {
            textFieldMaxMembers.text = [dictData valueForKey:@"MaxMembers"];
        }
        if ([[dictData valueForKey:@"Zipcode"] length] > 0 ) {
            textFieldZipCode.text = [dictData valueForKey:@"Zipcode"];
        }
        if ([[dictData valueForKey:@"City"] length] > 0) {
            textFieldCity.text = [[dictData valueForKey:@"City"] capitalizedString];
        }
        if ([[dictData valueForKey:@"Country"] length] > 0) {
            textFieldCountry.text = [[dictData valueForKey:@"Country"] capitalizedString];
        }
        if ([[dictData valueForKey:@"State"]length] > 0) {
            textFieldState.text = [[dictData valueForKey:@"State"] capitalizedString];
        }
    }
    
    if (_isEdit) {
        //    NSString *strPrice = [arrYachtDetail valueForKey:@"price"];
        //  textFieldPrice.text = strPrice;
        flTextView.text = [arrYachtDetail valueForKey:@"description"];
        textFieldYachtName.text = [arrYachtDetail valueForKey:@"name"];
        textFieldCity.text = [[arrYachtDetail valueForKey:@"city"] capitalizedString];
        textFieldPlace.text = [[arrYachtDetail valueForKey:@"address"] capitalizedString];
        textFieldState.text = [[arrYachtDetail valueForKey:@"state"] capitalizedString];
        textFieldCountry.text = [[arrYachtDetail valueForKey:@"country"] capitalizedString];
        textFieldZipCode.text = [arrYachtDetail valueForKey:@"zipcode"];
        textFieldMaxMembers.text = [[arrYachtDetail valueForKey:@"max_guests"] stringValue];
        if (dictData.count > 0) {
            if ([[dictData valueForKey:@"YachtName"]  length] > 0) {
                textFieldYachtName.text = [dictData valueForKey:@"YachtName"];
            }
            //            if ([[dictData valueForKey:@"Price"]  length] > 0) {
            //                textFieldPrice.text = [dictData valueForKey:@"Price"];
            //            }
            if ([[dictData valueForKey:@"Place"] length]>0) {
                textFieldPlace.text = [[dictData valueForKey:@"Place"] capitalizedString];
            }
            
            if ([[dictData valueForKey:@"Photos"] count] > 0) {
                arrImages = [[dictData valueForKey:@"Photos"] mutableCopy];
                [collectionViewImages reloadData];
            }
            if ([[dictData valueForKey:@"Description"] length] >0) {
                flTextView.text = [dictData valueForKey:@"Description"];
            }
            if ([[dictData valueForKey:@"MaxMembers"] length] > 0 ) {
                textFieldMaxMembers.text = [dictData valueForKey:@"MaxMembers"];
            }
            if ([[dictData valueForKey:@"Zipcode"] length] > 0 ) {
                textFieldZipCode.text = [dictData valueForKey:@"Zipcode"];
            }
            if ([[dictData valueForKey:@"City"] length] > 0) {
                textFieldCity.text = [dictData valueForKey:@"City"];
            }
            if ([[dictData valueForKey:@"Country"] length] > 0) {
                textFieldCountry.text = [dictData valueForKey:@"Country"];
            }
            if ([[dictData valueForKey:@"State"]length] > 0) {
                textFieldState.text = [dictData valueForKey:@"State"];
            }
        }
    }
    sViewContentSize = CGSizeMake(UIScreen.mainScreen.bounds.size.width,buttonAdd.frame.origin.y+150);
    [scrollView setContentSize:sViewContentSize];
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.delegate = self;
    lpgr.delaysTouchesBegan = YES;
    [collectionViewImages addGestureRecognizer:lpgr];
    [self registerForKeyboardNotifications];
    //    self.title = @"Hello";
}

- (void) switchToggled:(id)sender {
    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]) {
        [dictData setObject:[NSNumber numberWithBool:true] forKey:@"isCaptain"];
    } else {
        [dictData setObject:[NSNumber numberWithBool:false] forKey:@"isCaptain"];
    }
}

#pragma mark - PickerView delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView == pickerViewCountry) {
        return arrCountry.count;
    }
    else{
        return arrayState.count;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (pickerView == pickerViewCountry) {
        return [[arrCountry  objectAtIndex:row] valueForKey:@"country_name"];
        
    }
    else{
        return [[arrayState  objectAtIndex:row] valueForKey:@"state_name"];
        
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    int countryId;
    if (pickerView == pickerViewCountry) {
        textFieldCountry.text = [[arrCountry objectAtIndex:row]valueForKey:@"country_name"];
        countryId = [[[arrCountry objectAtIndex:row] valueForKey:@"country_id"] intValue];
        arr = [[NSMutableArray alloc]init];
        NSString *filePathState = [[NSBundle mainBundle] pathForResource:@"State" ofType:@"json"];
        NSData *contentState = [[NSData alloc] initWithContentsOfFile:filePathState];
        NSDictionary *dictState = [NSJSONSerialization JSONObjectWithData:contentState options:kNilOptions error:nil];
        NSLog(@"%@",dictState);
        arrayState = [[NSMutableArray alloc]init];
        arrayState = [dictState valueForKey:@"array"];
        for (NSDictionary *dict in arrayState) {
            if ([[dict valueForKey:@"country_id"] intValue] == countryId) {
                [arr addObject:dict];
            }
        }
    }
    else{
        textFieldState.text = [[arrayState objectAtIndex:row] valueForKey:@"state_name"];
    }
    
}

#pragma mark - viewwilldisappear
-(void)viewWillDisappear:(BOOL)animated{
    dictData = [[NSMutableDictionary alloc]init];
    if (textFieldYachtName.text.length > 0) {
        [dictData setObject:textFieldYachtName.text forKey:@"YachtName"];
    }
    if (textFieldPrice.text.length > 0) {
        [dictData setObject:textFieldPrice.text forKey:@"Price"];
    }
    if (textFieldPlace.text.length > 0) {
        [dictData setObject:textFieldPlace.text forKey:@"Place"];
    }
    if (arrAmenities.count > 0) {
        [dictData setObject:arrAmenities forKey:@"Amenities"];
    }
    if (flTextView.text.length > 0) {
        [dictData setObject:flTextView.text forKey:@"Description"];
    }
    if (textFieldMaxMembers.text.length > 0 ) {
        [dictData setObject:textFieldMaxMembers.text forKey:@"MaxMembers"];
    }
    if (textFieldZipCode.text.length > 0) {
        [dictData setObject:textFieldZipCode.text forKey:@"Zipcode"];
    }
    if (textFieldCity.text.length > 0) {
        [dictData setObject:textFieldCity.text forKey:@"City"];
    }
    if (textFieldCountry.text.length > 0) {
        [dictData setObject:textFieldCountry.text forKey:@"Country"];
    }
    if (textFieldState.text.length > 0) {
        [dictData setObject:textFieldState.text forKey:@"State"];
    }
    if (arrImages.count > 0) {
        [dictData setObject:arrImages forKey:@"Photos"];
    }
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    dispatch_async(dispatch_get_main_queue(), ^{
        [[LoaderNew sharedLoader]hideLoader];
    });
}
#pragma mark - Tap Gesture

-(void)TapGesture:(id)sender{
    [self.view endEditing:true];
    
    [scrollView setContentSize:sViewContentSize];
}


#pragma mark - TextField Custom method

-(UITextField *)TextFieldCall:(CGRect)Rect andPlaceHolder:(NSString *)StrPlaceholder {
    CGRect frame = Rect;
    RPFloatingPlaceholderTextField *flTextField = [[RPFloatingPlaceholderTextField alloc] initWithFrame:frame];
    flTextField.floatingLabelActiveTextColor = [UIColor lightGrayColor];
    flTextField.floatingLabelInactiveTextColor = [UIColor grayColor];
    flTextField.placeholder = StrPlaceholder;
    flTextField.font = FontOpenSans(12);//[UIFont fontWithName:@"Helvetica" size:12.f];
    UIColor *color = [UIColor lightGrayColor];
    flTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:StrPlaceholder attributes:@{NSForegroundColorAttributeName: color}];
    flTextField.textColor = [UIColor blackColor];
    flTextField.tintColor = [UIColor blackColor];
    flTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    flTextField.rightViewMode = true;
    flTextField.delegate = self;
    flTextField.returnKeyType = UIReturnKeyNext;
    flTextField.floatingLabel.font = FontOpenSansLight(12);
    return flTextField;
}


#pragma mark - Button Click Method Amenities

-(void)ButtonAminitiesClicked{
    
    isPresent = false;
    if (str.length == 0) {
        str = @"isAppear";
    }
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AmenitiesViewController *controller = [story instantiateViewControllerWithIdentifier:@"AmenitiesViewController"];
    controller.delegate = self;
    controller.arrSelectedAmenities = [[NSMutableArray alloc] init];
    controller.arrSelectedAmenities = [arrAmenities mutableCopy];
    [self.navigationController pushViewController:controller animated:true];
}

#pragma mark - Delegate method AmenitiesDelegate
-(void)amenitiesRecieved:(NSMutableArray *)arrAmenities{
    self->arrAmenities = [[NSMutableArray alloc]init];
    NSSet *set = [NSSet setWithArray:arrAmenities];
    // NSArray *uniqueArray = [set allObjects];
    self->arrAmenities = [set allObjects];
}

#pragma mark - Delegate method For Price of Yacht
-(void)priceForYacht:(NSMutableArray *)arr{
    arrPrices = [[NSMutableArray alloc]init];
    arrPrices = [arr mutableCopy];
    NSLog(@"%@",arrPrices);
}

#pragma mark - Delegate method for recieve location
-(void)AddressOfYacht:(NSMutableArray *)arrCompleteAddress{
    
    
    NSArray *substrings = [[[arrCompleteAddress objectAtIndex:0]valueForKey:@"formatted_address"] componentsSeparatedByString:@","];
    //    NSString *first = [substrings objectAtIndex:0];
    
    NSString *strAddress = [[NSString alloc]init];
    int count = 0;
    for (NSUInteger i = substrings.count; i > 3 ; i--) {
        NSString *first = [substrings objectAtIndex:count];
        strAddress = [strAddress stringByAppendingString:[NSString stringWithFormat:@"%@ ,",first]];
        count++;
    }
    
    [dictData setObject:strAddress forKey:@"Place"];
    //    for (NSDictionary *dict in [[arrCompleteAddress valueForKey:@"address_components"] objectAtIndex:0]) {
    //        if ([[[dict valueForKey:@"types"]objectAtIndex:0] isEqualToString:@"country"]) {
    //            [dictData setObject:[dict valueForKey:@"long_name"] forKey:@"Country"];
    //        }
    //        if ([[[dict valueForKey:@"types"]objectAtIndex:0] isEqualToString:@"administrative_area_level_1"]) {
    //            [dictData setObject:[dict valueForKey:@"long_name"] forKey:@"State"];
    //        }
    //        if ([[[dict valueForKey:@"types"]objectAtIndex:0] isEqualToString:@"postal_code"]) {
    //            [dictData setObject:[dict valueForKey:@"long_name"] forKey:@"Zipcode"];
    //        }
    //        if ([[[dict valueForKey:@"types"]objectAtIndex:0] isEqualToString:@"locality"]) {
    //            [dictData setObject:[dict valueForKey:@"long_name"] forKey:@"City"];
    //        }
    //    }
    
    [dictData setObject:[[arrCompleteAddress objectAtIndex:1] valueForKey:@"Country"] forKey:@"Country"];
    [dictData setObject:[[arrCompleteAddress objectAtIndex:2] valueForKey:@"State"] forKey:@"State"];
    [dictData setObject:[[arrCompleteAddress objectAtIndex:4] valueForKey:@"ZipCode"] forKey:@"Zipcode"];
    [dictData setObject:[[arrCompleteAddress objectAtIndex:3] valueForKey:@"City"] forKey:@"City"];
    [dictData setObject:[[arrCompleteAddress objectAtIndex:0] valueForKey:@"Place"] forKey:@"Place"];
}

#pragma mark - Switch to Map view
-(void)SwitchToMap{
    isPresent = false;
    if (str.length == 0) {
        str = @"isAppear";
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // [[Loader sharedLoader]showLoaderOnScreenWithVc:self.view.window];
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MapViewController *controller = [story instantiateViewControllerWithIdentifier:@"MapViewController"];
        controller.delegate = self;
        [self.navigationController pushViewController:controller animated:true];
    });
    
}


#pragma mark - CollectionView Delegate and dataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 1 + arrImages.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == collectionViewDeleteImage) {
        static NSString *cellIdentifier = @"DeleteImagesCollectionViewCell";
        DeleteImagesCollectionViewCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.imageYacht.image = [arrImages objectAtIndex:indexPath.row-1];
        return cell;
        
    }
    else{
        static NSString *cellIdentifier = @"PhotosCollectionViewCell";
        PhotosCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        // cell.imageViewPhotos.image = [UIImage imageNamed:@"green unticked.png"];
        cell.imageViewPhotos.backgroundColor = [UIColor colorWithRed:90.0/255.0 green:190.0/255.0 blue:240.0/255.0 alpha:1.0];
        cell.imageViewPhotos.layer.cornerRadius = 2.0;
        cell.imageViewPhotos.clipsToBounds = true;
        
        cell.buttonPlus.tag = indexPath.row;
        if (indexPath.row == 0) {
            //        cell.imageViewPhotos.layer.borderColor = [UIColor lightGrayColor].CGColor;
            //        cell.imageViewPhotos.layer.borderWidth = 1.0;
            cell.imageViewPhotos.backgroundColor = [UIColor clearColor];
            //        cell.imageViewPhotos.image = nil;
            cell.imageViewPhotos.image = [UIImage imageNamed:@"add photo.png"];
            cell.buttonPlus.hidden = false;
            cell.buttonPlus.userInteractionEnabled = true;
        }
        else{
            cell.buttonPlus.userInteractionEnabled = true;
            [cell.buttonPlus setImage:nil forState:UIControlStateNormal];
            if(_isEdit){
                
                if (![str isEqualToString:@"isAppear"]) {
                    NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[arrImages objectAtIndex:indexPath.row -1]valueForKey:@"image_url"]];
                    
                    
                    __block NSMutableDictionary *dic = arrImages[indexPath.row-1];
                    
                    NSURL *url = [NSURL URLWithString:strUrl];
                    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                    dispatch_async(q, ^{
                        /* Fetch the image from the server... */
                        NSData *data = [NSData dataWithContentsOfURL:url];
                        UIImage *img = [[UIImage alloc] initWithData:data];
                        
                        // NSInteger index = [arrImages indexOfObject:dic];
                        [dic setObject:img forKey:@"image"];
                        // [dic removeObjectForKey:@"image_url"];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            /* This is the main thread again, where we set the tableView's image to
                             be what we just fetched. */
                            cell.imageViewPhotos.image = img;
                        });
                    });
                    
                }
                else{
                    cell.imageViewPhotos.image = [[arrImages objectAtIndex:indexPath.row-1]valueForKey:@"image"];
                }
                
            }
            else{
                NSDictionary *dictImage = [arrImages objectAtIndex:indexPath.row - 1];
                cell.imageViewPhotos.image = [dictImage objectForKey:@"image"];
            }
        }
        [cell.buttonPlus addTarget:self action:@selector(buttonPlusTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(50, 50);
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 3.0;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        
    }
    else{
        [UIView transitionWithView:viewDeleteImage
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
            viewDeleteImage.hidden = false;
        }
                        completion:NULL];
    }
}

-(void)DownloadImage:(NSURL *)url{
    //    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    //    dispatch_async(q, ^{
    //        /* Fetch the image from the server... */
    //        NSData *data = [NSData dataWithContentsOfURL:url];
    //        UIImage *img = [[UIImage alloc] initWithData:data];
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            /* This is the main thread again, where we set the tableView's image to
    //             be what we just fetched. */
    //            cell.imgview.image = img;
    //        });
    //    });
}



//-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.row == 0) {
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Choose Options"
//                                                                       message:@""
//                                                                preferredStyle:UIAlertControllerStyleActionSheet]; // 1
//        UIAlertAction *galleryAction = [UIAlertAction actionWithTitle:@"GALLERY"
//                                                                style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//                                                                    [self btnGalleryClicked];
//                                                                    NSLog(@"GALLERY");
//                                                                }]; // 2
//        UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"CAMERA"
//                                                               style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//                                                                   [self btnCameraClicked];
//                                                                   NSLog(@"TAKE PHOTO");
//                                                               }]; // 3
//
//        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL"
//                                                               style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
//
//                                                               }];
//
//        [alert addAction:galleryAction]; // 4
//        [alert addAction:cameraAction]; // 5
//        [alert addAction:cancelAction];
//
//        [self presentViewController:alert animated:YES completion:nil]; // 6
//    }
//}

#pragma mark - Button Plus action

-(void)buttonPlusTapped:(id)sender{
    UIButton *button = (UIButton *)sender;
    NSInteger buttonId = [button tag];
    if (buttonId == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Choose Options"
                                                                       message:@""
                                                                preferredStyle:UIAlertControllerStyleActionSheet]; // 1
        UIAlertAction *galleryAction = [UIAlertAction actionWithTitle:@"Gallery"
                                                                style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self btnGalleryClicked];
            NSLog(@"GALLERY");
        }]; // 2
        UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"Camera"
                                                               style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self btnCameraClicked];
            NSLog(@"TAKE PHOTO");
        }]; // 3
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                               style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            
        }];
        
        [alert addAction:galleryAction]; // 4
        [alert addAction:cameraAction]; // 5
        [alert addAction:cancelAction];
        
        [self presentViewController:alert animated:YES completion:nil]; // 6
    }
    else{
        ImageViewController *ivc = [[ ImageViewController alloc] init];
        ivc.selectedImages = arrImages;
        ivc.index          = (int) buttonId - 1;
        //        ivc.product        = self.product;
        //        ivc.vendor         = self.vendor;
        [self.navigationController pushViewController:ivc animated:YES];
    }
}

#pragma mark - Method for opening gallery

- (void)btnGalleryClicked
{
    
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {

            dispatch_async(dispatch_get_main_queue(), ^{

                switch (status) {
                    case PHAuthorizationStatusAuthorized:
                        multipleImagePicker = [[QBImagePickerController alloc]init];
                        multipleImagePicker.delegate = self;
                        multipleImagePicker.allowsMultipleSelection = YES;
                        multipleImagePicker.mediaType = QBImagePickerMediaTypeImage;
                        multipleImagePicker.prompt = @"Select the photos you want to upload!";
                        multipleImagePicker.showsNumberOfSelectedAssets = YES;
                        multipleImagePicker.numberOfColumnsInPortrait = 3;
                        multipleImagePicker.assetCollectionSubtypes = @[
                            @(PHAssetCollectionSubtypeSmartAlbumUserLibrary), // Camera Roll
                            @(PHAssetCollectionSubtypeAlbumMyPhotoStream), // My Photo Stream
                           // @(PHAssetCollectionSubtypeSmartAlbumPanoramas), // Panoramas
                            @(PHAssetCollectionSubtypeSmartAlbumBursts) // Bursts
                        ];
                        str = @"isAppear";
                        [self presentViewController:multipleImagePicker animated:YES completion:NULL];
                        NSLog(@"Authorised");
                        break;
                    case PHAuthorizationStatusRestricted:
                        NSLog(@"Restricted");
                        break;
                    case PHAuthorizationStatusDenied:{
                        NSString *accessDescription = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSPhotoLibraryUsageDescription"];
                        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:accessDescription message:@"To give permissions tap on 'Change Settings' button" preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
                        [alertController addAction:cancelAction];
                        
                        UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:@"Change Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                        }];
                        [alertController addAction:settingsAction];
                        
                        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
                    }
                        break;
                    default:
                        break;
                }
            });
        }];
  
}



- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets {
    PHImageManager *imageManager = [PHImageManager new];
    for (PHAsset *asset in assets) {
        [imageManager requestImageDataForAsset:asset
                                       options:0
                                 resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
            UIImage *image = [UIImage imageWithData:imageData];
            int idImage = -1;
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:image forKey:@"image"];
            [dict setObject:[NSNumber numberWithInt:idImage] forKey:@"id"];
            [arrImages addObject:dict];
            //                                     if ([arrImages count] == [ assets count]) {
            [collectionViewImages reloadData];
            //  }
            // Do something with image
        }];
        
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController {
    [self dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - ImagePickerController Delegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    isPresent = true;
    str = @"isAppear";
    UIImage *image =  [info objectForKey:UIImagePickerControllerOriginalImage];
    int idImage = -1;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:image forKey:@"image"];
    [dict setObject:[NSNumber numberWithInt:idImage] forKey:@"id"];
    [arrImages addObject:dict];
    [collectionViewImages reloadData];
    //[info objectForKey:UIImagePickerControllerOriginalImage];
    
    
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
//
#pragma mark - Method for Opening camera
- (void)btnCameraClicked{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    dispatch_async(dispatch_get_main_queue(), ^{
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        [self popCamera];
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        NSLog(@"%@", @"Camera access not determined. Ask for permission.");
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
            if(granted)
            {
                NSLog(@"Granted access to %@", AVMediaTypeVideo);
                [self popCamera];
            }
            else
            {
                NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                [self camDenied];
            }
        }];
    }
    else if (authStatus == AVAuthorizationStatusRestricted)
    {
        // My own Helper class is used here to pop a dialog in one simple line.
        [[CommonMethods sharedInstance]AlertMessage:@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access." andViewController:self];
    }
    else
    {
        [self camDenied];
    }
    });
}
- (void)camDenied{
    NSLog(@"%@", @"Denied camera access");
    
    NSString *alertText;
    NSString *alertButton;
    
    BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings){
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings app.\n\n2. Touch Privacy.\n\n3. Turn the Camera on.\n\n4. Open this app and try again.";
        
        alertButton = @"Go";
    }
    else{
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Touch Privacy.\n\n5. Turn the Camera on.\n\n6. Open this app and try again.";
        
        alertButton = @"OK";
    }
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Error"
                          message:alertText
                          delegate:self
                          cancelButtonTitle:alertButton
                          otherButtonTitles:nil];
    alert.tag = 3491832;
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 3491832)
    {
        BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
        if (canOpenSettings)
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}

-(void)popCamera{
    ipc = [[UIImagePickerController alloc] init];
    ipc.delegate = self;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:ipc animated:YES completion:NULL];
    }
    else
    {
        [[CommonMethods sharedInstance]AlertMessage:@"No Camera Available." andViewController:self];
    }
    
}
//
//    NSString *mediaType = AVMediaTypeVideo;
//    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
//    if(authStatus == AVAuthorizationStatusAuthorized) {
//        [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
//            if(granted){
//                ipc = [[UIImagePickerController alloc] init];
//                ipc.delegate = self;
//                if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
//                {
//                    ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
//                    [self presentViewController:ipc animated:YES completion:NULL];
//                }
//                else
//                {
//                    [[CommonMethods sharedInstance]AlertMessage:@"No Camera Available." andViewController:self];
//                }
//
//                NSLog(@"Granted access to %@", mediaType);
//            } else {
//                NSLog(@"Not granted access to %@", mediaType);
//            }
//        }];
//    } else if(authStatus == AVAuthorizationStatusDenied){
//        NSString *accessDescription = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Privacy - Camera Usage Description"];
//        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:accessDescription message:@"To give permissions tap on 'Change Settings' button" preferredStyle:UIAlertControllerStyleAlert];
//
//        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
//        [alertController addAction:cancelAction];
//
//        UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:@"Change Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
//        }];
//        [alertController addAction:settingsAction];
//
//        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
//
//
//
//    } else if(authStatus == AVAuthorizationStatusRestricted){
//    } else if(authStatus == AVAuthorizationStatusNotDetermined){
//    } else {
//        // impossible, unknown authorization status
//    }
//
//
//
//
//}
#pragma mark - Text view delegate

-(void)textViewDidBeginEditing:(UITextView *)textView{
    //    if (IS_IPHONE_6P) {
    //        [scrollView setContentOffset:CGPointMake(0, 200) animated:YES];
    //
    //    }
    //    else if (IS_IPHONE_6){
    //        [scrollView setContentOffset:CGPointMake(0, 470) animated:YES];
    //
    //    }
    //    else if (IS_IPHONE_X){
    //        [scrollView setContentOffset:CGPointMake(0.0, 400) animated:TRUE];
    //    }
    //    else{
    //        [scrollView setContentOffset:CGPointMake(0.0, 100) animated:TRUE];
    //    }
    //    CGRect rect = scrollView.contentSize.height;
    //    rect.origin.y = -200 - textView.frame.size.height;
    //    scrollView.frame = rect;
    
    CGSize size = CGSizeMake(0.0, 80.0);
    if (IS_IPHONE_X) {
        size = CGSizeMake(0.0, 100.0);
    }
    keyboardSize.height = keyboardSize.height + size.height;
    
    CGPoint buttonOrigin = flTextView.frame.origin;
    
    CGFloat buttonHeight = flTextView.frame.size.height;
    
    CGRect visibleRect = self.view.frame;
    
    visibleRect.size.height -= keyboardSize.height;
    
    if (!CGRectContainsPoint(visibleRect, buttonOrigin)){
        
        CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y - visibleRect.size.height + buttonHeight);
        [scrollView setContentOffset:scrollPoint animated:YES];
    }
    scrollView.scrollEnabled = false;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    CGPoint buttonOrigin = flTextView.frame.origin;
    
    CGFloat buttonHeight = flTextView.frame.size.height;
    
    CGRect visibleRect = self.view.frame;
    
    visibleRect.size.height -= keyboardSize.height;
    
    if (!CGRectContainsPoint(visibleRect, buttonOrigin)){
        
        CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y - visibleRect.size.height + buttonHeight);
        [scrollView setContentOffset:scrollPoint animated:YES];
    }
    scrollView.scrollEnabled = true;
    
}
#pragma mark - TextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == textFieldState) {
        if (textFieldCountry.text.length == 0) {
            [[CommonMethods sharedInstance]AlertMessage:@"Please select the country " andViewController:self];
        }
        else{
            [textField becomeFirstResponder];
            if (_isEdit) {
                for (int i = 0; i<arrCountry.count;i++) {
                    if ([[[arrCountry objectAtIndex:i]valueForKey:@"country_name"] isEqualToString:[textFieldCountry text]]) {
                        int countryId = [[[arrCountry objectAtIndex:i] valueForKey:@"country_id"] intValue];
                        arr = [[NSMutableArray alloc]init];
                        NSString *filePathState = [[NSBundle mainBundle] pathForResource:@"State" ofType:@"json"];
                        NSData *contentState = [[NSData alloc] initWithContentsOfFile:filePathState];
                        NSDictionary *dictState = [NSJSONSerialization JSONObjectWithData:contentState options:kNilOptions error:nil];
                        NSLog(@"%@",dictState);
                        arrayState = [[NSMutableArray alloc]init];
                        arrayState = [dictState valueForKey:@"array"];
                        for (NSDictionary *dict in arrayState) {
                            if ([[dict valueForKey:@"country_id"] intValue] == countryId) {
                                [arr addObject:dict];
                            }
                        }
                    }
                }
                if (arr.count >0) {
                    arrayState = [arr mutableCopy];
                }
                else{
                    [[CommonMethods sharedInstance]AlertMessage:@"Please select the country " andViewController:self];
                }
            }
            else{
                if (arr.count >0 ) {
                    arrayState = [arr mutableCopy];
                }
            }
        }
    }
    else if (textField == textFieldCity){
        if (textFieldCountry.text.length == 0) {
            [[CommonMethods sharedInstance]AlertMessage:@"Please select the country" andViewController:self];
        }
        else if (textFieldState.text.length == 0){
            [[CommonMethods sharedInstance]AlertMessage:@"Please select state" andViewController:self];
        }
        else{
            [textField becomeFirstResponder];
        }
    }
    else if (textField == textFieldMaxMembers){
        if (IS_IPHONE_5 || IS_IPHONE_6) {
            [scrollView setContentOffset:CGPointMake(0, 100) animated:YES];
        }
    }
    else if (textField == textFieldPrice){
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        PriceTableViewController *controller = [story instantiateViewControllerWithIdentifier:@"PriceTableViewController"];
        controller.delegate = self;
        controller.arrPrice = arrPrices;
        [self.navigationController pushViewController:controller animated:true];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == textFieldYachtName) {
        [textField resignFirstResponder];
        [textFieldPlace becomeFirstResponder];
    }
    else if (textField == textFieldPlace){
        [textField resignFirstResponder];
        [textFieldCountry becomeFirstResponder];
    }
    else if (textField == textFieldCountry){
        [textField resignFirstResponder];
        [textFieldState becomeFirstResponder];
    }
    else if (textField == textFieldState){
        [textField resignFirstResponder];
        [textFieldCity becomeFirstResponder];
    }
    else if (textField == textFieldCity){
        [textField resignFirstResponder];
        [textFieldZipCode becomeFirstResponder];
    }
    else if (textField == textFieldZipCode){
        [textField resignFirstResponder];
        [textFieldPrice becomeFirstResponder];
        textFieldPrice.keyboardType = UIKeyboardTypeNumberPad;
    }
    else if (textField == textFieldPrice){
        [textField resignFirstResponder];
        [textFieldMaxMembers becomeFirstResponder];
        textFieldMaxMembers.keyboardType = UIKeyboardTypeNumberPad;
    }
    else if (textField == textFieldMaxMembers){
        [textField resignFirstResponder];
    }
    
    return true;
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == textFieldMaxMembers) {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
        
        return newLength <= 4 || returnKey;
    }
    else{
        return true;
    }
    
}
-(void)doneButtonClicked:(id)sender{
    [scrollView endEditing:true];
}

#pragma mark - Api for adding yacht

-(void)ApiForAddingYacht{
    
    if (textFieldYachtName.text.length == 0) {
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter the name for Yacht" andViewController:self];
        return;
    }
    else if (textFieldPlace.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter the locality for the Yacht" andViewController:self];
        return;
    }
    else if (textFieldCountry.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter the country for the Yacht" andViewController:self];
        return;
    }
    else if (textFieldState.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter the state for the Yacht" andViewController:self];
        return;
    }
    else if (textFieldCity.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter the city for the Yacht" andViewController:self];
        return;
    }
    else if (arrPrices.count == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter the price for Yacht" andViewController:self];
        return;
    }
    else if (textFieldMaxMembers.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter the Maximum members allowed in the Yacht" andViewController:self];
        return;
    }
    else if(flTextView.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter the description of Yacht" andViewController:self];
        return;
    }
    else if (flTextView.text.length < 100){
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter atleast 100 words description for the Yacht" andViewController:self];
        return;
    }
    else{
        if ([[ CommonMethods sharedInstance] checkInternetConnection]) {
            NSMutableArray *arrAmenitiesId = [[NSMutableArray alloc]init];
            for (NSDictionary *dict in arrAmenities) {
                [arrAmenitiesId addObject:[dict valueForKey:@"id"]];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view.window];
            });
            NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
            NSString *strRole = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:textFieldYachtName.text forKey:@"name"];
            [dict setObject:textFieldPlace.text forKey:@"address"];
            [dict setObject:textFieldCity.text forKey:@"city"];
            [dict setObject:textFieldState.text forKey:@"state"];
            [dict setObject:textFieldCountry.text forKey:@"country"];
            [dict setObject:textFieldZipCode.text forKey:@"zipcode"];
            [dict setObject:arrPrices forKey:@"prices_attributes"];
            [dict setObject:flTextView.text forKey:@"description"];
            [dict setObject:textFieldMaxMembers.text forKey:@"max_guests"];
            [dict setObject:arrAmenitiesId forKey:@"amenity_ids"];
            [dict setObject:[NSNumber numberWithBool:switchCaptain.isOn] forKey:@"is_captain"];
            if (_isEdit) {
                [dict setObject:[arrYachtDetail valueForKey:@"id"] forKey:@"id"];
                
            }
            
            if (arrImages.count > 0){
                NSMutableArray *tempList = [NSMutableArray new];
                for (NSDictionary *dict in arrImages ) {
                    if ([[dict valueForKey:@"id"] intValue] == -1) {
                        UIImage *obj = [dict objectForKey:@"image"];
                        NSData *data = UIImageJPEGRepresentation(obj, .5);// UIImagePNGRepresentation(obj);
                        NSString *imageData = [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
                        imageData = [NSString stringWithFormat:@"data:image/jpeg;base64,%@",imageData];
                        NSMutableDictionary* tempImageObj = [NSMutableDictionary new];
                        [tempImageObj setObject:imageData forKey:@"image"];
                        [tempList addObject:tempImageObj];
                    }
                }
                [dict setObject:tempList forKey:@"yacht_images_attributes"];
            }
            NSMutableDictionary *dictYacht = [[NSMutableDictionary alloc]init];
            [dictYacht setObject:dict forKey:@"yachts"];
            if (_isEdit) {
                [WebServiceManager putRequestWithUrlString:[NSString stringWithFormat:@"%@yachts",kBaseUrlVersion2] withPostString:dictYacht emailString:strRole accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[LoaderNew sharedLoader]hideLoader];
                        [self AlertController:@"Yacht updated successfully!!!"];
                    });
                } failure:^(NSString *msg, BOOL success) {
                    NSLog(@"%@",msg);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[LoaderNew sharedLoader]hideLoader];
                        [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
                    });
                }];
            }
            else{
                [WebServiceManager postRequestWithUrlString:[NSString stringWithFormat:@"%@yachts",kBaseUrlVersion2] withPostString:dictYacht emailString:strRole accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[LoaderNew sharedLoader]hideLoader];
                        [self AlertController:@"Yacht added successfully"];
                        //                        self.tabBarController.tabBar.hidden = false;
                        //                        [self.navigationController popViewControllerAnimated:YES];
                        
                    });
                    NSLog(@"%@",result);
                    
                    
                } failure:^(NSString *msg, BOOL success) {
                    NSLog(@"%@",msg);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[LoaderNew sharedLoader]hideLoader];
                        [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
                    });
                }];
            }
        }
        else{
            [[CommonMethods sharedInstance]AlertMessage:@"No internet connection" andViewController:self];
        }
    }
}

-(void)AlertController:(NSString *)msg{
    UIAlertController *alertcontroller = [UIAlertController alertControllerWithTitle:@"" message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self.navigationController popViewControllerAnimated:true];
    }];
    [alertcontroller addAction:yesButton];
    [self presentViewController:alertcontroller animated:YES completion:nil];
}

-(UIImage *) downloadImageFromURL :(NSString *)imageUrl{
    
    NSURL  *url = [NSURL URLWithString:imageUrl];
    NSData *urlData = [NSData dataWithContentsOfURL:url];
    if ( urlData )
    {
        NSLog(@"Downloading started...");
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"dwnld_image.png"];
        NSLog(@"FILE : %@",filePath);
        [urlData writeToFile:filePath atomically:YES];
        UIImage *image1=[UIImage imageWithContentsOfFile:filePath];
        return image1;
    }
    else{
        return nil;
    }
    
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}


- (void)keyboardWasShown:(NSNotification *)notification {
    
    NSDictionary* info = [notification userInfo];
    
    keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
    CGPoint p = [gestureRecognizer locationInView:collectionViewImages];
    
    NSIndexPath *indexPath = [collectionViewImages indexPathForItemAtPoint:p];
    if (indexPath == nil){
        NSLog(@"couldn't find index path");
    } else {
        // get the cell at indexPath (the one you long pressed)
        PhotosCollectionViewCell* cell = [collectionViewImages cellForItemAtIndexPath:indexPath];
        // do stuff with the cell
    }
}


- (void)keyboardWillBeHidden:(NSNotification *)notification {
    
    NSDictionary* info = [notification userInfo];
    
    keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
