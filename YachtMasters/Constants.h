//
//  Constants.h
//  Museum
//
//  Created by Anju Singh on 12/8/14.
//  Copyright (c) 2014 puran. All rights reserved.
//

#ifndef Museum_Constants_h
#define Museum_Constants_h
//#define //NSLog //

//#define //NSLog //R: 252 G: 191 B: 96//R: 85 G: 96 B: 169//R: 252 G: 191 B: 96

#define kOragneColor [UIColor colorWithRed:(85.0/255.0) green:(96.0/255.0) blue:(169.0/255.0) alpha:1.0]

#define kgray1Color [UIColor colorWithRed:150.0/255.0 green:149.0/255.0 blue:149.0/255.0 alpha:1.0];
#define kBlueColor [UIColor colorWithRed:42.0/255.0 green:55.0/255.0 blue:68.0/255.0 alpha:1.0];
#define kgray2Color [UIColor colorWithRed:116.0/255.0 green:111.0/255.0 blue:117.0/255.0 alpha:1.0];
#define kContentViewColor  [UIColor colorWithRed:211.0/255.0 green:209.0/255.0 blue:233.0/255.0 alpha:1.0];
#define kgray3Color [UIColor colorWithRed:238.0/255.0 green:237.0/255.0 blue:246.0/255.0 alpha:1.0];
#define kHeaderbluecolor  [UIColor colorWithRed:51.0/255.0 green:61.0/255.0 blue:152.0/255.0 alpha:1.0];

#define FontOpenSans(s)     [UIFont fontWithName:@"OpenSans-Regular" size:s]
#define FontOpenSansBold(s) [UIFont fontWithName:@"OpenSans-Bold" size:s]
#define FontOpenSansSemiBold(s) [UIFont fontWithName:@"OpenSans-SemiBold" size:s]
#define FontOpenSansLight(s) [UIFont fontWithName:@"OpenSans-Light" size:s]

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define IS_IPHONE_X (IS_IPHONE && SCREEN_MAX_LENGTH == 812.0)
#define IS_IPHONE_XSMAX (IS_IPHONE && SCREEN_MAX_LENGTH == 896.0)

#define Customer @"customer"
#define Owner @"owner"
#define Role @"role"
#define Auth_token @"Auth_token"
#define NoInternet @"No internet connection"

// Live Data Url
//#define kBaseUrl @"https://yachtsmarter.co/"
//#define kBaseUrlVersion2 @"https://yachtsmarter.co/api/v2/"
//#define kBaseUrlImage @"https://yachtsmarter.co"

// Staging Data Url
//#define kBaseUrl @"http://africanpost.gharwale.com/"
//#define kBaseUrlImage @"http://africanpost.gharwale.com"
//#define kBaseUrlVersion2 @"http://africanpost.gharwale.com/api/v2/"

// Local Data Url
#define kBaseUrl @"http://35.224.94.187/"
#define kBaseUrlVersion2 @"http://35.224.94.187/api/v2/"
#define kBaseUrlImage @"http://35.224.94.187"



#define ACCEPTABLE_CHARACTERS @"+0123456789"
#define xres (([[UIScreen mainScreen] nativeBounds].size.width  / [UIScreen mainScreen].nativeScale))
#define yres (([[UIScreen mainScreen] nativeBounds].size.height / [UIScreen mainScreen].nativeScale))



#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define IS_IPAD_PRO ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad && [UIScreen mainScreen].bounds.size.height == 1366)



#if defined(IS_IPAD)
#define FontKhmerIPADSIZE  [UIFont fontWithName:@"KhmerUI" size:25]
#else
#define FontKhmerIPADSIZE  [UIFont fontWithName:@"KhmerUI" size:25]


#endif



#endif
