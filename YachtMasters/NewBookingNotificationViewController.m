//
//  NewBookingNotificationViewController.m
//  YachtMasters
//
//  Created by Anvesh on 27/04/18.
//  Copyright © 2018 EL. All rights reserved.
//

#import "NewBookingNotificationViewController.h"
#import "Constants.h"
#import "CommonMethods.h"
#import "WebServiceManager.h"
#import "YachtMasters-Swift.h"

@interface NewBookingNotificationViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewBase;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@end

@implementation NewBookingNotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",_dictYacht);
    self.yachtNameLabel.text = [[_dictYacht valueForKey:@"yacht"] objectForKey:@"name"];
    self.amountLabel.attributedText = [self PlainStringToAttributedStringForPrice:[[_dictYacht valueForKey:@"yacht"] objectForKey:@"price"] andCurrency:[[_dictYacht valueForKey:@"yacht"] objectForKey:@"currency_symbol"] andNights:[_dictYacht valueForKey:@"total_nights"]];
    self.startDatelabel.text = [self DateFoematter:[self.dictYacht valueForKey:@"start_date"]];
    self.endDateLabel.text = [self DateFoematter:[self.dictYacht valueForKey:@"end_date"]];
    NSLog(@"%@",_amountLabel.text);
    _viewBase.layer.shadowColor = [UIColor colorWithRed:0.0/255.0 green:196.0/255.0 blue:56.0/255.0 alpha:1].CGColor;
    _viewBase.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    _viewBase.layer.shadowOpacity = 0.3;
    _viewBase.layer.shadowRadius = 5;
    _viewBase.layer.masksToBounds = false;
    _viewBase.layer.cornerRadius = 5.0;
    _viewBase.clipsToBounds = true;
    self.userNameLabel.text = [[self.dictYacht valueForKey:@"user"] valueForKey:@"name"];
    // Do any additional setup after loading the view.
}

#pragma mark - Button Actions

- (IBAction)buttonApproveClicked:(id)sender {
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:[_dictYacht valueForKey:@"id"] forKey:@"id"];
        [dict setObject:@"approved" forKey:@"status"];
        NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
        [dictResponse setObject:dict forKey:@"bookings"];
        [WebServiceManager putRequestWithUrlString:[NSString stringWithFormat:@"%@bookings",kBaseUrl] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                //[[CommonMethods sharedInstance]AlertMessage:[result valueForKey:@"message"] andViewController:self];
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Success"
                                             message:[result valueForKey:@"message"]
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                //Add Buttons
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"Ok"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action) {
                                                [self dismissViewControllerAnimated:true completion:nil];
                                            }];
                
                [alert addAction:yesButton];
                [self presentViewController:alert animated:YES completion:nil];
            });
            
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
            
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
        
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}
- (IBAction)buttonDeclineClicked:(id)sender {
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:[_dictYacht valueForKey:@"id"] forKey:@"id"];
        [dict setObject:@"declined" forKey:@"status"];
        NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
        [dictResponse setObject:dict forKey:@"bookings"];
        [WebServiceManager putRequestWithUrlString:[NSString stringWithFormat:@"%@bookings",kBaseUrl] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
               // [[CommonMethods sharedInstance]AlertMessage:[result valueForKey:@"message"] andViewController:self];
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Success"
                                             message:[result valueForKey:@"message"]
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                //Add Buttons
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"Ok"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action) {
                                                [self dismissViewControllerAnimated:true completion:nil];
                                            }];
                
                [alert addAction:yesButton];
                [self presentViewController:alert animated:YES completion:nil];
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}
- (IBAction)buttonHomeClicked:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
    
}


#pragma mark - Attributed string for Price label
-(NSMutableAttributedString *)PlainStringToAttributedStringForPrice:(NSString *)strPrice andCurrency:(NSString *)strCurrency andNights:(NSString *)strNights{
    
    // NSString *strPrice = [[[[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"yacht"]valueForKey:@"price"] stringValue];
    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"You get\n%@%@\nfor %@ night",strCurrency,strPrice,strNights]];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:attrText];
    strPrice  = [strPrice stringByAppendingString:[NSString stringWithFormat:@"You get\n%@",strCurrency]];
    NSRange range = [strPrice rangeOfString:strPrice];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:FontOpenSansSemiBold(12)} range:range];
    return attributedText;
}

-(NSString *)DateFoematter:(NSString *)strDate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *startDate = [formatter dateFromString:strDate];
    [formatter setDateFormat:@"dd MMM yy"];
    strDate = [formatter stringFromDate:startDate];
    return  strDate;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
