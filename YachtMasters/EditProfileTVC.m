//
//  EditProfileTVC.m
//  YachtMasters
//
//  Created by Anvesh on 11/07/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "EditProfileTVC.h"
#import "Constants.h"
#import "RPFloatingPlaceholderTextField.h"
#import "OTPViewController.h"
#import "CommonMethods.h"
#import "WebServiceManager.h"
#import "RPFloatingPlaceholderTextView.h"
#import "UIImageView+WebCache.h"
#import "MapViewController.h"
#import "MBProgressHUD.h"
#import "Validation.h"
#import "ResetPasswordViewController.h"
#import "SDWebImageManager.h"
#import "UIImageView+WebCache.h"
#import "YachtMasters-Swift.h"

@interface EditProfileTVC ()<UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,LocationDelegate,UIPickerViewDelegate,UIPickerViewDataSource>{
    NSMutableArray *arrResponse;
    UIButton *buttonPhone;
    UIButton *buttonEmail;
    UIImagePickerController *ipc;
    MBProgressHUD *hud;
    UIButton *buttonCountryCode;
    NSArray *arrayCountryCode;
    BOOL isPickerDisplay;
    UIToolbar *toolBar;
    BOOL isTextEdited;
    BOOL isImageChanged;
    IBOutlet NSLayoutConstraint *lcBottom;
    UIPickerView *pickerViewContryList;
    NSMutableArray *arrCountry;
}
@property (strong, nonatomic) IBOutlet UITableView *tableViewEditProfile;
@property (weak, nonatomic) IBOutlet UIView *backGroundViewImage;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProfilePic;
@property (weak, nonatomic) IBOutlet UIButton *buttonCamera;
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextField *textFieldName;
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextField *textFieldPlace;
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextField *textFieldPhone;
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextField *textFieldCurrentPassword;
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextField *textFieldNewPassword;
@property (weak, nonatomic) IBOutlet UIButton *buttonSave;
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextView *textViewAboutMe;
@property (strong, nonatomic) UIPickerView *pickerCountryCode;

@end

@implementation EditProfileTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self ApiForGettinfProfileDetails];
    
    NSString *filePath1 = [[NSBundle mainBundle] pathForResource:@"Country" ofType:@"json"];
    NSData *content1 = [[NSData alloc] initWithContentsOfFile:filePath1];
    NSDictionary *dictCountryList = [NSJSONSerialization JSONObjectWithData:content1 options:kNilOptions error:nil];
    arrCountry = [[NSMutableArray alloc]init];
    arrCountry = [[dictCountryList valueForKey:@"array"]valueForKey:@"country_name"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"generated" ofType:@"json"];
    NSData *content = [[NSData alloc] initWithContentsOfFile:filePath];
    NSDictionary *dictCountryCode = [NSJSONSerialization JSONObjectWithData:content options:kNilOptions error:nil];
    arrayCountryCode = [dictCountryCode valueForKey:@"countries"];
    
    toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height, self.view.frame.size.width, 44.0)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(DoneBtnClicked:)];
    toolBar.items = @[barButtonDone];
    barButtonDone.tintColor=[UIColor lightGrayColor];
    [self.navigationController.view addSubview:toolBar];
    
    _pickerCountryCode = [[UIPickerView alloc]initWithFrame:CGRectMake(0.0, [[UIScreen mainScreen] bounds].size.height+44, [[UIScreen mainScreen]bounds].size.width,190.0)];
    [self.navigationController.view addSubview:_pickerCountryCode];
    _pickerCountryCode.backgroundColor = [UIColor lightGrayColor];
    _pickerCountryCode.delegate = self;
    _pickerCountryCode.dataSource = self;
    _pickerCountryCode.tag = 1101;
    
    pickerViewContryList = [[UIPickerView alloc]init];
    pickerViewContryList.backgroundColor = [UIColor lightGrayColor];
    pickerViewContryList.delegate = self;
    pickerViewContryList.dataSource = self;
    pickerViewContryList.tag = 1102;
    [self CreateUI];
    
}


-(void)CustomNavigationBar{
    
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    //[backButton setBackgroundColor:[UIColor redColor]];
    [backButton addTarget:self action:@selector(BackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 5.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}

-(void)CreateUI{
    self.tabBarController.tabBar.hidden =  true;
    UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapped)];
    [_tableViewEditProfile addGestureRecognizer:tapped];
    
    self.backGroundViewImage.layer.cornerRadius = self.backGroundViewImage.frame.size.width/2;
    self.backGroundViewImage.clipsToBounds = true;
    self.imageViewProfilePic.layer.cornerRadius = self.imageViewProfilePic.frame.size.width/2;
    self.imageViewProfilePic.clipsToBounds = true;
    //    buttonEmail = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 12)];
    //    buttonEmail.titleLabel.font = FontOpenSans(12);
    //    [buttonEmail addTarget:self action:@selector(VerifyEmail) forControlEvents:UIControlEventTouchUpInside];
    //    _textFieldEmail.rightViewMode = UITextFieldViewModeAlways;
    //    _textFieldEmail.rightView = buttonEmail;
    
    
    buttonPhone = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 12)];
    [buttonPhone setTitleColor:[UIColor colorWithRed:126.0/255.0 green:218.0/255.0 blue:157.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [buttonPhone setTitle:@"Verified" forState:UIControlStateNormal];
    buttonPhone.titleLabel.font = FontOpenSans(12);
    buttonPhone.tag = 1001;
    // [buttonPhone addTarget:self action:@selector(CustomAlert) forControlEvents:UIControlEventTouchUpInside];
    self.textFieldPhone.rightViewMode = UITextFieldViewModeAlways;
    self.textFieldPhone.rightView = buttonPhone;
    buttonPhone.userInteractionEnabled = false;
    
    buttonCountryCode = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, _textFieldPhone.frame.size.height)];
    [buttonCountryCode setTitleColor:[UIColor colorWithRed:126.0/255.0 green:218.0/255.0 blue:157.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [buttonCountryCode setTitle:@"Verified" forState:UIControlStateNormal];
    buttonCountryCode.titleLabel.font = FontOpenSans(12);
    [buttonCountryCode addTarget:self action:@selector(buttonCountryCodeClicked) forControlEvents:UIControlEventTouchUpInside];
    
    self.textFieldPlace.inputView = pickerViewContryList;
    self.textFieldPlace.inputAccessoryView = toolBar;
    
    UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 12, 12)];
    [addButton setImage:[UIImage imageNamed:@"place target.png"] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(SwitchToMap) forControlEvents:UIControlEventTouchUpInside];
   
    self.textViewAboutMe.placeholder = @"ABOUT ME";
    [[CommonMethods sharedInstance]DropShadow:_buttonSave UIColor:[UIColor colorWithRed:90.0/255.0 green:190.0/255.0 blue:240.0/255.0 alpha:1.0] andShadowRadius:3.0];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self CustomNavigationBar];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // [self CustomNavigationBar];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

-(void)tapped{
    [self.view endEditing:true];
}

-(void)BackButtonPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:true];
    self.navigationController.tabBarController.navigationController.navigationBar.hidden = true;
}

#pragma mark - Phone Verification

-(void)SwitchToOtp{
    BOOL isEdit = true;
    OTPViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"OTPViewController"];
    controller.isEditProfile = isEdit;
    [self.navigationController pushViewController:controller animated:true];
}


#pragma mark - custom Alert
-(void)CustomAlert{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Alert"
                                     message:@"Code will be send to entered number for verification"
                                     preferredStyle:UIAlertControllerStyleAlert];
        //Add Buttons
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"YES"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        [self SwitchToOtp];
                                    }];
        
        //Add your buttons to alert controller
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"NO"
                                   style:UIAlertActionStyleDestructive
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    });
    
}


#pragma mark - Email Verification

-(void)VerifyEmail{
    
    if (![Validation validateEmail:_textFieldEmail.text] ) {
        [[CommonMethods sharedInstance]AlertMessage:@"Please enter valid email" andViewController:self];
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Alert"
                                         message:@"Code will be send to entered mailId for verification"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"YES"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            [self ApiforEmailUpdate];
                                        }];
            
            //Add your buttons to alert controller
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"NO"
                                       style:UIAlertActionStyleDestructive
                                       handler:^(UIAlertAction * action) {
                                           //Handle no, thanks button
                                       }];
            
            [alert addAction:yesButton];
            [alert addAction:noButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        });
    }
    
}

#pragma mark - Email Updation


-(void)ApiforEmailUpdate{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:@"Auth_token"];
        NSString *strRole = [[NSUserDefaults standardUserDefaults]valueForKey:@"role"];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:_textFieldEmail.text forKey:@"email"];
        NSMutableDictionary *dictPostData = [[NSMutableDictionary alloc]init];
        [dictPostData setObject:dict forKey:@"users"];
        [WebServiceManager putRequestWithUrlString:[NSString stringWithFormat:@"%@users/update_email",kBaseUrl] withPostString:dictPostData emailString:strRole accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            [[CommonMethods sharedInstance]AlertMessage:[result valueForKey:@"message"] andViewController:self];
        } failure:^(NSString *msg, BOOL success) {
            [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
        }];
        
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}


#pragma mark - Switch to Map view
-(void)SwitchToMap{
    // isPresent = false;
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MapViewController *controller = [story instantiateViewControllerWithIdentifier:@"MapViewController"];
    controller.delegate = self;
    controller.isEditProfile = true;
    [self.navigationController pushViewController:controller animated:true];
}

-(void)AddressOfYacht:(NSMutableArray *)arrCompleteAddress{
    
    
    NSString *substrings = [[arrCompleteAddress objectAtIndex:0]valueForKey:@"formatted_address"];
    _textFieldPlace.text = substrings;
}

#pragma mark - Api for Getting Profile detail

-(void)ApiForGettinfProfileDetails{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:@"Auth_token"];
        NSString *strRole = [[NSUserDefaults standardUserDefaults]valueForKey:@"role"];
        
        [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@users/profile",kBaseUrl] emailString:strRole accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            arrResponse = [[NSMutableArray alloc]init];
            arrResponse = [result valueForKey:@"user"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                //[[Loader shared] hideLoader];
                
                _textFieldName.text = [arrResponse valueForKey:@"name"];
                _textViewAboutMe.text = [arrResponse valueForKey:@"about"];
                _textFieldPhone.text = [arrResponse valueForKey:@"phone_number"];
                if ([[arrResponse valueForKey:@"address"] length] == 0) {
                    _textFieldPlace.text = [[arrResponse valueForKey:@"country"] capitalizedString];
                    
                }
                else{
                    _textFieldPlace.text = [[arrResponse valueForKey:@"address"] capitalizedString];
                }
                if ([[arrResponse valueForKey:@"email"] isKindOfClass:[NSNull class]]) {
                    _textFieldEmail.text = @"";
                    
                }
                else{
                    _textFieldEmail.text = [arrResponse valueForKey:@"email"];
                }
                
                if ([arrResponse valueForKey:@"is_email_confirmed"]) {
                    [buttonEmail setTitle:@"Verified" forState:UIControlStateNormal];
                    [buttonEmail setTitleColor:[UIColor colorWithRed:126.0/255.0 green:218.0/255.0 blue:157.0/255.0 alpha:1.0] forState:UIControlStateNormal];
                    buttonEmail.userInteractionEnabled = false;
                    
                }
                else if(_textFieldEmail.text.length > 0){
                    [buttonEmail setTitle:@"Verify" forState:UIControlStateNormal];
                    [buttonEmail setTitleColor:[UIColor colorWithRed:70.0/255.0 green:181.0/255.0 blue:222.0/255.0 alpha:1.0] forState:UIControlStateNormal];
                    buttonEmail.userInteractionEnabled = true;
                }
                else{
                    [buttonEmail setTitle:@"" forState:UIControlStateNormal];
                    buttonEmail.userInteractionEnabled = true;
                }
                
                NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[arrResponse valueForKey:@"image"]];
                NSURL *url = [NSURL URLWithString:strUrl];
                [_imageViewProfilePic sd_setImageWithURL:url placeholderImage:nil];
                [self.tableView reloadData];
            });
            
        } failure:^(NSString *msg, BOOL success) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
            
                //[[Loader sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view.superview];
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}

#pragma mark -  TextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    //    if (textField == _textFieldName) {
    //        [textField becomeFirstResponder];
    //    }
    //    else if (textField == _textFieldPlace){
    //        [_textFieldName resignFirstResponder];
    //        [textField becomeFirstResponder];
    //    }
    //    else if (textField == _textFieldEmail){
    //        [_textFieldPlace resignFirstResponder];
    //        [textField becomeFirstResponder];
    //    }
    //    else if (textField == _textFieldPhone){
    //        [_textFieldEmail resignFirstResponder];
    //        [textField becomeFirstResponder];
    //    }
    //    else if (textField == _textFieldCurrentPassword){
    //
    //    }
    
    if (textField == _textFieldPhone) {
        //        if (textField.text.length == 0) {
        //         //   _textFieldPhone.leftViewMode = UITextFieldViewModeAlways;
        //         //   _textFieldPhone.leftView = buttonCountryCode;
        //        }
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == _textFieldEmail) {
        if (![_textFieldEmail.text isEqualToString:[arrResponse valueForKey:@"email"]]) {
            [buttonEmail setTitle:@"Verify" forState:UIControlStateNormal];
            [buttonEmail setTitleColor:[UIColor colorWithRed:28.0/255.0 green:170.0/255.0 blue:222.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            buttonEmail.userInteractionEnabled = true;
        }
        else{
            [buttonEmail setTitle:@"Verified" forState:UIControlStateNormal];
            [buttonEmail setTitleColor:[UIColor colorWithRed:126.0/255.0 green:218.0/255.0 blue:157.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            buttonEmail.userInteractionEnabled = false;
        }
    }
    else if (textField == _textFieldPhone){
        if (![textField.text isEqualToString:[arrResponse valueForKey:@"phone_number"]]) {
            [buttonPhone setTitle:@"Verify" forState:UIControlStateNormal];
            [buttonPhone setTitleColor:[UIColor colorWithRed:28.0/255.0 green:170.0/255.0 blue:222.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            buttonPhone.userInteractionEnabled = true;
        }
        else{
            [buttonPhone setTitle:@"Verified" forState:UIControlStateNormal];
            [buttonPhone setTitleColor:[UIColor colorWithRed:126.0/255.0 green:218.0/255.0 blue:157.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            buttonPhone.userInteractionEnabled = false;
            
            
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == _textFieldName) {
        [textField resignFirstResponder];
        [_textFieldPlace becomeFirstResponder];
    }
    else if(textField == _textFieldPlace){
        [textField resignFirstResponder];
        [_textFieldEmail becomeFirstResponder];
    }
    else if (textField == _textFieldEmail){
        [textField resignFirstResponder];
        [_textFieldPhone becomeFirstResponder];
    }
    else if (textField == _textFieldPhone){
        [textField resignFirstResponder];
        [_textViewAboutMe becomeFirstResponder];
    }
    else if (textField == _textFieldCurrentPassword){
        [textField resignFirstResponder];
        [_textFieldNewPassword becomeFirstResponder];
    }
    else if (textField == _textFieldNewPassword){
        [textField resignFirstResponder];
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    isTextEdited = true;
    return true;
}

- (IBAction)ButtonCameraClicked:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Choose Options"
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleActionSheet]; // 1
    UIAlertAction *galleryAction = [UIAlertAction actionWithTitle:@"Gallery"
                                                            style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                [self btnGalleryClicked];
                                                            }]; // 2
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"Camera"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               [self btnCameraClicked];
                                                           }]; // 3
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                                                               
                                                           }];
    
    [alert addAction:galleryAction]; // 4
    [alert addAction:cameraAction]; // 5
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil]; // 6
}


- (void)buttonCountryCodeClicked{
    [self PickerHideShow];
}

#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if ([pickerView tag] == 1101){
        return arrayCountryCode.count;
    }
    else{
        return arrCountry.count;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if ([pickerView tag] == 1101){
        return [[arrayCountryCode valueForKey:@"name"] objectAtIndex:row];
    }
    else{
        return  [arrCountry objectAtIndex:row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (pickerView.tag == 1101){
        [buttonCountryCode setTitle:[[arrayCountryCode valueForKey:@"code"] objectAtIndex:row] forState:UIControlStateNormal];
    }
    else{
        self.textFieldPlace.text = [arrCountry objectAtIndex:row];
    }
}

-(void)PickerHideShow{
    if (!isPickerDisplay) {
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
            [_textFieldPhone resignFirstResponder];
            
        }];
        [UIView animateWithDuration:0.25 animations:^{
            
            
            CGRect temp1 = toolBar.frame;
            temp1.origin.y = self.view.frame.size.height - toolBar.frame.size.height-180;
            toolBar.frame = temp1;
            
            CGRect temp = self.pickerCountryCode.frame;
            temp.origin.y = self.view.frame.size.height - self.pickerCountryCode.frame.size.height;
            self.pickerCountryCode.frame = temp;
            
            
            
        } completion:^(BOOL finished) {
            NSLog(@"picker displayed");
            isPickerDisplay = YES;
        }];
    }else {
        [UIView animateWithDuration:0.25 animations:^{
            
            CGRect temp1 = toolBar.frame;
            temp1.origin.y = self.view.frame.size.height;
            toolBar.frame = temp1;
            
            CGRect temp = self.pickerCountryCode.frame;
            temp.origin.y = self.view.frame.size.height;
            self.pickerCountryCode.frame = temp;
        } completion:^(BOOL finished) {
            NSLog(@"picker hide");
            isPickerDisplay = NO;
        }];
    }
    
    
}


#pragma mark - done button action
-(void)DoneBtnClicked:(id)sender{
    
    [self PickerHideShow];
}


#pragma mark - Method for opening gallery

- (void)btnGalleryClicked
{
    ipc= [[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [self presentViewController:ipc animated:YES completion:nil];
}



#pragma mark - ImagePickerController Delegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    isImageChanged = true;
    isTextEdited = true;
    _imageViewProfilePic.image = [info valueForKey:@"UIImagePickerControllerOriginalImage"];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Method for Opening camera
- (void)btnCameraClicked{
    ipc = [[UIImagePickerController alloc] init];
    ipc.delegate = self;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:ipc animated:YES completion:NULL];
    }
    else
    {
        [[CommonMethods sharedInstance]AlertMessage:@"No Camera Available." andViewController:self];
    }
}

- (IBAction)ButtonChangePasswordPressed:(id)sender {
    ResetPasswordViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ResetPasswordViewController"];
    [self.navigationController pushViewController:controller animated:true];
}

#pragma mark - APi for editing the profile

- (IBAction)ButtonSaveClicked:(id)sender {
    [self.view endEditing:true];
    
    if (isTextEdited){
        if (_textFieldName.text.length == 0) {
            [[CommonMethods sharedInstance]AlertMessage:@"Please enter the name" andViewController:self];
        }
        else if (_textFieldPlace.text.length == 0){
            [[CommonMethods sharedInstance]AlertMessage:@"Please enter your place" andViewController:self];
        }
        else if (_textFieldEmail.text.length == 0){
            [[CommonMethods sharedInstance]AlertMessage:@"Please enter the verified email" andViewController:self];
        }
        else if (_textFieldPhone.text.length == 0){
            [[CommonMethods sharedInstance]AlertMessage:@"Please enter the verified phone number" andViewController:self];
        }
        else if (![_textFieldPhone.text isEqualToString:[arrResponse valueForKey:@"phone_number"]]){
            [[CommonMethods sharedInstance]AlertMessage:@"Please verify the phone number" andViewController:self];
        }
        else{
            isTextEdited = false;
            [self apiRequestForEditDetails];
        }
    }
    
}


#pragma mark - Request for changing the details

-(void)apiRequestForEditDetails{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:@"Auth_token"];
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:@"role"];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:_textFieldName.text forKey:@"name"];
        [dict setObject:_textFieldPlace.text forKey:@"address"];
        [dict setObject:_textViewAboutMe.text forKey:@"about"];
        [dict setObject:_textFieldEmail.text forKey:@"email"];
        if (isImageChanged){
            UIImage *obj = _imageViewProfilePic.image;
            NSData *data = UIImageJPEGRepresentation(obj, .8);// UIImagePNGRepresentation(obj);
            NSString *imageData = [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
            imageData = [NSString stringWithFormat:@"data:image/jpeg;base64,%@",imageData];
            [dict setObject:imageData forKey:@"image"];
        }
        NSMutableDictionary *dictPostData = [[NSMutableDictionary alloc]init];
        [dictPostData setObject:dict forKey:@"users"];
        [WebServiceManager putRequestWithUrlString:[NSString stringWithFormat:@"%@users/profile",kBaseUrl] withPostString:dictPostData emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            isTextEdited = false;
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:[[result valueForKey:@"user"]valueForKey:@"name"] forKey:@"Name"];
            if ([[[result valueForKey:@"user"] valueForKey:@"address"] length] == 0) {
                [defaults setObject:[[result valueForKey:@"user"]valueForKey:@"country"] forKey:@"Address"];
            }
            else{
                [defaults setObject:[[result valueForKey:@"user"]valueForKey:@"address"] forKey:@"Address"];
            }
            if (isImageChanged){
                [[SDImageCache sharedImageCache]removeImageForKey:[NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[result valueForKey:@"user"]valueForKey:@"image"]] withCompletion:^{
                    
                }];
                [defaults setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[result valueForKey:@"user"]valueForKey:@"image"]]] forKey:@"Image"];
                [defaults synchronize];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:@"Your profile was updated successfully" andViewController:self];
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                isTextEdited = true;
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view.superview];
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 5) {
        
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 6) {
        lcBottom.constant = self.textViewAboutMe.contentSize.height;
        return lcBottom.constant+80;
    }
    else{
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
