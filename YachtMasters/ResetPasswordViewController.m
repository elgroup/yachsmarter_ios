//
//  ResetPasswordViewController.m
//  YachtMasters
//
//  Created by Anju Singh Yadav on 8/27/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "CommonMethods.h"
#import "WebServiceManager.h"
#import "RPFloatingPlaceholderTextField.h"
#import "LoginViewController.h"
#import "Validation.h"
#import "AppDelegate.h"
#import "YachtMasters-Swift.h"

@interface ResetPasswordViewController ()<UITextFieldDelegate>{
    AppDelegate *appDelegate;
    __weak IBOutlet RPFloatingPlaceholderTextField *textFieldPassword;
    __weak IBOutlet RPFloatingPlaceholderTextField *textFieldNewPassword;
    __weak IBOutlet RPFloatingPlaceholderTextField *textFieldConfirmNewPassword;
    __weak IBOutlet UIButton *buttonChangePassword;
    MBProgressHUD *hud;
}

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self CustomNavigation];
}
- (IBAction)Tapped:(id)sender {
    [self.view endEditing:true];
}

-(void)CustomNavigation{
    
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    //[backButton setBackgroundColor:[UIColor redColor]];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 5.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    self.title = @"Change Password";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}

-(void)BackButtonPressed{
    [self.navigationController popViewControllerAnimated:true];
}
- (IBAction)ButtonChangePasswordPressed:(id)sender {
    if (textFieldNewPassword.text.length == 0) {
        [[CommonMethods sharedInstance]AlertMessage:@"Enter new password" andViewController:self];
    }
    else if (![Validation validatePasswordLength:textFieldPassword.text]){
        [[CommonMethods sharedInstance]AlertMessage:@"Password must be between 8 to 15 characters" andViewController:self];
        return;
    }
    else if (textFieldConfirmNewPassword.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please confirm your password" andViewController:self];
        return;
    }
    else if (![Validation validatePassword:textFieldNewPassword ConfirmPassword:textFieldConfirmNewPassword]){
        [[CommonMethods sharedInstance]AlertMessage:@"Password is not same" andViewController:self];
        return;
    }
    else{
        if ([[CommonMethods sharedInstance]checkInternetConnection]) {
            NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:@"Auth_token"];
            NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:@"role"];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:textFieldPassword.text forKey:@"current_password"];
            [dict setObject:textFieldNewPassword.text forKey:@"new_password"];
            NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
            [dictResponse setObject:dict forKey:@"users"];
            [WebServiceManager putRequestWithUrlString:[NSString stringWithFormat:@"%@users/update_password",kBaseUrl] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[MBProgressHUD hideHUDForView:self.view animated:YES];
                    [[LoaderNew sharedLoader]hideLoader];
                    //  [[CommonMethods sharedInstance]AlertMessage:[result valueForKey:@"message"] andViewController:self];
                    textFieldNewPassword.text = @"";
                    textFieldPassword.text = @"";
                    textFieldConfirmNewPassword.text = @"";
                    
                    
                    UIAlertController * alert = [UIAlertController
                                                 alertControllerWithTitle:@""
                                                 message:@"You have successfully changed the password. Please relogin ."
                                                 preferredStyle:UIAlertControllerStyleAlert];
                    
                    //Add Buttons
                    
                    UIAlertAction* yesButton = [UIAlertAction
                                                actionWithTitle:@"OK"
                                                style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action) {
                                                    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                                    LoginViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"]; //or the homeController
                                                    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                                                    appDelegate.window.rootViewController = navController;
                                                    }];
                    
                    [alert addAction:yesButton];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    
                    
                });
                
            } failure:^(NSString *msg, BOOL success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[MBProgressHUD hideHUDForView:self.view animated:YES];
                    [[LoaderNew sharedLoader]hideLoader];
                    [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
                });
            }];
            dispatch_async(dispatch_get_main_queue(), ^{
//                hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//                hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
                [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
            });
        }
        else{
            [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
        }
    }
    
}

#pragma mark - Delegates of UitextField

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == textFieldPassword) {
        [textField resignFirstResponder];
        [textFieldNewPassword becomeFirstResponder];
    }
    else if (textField == textFieldNewPassword){
        [textField resignFirstResponder];
        [textFieldConfirmNewPassword becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
    return true;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 14;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
