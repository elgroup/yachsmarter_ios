//
//  CommonMethods.m
//  YachtMasters
//
//  Created by Anvesh on 07/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "CommonMethods.h"
#import "Reachability.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreLocation/CoreLocation.h>
#import "APTimeZones.h"



@interface CommonMethods()
{
    
}
@end
@implementation CommonMethods


+(CommonMethods *)sharedInstance
{
    static CommonMethods *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[CommonMethods alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}



#pragma mark-checkInternetConnection
-(BOOL)checkInternetConnection{
    Reachability *r = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    BOOL internet;
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN))/*14-10-14*/{
        internet = NO;
        
    } else
    {
        internet = YES;
    }
    return internet;
}


-(UIImage*)ScaleImageWithImage:(UIImage*)image maxDimension:(CGFloat)maxDimension
{
    if (fmax(image.size.width, image.size.height) <= maxDimension) {
        return image;
    }
    
    CGFloat aspect = image.size.width / image.size.height;
    CGSize newSize;
    
    if (image.size.width > image.size.height) {
        newSize = CGSizeMake(maxDimension, maxDimension / aspect);
    } else {
        newSize = CGSizeMake(maxDimension * aspect, maxDimension);
    }
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 1.0);
    CGRect newImageRect = CGRectMake(0.0, 0.0, newSize.width, newSize.height);
    [image drawInRect:newImageRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;}

-(void)AlertMessage:(NSString *)strMessage andViewController:(UIViewController *)controller{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:strMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    //Add your buttons to alert controller
    
    
    [alert addAction:noButton];
    
    [controller presentViewController:alert animated:YES completion:nil];
}


-(void)NavigationBarShadow :(UINavigationController *)navigationController{
    
    [navigationController navigationBar].layer.shadowColor=[UIColor colorWithRed:53.0/255.0 green:108.0/255.0 blue:130.0/255.0 alpha:1.0f].CGColor;
    [navigationController navigationBar].layer.shadowOffset=CGSizeMake(0, 20);
    [navigationController navigationBar].layer.shadowOpacity=0.8;
    [navigationController navigationBar].layer.shadowRadius=5.5;
//    navigationController.navigationBar.layer.borderColor = [[UIColor whiteColor] CGColor];
//    navigationController.navigationBar.layer.borderWidth=2;// set border you can see the shadow
//    navigationController.navigationBar.layer.shadowColor = [[UIColor blackColor] CGColor];
//    navigationController.navigationBar.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
//    navigationController.navigationBar.layer.shadowRadius = 3.0f;
//    navigationController.navigationBar.layer.shadowOpacity = 1.0f;
//    navigationController.navigationBar.layer.masksToBounds=NO;
}

-(void)setButtonShadow:(UIButton *)btn andShadowcolor:(UIColor *)color{
    btn.layer.shadowColor = color.CGColor;//[[UIColor colorWithRed:0 green:0 blue:0 alpha:0.25f] CGColor];
    btn.layer.shadowOffset = CGSizeMake(0.0f, 4.0f);
    btn.layer.shadowOpacity = 0.3f;
    btn.layer.shadowRadius = 5.0f;
    btn.layer.masksToBounds = NO;
    btn.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:btn.bounds cornerRadius:40.0].CGPath;
}



#pragma mark - ❉===❉=== SET BOARDER OF BUTTON ===❉===❉

-(void)setBorderOfButton:(UIButton *)btn borderColor:(UIColor *)borderColor borderWidth:(CGFloat)width cornerRadius:(CGFloat)cornerRadius{
    btn.layer.borderColor = [borderColor CGColor];
    btn.layer.borderWidth = width;
    btn.layer.cornerRadius = cornerRadius;
    btn.clipsToBounds = true;
}



-(UIView *)CornerRadius:(UIView *)View Radius:(int)radius BorderWidth:(CGFloat )border andColorForBorder:(UIColor *)Color{
    View.layer.cornerRadius = radius;
    View.clipsToBounds = true;
    View.layer.borderColor = Color.CGColor;
    View.layer.borderWidth = border;
    return View;
}

-(void)DropShadow:(UIView *)view UIColor:(UIColor *)color andShadowRadius:(float)radius{
    view.layer.shadowColor = color .CGColor;
    view.layer.shadowOffset = CGSizeMake(1.0, 1.0);
    view.layer.shadowOpacity = 0.7;
    view.layer.shadowRadius = radius;
    view.layer.masksToBounds = false;
}


#pragma mark - Height For text

- (CGSize)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font {
    CGSize size = CGSizeZero;
    if (text) {
        //iOS 7
        CGRect frame = [text boundingRectWithSize:CGSizeMake(widthValue, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:font } context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height + 1);
    }
    return size;
}


#pragma mark - Timezone with respect to lat long

-(NSTimeZone *)TimeZoneWithRespectToLatitiue:(double)lat andLongitude:(double)lang{
   
    CLLocation *location = [[CLLocation alloc] initWithLatitude:lat longitude:lang];
    
    NSTimeZone *timeZone = [[APTimeZones sharedInstance] timeZoneWithLocation:location];
    NSLog(@"%@", timeZone);
    return timeZone;

}


#pragma mark - Rating

-(void)RatingAndStar1:(UIImageView *)image1 Star2:(UIImageView *)image2 Star3:(UIImageView *)image3 Star4:(UIImageView *)image4 Star5:(UIImageView *)image5 andRating:(NSInteger )rating{
    switch (rating) {
        case 0:
            image1.image = [UIImage imageNamed:@"star outline.png"];
            image2.image = [UIImage imageNamed:@"star outline.png"];
            image3.image = [UIImage imageNamed:@"star outline.png"];
            image4.image = [UIImage imageNamed:@"star outline.png"];
            image5.image = [UIImage imageNamed:@"star outline.png"];
            break;
        case 1:
            image1.image = [UIImage imageNamed:@"star blue.png"];
            image2.image = [UIImage imageNamed:@"star outline.png"];
            image3.image = [UIImage imageNamed:@"star outline.png"];
            image4.image = [UIImage imageNamed:@"star outline.png"];
            image5.image = [UIImage imageNamed:@"star outline.png"];
            break;
        case 2:
            
            image1.image = [UIImage imageNamed:@"star blue.png"];
            image2.image = [UIImage imageNamed:@"star blue.png"];
            image3.image = [UIImage imageNamed:@"star outline.png"];
            image4.image = [UIImage imageNamed:@"star outline.png"];
            image5.image = [UIImage imageNamed:@"star outline.png"];

            break;
            
        case 3:
            image1.image = [UIImage imageNamed:@"star blue.png"];
            image2.image = [UIImage imageNamed:@"star blue.png"];
            image3.image = [UIImage imageNamed:@"star blue.png"];
            image4.image = [UIImage imageNamed:@"star outline.png"];
            image5.image = [UIImage imageNamed:@"star outline.png"];
            break;
            
        case 4:
            image1.image = [UIImage imageNamed:@"star blue.png"];
            image2.image = [UIImage imageNamed:@"star blue.png"];
            image3.image = [UIImage imageNamed:@"star blue.png"];
            image4.image = [UIImage imageNamed:@"star blue.png"];
            image5.image = [UIImage imageNamed:@"star outline.png"];

            break;
            
        case 5:
          //  cell.imageViewStar1.image = [UIImage imageNamed:@"star blue.png"];
            image1.image = [UIImage imageNamed:@"star blue.png"];
            image2.image = [UIImage imageNamed:@"star blue.png"];
            image3.image = [UIImage imageNamed:@"star blue.png"];
            image4.image = [UIImage imageNamed:@"star blue.png"];
            image5.image = [UIImage imageNamed:@"star blue.png"];
            break;
        default:
            break;
    }

}

-(BOOL)validateEmailForDots:(NSString *)strEmail{
    
    if ([strEmail containsString:@"@"]) {
        NSString *str = strEmail;
        NSArray *Array = [str componentsSeparatedByString:@"@"];
        NSString *t1 = [Array objectAtIndex:1];
        if ([t1 containsString:@".."]) {
            return false;
        }
        else{
            return true;
        }
    }
    else{
        return false;
    }
    
}

-(BOOL)validatePhoneNumber:(NSString *)phoneNumber{
    NSString *phoneRegex = @"[235689][0-9]{6}([0-9]{3})?";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    BOOL matches = [test evaluateWithObject:phoneNumber];
    return  matches;
}
#pragma Mark - Add Zero to the price

-(NSString *)priceFormatting:(NSString *)strPrice{
    if ([strPrice containsString:@"."]) {
        NSString *str = strPrice;
        NSArray *Array = [str componentsSeparatedByString:@"."];
      //  NSString *t = [Array objectAtIndex:0];
        NSString *t1 = [Array objectAtIndex:1];
        if (t1.length == 1){
            strPrice = [NSString stringWithFormat:@"%@0",strPrice];
        }
        else{
            
        }
    }
    else{
        strPrice = [NSString stringWithFormat:@"%@.00",strPrice];
    }
    return strPrice;
}


#pragma mark - change color of font
- (UIImage *)useColor:(UIColor *)color forImage:(UIImage *)image
{
    if(!color)
        return image;
    
    NSUInteger width = CGImageGetWidth([image CGImage]);
    NSUInteger height = CGImageGetHeight([image CGImage]);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    NSUInteger bitmapByteCount = bytesPerRow * height;
    
    unsigned char *rawData = (unsigned char*) calloc(bitmapByteCount, sizeof(unsigned char));
    
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), [image CGImage]);
    
    CGColorRef cgColor = [color CGColor];
    const CGFloat *components = CGColorGetComponents(cgColor);
    float r = components[0] * 255.0;
    float g = components[1] * 255.0;
    float b = components[2] * 255.0;
    //float a = components[3]; // not needed
    
    int byteIndex = 0;
    
    while (byteIndex < bitmapByteCount)
    {
        int oldR = rawData[byteIndex];
        int oldG = rawData[byteIndex + 1];
        int oldB = rawData[byteIndex + 2];
        int oldA = rawData[byteIndex + 3];
        if(oldR != 0 || oldG != 0 || oldB != 0 || oldA != 0)
        {
            rawData[byteIndex] = r;
            rawData[byteIndex + 1] = g;
            rawData[byteIndex + 2] = b;
        }
        
        byteIndex += 4;
    }
    
    UIImage *result = [UIImage imageWithCGImage:CGBitmapContextCreateImage(context) scale:image.scale orientation:image.imageOrientation];
    
    CGContextRelease(context);
    free(rawData);
    
    return result;
}

@end
