//
//  RecieverTableViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 31/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecieverTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewReciever;
@property (weak, nonatomic) IBOutlet UIView *viewText;
@property (weak, nonatomic) IBOutlet UILabel *labelText;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcWidthViewText;

@end
