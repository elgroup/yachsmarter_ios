//
//  ImageViewController.m
//  Sho N Go
//
//  Created by Navyug on 01/04/15.
//  Copyright (c) 2015 otpapps. All rights reserved.
//

#import "ImageViewController.h"
#import "CommonMethods.h"
#import "WebServiceManager.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "Constants.h"
#import "YachtMasters-Swift.h"

@interface ImageViewController ()<UIScrollViewDelegate, UIAlertViewDelegate>{
    
    IBOutlet UIScrollView *_scrollerImage;
    IBOutlet UIButton     *_btnNext;
    IBOutlet UIButton     *_btnPrevious;
    
    UIView *_viewContainer;
    UILabel *_lblNoImage;
    UIScrollView *_scrollerChild;
    
    UIBarButtonItem *_btnDelete;
    UIBarButtonItem *_btnBack;
    
    int  _direction;
    int  _x;
    BOOL _scrolled;
    int  _currentImageIndex;
    CGFloat _zoomFactor;
    BOOL _nextEnable;
    BOOL _previousEnable;
    BOOL _inScroll;
    BOOL _buttonPressed;
    
    
    IBOutlet UIImageView *_representerImage;
    BOOL _doubleTapOn;
    MBProgressHUD *hud;
    IBOutlet NSLayoutConstraint *topConstarint;
}

- (IBAction)btnPreviousTapped:(UIButton *)sender;
- (IBAction)btnNextTapped:(UIButton *)sender;

@end

@implementation ImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];//BACKGROUND_COLOR;// [UIColor colorWithPatternImage:[UIImage imageNamed:@"background_pattern.jpg"]];
    
    self.title = [NSString stringWithFormat:@"1/%lu", (unsigned long)_selectedImages.count];
    NSLog(@"selected images = %@", _selectedImages);
    
    //// adding delete button in navigation bar ////
    //    UIButton *btnDelete = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if (!_deletionNotAllowed) {
        
        _btnDelete = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteImage:)];
        self.navigationItem.rightBarButtonItem = _btnDelete;
    }
    
    _btnBack = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(goBack:)];
    self.navigationItem.leftBarButtonItem = _btnBack;
    
    //// setting bottom button states ////
    _btnPrevious.selected = YES;
    [_btnPrevious setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnPrevious setTitle:@"Previous" forState:UIControlStateNormal];
    _btnNext.selected     = _selectedImages == nil || _selectedImages.count < 2;
    
    _viewContainer = [UIView new];
    _zoomFactor = 1;
    
    //    if (IS_IPHONE_X) {
    //        topConstarint.constant = 88;
    //    }
    //    else{
    //        topConstarint.constant = 64;
    //    }
    //// test code ////
    //    _scrollerImage.layer.borderColor = [UIColor blueColor].CGColor;
    //    _scrollerImage.layer.borderWidth = 2;
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    _x                      = 0;
    _currentImageIndex      = 1;
    _direction              = 1;
    _scrollerImage.delegate = self;
    
    //// setting all images in scrollview ////
    [self setImageScroller];
    
    //// showing image which was selected in last view ////
    [_scrollerImage setContentOffset:CGPointMake(_index * _scrollerImage.frame.size.width, 0)];
    
    //// when there is no image left ////
    _lblNoImage = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    _lblNoImage.text = @"No image left.";
    _lblNoImage.hidden = YES;
    _lblNoImage.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:_lblNoImage];
    _lblNoImage.center = self.view.center;
}

/**
 * this method will set all the images in scroll view
 */
- (void) setImageScroller{
    
    //// flushing image view ////
    UIView *subview = nil;
    for (int i = 0; _viewContainer.subviews && i < _viewContainer.subviews.count; i ++) {
        
        subview = [_viewContainer.subviews objectAtIndex:i];
        [subview removeFromSuperview];
        subview = nil;
    }
    
    //// adding images in scrollview ////
    UIImageView *iv = nil;
    for (int i = 0;_selectedImages && i < _selectedImages.count; i++) {
        
        NSObject *object = [_selectedImages objectAtIndex:i];
        UIImage *image;
        
        if ([object isKindOfClass:[NSDictionary class]]) {
            
            //            UIImageView *imageView = [((NSDictionary *) object) objectForKey:@"Image"];
            //            image = imageView.image;
            if ([[((NSDictionary *) object) objectForKey:@"image"] isKindOfClass:[NSNull class]]) {
                //                NSString *strUlr = [NSString stringWithFormat:@"http://gharwale.com%@",[((NSDictionary *) object) objectForKey:@"image_url"]];
                //                NSURL *url = [NSURL URLWithString:strUlr];
                //                // image = [sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"PlaceHolder_yachtOwner@2x.png"]];
            }
            else{
                image = [((NSDictionary *) object) objectForKey:@"image"];
            }
        }
        else if ([object isKindOfClass:[UIImage class]]){
            
            image = (UIImage *)object;
        }
        
        UITapGestureRecognizer *doubleTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapped:)];
        doubleTapped.numberOfTapsRequired = 2;
        
        UIScrollView *childScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(i * _scrollerImage.frame.size.width + 3, 0, _scrollerImage.frame.size.width - 6, _scrollerImage.frame.size.height)];
        childScrollView.delegate         = self;
        //        childScrollView.backgroundColor  = [UIColor redColor];
        childScrollView.maximumZoomScale = 4;
        childScrollView.minimumZoomScale = 1;
        childScrollView.tag              = i + 1;
        childScrollView.bounces          = NO;
        //        childScrollView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight;
        
        [childScrollView addGestureRecognizer:doubleTapped];
        
        iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _scrollerImage.frame.size.width - 6, _scrollerImage.frame.size.height)];
        //        iv.backgroundColor = [UIColor purpleColor];
        if ([[((NSDictionary *) object) objectForKey:@"image"] isKindOfClass:[NSNull class]]) {
            NSString *strUlr = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[((NSDictionary *) object) objectForKey:@"image_url"]];
            NSURL *url = [NSURL URLWithString:strUlr];
            [iv sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"PlaceHolder_yachtOwner@2x.png"]];
        }
        else{
            // image = [((NSDictionary *) object) objectForKey:@"image"];
            iv.image = image;
        }
        
        
        iv.contentMode = UIViewContentModeScaleAspectFit;
        
        [childScrollView addSubview:iv];
        
        [_scrollerImage addSubview:childScrollView];
        CGPoint center = iv.center;
        
        iv.frame = [self rectForImage:iv.image inView:iv];
        iv.center = center;
        
        
        //        UIImageView *second = [[UIImageView alloc] initWithFrame:[self rectForImage:iv.image inView:iv]];
        //        second.image = iv.image;
        //        [_viewContainer addSubview:second];
    }
    
    _viewContainer.frame = CGRectMake(0, 0, (_selectedImages.count) * _scrollerImage.frame.size.width, _scrollerImage.frame.size.height);
    _scrollerImage.contentSize = CGSizeMake((_selectedImages.count) * _scrollerImage.frame.size.width, _scrollerImage.frame.size.height);
    
    //    [_scrollerImage addSubview:_viewContainer];
}

- (void) doubleTapped : (UITapGestureRecognizer *)gesture{
    
    UIScrollView *scrollView = (UIScrollView *)gesture.view;
    
    if (!_doubleTapOn) {
        
        scrollView.zoomScale = 2;
    }
    else{
        
        scrollView.zoomScale = 1;
    }
    
    
    if (scrollView.zoomScale == 1) {
        
        UIScrollView *parentScroller = (UIScrollView *)scrollView.superview;
        int num = parentScroller.contentOffset.x / parentScroller.frame.size.width + 1;
        
        if (num > 1) {
            
            _btnPrevious.selected = NO;
        }
        else{
            
            _btnPrevious.selected = YES;
        }
        
        if (num - 1 < _selectedImages.count - 1) {
            
            _btnNext.selected = NO;
        }
        else{
            
            _btnNext.selected = YES;
        }
    }
    else{
        
        _btnNext.selected = YES;
        _btnPrevious.selected = YES;
    }
    
    _doubleTapOn = !_doubleTapOn;
}

#pragma mark- Saroller methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    NSLog(@"scrolling y = %f", scrollView.contentOffset.y);
    
    if (scrollView.contentOffset.y < 0) {
        
        scrollView.contentOffset = CGPointMake(scrollView.contentOffset.x, 0);
    }
    
    
    if (scrollView != _scrollerImage) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIView *view = [scrollView.subviews objectAtIndex:0];
                
                if (view.frame.size.height <= scrollView.frame.size.height) {
                    //                if (scrollView.zoomScale <= 1) {
                    
                    scrollView.contentSize = CGSizeMake(scrollView.contentSize.width, scrollView.frame.size.height);
                }
                
                if (view.frame.size.width <= scrollView.frame.size.width) {
                    
                    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height);
                }
                //                else{
                //
                //
                //                    scrollView.contentSize = CGSizeMake(scrollView.contentSize.width * scrollView.zoomScale, scrollView.frame.size.height * scrollView.zoomScale);
                //                }
                
                if (_scrollerChild) {
                    
                    UIView *viewZ = [_scrollerChild.subviews objectAtIndex:0];
                    viewZ.center = CGPointMake(scrollView.contentSize.width / 2, scrollView.contentSize.height / 2);
                }
            });
        });
        
        
        
        if (scrollView.zoomScale != 1) {
            
            _btnPrevious.selected = YES;
            _btnNext.selected     = YES;
        }
        
        return;
    }
    
    if (_x > scrollView.contentOffset.x) {
        
        _direction = -1;
    }
    else{
        
        _direction = 1;
    }
    
    _x = scrollView.contentOffset.x;
    
    int num = scrollView.contentOffset.x / scrollView.frame.size.width + 1;
    _currentImageIndex = num;
    self.title = [NSString stringWithFormat:@"%d/%lu", num, (unsigned long)_selectedImages.count];
    
    //    if (_zoomFactor == 1) {
    
    if (num > 1) {
        
        _previousEnable = YES;
    }
    else{
        
        _previousEnable = NO;
    }
    
    NSArray *arr = _viewContainer.subviews;
    if (num - 1 < _selectedImages.count - 1) {
        
        _nextEnable = YES;
    }
    else{
        
        _nextEnable = NO;
    }
    //    }
    //    else{
    //
    //        _nextEnable = NO;
    //        _previousEnable = NO;
    //
    //        _btnPrevious.selected = YES;
    //        _btnNext.selected = YES;
    //    }
    
    
    if (_scrollerChild == nil || _scrollerChild.zoomScale == 1) {
        
        if (!_buttonPressed) {
            
            if (num > 1) {
                
                _btnPrevious.selected = NO;
            }
            else{
                
                _btnPrevious.selected = YES;
            }
            
            if (num - 1 < _selectedImages.count - 1) {
                
                _btnNext.selected = NO;
            }
            else{
                
                _btnNext.selected = YES;
            }
        }
        else{
            
            _btnNext.selected = YES;
            _btnPrevious.selected = YES;
        }
    }
    
    _scrolled = YES;
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    
    if (scrollView != _scrollerImage) {
        
        return;
    }
    
    
    
    int num = scrollView.contentOffset.x / scrollView.frame.size.width + 1;
    
    if (num != _currentImageIndex) {
        
        _scrollerChild.zoomScale = 1;
        _doubleTapOn = NO;
        
    }
    
    if (!_buttonPressed) {
        
        if (num > 1) {
            
            _btnPrevious.selected = NO;
        }
        else{
            
            _btnPrevious.selected = YES;
        }
        
        if (num - 1 < _selectedImages.count - 1) {
            
            _btnNext.selected = NO;
        }
        else{
            
            _btnNext.selected = YES;
        }
    }
    else{
        
        _btnNext.selected = YES;
        _btnPrevious.selected = YES;
    }
    
    [self enablingBottomButton];
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    
    if (scrollView != _scrollerImage) {
        
        return;
    }
    
    int num = scrollView.contentOffset.x / scrollView.frame.size.width + 1;
    
    //    if (num != _scrollerChild.tag) {
    //
    //        _scrollerChild.zoomScale = 1;
    //    }
    
    //    NSLog(@"Dragging in progress");
    _buttonPressed = NO;
    
    
    
    _x = scrollView.contentOffset.x;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    
    //    UIImageView *iv = [_scrollerImage.subviews objectAtIndex:_currentImageIndex - 1];
    
    UIView *view = nil;
    
    if (scrollView != _scrollerImage) {
        
        view = [[scrollView subviews] objectAtIndex:0];
        _scrollerChild = scrollView;
    }
    
    return view;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale{
    
    _zoomFactor = scale;
    
    
    UIView *viewZ = [scrollView.subviews objectAtIndex:0];
    
    
    NSLog(@"------ %@  ---  %@", NSStringFromCGSize(scrollView.contentSize), NSStringFromCGRect(viewZ.frame));
    
    if (scale <= 1) {
        
        scrollView.contentSize = CGSizeMake(scrollView.contentSize.width, scrollView.frame.size.height);
    }
    
    
    
    viewZ.center = CGPointMake(viewZ.center.x, scrollView.contentSize.height / 2);
    
    
    
    if (scale == 1) {
        
        //        scrollView.pagingEnabled = YES;
        [self scrollViewDidEndScrollingAnimation:_scrollerImage];
    }
    else{
        
        //        scrollView.pagingEnabled = NO;
        
        _btnPrevious.selected = YES;
        _btnPrevious.selected = YES;
    }
    //
    //    int num = scrollView.contentOffset.x/ scrollView.zoomScale / scrollView.frame.size.width + 1;
    //    self.title = [NSString stringWithFormat:@"%d/%d", num, _selectedImages.count];
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
    
    NSLog(@"zooming = %f", scrollView.zoomScale);
    //    int num = scrollView.contentOffset.x/ scrollView.zoomScale / scrollView.frame.size.width + 1;
    //    self.title = [NSString stringWithFormat:@"%d/%d", num, _selectedImages.count];
    
    
    
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIView *view = [scrollView.subviews objectAtIndex:0];
                
                if (view.frame.size.height <= scrollView.frame.size.height) {
                    //                if (scrollView.zoomScale <= 1) {
                    
                    scrollView.contentSize = CGSizeMake(scrollView.contentSize.width, scrollView.frame.size.height);
                }
                
                if (view.frame.size.width <= scrollView.frame.size.width) {
                    
                    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollView.contentSize.height);
                }
                //                else{
                //
                //                    scrollView.contentSize = CGSizeMake(scrollView.contentSize.width * scrollView.zoomScale, scrollView.frame.size.height * scrollView.zoomScale);
                //                }
                
                if (_scrollerChild) {
                    
                    UIView *viewZ = [_scrollerChild.subviews objectAtIndex:0];
                    viewZ.center = CGPointMake(scrollView.contentSize.width / 2, scrollView.contentSize.height / 2);
                }
            });
        });
    });
    
    
    
}




- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (scrollView != _scrollerImage) {
        
        return;
    }
    
    int num = scrollView.contentOffset.x / scrollView.frame.size.width + 1;
    
    
    if (scrollView.contentOffset.x > _x) {
        
        _scrollerChild.zoomScale = 1;
        _doubleTapOn = NO;
        
        if (num > 1) {
            
            _btnPrevious.selected = NO;
        }
        else{
            
            _btnPrevious.selected = YES;
        }
        
        if (num - 1 < _selectedImages.count - 1) {
            
            _btnNext.selected = NO;
        }
        else{
            
            _btnNext.selected = YES;
        }
    }
    
    if (num != _scrollerChild.tag) {
        
        _scrollerChild.zoomScale = 1;
        
        if (num > 1) {
            
            _btnPrevious.selected = NO;
        }
        else{
            
            _btnPrevious.selected = YES;
        }
        
        if (num - 1 < _selectedImages.count - 1) {
            
            _btnNext.selected = NO;
        }
        else{
            
            _btnNext.selected = YES;
        }
    }
    
    
}




#pragma mark- Alert method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        
        [self deleteImage:nil];
    }
}

#pragma mark- buttons
- (IBAction)btnPreviousTapped:(UIButton *)sender{
    
    if (_scrollerImage.contentOffset.x > 0 && !sender.selected) {
        
        int num = _scrollerImage.contentOffset.x / (_scrollerImage.frame.size.width * _zoomFactor);
        _currentImageIndex = num;
        [_scrollerImage setContentOffset:CGPointMake((num - 1) * _scrollerImage.frame.size.width * _zoomFactor, 0) animated:YES];
    }
    
    sender.selected = YES;
    _buttonPressed = YES;
    //    [self performSelector:@selector(reset) withObject:nil afterDelay:.5];
}

- (IBAction)btnNextTapped:(UIButton *)sender{
    
    if (_scrollerImage.contentOffset.x < _scrollerImage.contentSize.width - _scrollerImage.frame.size.width && !sender.selected) {
        
        int num = _scrollerImage.contentOffset.x / (_scrollerImage.frame.size.width * _zoomFactor);
        _currentImageIndex = num;
        [_scrollerImage setContentOffset:CGPointMake((num + 1) * _scrollerImage.frame.size.width, 0) animated:YES];
    }
    
    sender.selected = YES;
    _buttonPressed = YES;
    //    [self performSelector:@selector(reset) withObject:nil afterDelay:.5];
}

- (void) reset{
    
    _buttonPressed = NO;
}

- (void) enablingBottomButton{
    
    _btnNext.selected     = !_nextEnable;
    _btnPrevious.selected = !_previousEnable;
    _buttonPressed = NO;
}

- (void) deleteImage:(UIBarButtonItem *)barButton{
    
    //// when actual delete button will click, only an alert will be shown ////
    if (barButton != nil) {
        
        UIAlertView *deletionAlert = [[UIAlertView alloc] initWithTitle:@""
                                                                message:@"Do you want to delete the image?"
                                                               delegate:self
                                                      cancelButtonTitle:@"No"
                                                      otherButtonTitles:@"Yes", nil];
        [deletionAlert show];
        return;
    }
    
    NSLog(@"delete for image = %d", _currentImageIndex);
    
    UIScrollView *sv = [_scrollerImage.subviews objectAtIndex:_currentImageIndex - 1];
    UIImageView  *iv = [sv.subviews objectAtIndex:0];
    iv.image = nil;
    [sv removeFromSuperview];
    sv.hidden = YES;
    
    
    NSMutableArray * objectsToDelete = [NSMutableArray array];
    NSMutableArray * picturesToDelete = [NSMutableArray array];
    
    NSError * error;
    NSDictionary *object = [_selectedImages objectAtIndex:_currentImageIndex -1];
    
    [objectsToDelete addObject:object];
    
    //Picture * pic = (Picture *)[object objectForKey:@"Picture"];
    // [self.product removePicturesObject:pic];
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    //    BOOL success = [fileManager removeItemAtPath:pic.pictureUrl error:&error];
    //    if(!success){
    //        //NSLog(@"Can't remove file: %@", error);
    //    }
    
    NSSet *picturesSet = [[NSSet alloc] init];
    [picturesSet setByAddingObjectsFromArray:picturesToDelete];
    
    //    [self.selectedPictures removeObjectsInArray:objectsToDelete];
    // [self.selectedImages removeObjectsInArray:objectsToDelete];
    
    if ([[[objectsToDelete valueForKey:@"id"] objectAtIndex:0] intValue] == -1) {
        [self.selectedImages removeObjectsInArray:objectsToDelete];
    }
    else
    {
        
        [self DeleteImageFromServer:objectsToDelete];
        [self.selectedImages removeObjectsInArray:objectsToDelete];
    }
    //    _currentImageIndex --;
    
    
    
    int num = _scrollerImage.contentOffset.x/ _zoomFactor / _scrollerImage.frame.size.width + 1;
    _currentImageIndex = num;
    self.title = [NSString stringWithFormat:@"%d/%lu", num, (unsigned long)_selectedImages.count];
    
    
    
    
    [self setImageScroller];
    
    if (_selectedImages.count < 2) {
        
        _btnNext.selected = YES;
    }
    
    if (_selectedImages.count == 0) {
        
        _lblNoImage.hidden = NO;
        self.navigationItem.rightBarButtonItem = nil;
        
        self.title = @"";
        
        [self goBack:_btnBack];
    }
}

-(void)DeleteImageFromServer:(NSArray *)arrObj{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            //            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
        
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSString *strRole = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        int Id = [[[arrObj objectAtIndex:0] valueForKey:@"id"] intValue];
        [WebServiceManager deleteRequestWithUrlString:[NSString stringWithFormat:@"%@yacht_images/%d",kBaseUrl,Id] emailString:strRole accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                //[self.selectedImages removeObjectsInArray:arrObj];
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
            });
        }];
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}
- (void) goBack:(UIBarButtonItem *)barButton{
    
    _scrollerImage.delegate = nil;
    //    [self saveProduct:^{
    
    [self.navigationController popViewControllerAnimated:YES];
    //    }];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

/*
 -(void)saveProduct:(void(^)(void))onComplete{
 
 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
 
 for(NSDictionary * object in self.selectedImages){
 
 //UIImageView * imageView = [object objectForKey:@"Image"];
 Picture * picture = [object objectForKey:@"Picture"];
 NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
 NSString *path = [paths objectAtIndex:0];
 //NSString *tmpPathToFile = [[NSString alloc] initWithString:[NSString stringWithFormat:@"%@/%@.jpg", path, picture.pictureId]];
 
 picture.active = @1;
 picture.lastmoddate = [NSDate date];
 
 NSString * pictureName = [NSString stringWithFormat:@"%@-%@-%@",picture.product.vendor.tradeShow.name, picture.product.name, picture.pictureId];
 pictureName = [OTPString stringforFileName:pictureName];
 
 NSString *oldTmpPathToFile = [[NSString alloc] initWithString:[NSString stringWithFormat:@"%@/%@.jpg", path, picture.pictureId]];
 NSString *tmpPathToFile = [[NSString alloc] initWithString:[NSString stringWithFormat:@"%@/%@.jpg", path, pictureName]];
 
 //NSLog(@"pic: %@", picture);
 
 NSError * err = NULL;
 NSFileManager * fm = [[NSFileManager alloc] init];
 picture.product = self.product;
 
 BOOL result = [fm moveItemAtPath:oldTmpPathToFile toPath:tmpPathToFile error:&err];
 if(!result)
 NSLog(@"Error: %@", err);
 else
 picture.pictureUrl = tmpPathToFile;
 
 picture.active = @1;
 picture.lastmoddate = [NSDate date];
 picture.createddate = self.product.createddate;
 
 }
 dispatch_async(dispatch_get_main_queue(), ^{
 
 onComplete();
 });
 });
 }
 */
- (CGRect) rectForImage:(UIImage *)image inView:(UIView *)view{
    
    CGRect frame = view.frame;
    BOOL widthConstant = YES;
    CGFloat width, height;
    
    if (image.size.width <= view.frame.size.width && image.size.height <= view.frame.size.height) {
        
        frame.size = image.size;
        return frame;
    }
    
    if (image.size.height / view.frame.size.height > image.size.width / view.frame.size.width) {
        
        widthConstant = NO;
    }
    
    if (widthConstant) {
        
        width = view.frame.size.width;
        height = view.frame.size.width * image.size.height / image.size.width;
    }
    else{
        
        width = view.frame.size.height * image.size.width / image.size.height;
        height = view.frame.size.height;
    }
    
    frame.size.height = height;
    frame.size.width  = width;
    
    return frame;
}
@end
