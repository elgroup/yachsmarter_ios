//
//  MapViewController.m
//  YachtMasters
//
//  Created by Anvesh on 14/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "MapViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GooglePlaces/GooglePlaces.h>
#import "SDKDemoAPIKey.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Constants.h"
#import "CustomNavigationViewController.h"
#import "CommonMethods.h"

@interface MapViewController ()<GMSAutocompleteViewControllerDelegate,CLLocationManagerDelegate,GMSMapViewDelegate>
{
    CLLocationManager *locationManager;
    GMSPlacesClient *_placesClient;
    CLLocationCoordinate2D coordinate;
    CLLocationCoordinate2D startPoint;
    UIButton *buttonSearchAddress;
    NSMutableArray *arrAddress;
    GMSMarker *marker;
    __weak IBOutlet UIView *viewSearch;
    __weak IBOutlet UITextField *textFieldSearch;
}
@property (strong, nonatomic)GMSMapView *cutomMapView;
@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = false;
    self.navigationItem.hidesBackButton = true;
    
    [[CommonMethods sharedInstance]CornerRadius:viewSearch Radius:20.0 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
    UIImageView *search = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 12)];
    search.image = [UIImage imageNamed:@"search@2x.png"];
    textFieldSearch.rightViewMode = UITextFieldViewModeAlways;
    textFieldSearch.rightView = search;
    [self GetCurrentLocation];
    // self.title = @"Select Place";
    coordinate = [self getLocation];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self CustomNavigationBar];
    float latitude = coordinate.latitude;
    float  longitude = coordinate.longitude;
    [self showGoogleMapView:latitude lng:longitude];
    [self getAddress:latitude andLongitude:longitude];
   
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    
    self.navigationController.navigationBar.hidden = NO;
    if (_isEditProfile) {
        // self.title = @"select Place";
        //  [self NavigationBar];
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

-(void)CustomNavigationBar{
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 10.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    
    
    UIImage* image = [UIImage imageNamed:@"check-mark"];
    CGRect frame = CGRectMake(UIScreen.mainScreen.bounds.size.width - 40, 10, 30,30);
    UIButton *doneButton = [[UIButton alloc] initWithFrame:frame];
    [doneButton addTarget:self action:@selector(buttonTickClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButton =[[UIBarButtonItem alloc] initWithCustomView:doneButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 10.0, 15.0, 15.0)];
    imageView.image = image;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [doneButton addSubview:imageView];
    
    self.title = @"Select Place";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}

#pragma mark - Custom Navigation Bar
-(void)NavigationBar{
   
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 5.0, 20.0, 20.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    self.title = @"Select Place";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}

#pragma mark - back button pressed
-(void)BackButtonPressed{
    
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - Button tick action
-(void)buttonTickClicked{
    if (arrAddress.count > 0) {
        [self.delegate AddressOfYacht:arrAddress];
        [self.navigationController popViewControllerAnimated:true];
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:@"Please select address" andViewController:self];
    }
}


#pragma mark - TextField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [textField resignFirstResponder];
    [self onLaunchClicked];
}


#pragma mark --- : Show Google MapView :----
-(void)showGoogleMapView:(float)lat lng:(float)lng
{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lat longitude:lng zoom:16];
    CGRect rect;
    rect = [[UIApplication sharedApplication] statusBarFrame];
    NSLog(@"Statusbar frame: %1.0f, %1.0f, %1.0f, %1.0f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
    _cutomMapView = [GMSMapView mapWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height+rect.size.height, [[UIScreen mainScreen] bounds].size.width,[[UIScreen mainScreen] bounds].size.height - self.navigationController.navigationBar.frame.size.height-rect.size.height) camera:camera];
    [_cutomMapView setCamera:camera];
    [_cutomMapView animateToCameraPosition:camera];
    _cutomMapView.indoorEnabled = YES;
    _cutomMapView.myLocationEnabled = YES;
    _cutomMapView.settings.zoomGestures = YES;
    _cutomMapView.settings.compassButton = YES;
    _cutomMapView.settings.scrollGestures = YES;
    _cutomMapView.settings.myLocationButton = YES;
    _cutomMapView.accessibilityElementsHidden = NO;
    _cutomMapView.delegate = self;
    marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(lat, lng);
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.icon = [UIImage imageNamed:@"map-pin.png"];
    marker.map = _cutomMapView;
    [self.view addSubview:_cutomMapView];
    
    
    buttonSearchAddress = [UIButton buttonWithType:UIButtonTypeCustom];
//    buttonSearchAddress.frame = CGRectMake(40.0, 50.0, [[UIScreen mainScreen] bounds].size.width - 80, 40.0);
//    [_cutomMapView addSubview:buttonSearchAddress];
//    [buttonSearchAddress setTitle:@"Search Address" forState:UIControlStateNormal];
//    [buttonSearchAddress setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [buttonSearchAddress setBackgroundColor:[UIColor whiteColor]];
//    [buttonSearchAddress addTarget:self action:@selector(onLaunchClicked:) forControlEvents:UIControlEventTouchUpInside];
//    buttonSearchAddress.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
//    buttonSearchAddress.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.view bringSubviewToFront:viewSearch];
    
}
- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position{
    
}

#pragma mark --- : Get Coordinate2D :----
-(CLLocationCoordinate2D) getLocation{
    sleep(2);
    CLLocation *location = [locationManager location];
    coordinate = [location coordinate];
    return coordinate;
}

#pragma mark - Get Current location and authorization
-(void)GetCurrentLocation{
    locationManager = [[CLLocationManager alloc]init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    // _cutomMapView.showsUserLocation = YES;
    [self->locationManager startUpdatingLocation];
    [locationManager requestWhenInUseAuthorization];
}

#pragma mark: Get Address from Coordinates
-(void)getAddress:(float )lat andLongitude:(float)lang{

    CLLocation *location=[[CLLocation alloc]initWithLatitude:lat longitude:lang];
    //from Google Map SDK
    GMSGeocoder *geocode=[[GMSGeocoder alloc]init];
    GMSReverseGeocodeCallback handler=^(GMSReverseGeocodeResponse *response,NSError *error)
    {
        GMSAddress *address=response.firstResult;
        if (address)
        {
            NSLog(@"%@",[NSString stringWithFormat:@"%@, %@",[address.lines objectAtIndex:0],address.country]);
            
            NSMutableDictionary *dictPlace = [[NSMutableDictionary alloc]init];
            [dictPlace setObject:[NSString stringWithFormat:@"%@",[address.lines objectAtIndex:0]] forKey:@"Place"];
            NSMutableDictionary *dictCountry = [[NSMutableDictionary alloc]init];
            [dictCountry setObject:[NSString stringWithFormat:@"%@",address.country] forKey:@"Country"];
            
            NSMutableDictionary *dictState = [[NSMutableDictionary alloc]init];
            [dictState setObject:[NSString stringWithFormat:@"%@",address.administrativeArea] forKey:@"State"];
            
            NSMutableDictionary *dictCity = [[NSMutableDictionary alloc]init];
            [dictCity setObject:[NSString stringWithFormat:@"%@",address.locality] forKey:@"City"];
            
            NSMutableDictionary *dictZipCode = [[NSMutableDictionary alloc]init];
            [dictZipCode setObject:[NSString stringWithFormat:@"%@",address.postalCode] forKey:@"ZipCode"];
            
            
            arrAddress = [[NSMutableArray alloc]init];
            [arrAddress addObject:dictPlace];
            [arrAddress addObject:dictCountry];
            [arrAddress addObject:dictState];
            [arrAddress addObject:dictCity];
            [arrAddress addObject:dictZipCode];
        }
        //[buttonSearchAddress setTitle:[address.lines objectAtIndex:0] forState:UIControlStateNormal];
        textFieldSearch.text = [address.lines objectAtIndex:0];
    };
    
    [geocode reverseGeocodeCoordinate:location.coordinate completionHandler:handler];
}



// Present the autocomplete view controller when the button is pressed.
- (void)onLaunchClicked {
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    [self presentViewController:acController animated:YES completion:nil];
}

// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
   [self dismissViewControllerAnimated:YES completion:nil];
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
    NSLog(@"%f",place.coordinate.latitude);
    //CLLocationCoordinate2D location = place.coordinate;
    [self getAddress:place.coordinate.latitude andLongitude:place.coordinate.longitude];
    [self showGoogleMapView:place.coordinate.latitude lng:place.coordinate.longitude];
    
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    [self->locationManager startUpdatingLocation];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
