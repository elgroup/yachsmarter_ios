//
//  NewBookingVC.m
//  YachtMasters
//
//  Created by Anvesh on 17/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "NewBookingVC.h"
#import "Constants.h"
#import "NewBookingsTableViewCell.h"
#import "CommonMethods.h"
#import "MBProgressHUD.h"
#import "Constants.h"
#import "WebServiceManager.h"
#import "YachtMasters-Swift.h"
#import "UIImageView+WebCache.h"

@interface NewBookingVC ()<UITableViewDelegate,UITableViewDataSource>
{
    MBProgressHUD *hud;
    BOOL isApi;
}
@end

@implementation NewBookingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self CustomNavigationBar];
    [self RegisterForLastUse];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.hidesBottomBarWhenPushed = true;
    self.tabBarController.tabBar.hidden = true;
}
-(void)CustomNavigationBar{
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 10.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    self.title =@"New Bookings";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}
-(void)BackButtonPressed{
    self.tabBarController.tabBar.hidden = false;
    [self.navigationController popViewControllerAnimated:true];
}


-(void)ButtonApprovePressed:(id)sender{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:[[self.arrNewBookings objectAtIndex:[sender tag]]valueForKey:@"id"] forKey:@"id"];
        [dict setObject:@"approved" forKey:@"status"];
        NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
        [dictResponse setObject:dict forKey:@"bookings"];
        [WebServiceManager putRequestWithUrlString:[NSString stringWithFormat:@"%@bookings",kBaseUrl] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:[sender tag]];
            NewBookingsTableViewCell *cell = (NewBookingsTableViewCell*)[self.tableViewNewBookings cellForRowAtIndexPath:indexpath];
            [cell.buttonApprove setTitle:@"Approved" forState:UIControlStateNormal];
            [cell.buttonDecline setTitle:@"Cancel" forState:UIControlStateNormal];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:[result valueForKey:@"message"] andViewController:self];
            });
            
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
            
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
        
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
    
}
-(void)ButtonCancelPressed:(id)sender{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:[[self.arrNewBookings objectAtIndex:[sender tag]]valueForKey:@"id"] forKey:@"id"];
        [dict setObject:@"declined" forKey:@"status"];
        NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
        [dictResponse setObject:dict forKey:@"bookings"];
        [WebServiceManager putRequestWithUrlString:[NSString stringWithFormat:@"%@bookings",kBaseUrl] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:[sender tag]];
            NewBookingsTableViewCell *cell = (NewBookingsTableViewCell*)[self.tableViewNewBookings cellForRowAtIndexPath:indexpath];
            [cell.buttonDecline setTitle:@"Declined" forState:UIControlStateNormal];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:[result valueForKey:@"message"] andViewController:self];
            });
            
            
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
    
}

#pragma mark- tableview delegate and datasource

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"NewBookingsTableViewCell";
    NewBookingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewBookingsTableViewCell"];
    
    if (cell == nil) {
        NSArray *nib =[[NSBundle mainBundle]loadNibNamed:simpleTableIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.labelYachName.text = [[[self.arrNewBookings objectAtIndex:indexPath.section]valueForKey:@"yacht"] valueForKey:@"name"];
    NSString *strStartDate = [[self.arrNewBookings objectAtIndex:indexPath.section]valueForKey:@"start_date"];
    
    //NSString *strStartDate = [[self.arrNewBookings objectAtIndex:indexPath.section]valueForKey:@"start_date"];
    NSString *strStartTime = [[self.arrNewBookings objectAtIndex:indexPath.section]valueForKey:@"start_time"];
    NSString *strTimeDuration = [[self.arrNewBookings objectAtIndex:indexPath.section]valueForKey:@"term"];
    int hours = [[[self.arrNewBookings objectAtIndex:indexPath.section]valueForKey:@"hours"] intValue];
    strStartTime = [self FormattedTime:strStartTime];
    strStartDate = [self FormattedDate:strStartDate];
    cell.labelBookingDate.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Booking Date: %@",strStartDate] andRangeString:@"Booking Date:"];
    cell.labelDuration.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Booking Time: %@",strStartTime] andRangeString:@"Booking Time:"];
    if ([strTimeDuration isEqualToString:@"per_day"] || [strTimeDuration isEqualToString:@"half_day"]){
        cell.labelTimeDuration.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Duration: %@ (%d hours)",[self timeDurationforBookings:strTimeDuration],hours] andRangeString:@"Duration:"];
    }
    else{
        cell.labelTimeDuration.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Duration: %@",[self timeDurationforBookings:strTimeDuration]] andRangeString:@"Duration:"];
    }
    NSString *strPrice = [NSString stringWithFormat:@"%@",[[self.arrNewBookings objectAtIndex:indexPath.section ]valueForKey:@"booking_price"]];
    NSString *strCurrency = [[self.arrNewBookings objectAtIndex:indexPath.section ]valueForKey:@"booking_currency_symbol"];
    NSString *strUrl = [[[self.arrNewBookings objectAtIndex:indexPath.section]valueForKey:@"current_user"] valueForKey:@"image"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseUrlImage,strUrl]];
    [cell.imageViewUser sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    cell.labelPricing.attributedText = [self PlainStringToAttributedStringForPrice:strPrice andCurrency:strCurrency];
    cell.buttonDecline.tag = indexPath.section;
    cell.buttonApprove.tag = indexPath.section;
    [cell.buttonApprove addTarget:self action:@selector(ButtonApprovePressed:) forControlEvents:UIControlEventTouchUpInside];
    [cell.buttonDecline addTarget:self action:@selector(ButtonCancelPressed:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.arrNewBookings.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 290;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

-(NSMutableAttributedString *)PlainStringToAttributedStringForPrice:(NSString *)strPrice andCurrency:(NSString *)strCurrency{
    
    // NSString *strPrice = [[[[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"yacht"]valueForKey:@"price"] stringValue];
    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@%@",strCurrency,strPrice]];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:attrText];
    strPrice  = [strPrice stringByAppendingString:strCurrency];
    NSRange range = [strPrice rangeOfString:strPrice];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:FontOpenSansSemiBold(12)} range:range];
    return attributedText;
}

-(NSString *)FormattedDate:(NSString *)startDate{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *strDate = [formatter dateFromString:startDate];
    [formatter setDateFormat:@"MMM dd yyyy"];
    startDate = [formatter stringFromDate:strDate];
    return startDate;
}
-(NSString *)FormattedTime:(NSString *)startTime{
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setTimeZone:sourceTimeZone];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *strTime = [formatter dateFromString:startTime];
    [formatter setDateFormat:@"hh:mm a"];
    startTime = [formatter stringFromDate:strTime];
   
    return startTime;
}
-(NSAttributedString *)AttributedTextForOwner:(NSString *)stringToShow andRangeString:(NSString *)boldString{
    
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor lightGrayColor],
                              NSFontAttributeName: FontOpenSans(12)
                              };
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:stringToShow attributes:attribs];
    
    
    UIFont *boldFont = FontOpenSansBold(12);
    
    NSRange range = [stringToShow rangeOfString:boldString];
    [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                    NSFontAttributeName:boldFont} range:range];
    return  attributedText;
}
-(NSString *)timeDurationforBookings:(NSString *)strDuration{
    NSString *str;
    
        if ([strDuration isEqualToString:@"per_day"]){
            str = @"Full Day";
        }
        else if ([strDuration isEqualToString:@"half_day"]){
            str = @"Half Day";
        }
        else if ([strDuration isEqualToString:@"per_hour"]){
            str = @"Hour";
        }
        else if([strDuration isEqualToString:@"week"]) {
            str = @"Week";
        }
        else{
            str = @"";
        }
        return str;
    
}
-(void)RegisterForLastUse{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        
        NSDate *date = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"dd-MM-yyyy"];
        NSString *strDate = [formatter stringFromDate:date];
        [formatter setDateFormat:@"HH:mm"];
        NSString *strTime = [formatter stringFromDate:date];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:strDate forKey:@"last_used_date"];
        [dict setObject:strTime forKey:@"last_used_time"];
        [WebServiceManager putRequestWithUrlString:[NSString stringWithFormat:@"%@users/last_used",kBaseUrl] withPostString:dict emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            
            [[NSUserDefaults standardUserDefaults]setBool:true forKey:@"LastUsed"];
        } failure:^(NSString *msg, BOOL success) {
            
        }];
    }
}

-(void)ApiForNewBookings{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        dispatch_async(dispatch_get_main_queue(), ^{
            //            hud = [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
            //            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
            
        });
        [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@get_new_bookings",kBaseUrl] emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            _arrNewBookings = [[NSMutableArray alloc]init];
            _arrNewBookings = [result valueForKey:@"bookings"];
            [_tableViewNewBookings reloadData];
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
            });
            if ([_arrNewBookings count] >0) {
                //buttonNewBookings.hidden = false;
                //[buttonNewBookings setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[arrNewBookings count]] forState:UIControlStateNormal];
                //[buttonNewBookings setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                //buttonNewBookings.titleLabel.font = FontOpenSansLight(11);
            }
            //arrNewBookings
            
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // [MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
    
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
