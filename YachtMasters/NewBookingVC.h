//
//  NewBookingVC.h
//  YachtMasters
//
//  Created by Anvesh on 17/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewBookingVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableViewNewBookings;
@property (strong,nonatomic)NSMutableArray *arrNewBookings;

@end
