//
//  AppDelegate.h
//  YachtMasters
//
//  Created by Anvesh on 06/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDKDemoAPIKey.h"
#import <GooglePlaces/GooglePlaces.h>
#import <GoogleMaps/GoogleMaps.h>
#import <PocketSocket/PSWebSocket.h>
#import <PocketSocket/PSWebSocketServer.h>
#import <UserNotifications/UserNotifications.h>
#import "AppSocket.h"
#import <CoreLocation/CoreLocation.h>
//#import "YachtMasters-Swift.h"
@import Stripe;
@interface UINavigationBar (CustomHeight)

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableDictionary *dictFilterItems;
@property (strong, nonatomic) NSString *strDeviceToken;
@property (strong, nonatomic) NSMutableDictionary *dictFacebookDetails;
@property (strong, nonatomic) AppSocket *socket;
@property (strong, nonatomic) CLLocation *currentLocation;
@property BOOL isAppLaunch;
@property BOOL isNotification;


+ (NSString *)stringFromDeviceToken:(NSData *)deviceToken;
@end

