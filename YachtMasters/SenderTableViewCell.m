//
//  SenderTableViewCell.m
//  YachtMasters
//
//  Created by Anvesh on 31/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "SenderTableViewCell.h"

@implementation SenderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
        // Initialization code
   // [self AddCornerTotheView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)AddCornerTotheView{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.viewText.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(10.0, 10.0)];
    self.maskLayer = [[CAShapeLayer alloc] init];
    self.maskLayer.frame = self.bounds;
    self.maskLayer.path  = maskPath.CGPath;
    self.viewText.layer.mask = self.maskLayer;
    
    self.borderLayer.frame = self.contentView.bounds;
    self.borderLayer.path  = maskPath.CGPath;
    self.borderLayer.lineWidth   = 1.0f;
    self.borderLayer.strokeColor = [UIColor lightGrayColor].CGColor;
    self.borderLayer.fillColor   = [UIColor clearColor].CGColor;
    [self.viewText.layer addSublayer:self.borderLayer];
    _borderLayer = [[CAShapeLayer alloc] init];

}
@end
