//
//  NewBookingsTableViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 17/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewBookingsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewBackGround;
@property (weak, nonatomic) IBOutlet UIView *viewImageBackGround;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewUser;
@property (weak, nonatomic) IBOutlet UILabel *labelPricing;
@property (weak, nonatomic) IBOutlet UIButton *buttonApprove;
@property (weak, nonatomic) IBOutlet UIButton *buttonDecline;
@property (weak, nonatomic) IBOutlet UILabel *labelBookingDate;
@property (weak, nonatomic) IBOutlet UILabel *labelDuration;
@property (weak, nonatomic) IBOutlet UILabel *labelYachName;
@property (weak, nonatomic) IBOutlet UILabel *labelTimeDuration;

@end
