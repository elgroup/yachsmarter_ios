//
//  AmenitiesViewController.m
//  YachtMasters
//
//  Created by Anvesh on 15/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "AmenitiesViewController.h"
#import "AddYachtViewController.h"
#import "WebServiceManager.h"
#import "MBProgressHUD.h"
#import "Constants.h"
#import "CommonMethods.h"
#import "YachtMasters-Swift.h"

@interface AmenitiesViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableDictionary* dictAmenities;
    MBProgressHUD *hud;
    NSArray *arrAllKeys;
}
@property (weak, nonatomic) IBOutlet UITableView *aminitiesTableView;

@end

@implementation AmenitiesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationItem.hidesBackButton = true;
    [self CustomNavigationBar];
    [self ApiForGettingAmenitiesList];
    dictAmenities = [[NSMutableDictionary alloc]init];
    
     if (_arrSelectedAmenities) {
     
     }
     else{
     _arrSelectedAmenities = [[NSMutableArray alloc]init];
     }
    arrAllKeys = [[NSArray alloc]init];
    _aminitiesTableView.allowsMultipleSelection = true;
}


-(void)ApiForGettingAmenitiesList{
    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    dispatch_async(myQueue, ^{
        NSString *strRole = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@amenities",kBaseUrl] emailString:strRole accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            dictAmenities = [[result valueForKey:@"amenities"] mutableCopy];
            arrAllKeys = [dictAmenities allKeys];
            
            NSLog(@"%@",_arrSelectedAmenities);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // [MBProgressHUD hideHUDForView:self.view animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [self.aminitiesTableView reloadData];
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // [MBProgressHUD hideHUDForView:self.view animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            //            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            //            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            //
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
        
    });
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}
-(void)viewWillAppear:(BOOL)animated{
    
}

-(void)CustomNavigationBar{
    self.navigationItem.hidesBackButton = true;
    self.navigationItem.title = @"Select Amenities";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:13]}];
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    //[backButton setBackgroundColor:[UIColor redColor]];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 10.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    
    UIImage* image = [UIImage imageNamed:@"check-mark"];
    CGRect frame = CGRectMake(UIScreen.mainScreen.bounds.size.width - 40, 10, 30,30);
    UIButton *doneButton = [[UIButton alloc] initWithFrame:frame];
    [doneButton addTarget:self action:@selector(buttonTickClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarButton =[[UIBarButtonItem alloc] initWithCustomView:doneButton];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 10.0, 15.0, 15.0)];
    imageView.image = image;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [doneButton addSubview:imageView];
}

-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)buttonTickClicked{
    [self.delegate amenitiesRecieved:_arrSelectedAmenities];
    [self.navigationController popViewControllerAnimated:YES];

}
#pragma mark - tableview delegates and data source

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    //cell.textLabel.text = [[dictAmenities objectAtIndex:indexPath.section]valueForKey:@"name"];
    NSString *strKey = [arrAllKeys objectAtIndex:indexPath.section];
    cell.textLabel.text = [[[dictAmenities valueForKey:strKey]objectAtIndex:indexPath.row] valueForKey:@"name"];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    //[cell setTintColor:[UIColor blueColor]];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.textLabel.font = FontOpenSans(12);//[UIFont fontWithName:@"Helvetica" size:12];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *strKey = [arrAllKeys objectAtIndex:indexPath.section];
    NSDictionary *dict = [[dictAmenities valueForKey:strKey]objectAtIndex:indexPath.row];
    
    if ([_arrSelectedAmenities containsObject:dict]) {
        // [cell setSelected:YES animated:NO];
        
        NSIndexPath* selectedCellIndexPath= [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        [self tableView:_aminitiesTableView didSelectRowAtIndexPath:selectedCellIndexPath];
        [_aminitiesTableView selectRowAtIndexPath:selectedCellIndexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSString *strKey = [arrAllKeys objectAtIndex:section];
    NSArray *arr = [dictAmenities valueForKey:strKey];
    return arr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *strKey = [arrAllKeys objectAtIndex:indexPath.section];
    [_arrSelectedAmenities addObject:[[dictAmenities valueForKey:strKey] objectAtIndex:indexPath.row]];
    
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSString *strKey = [arrAllKeys objectAtIndex:indexPath.section];
    NSDictionary *dict = [[dictAmenities valueForKey:strKey] objectAtIndex:indexPath.row];
    if ([_arrSelectedAmenities containsObject:dict]) {
        [_arrSelectedAmenities removeObject:dict];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return dictAmenities.count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.frame = CGRectMake(0.0, 0.0, [[UIScreen mainScreen]bounds].size.width, 50);
    headerView.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(15.0, 10.0, headerView.frame.size.width - 30, 40)];
    [headerView addSubview:label];
    label.text = [arrAllKeys objectAtIndex:section];
    label.font = FontOpenSansSemiBold(12);//[UIFont fontWithName:@"Helvetica-Bold" size:14];
    UILabel *labelLine = [[UILabel alloc]init];
    labelLine.frame = CGRectMake(15.0, 49.0, headerView.frame.size.width - 30, 1.0);
    labelLine.backgroundColor = [UIColor lightGrayColor];
    [headerView addSubview:labelLine];
    
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
