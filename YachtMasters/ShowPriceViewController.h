//
//  ShowPriceViewController.h
//  YachtMasters
//
//  Created by Anvesh on 18/09/18.
//  Copyright © 2018 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowPriceViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *viewPrices;
@property (weak, nonatomic) IBOutlet UIView *viewBottom;
@property (weak, nonatomic) NSMutableArray *arrPrices;
@property (weak, nonatomic) IBOutlet UIView *viewBar;
@property (weak, nonatomic) NSString *strCurrencySymbol;
@end
