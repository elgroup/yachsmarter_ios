//
//  OwnerBookingsTableViewCell.m
//  YachtMasters
//
//  Created by Anvesh on 17/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "OwnerBookingsTableViewCell.h"

@implementation OwnerBookingsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _viewBase.layer.shadowColor = [UIColor colorWithRed:0.0/255.0 green:196.0/255.0 blue:56.0/255.0 alpha:1].CGColor;
    _viewBase.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    _viewBase.layer.shadowOpacity = 0.3;
    _viewBase.layer.shadowRadius = 5;
    _viewBase.layer.masksToBounds = false;
    _viewBase.layer.cornerRadius = 5.0;
    _viewImagebackGround.layer.cornerRadius = _viewImagebackGround.frame.size.height/2;
    _imageViewUser.layer.cornerRadius = _imageViewUser.frame.size.height/2;
    _imageViewUser.clipsToBounds = true;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
