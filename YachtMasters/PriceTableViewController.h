//
//  PriceTableViewController.h
//  YachtMasters
//
//  Created by Anvesh on 03/09/18.
//  Copyright © 2018 EL. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PriceDelegate <NSObject>
@required
- (void)priceForYacht:(NSMutableArray *)arr;
@end
@interface PriceTableViewController : UITableViewController
@property (nonatomic,strong) id<PriceDelegate> delegate;
@property (nonatomic,strong) NSMutableArray *arrPrice;
@end
