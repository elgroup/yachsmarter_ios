//
//  ReviewTableViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 17/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewUser;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelReview;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewStar1;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewStar2;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewStar4;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewStar3;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewStar5;

@end
