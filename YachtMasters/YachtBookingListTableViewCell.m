//
//  YachtBookingListTableViewCell.m
//  YachtMasters
//
//  Created by Anvesh on 24/07/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "YachtBookingListTableViewCell.h"
#import "CommonMethods.h"

@implementation YachtBookingListTableViewCell

- (void)awakeFromNib {
    [[CommonMethods sharedInstance]CornerRadius:_baseViewForImage Radius:_baseViewForImage.frame.size.height/2 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
    [[CommonMethods sharedInstance]CornerRadius:_imageViewUser Radius:_imageViewUser.frame.size.height/2 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
   // [[CommonMethods sharedInstance]DropShadow:_baseView UIColor:[UIColor lightGrayColor] andShadowRadius:5.0];
    _baseView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    _baseView.layer.shadowOffset = CGSizeMake(0.0, 5.0);
    _baseView.layer.shadowOpacity = 0.5;
    _baseView.layer.shadowRadius = 4.0;
    _baseView.layer.masksToBounds = false;
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
