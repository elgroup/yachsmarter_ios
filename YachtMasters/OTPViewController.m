//
//  OTPViewController.m
//  YachtMasters
//
//  Created by Anvesh on 06/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "OTPViewController.h"
#import "Constants.h"
#import "WebServiceManager.h"
#import "CommonMethods.h"
#include "MBProgressHUD.h"
#import "SignUpTVC.h"
#import "YachtMasters-Swift.h"

@interface OTPViewController ()<UITextFieldDelegate,MBProgressHUDDelegate>
{
    MBProgressHUD *hud;
    __weak IBOutlet UIView *viewForProgressIndicator;
    IBOutlet NSLayoutConstraint *lcHeightForProceed;
}
@property (weak, nonatomic) IBOutlet UIView *view3;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;
@property (weak, nonatomic) IBOutlet UIButton *buttonProceed;
@property (weak, nonatomic) IBOutlet UITextField *textFieldOtp;
@property (weak, nonatomic) IBOutlet UIButton *buttonResendCode;
@property (weak, nonatomic) IBOutlet UIButton *ButtonChangePhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *labelOR;
@end

@implementation OTPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem=nil;
    self.navigationItem.hidesBackButton=YES;
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"bgpic.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    
    
    self.view1.layer.cornerRadius = 14;
    self.view1.clipsToBounds = true;
    self.label1.layer.cornerRadius =13;
    self.label1.clipsToBounds = true;
    
    self.view2.layer.cornerRadius = 18;
    self.view2.clipsToBounds = true;
    self.label2.layer.cornerRadius = 13;
    self.label2.clipsToBounds = true;
    
    self.view3.layer.cornerRadius = 14;
    self.view3.clipsToBounds = true;
    self.label3.layer.cornerRadius = 13;
    self.label3.clipsToBounds = true;
    
    self.buttonProceed.layer.cornerRadius = 22.5;
    self.buttonProceed.clipsToBounds = true;
    
    self.labelOR.layer.cornerRadius = 21.0;
    self.labelOR.clipsToBounds = true;
    self.labelOR.layer.borderColor = [UIColor whiteColor].CGColor;
    self.labelOR.layer.borderWidth = 1.0;
    
    self.buttonResendCode.layer.cornerRadius = 15.0;
    self.buttonResendCode.clipsToBounds = true;
    
    self.ButtonChangePhoneNumber.layer.cornerRadius = 15.0;
    self.ButtonChangePhoneNumber.clipsToBounds = true;
    
    
    UIColor *color = [UIColor whiteColor];
    _textFieldOtp.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Code" attributes:@{NSForegroundColorAttributeName: color}];
    
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonClicked:)];
    UIToolbar *toolBarPhoneNumber = [[UIToolbar alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-200, self.view.frame.size.width, 50)];
    [toolBarPhoneNumber setBarStyle:UIBarStyleBlackOpaque];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton, nil];
    [toolBarPhoneNumber setItems:toolbarItems];
    doneButton.tintColor = [UIColor lightGrayColor];
    _textFieldOtp.inputAccessoryView = toolBarPhoneNumber;
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (_isEditProfile) {
        viewForProgressIndicator.hidden = true;
    }
    else{
        viewForProgressIndicator.hidden = false;
    }
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (IS_IPHONE_6P) {
        lcHeightForProceed.constant = 50.0;
        self.buttonProceed.titleLabel.font = FontOpenSansBold(16);
    }
}
- (IBAction)Tapped:(id)sender {
    [self.view endEditing:true];
}

- (IBAction)ButtonProceedClicked:(id)sender {
    if(_textFieldOtp.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Enter the code!" andViewController:self];
    }
    else if (_textFieldOtp.text.length <5){
        [[CommonMethods sharedInstance]AlertMessage:@"Enter the valid code!" andViewController:self];
        
    }
    else{
        BOOL isInternet = [[CommonMethods sharedInstance]checkInternetConnection];
        if (isInternet) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //                hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                //                hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
                [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
                
            });
            NSString *strPhoneId = [[NSUserDefaults standardUserDefaults]objectForKey:@"phone_id"];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            int Code = [_textFieldOtp.text intValue];
            [dict setObject:[NSNumber numberWithInt:Code] forKey:@"otp"];
            [dict setObject:strPhoneId forKey:@"phone_id"];
            NSMutableDictionary *dictRequest = [[NSMutableDictionary alloc]init];
            [dictRequest setValue:dict forKey:@"phones"];
            [WebServiceManager PutRequest:[NSString stringWithFormat:@"%@users/phones",kBaseUrl] andParam:dictRequest andcompletionhandler:^(NSArray *returArray, NSError *error){
                if(!error){
                    if([[returArray valueForKey:@"status"] boolValue] ){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            //                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                            [[LoaderNew sharedLoader]hideLoader];
                            if(_isEditProfile){
                                [self.navigationController popViewControllerAnimated:true];
                            }
                            else{
                                UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                SignUpTVC *controller = [story instantiateViewControllerWithIdentifier:@"SignUpTVC"];
                                [self.navigationController pushViewController:controller animated:true];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    //                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    [[LoaderNew sharedLoader]hideLoader];
                                });
                            }
                        });
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                            [[LoaderNew sharedLoader]hideLoader];
                        });
                        [[CommonMethods sharedInstance]AlertMessage:[returArray valueForKey:@"message"] andViewController:self];
                    }
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        //                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        [[LoaderNew sharedLoader]hideLoader];
                    });
                    [[CommonMethods sharedInstance]AlertMessage:[returArray valueForKey:@"message"] andViewController:self];
                }
            }];
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // [MBProgressHUD hideHUDForView:self.view animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
            });
            [[CommonMethods sharedInstance]AlertMessage:@"No internet connection" andViewController:self];
        }
    }
    //                if(_isEditProfile){
    //                    [self.navigationController popViewControllerAnimated:true];
    //                }
    //                else{
    //                    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //                    SignUpTVC *controller = [story instantiateViewControllerWithIdentifier:@"SignUpTVC"];
    //                    [self.navigationController pushViewController:controller animated:true];
    //                }
}

-(void)doneButtonClicked:(id)sender{
    
    //[_textFieldPhoneNumber resignFirstResponder];
    [self.view endEditing:true];
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    }];
}

#pragma mark-TextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField== _textFieldOtp  && IS_IPHONE_5) {
        [UIView animateWithDuration:0.45 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, -50, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-50);
        }];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(range.length + range.location > textField.text.length){
        return NO;
    }
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:textField.text];
    [attributedString addAttribute:NSKernAttributeName
                             value:@(13.0)
                             range:NSMakeRange(0, textField.text.length)];
    textField.attributedText = attributedString;
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 5;
}

#pragma mark - Resend the Code

- (IBAction)buttonResendClicked:(id)sender {
    BOOL isInternet = [[CommonMethods sharedInstance]checkInternetConnection];
    if (isInternet) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            //            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        });
        NSDictionary *dict =[NSDictionary  dictionaryWithObject:_strNumber forKey:@"number"];
        NSMutableDictionary *dictRequest = [[NSMutableDictionary alloc]init];
        [dictRequest setObject:dict forKey:@"phones"];
        NSLog(@"%@",dictRequest);
        [WebServiceManager PostRequest:[NSString stringWithFormat:@"%@users/phones",kBaseUrl] andParam:dictRequest andcompletionhandler:^(NSArray *returArray, NSError *error) {
            if (!error) {
                if([[returArray valueForKey:@"status"] boolValue]){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //  [MBProgressHUD hideHUDForView:self.view animated:YES];
                        [[LoaderNew sharedLoader]hideLoader];
                        _textFieldOtp.text = @"";
                        [[CommonMethods sharedInstance]AlertMessage:@"Code has been sent again to the number provided" andViewController:self];
                    });
                    
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //[MBProgressHUD hideHUDForView:self.view animated:YES];
                        [[LoaderNew sharedLoader]hideLoader];
                    });
                    //  [[CommonMethods sharedInstance]AlertMessage:[returArray valueForKey:@"message"] andViewController:self];
                }
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    // [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [[LoaderNew sharedLoader]hideLoader];
                });
                // [[CommonMethods sharedInstance]AlertMessage:[returArray valueForKey:@"message"] andViewController:self];
            }
        }];
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:@"No internet connection" andViewController:self];
    }
    
}

#pragma mark - Change phone number
- (IBAction)buttonChangePhonenUmberClicked:(id)sender {
    _isEditProfile = false;
    [self.navigationController popViewControllerAnimated:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
