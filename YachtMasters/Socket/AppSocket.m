//
//  AppSocket.m
//  NewWorldOrder
//
//  Created by Meena on 12/13/17.
//  Copyright © 2017 Meena. All rights reserved.
//

#import "AppSocket.h"
#import "AppDelegate.h"
#import "Constants.h"

@implementation AppSocket
@synthesize delegate;

+ (AppSocket *) sharedSocket{
    
    static AppSocket *socket;
    if (socket == nil) {
        
        socket = [AppSocket new];
    }
    return socket;
}

-(void)startSerevr {
    _server = [PSWebSocketServer serverWithHost:nil port:9001];
    _server.delegate = self;
    [_server start];
}

- (void)serverDidStart:(PSWebSocketServer *)server {
    NSLog(@"Server did start…");
}
- (void)serverDidStop:(PSWebSocketServer *)server {
    NSLog(@"Server did stop…");
}
- (BOOL)server:(PSWebSocketServer *)server acceptWebSocketWithRequest:(NSURLRequest *)request {
    NSLog(@"Server should accept request: %@", request);
    return YES;
}
- (void)server:(PSWebSocketServer *)server webSocket:(PSWebSocket *)webSocket didReceiveMessage:(id)message {
    NSLog(@"Server websocket did receive message: %@", message);
}
- (void)server:(PSWebSocketServer *)server webSocketDidOpen:(PSWebSocket *)webSocket {
    NSLog(@"Server websocket did open");
}
- (void)server:(PSWebSocketServer *)server webSocket:(PSWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    NSLog(@"Server websocket did close with code: %@, reason: %@, wasClean: %@", @(code), reason, @(wasClean));
}
- (void)server:(PSWebSocketServer *)server webSocket:(PSWebSocket *)webSocket didFailWithError:(NSError *)error {
    NSLog(@"Server websocket did fail with error: %@", error);
}

- (void)server:(PSWebSocketServer *)server didFailWithError:(NSError *)error {
    //[self initSocket];
}

#pragma - Socket Support Methods.

-(void)initializeSocket{
    
    NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
    //wss://yachtsmarter.co
    NSString *strUrl = [NSString stringWithFormat:@"ws://africanpost.gharwale.com/cable?token=%@",strAuthToken];
    //  NSString *strUrl = [NSString stringWithFormat:@"ws://http://192.168.2.73:3003/cable?token=%@",strAuthToken];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
    _socket = [PSWebSocket clientSocketWithRequest:request];
    _socket.delegate = self;
    [_socket open];
}

-(void)joinChannel:(NSString *)channelName
{
    NSString *strChannel = [NSString stringWithFormat:@"{\"channel\": \"%@\"}",channelName];
    id data = @{
                @"command": @"subscribe",
                @"identifier": strChannel
                };
    
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:data options:0 error:nil];
    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"myString= %@", myString);
    
    [_socket send:myString];
}

-(void)unSubscribeChannel:(NSString *)channelName{
    NSString *strChannel = [NSString stringWithFormat:@"{\"channel\": \"%@\"}",channelName];
    id data = @{
                @"command": @"unsubscribe",
                @"identifier": strChannel
                };
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:data options:0 error:nil];
    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"myString= %@", myString);
    
    [_socket send:myString];
}

#pragma mark - PSWebSocketDelegate Methods -

-(void)webSocketDidOpen:(PSWebSocket *)webSocket
{
    [self joinChannel:@"AppearanceChannel"];
}

-(void)webSocket:(PSWebSocket *)webSocket didReceiveMessage:(id)message {
    
    NSData *data = [message dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    NSString *messageType = json[@"type"];
    
    if(![messageType isEqualToString:@"ping"] && ![messageType isEqualToString:@"welcome"])
    {
        NSLog(@"The websocket received a message: %@", json[@"message"]);
        
        NSDictionary *msgDict = json[@"message"];
        if(![messageType isEqualToString:@"ping"] && ![messageType isEqualToString:@"welcome"] && ![messageType isEqualToString:@"confirm_subscription"]){
            if ([[json valueForKey:@"identifier"] isEqualToString:@"{\"channel\": \"AppearanceChannel\"}"]) {
                [self.delegate userStatusDidChangeWithData:msgDict];
            }
            else{
                [self.delegate socketDidReceiveMessage:json];
            }
            NSLog(@"The websocket received a message: %@", json[@"message"]);
        }
        
        
        
        //        if (msgDict != nil) {
        //
        //            if ([msgDict valueForKey:@"user_typing"]) {
        //                [self.delegate userStatusDidChangeWithData:msgDict];
        //            }else if ([msgDict valueForKey:@"online"]) {
        //                [self.delegate userStatusDidChangeWithData:msgDict];
        //            }else if ([msgDict valueForKey:@"message_type"] && ([UIAPPDelegate.obj.userInformation.identifier intValue] != [[msgDict valueForKey:@"user_id"] intValue])) {
        //                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        //
        ////                long conId = [[msgDict valueForKey:@"conversation_id"] longValue];
        ////                [dict setValue:[msgDict valueForKey:@"created_at"] forKey:@"created_at"];
        ////                [dict setValue:[msgDict valueForKey:@"body"] forKey:@"body"];
        ////                [dict setValue:[msgDict valueForKey:@"user_id"] forKey:@"user_id"];
        ////                [dict setValue:[msgDict valueForKey:@"receiver_id"] forKey:@"receiver_id"];
        ////                [dict setValue:[NSNumber numberWithLong:conId] forKey:@"conversation_id"];
        ////                [dict setValue:[msgDict valueForKey:@"message_type"] forKey:@"message_type"];
        ////                NSString *msg_id = [msgDict valueForKey:@"id"];
        ////                [dict setValue:msg_id forKey:k_message_id];
        ////
        ////                [Chat saveChatMessageInDBwithData:dict];
        //                [self.delegate socketDidReceiveMessage:dict];
        //            }
        //        }
        //    }else if ([messageType isEqualToString:@"user_typing"]) {
        //
        //        NSLog(@"Statred Typing");
        //    }
    }
}

-(void)webSocket:(PSWebSocket *)webSocket didFailWithError:(NSError *)error
{
    NSLog(@"The websocket handshake/connection failed with an error: %@", error);
    //[self initSocket];
}

-(void)webSocket:(PSWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean
{
    NSLog(@"The websocket closed with code: %@, reason: %@, wasClean: %@", @(code), reason, (wasClean) ? @"YES": @"NO");
}

@end
