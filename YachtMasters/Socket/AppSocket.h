//
//  MySocket.h
//  NewWorldOrder
//
//  Created by Meena on 12/13/17.
//  Copyright © 2017 Meena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <PocketSocket/PSWebSocket.h>
#import <PocketSocket/PSWebSocketServer.h>
#import "ChatViewController.h"

@protocol AppSocketDelegate <NSObject>
-(void)socketDidReceiveMessage:(NSDictionary*)messageInfo;
-(void)userStatusDidChangeWithData:(NSDictionary*)userInfo;
@end

@interface AppSocket : NSObject <PSWebSocketDelegate,PSWebSocketServerDelegate>
{
//     __weak id <AppSocketDelegate> delegate;
}

+(AppSocket*)sharedSocket;
@property (nonatomic, strong) PSWebSocket *socket;
@property(nonatomic,strong) PSWebSocketServer *server;
@property(nonatomic,weak) id <AppSocketDelegate> delegate;

-(void)startSerevr;
-(void)initializeSocket;
-(void)joinChannel:(NSString *)channelName;
-(void)unSubscribeChannel:(NSString *)channelName;

@end
