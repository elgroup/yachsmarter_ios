//
//  WalletViewController.m
//  YachtMasters
//
//  Created by Anvesh on 30/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "WalletViewController.h"
#import "Constants.h"
#import "CommonMethods.h"
#import "WebServiceManager.h"
#import "MBProgressHUD.h"
#import "YachtMasters-Swift.h"
#import "AddAccountTVC.h"


@interface WalletViewController (){
    IBOutlet UILabel *labelAmount;
    IBOutlet UIButton *buttonWithDraw;
    MBProgressHUD *hud;
    __weak IBOutlet UILabel *labelAbailableAmount;
    __weak IBOutlet UILabel *labelPendingAmount;
    __weak IBOutlet UILabel *labelInstructions;
}

@end

@implementation WalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self CustomNavigationBar];
    labelPendingAmount.text = [NSString stringWithFormat:@"* Pending Amount: %@",_strPendingAmount];
    labelAbailableAmount.text = [NSString stringWithFormat:@"** Available Amount: %@",_strAvailableWalletAmount];
    if (self.isNotTransferable){
        [buttonWithDraw setTitle:@"Request Payout" forState:UIControlStateNormal];
        labelInstructions.text = @"** Walllet money can be withdraw to Bank account  added. If the account is not added firstly add the Bank account, then request to withdraw the money will be send to support team.";
    }
    else{
        [buttonWithDraw setTitle:@"Withdraw Money" forState:UIControlStateNormal];
        labelInstructions.text = @"** Walllet money can be withdraw to the stripe account  added. If the account is not added firstly add the account then withdraw the  money.";
        
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[CommonMethods sharedInstance]DropShadow:buttonWithDraw UIColor:[UIColor colorWithRed:90.0/255.0 green:190.0/255.0 blue:238.0/255.0 alpha:1.0] andShadowRadius:4.0];
    
}
-(void)CustomNavigationBar{
    self.tabBarController.tabBar.hidden =  true;
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    //[backButton setBackgroundColor:[UIColor redColor]];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 5.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    self.title = @"Wallet Balance";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}

-(void)BackButtonPressed{
    self.navigationController.navigationBar.hidden = true;
    self.tabBarController.tabBar.hidden = false;
    [self.navigationController popViewControllerAnimated:true];
}

-(IBAction)ButtonWithdrawalClicked:(id)sender{
    if (self.isNotTransferable){
        [self ApiForPayoutRequest];
    }
    else{
        if (self.strAvailableWalletAmount > 0){
            [[CommonMethods sharedInstance]AlertMessage:@"There is no amount available to withdraw." andViewController:self ];
        }
        else{
            [self ApiForWihtDraw];
        }
    }
}

-(void)ApiForWihtDraw{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [WebServiceManager postRequestWithUrlString:[NSString stringWithFormat:@"%@withdrawal",kBaseUrl] withPostString:dict emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                labelAbailableAmount.text = [NSString stringWithFormat:@"** Available Amount: %d",0];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:[result valueForKey:@"message"] andViewController:self];
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
        
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}

-(void)ApiForPayoutRequest{
    if (self.isAccountCreated){
            if ([[CommonMethods sharedInstance]checkInternetConnection]) {
                NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
                NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
                [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@transfer/request",kBaseUrlVersion2] emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[LoaderNew sharedLoader]hideLoader];
                        [[CommonMethods sharedInstance]AlertMessage:[result valueForKey:@"message"] andViewController:self];
                    });
        
                } failure:^(NSString *msg, BOOL success) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[LoaderNew sharedLoader]hideLoader];
                        [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
                    });
                }];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
                });
            }
            else{
                [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
            }
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:@"Please add Bank account. Then only the request for Payout will be send." andViewController:self];
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

