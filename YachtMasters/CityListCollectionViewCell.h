//
//  CityListCollectionViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 29/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityListCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *viewLabelCityName;

@property (weak, nonatomic) IBOutlet UILabel *labelCityName;

@end
