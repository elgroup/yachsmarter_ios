//
//  HelpViewController.m
//  YachtMasters
//
//  Created by Anvesh on 22/09/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "HelpViewController.h"
#import "Constants.h"
#import "CommonMethods.h"
#import "QuestionTableViewCell.h"
#import "SectionHeader.h"

@interface HelpViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UIView *sectionView;
    IBOutlet UITextField *textFieldAskQuestion;
    IBOutlet UITableView *tableViewHelp;
    IBOutlet UIView *viewForTextField;
    NSMutableArray *arrFAQs;
    BOOL isClicked;
    NSInteger indexPathSection;
    
}
@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrFAQs = [[NSMutableArray alloc]init];
    indexPathSection=200;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:@"What is YachtSmarter?" forKey:@"Quest"];
    [dict setObject: @"YachtSmarter is the world's biggest and quickest developing yacht  booking app. We enable individuals to locate their ideal yacht experience, regardless of whether it's leasing a yacht for a day or getting a gathering together and going out on a mega yacht. We offer the very best yachts available. YachtSmarter connects you directly with yacht owners via inbuilt chat feature in the app. " forKey:@"Ans"];
    [arrFAQs addObject:dict];
    dict = [NSMutableDictionary new];
    [dict setObject:@"Is there a minimum or maximum time frame of yacht booking?" forKey:@"Quest"];
    [dict setObject:@"Absolutely not.  Some yachts offer daily rates and you go as long or short as you like. Some yachts charge a surcharge if the charter is shorter then 4 or 5 nights." forKey:@"Ans"];
    [arrFAQs addObject:dict];
    
    dict = [NSMutableDictionary new];
    [dict setObject:@"How Does YachtSmarter Work?" forKey:@"Quest"];
    [dict setObject:@"YachtSmarter connects clients with yacht owners and facilitates the booking. We have the collection of the finest yacht listings around the globe. For booking a yacht these easy steps can be followed :Clients launch YachtSmarter app and select the location they're hoping to make a booking. A listing of yachts will be exhibited to clients. Clients may then use YachtSmarter features to narrow down the choice. Once the client find the right yacht, Clients will then submit request to the contact owner of the yacht.The owner will answer back with an offer to book the yacht with the information the client gave based on availability.Once the client accepts an offer, the client's credit card will be charged the amount of the booking and the booking will be confirmed." forKey:@"Ans"];
    [arrFAQs addObject:dict];
    
    dict = [NSMutableDictionary new];
    [dict setObject:@"Does YachtSmarter offer insurance?" forKey:@"Quest"];
    [dict setObject:@"YachtSmarter bookings do not include insurance. Clients should check with the yacht owner to see if they have the proper insurance for the booking. If an owner does not have insurance, either party may purchase daily rental insurance offered through any 3rd party insurance provider." forKey:@"Ans"];
    [arrFAQs addObject:dict];
    
    dict = [NSMutableDictionary new];
    [dict setObject:@"What are the YachtSmarter service fees?" forKey:@"Quest"];
    [dict setObject:@"YachtSmarter has a 10% service fee to facilitate the booking through the platform. This enables us to process secure payments and prevent fraud, provide protection to both owners and clients, and offer excellent customer support when you need it." forKey:@"Ans"];
    [arrFAQs addObject:dict];
    
    dict = [NSMutableDictionary new];
    [dict setObject:@"How does YachtSmarter handle cancellations and refunds?" forKey:@"Quest"];
    [dict setObject:@"If a customer cancels 1 week prior to the booked date, a full refund will be processed minus YachtSmarter service fee which is always non-refundable.If the client cancels a booking with in 1 week of the booked date a refund of 50% of booking value will be processed also the Yachtsmarter fee will not be refunded.If an owner cancels a booking, a full refund will be issued to the customer.Please keep in mind YachtSmarter service fee is non-refundable regardless of when the cancellation occurs." forKey:@"Ans"];
    [arrFAQs addObject:dict];
    
    dict = [NSMutableDictionary new];
    [dict setObject:@"Does YachtSmarter allow changes to confirmed booking?" forKey:@"Quest"];
    [dict setObject:@"If you would like to make minor changes to your confirmed booking, we suggest that you reach out to the owner. Changes can only be accepted if the owner agrees. If there are major changes to the booking that require price changes , you will have to cancel and re-book with the new details." forKey:@"Ans"];
    [arrFAQs addObject:dict];
    // Do any additional setup after loading the view.
}
-(BOOL)hidesBottomBarWhenPushed
{
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self CustomNavigationBar];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[CommonMethods sharedInstance]CornerRadius:viewForTextField Radius:4.0 BorderWidth:0.0 andColorForBorder:[UIColor clearColor]];
    [[CommonMethods sharedInstance]DropShadow:viewForTextField UIColor:[UIColor grayColor] andShadowRadius:5.0];
}

-(void)CustomNavigationBar{
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    //[backButton setBackgroundColor:[UIColor redColor]];
    [backButton addTarget:self action:@selector(BackButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 5.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    self.navigationItem.title = @"Help";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}

-(void)BackButtonPressed{
    [self.navigationController popViewControllerAnimated:true];
}


#pragma mark - UitableView DataSource and Delegates

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"QuestionTableViewCell";
    QuestionTableViewCell *cell = [tableViewHelp dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        NSArray *nib =[[NSBundle mainBundle]loadNibNamed:identifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.labelAnswer.text = [[arrFAQs objectAtIndex:indexPath.section]valueForKey:@"Ans"];
    
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [arrFAQs count];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    SectionHeader *rootView = [[[NSBundle mainBundle] loadNibNamed:@"SectionHeader" owner:nil options:nil] objectAtIndex:0];
    [rootView.buttonQuestion setTitle:[[arrFAQs objectAtIndex:section]valueForKey:@"Quest"] forState:UIControlStateNormal];
    rootView.buttonQuestion.tag=section;
    [rootView.buttonQuestion addTarget:self action:@selector(OpenAnswer:) forControlEvents:UIControlEventTouchUpInside];
    return rootView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (isClicked) {
        if (indexPathSection==section) {
            return 1;
        }
        return 0;
    }
    else
        return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *strText = [[arrFAQs objectAtIndex:indexPath.section] valueForKey:@"Ans"];
    CGSize Height = [[CommonMethods sharedInstance]findHeightForText:strText havingWidth:SCREEN_WIDTH - 90 andFont:FontOpenSans(13)];
    return  Height.height + 30;
}

//-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return UITableViewRowAnimationBottom;
//}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    CGSize size = [self getSizeForText:[[arrFAQs objectAtIndex:section]valueForKey:@"Quest"] maxWidth:SCREEN_WIDTH - 50 font:@"OpenSans-Bold" fontSize:13];
    return size.height+20;
}
//-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section{
//    return UITableViewAutomaticDimension;
//}



#pragma mark - getSizeForString
- (CGSize)getSizeForText:(NSString *)text maxWidth:(CGFloat)width font:(NSString *)fontName fontSize:(float)fontSize {
    CGSize constraintSize;
    constraintSize.height = MAXFLOAT;
    constraintSize.width = width;
    
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:fontName size:fontSize], NSFontAttributeName,
                                          nil];
    
    CGRect frame = [text boundingRectWithSize:constraintSize
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:attributesDictionary
                                      context:nil];
    
    CGSize stringSize = frame.size;
    return stringSize;
}


-(void)OpenAnswer:(id)sender
{
    UIButton *btn=(id)sender;
    if (btn.tag==indexPathSection) {
        if (!isClicked) {
            isClicked=YES;
            indexPathSection = btn.tag;
            NSLog(@"%ld",(long)indexPathSection);
            NSRange range = NSMakeRange(0, [self numberOfSectionsInTableView:tableViewHelp]);
            NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
            [tableViewHelp reloadSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];
            //[tableViewHelp reloadData];
            // [tableViewHelp reloadSections:indexPathSection withRowAnimation:UITableViewRowAnimationAutomatic];
            
        }
        else{
            isClicked=NO;
            //  [tableViewHelp reloadData];
            NSRange range = NSMakeRange(0, [self numberOfSectionsInTableView:tableViewHelp]);
            NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
            [tableViewHelp reloadSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        
    }else{
        isClicked=YES;
        indexPathSection = btn.tag;
        NSLog(@"%ld",(long)indexPathSection);
        //    [tableViewHelp reloadData];
        NSRange range = NSMakeRange(0, [self numberOfSectionsInTableView:tableViewHelp]);
        NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
        [tableViewHelp reloadSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];
        
        
        
    }
    
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

