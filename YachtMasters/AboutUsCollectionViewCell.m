//
//  AboutUsCollectionViewCell.m
//  YachtMasters
//
//  Created by Anvesh on 24/10/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "AboutUsCollectionViewCell.h"

@implementation AboutUsCollectionViewCell
- (void)awakeFromNib {
    [super awakeFromNib];
    [_labelContent sizeToFit];
}
@end
