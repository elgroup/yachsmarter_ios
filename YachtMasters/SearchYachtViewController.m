//
//  SearchYachtViewController.m
//  YachtMasters
//
//  Created by Anvesh on 27/06/17.
//  Copyright © 2017 EL. All rights reserved.

#import "SearchYachtViewController.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "WebServiceManager.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "CommonMethods.h"
#import "SearchYachtCollectionViewCell.h"
#import "CommonMethods.h"
#import "YachtMasters-Swift.h"
#import <RPSlidingMenu/RPSlidingMenu.h>
#import "UIImageView+WebCache.h"
#import "ComingSoonCollectionViewCell.h"
#define MAX_LENGTH ((int) 0)

@class UltravisualLayout;

@interface SearchYachtViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate>{
    AppDelegate *app;
    UITextField *textFieldSearchYacht;
    MBProgressHUD *hud;
    NSMutableArray *loadedArray;
    NSMutableArray *arrYachtCityList;
    BOOL isApi;
    IBOutlet UICollectionView *collectionViewYachtList;
    UltravisualLayout *layout;
}

@property (weak, nonatomic) IBOutlet UITableView *tableViewListOfYacht;
@end
@implementation SearchYachtViewController
@synthesize arrResponse;

-(void)viewDidLoad {
    [super viewDidLoad];
    if (IS_IPHONE_X || IS_IPHONE_XSMAX) {
        textFieldSearchYacht = [[UITextField alloc]initWithFrame:CGRectMake(60, 50, [[UIScreen mainScreen ]bounds].size.width - 120, 40)];
    }
    else{
        textFieldSearchYacht = [[UITextField alloc]initWithFrame:CGRectMake(60, 20, [[UIScreen mainScreen ]bounds].size.width - 120, 40)];
    }
    textFieldSearchYacht.hidden = true;
    [self.view addSubview:textFieldSearchYacht];
    
    UINib *cellNib1 = [UINib nibWithNibName:@"SearchYachtCollectionViewCell" bundle:nil];
    [collectionViewYachtList registerNib:cellNib1 forCellWithReuseIdentifier:@"SearchYachtCollectionViewCell"];
    UINib *cellNib2 = [UINib nibWithNibName:@"ComingSoonCollectionViewCell" bundle:nil];
    [collectionViewYachtList registerNib:cellNib2 forCellWithReuseIdentifier:@"ComingSoonCollectionViewCell"];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = true;
    isApi = false;
    self.currentPage = 1;
    [self ApiForCities];
    arrResponse = [[NSMutableArray alloc]init];
     [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewDidAppear:(BOOL)animated{
    
    textFieldSearchYacht.backgroundColor = [UIColor whiteColor];
    textFieldSearchYacht.layer.cornerRadius = 20.0;
    textFieldSearchYacht.clipsToBounds = true;
    textFieldSearchYacht.placeholder = @"Search for a country";
    textFieldSearchYacht.font = FontOpenSansSemiBold(13);
    textFieldSearchYacht.textAlignment = NSTextAlignmentCenter;
    textFieldSearchYacht.delegate = self;
    textFieldSearchYacht.returnKeyType = UIReturnKeyDone;
    textFieldSearchYacht.autocorrectionType = NO;
    [[CommonMethods sharedInstance]DropShadow:textFieldSearchYacht UIColor:[UIColor blackColor] andShadowRadius:3.0];
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - CollectionView Delegates and datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrResponse.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    BOOL coming_soon = [[[arrResponse objectAtIndex:indexPath.row]valueForKey:@"coming_soon"] boolValue];
    if (coming_soon) {
        ComingSoonCollectionViewCell *cell = [collectionViewYachtList dequeueReusableCellWithReuseIdentifier:@"ComingSoonCollectionViewCell" forIndexPath:indexPath];
        if (cell == nil)    {
            NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"ComingSoonCollectionViewCell" owner:self options:nil];
            cell = nibArray[0];
        }
        if (indexPath.row == 0) {
            cell.imageViewBackgrund.image = [UIImage imageNamed:@"top strip full.png"];
        }
        cell.labelCityname.text = [[[arrResponse objectAtIndex:indexPath.row]valueForKey:@"name"] uppercaseString];
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[arrResponse objectAtIndex:indexPath.row]valueForKey:@"image"]];
        NSURL *url = [NSURL URLWithString:strUrl];
        [cell.imageViewBackgrund sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"lower strip"]];
        cell.userInteractionEnabled = false;
        return cell;
    }
    else{
        SearchYachtCollectionViewCell *cell = [collectionViewYachtList dequeueReusableCellWithReuseIdentifier:@"SearchYachtCollectionViewCell" forIndexPath:indexPath];
        if (cell == nil)    {
            NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"SearchYachtCollectionViewCell" owner:self options:nil];
            cell = nibArray[0];
        }
        
        if (indexPath.row == 0) {
            cell.imageViewBackgrund.image = [UIImage imageNamed:@"top strip full.png"];
        }
        cell.labelCityname.text = [[[arrResponse objectAtIndex:indexPath.row]valueForKey:@"name"] uppercaseString];
        cell.labelNumberOfBookings.text = [[[arrResponse objectAtIndex:indexPath.row]valueForKey:@"yachts_count"] stringValue];
        cell.imageViewShip.hidden = false;
        //cell.imageViewAnchor.image = nil;
        cell.labelSubDistrict.text = [[arrResponse objectAtIndex:indexPath.row] valueForKey:@"address"];
        
        NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[arrResponse objectAtIndex:indexPath.row]valueForKey:@"image"]];
        NSURL *url = [NSURL URLWithString:strUrl];
        [cell.imageViewBackgrund sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"lower strip"]];
        return cell;
    }
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (isApi) {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, collectionViewYachtList.bounds.size.width, collectionViewYachtList.bounds.size.height)];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        if ([arrResponse count] ==0) {
            noDataLabel.hidden = false;
            button.hidden = false;
            noDataLabel.text             = @"No yacht found";
            noDataLabel.textColor        = [UIColor lightGrayColor];
            noDataLabel.textAlignment    = NSTextAlignmentCenter;
            collectionView.backgroundView = noDataLabel;
            noDataLabel.font = FontOpenSans(15);
            
            noDataLabel.userInteractionEnabled = true;
            //self.tableViewListOfYacht.separatorStyle = UITableViewCellSeparatorStyleNone;
            button.frame = CGRectMake(40.0, SCREEN_HEIGHT - 100, SCREEN_WIDTH  - 80, 48.0);
            [noDataLabel addSubview:button];
            [button setBackgroundImage:[UIImage imageNamed:@"add shape.png"] forState:UIControlStateNormal];
            [button setTitle:@"HOME" forState:UIControlStateNormal];
            button.titleLabel.font = FontOpenSansBold(15);
            [button addTarget:self action:@selector(ButtonHomePressed) forControlEvents:UIControlEventTouchUpInside];
            button.userInteractionEnabled = true;
            return 1;
        }
        else{
            collectionView.backgroundView.hidden = true;
            return [arrResponse count];
        }
    }
    else{
        return 1;
    }
}

- (BOOL) shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    BOOL coming_soon = [[[arrResponse objectAtIndex:indexPath.row]valueForKey:@"coming_soon"] boolValue];
    if (coming_soon) {
    }
    else{
        [self.delegate CityList:arrResponse];
        [textFieldSearchYacht resignFirstResponder];
        int count = 0;
        for (NSDictionary *dict in arrYachtCityList) {
            if ([[dict valueForKey:@"id"] intValue] == [[[arrResponse objectAtIndex:indexPath.row]valueForKey:@"id"] intValue]) {
                break;
            }else{
                count++;
            }
        }
        NSInteger countryId = [[[arrResponse objectAtIndex:indexPath.row] valueForKey:@"id"]integerValue];
        [[NSUserDefaults standardUserDefaults]setInteger:countryId forKey:@"CountryId"];
        UIStoryboard * story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SWRevealViewController *swReveal = [story instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
        [self.navigationController pushViewController:swReveal animated:true];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    for (UICollectionViewCell *cell in [collectionViewYachtList visibleCells]) {
        NSIndexPath *indexPath = [collectionViewYachtList indexPathForCell:cell];
        if (indexPath.row == [arrResponse count] -1) {
            [self loadNextPage:++self.currentPage];
        }
    }
    
    
}

- (void)loadNextPage:(int)pageNumber {
    if (!self.hasNextPage) return;
    self.isLoading = YES;
    [self ApiForPagination];
    
}

-(void)ButtonHomePressed{
    [self performSegueWithIdentifier:@"SearchYachtToHome" sender:collectionViewYachtList];
}


-(void)FilterCities:(NSString *)stringFilter{
    [self.arrResponse removeAllObjects];
    [self.arrResponse addObjectsFromArray:loadedArray];
    if (stringFilter.length>0){
        NSArray *searchWords = [stringFilter componentsSeparatedByString:@" "];
        NSMutableArray *searchResults1 = [[NSMutableArray alloc] init];
        for (NSDictionary *restDic in self.arrResponse){
            bool bAllWordsFound = NO;
            for(NSString* aWord in searchWords){
                if(![aWord isEqualToString:@""]){
                    if (([[NSString stringWithFormat:@"%@",[restDic valueForKey:@"name"]] rangeOfString:aWord options:NSCaseInsensitiveSearch].location == NSNotFound)){
                        bAllWordsFound = NO;
                        break;
                    }
                    else
                        bAllWordsFound = YES;
                }
            }
            if(bAllWordsFound)
                [searchResults1 addObject:restDic];
            NSLog(@"%@",searchResults1);
        }
        [self.arrResponse removeAllObjects];
        NSLog(@"%@",self.arrResponse);
        [self.arrResponse addObjectsFromArray:searchResults1];
        NSLog(@"%@",self.arrResponse);
        [collectionViewYachtList reloadData];
    }
}

#pragma mark - TextField Delegate Method list
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}// return NO to disallow editing.

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self FilterCities:textField.text];
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self FilterCities:textField.text];
}
- (BOOL)textField:(UITextField* )textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSUInteger newLength = [textField.text length] + [string length]- range.length;
    
    if(newLength > MAX_LENGTH)    {
        [self FilterCities:[NSString stringWithFormat:@"%@%@",textFieldSearchYacht.text,string]];
        NSLog(@"range1");
    }
    else{
        NSLog(@"range0");
        [self.arrResponse removeAllObjects];
        NSLog(@"%@",loadedArray);
        [self.arrResponse addObjectsFromArray:loadedArray];
        //[self.tableViewListOfYacht reloadData];
        [collectionViewYachtList reloadData];
    }
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    NSLog(@"range4");
    [self.arrResponse removeAllObjects];
    NSLog(@"%@",loadedArray);
    [self.arrResponse addObjectsFromArray:loadedArray];
    [self.tableViewListOfYacht reloadData];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textFieldSearchYacht resignFirstResponder];
    return  YES;
}

#pragma mark - Api For getting list of Cities
-(void)ApiForCities{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:@"Auth_token"];
        NSString *strRole = [[NSUserDefaults standardUserDefaults]valueForKey:@"role"];
        //yachts?paginate=%d&page=%d&per_page=%d
        [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@countries/",kBaseUrlVersion2] emailString:strRole accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success){
            arrResponse = [result valueForKey:@"countries"];
            loadedArray = [[NSMutableArray alloc]init];
            loadedArray = [arrResponse mutableCopy];
            arrYachtCityList= [[NSMutableArray alloc]init];
            arrYachtCityList = [arrResponse mutableCopy];
            if (arrYachtCityList.count == 0) {
                textFieldSearchYacht.hidden = true;
            }
            else{
                textFieldSearchYacht.hidden = false;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                isApi = true;
                self.isLoading = NO;
                if([[result valueForKey:@"total_pages"] integerValue] == self.currentPage){
                    self.hasNextPage = false;
                }
                else{
                    self.hasNextPage = true;
                }
                [collectionViewYachtList reloadData];
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [self AlertFOrreloadData:msg];
                [collectionViewYachtList reloadData];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqual:@"SearchYachtToHome"]){
        SWRevealViewController *controller =  (SWRevealViewController*) [segue destinationViewController];
        app.window.rootViewController = controller;
        
    }
    
}

-(void)AlertFOrreloadData:(NSString*)strMessage{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@""
                                     message:strMessage
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        //Add Buttons
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"TRY AGAIN"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        [self ApiForCities];
                                    }];
        
        //Add your buttons to alert controller
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"NO"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                   }];
        
        
        [alert addAction:noButton];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    });
}


-(void)ApiForPagination{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:@"Auth_token"];
        NSString *strRole = [[NSUserDefaults standardUserDefaults]valueForKey:@"role"];
        [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@search/yachts?paginate=%d&page=%d&per_page=%d",kBaseUrl,true,_currentPage,5] emailString:strRole accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success){
            NSMutableArray *arr = [result valueForKey:@"cities"];
            [arrResponse addObjectsFromArray:arr];
            [loadedArray addObjectsFromArray:arr];
            arrYachtCityList = [arrResponse mutableCopy];
            dispatch_async(dispatch_get_main_queue(), ^{
                // [MBProgressHUD hideHUDForView:self.view animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                isApi = true;
                self.isLoading = NO;
                if([[result valueForKey:@"total_pages"] integerValue] == self.currentPage){
                    self.hasNextPage = false;
                }
                else{
                    self.hasNextPage = true;
                }
                [collectionViewYachtList reloadData];
                textFieldSearchYacht.hidden = false;
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [collectionViewYachtList reloadData];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
