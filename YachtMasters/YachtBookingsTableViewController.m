//
//  YachtBookingsTableViewController.m
//  YachtMasters
//
//  Created by Anvesh on 24/07/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "YachtBookingsTableViewController.h"
#import "YachtImagesCollectionViewCell.h"
#import "YachtBookingListTableViewCell.h"
#import "AddYachtViewController.h"
#import "WebServiceManager.h"
#import "MBProgressHUD.h"
#import "CommonMethods.h"
#import "Constants.h"
#import "UIImageView+WebCache.h"
#import "UserProfileTVC.h"
#import "YachtMasters-Swift.h"
#import "ShowPriceViewController.h"


@interface YachtBookingsTableViewController () <UITableViewDelegate, UITableViewDataSource>{
    MBProgressHUD *hud;
    NSMutableArray * arrYachtDetail;
    IBOutlet UIPageControl *pcImageYacht;
    IBOutlet UIImageView *imageViewDropDown;
}
@property (strong, nonatomic) IBOutlet UITableView *_tableViewYachtBookingList;

@end

@implementation YachtBookingsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrYachtDetail = [[NSMutableArray alloc]init];
    [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    //[self._tableViewYachtBookingList.tableFooterView addSubview:_viewForFooter];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = true;
    self.navigationController.navigationBar.hidden = true;
    self.navigationItem.hidesBackButton = true;
    [self ApiForYachtDetail];
    buttonBack = [[UIButton alloc]init];
    buttonBack.frame = CGRectMake(15.0, 37.0, 30.0, 30.0);
    [buttonBack setImage:[UIImage imageNamed:@"back button.png"] forState:UIControlStateNormal];
    [buttonBack addTarget:self action:@selector(BackButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.view addSubview:buttonBack];
    [[CommonMethods sharedInstance]DropShadow:buttonBack UIColor:[UIColor blackColor] andShadowRadius:3.0];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear: animated];
    buttonBack.hidden = true;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)ButtonEditPressed:(id)sender {
    AddYachtViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AddYachtViewController"];
    controller.navigationController.navigationBar.hidden = false;
    controller.isEdit = true;
    controller.arrYachtDetail = arrYachtDetail;
    [self.navigationController pushViewController:controller animated:true];
}
- (IBAction)ButtonDeletePressed:(id)sender {
    BOOL isActive = [[arrYachtDetail valueForKey:@"active"] boolValue];
    NSString *strMessage;
    if (isActive) {
        strMessage = @"Your Yacht will not be visible to the Users";
    }
    else{
        strMessage = @"Your Yacht will become visible to the Users";
    }
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Alert"
                                 message:strMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    //Add Buttons
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    [self ApiToDeleteYacht];
                                }];
    //Add your buttons to alert controller
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"NO"
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    [alert addAction:yesButton];
    [alert addAction:noButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (IBAction)BackButtonPressed:(id)sender {
    self.navigationController.navigationBar.hidden = true;
    self.tabBarController.tabBar.hidden =false;
    [self.navigationController popViewControllerAnimated:true];
    //self.navigationController.navigationBar.hidden = false;
    buttonBack.hidden = true;
    self.tabBarController.tabBar.hidden =false;
    
}

#pragma mark - Button Call Action

-(void)ButtonCallPressed:(UIButton *)sender{
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",[[[[arrYachtDetail valueForKey:@"bookings" ] valueForKey:@"user"] objectAtIndex:[sender tag]] valueForKey:@"phone_number"]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        [[CommonMethods sharedInstance]AlertMessage:@"Call facility is not available!!!" andViewController:self];

    }
    
}

-(void)ApiToDeleteYacht{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        BOOL isActive = [[arrYachtDetail valueForKey:@"active"] boolValue];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:[arrYachtDetail valueForKey:@"id"] forKey:@"id"];
        [dict setObject:[NSNumber numberWithBool:!isActive] forKeyedSubscript:@"active"];
        NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
        [dictResponse setObject:dict forKey:@"yachts"];
        [WebServiceManager putRequestWithUrlString:[NSString stringWithFormat:@"%@yachts",kBaseUrl] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{

                [[LoaderNew sharedLoader]hideLoader];
                [self ApiForYachtDetail];
            });

        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
        });
        
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}



-(void)ApiForYachtDetail{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        [WebServiceManager getRequestUrlString:[NSString stringWithFormat:@"%@yachts/%@",kBaseUrl,_strID] emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            arrYachtDetail = [result valueForKey:@"yacht"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[LoaderNew sharedLoader]hideLoader];
                buttonDelete.userInteractionEnabled = true;
                buttonEdit.userInteractionEnabled = true;
                NSString *strPrice =  [[CommonMethods sharedInstance] priceFormatting:[[[arrYachtDetail valueForKey:@"price"]valueForKey:@"price"]stringValue]];
                NSString *strCurrencySymbol = [arrYachtDetail valueForKey:@"currency_symbol"];
                NSString *strTerm = [[arrYachtDetail valueForKey:@"price"] valueForKey:@"term"];
                if ([strTerm isEqualToString:@"per_day"]){
                    strTerm = @"/Full Day";
                }
                else if ([strTerm isEqualToString:@"half_day"]){
                    strTerm = @"/Half Day";
                }
                else if ([strTerm isEqualToString:@"per_hour"]){
                    strTerm = @"/Hour";
                }
                else if ([strTerm isEqualToString:@"week"]){
                    strTerm = @"/Week";
                }
                NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@%@%@",strCurrencySymbol,strPrice,strTerm]];
                NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:attrText];
                strPrice  = [strPrice stringByAppendingString:strCurrencySymbol];
                NSRange range = [strPrice rangeOfString:strPrice];
                [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:FontOpenSansSemiBold(12)} range:range];
                [self.btnPrice setAttributedTitle:attributedText forState:UIControlStateNormal];
                [self.btnPrice addTarget:self action:@selector(buttonPricePressed:) forControlEvents:UIControlEventTouchUpInside];
                _labelYachtName.text = [arrYachtDetail valueForKey:@"name"];
                BOOL isActive = [[arrYachtDetail valueForKey:@"active"] boolValue];
                if (isActive) {
                    [buttonDelete setTitle:@"Hide" forState:UIControlStateNormal];
                }
                else{
                    [buttonDelete setTitle:@"Show" forState:UIControlStateNormal];
                }
                 pcImageYacht.numberOfPages = [[arrYachtDetail valueForKey:@"yacht_images"] count];
                [_collectionViewYachtImage reloadData];
                [self.tableView reloadData];
            });
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                buttonDelete.hidden = true;
                buttonEdit.hidden = true;
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
            
        });
        
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
}


- (IBAction)buttonPricePressed:(id)sender{
    ShowPriceViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowPriceViewController"];
    [self addChildViewController:controller];
    controller.view.frame = CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:controller.view];
    [controller didMoveToParentViewController:self];
    controller.viewBottom.frame = CGRectMake(0.0, self.view.frame.size.width + 270, self.view.frame.size.width, 270);
    controller.arrPrices = [arrYachtDetail valueForKey:@"prices"];
    controller.strCurrencySymbol = [arrYachtDetail valueForKey:@"currency_symbol"];
    [UIView animateWithDuration:0.4
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         controller.viewBottom.frame = CGRectMake(0.0, self.view.frame.size.width - 270, self.view.frame.size.width, 270);
                     } completion:^(BOOL finished){
                         

                     }];
}

#pragma mark - collectionView Delegate and datasource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [[arrYachtDetail valueForKey:@"yacht_images"] count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    YachtImagesCollectionViewCell *cell = [_collectionViewYachtImage dequeueReusableCellWithReuseIdentifier:@"YachtImagesCollectionViewCell" forIndexPath:indexPath];
    if (cell == nil)    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"YachtImageCollectionViewCell" owner:self options:nil];
        cell = nibArray[0];
    }
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[[arrYachtDetail valueForKey:@"yacht_images"]objectAtIndex:indexPath.row]valueForKey:@"image_url"]];
    NSURL *url = [NSURL URLWithString:strUrl];
    [cell.imageYacht sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"PlaceHolder_yachtOwner@2x.png"]];
    //  pageControlImage.currentPage = indexPath.row;
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake([[UIScreen mainScreen] bounds].size.width, _collectionViewYachtImage.frame.size.height);
    
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    if (arrYachtDetail.count > 0) {
//        if ([[arrYachtDetail valueForKey:@"bookings"] count]>0){
//            return 2;
//        }
//        else{
//            return 1;
//        }
//    }
//    else{
//        return 0;
//    }
//    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        return 0;
    }
    else if (section == 1){
        if ([[arrYachtDetail valueForKey:@"bookings"] count]>0) {
            return [[arrYachtDetail valueForKey:@"bookings"] count];
        }
        else{
            return 0;
        }
        //  return 5;
    }
    else{
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        return _ViewForHeader;
    }
    else if (section == 1){
        if ([[arrYachtDetail valueForKey:@"bookings"] count]>0){
        UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0.0, -20.0, SCREEN_WIDTH, 64)];
            headerView.backgroundColor = [UIColor whiteColor];
        UILabel *label = [[UILabel alloc]init];
        label.frame = CGRectMake(30.0, 10.0, SCREEN_WIDTH - 60, 24);
        [headerView addSubview:label];
        label.text = @"CONFIRMED BOOKINGS";
        label.font = FontOpenSans(12);
        label.textColor = [UIColor grayColor];
        return headerView;
        }
        else{
            return 0;
        }
    }
    else{
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        if (IS_IPHONE_5) {
            return 375;
        }
        else if (IS_IPHONE_6){
            return 425;
        }
        else if (IS_IPHONE_6P){
            return 500;
        }
        else if(IS_IPHONE_X){
            return 425;
        }
        else{
            return 375;
        }
    }
    else if (section == 1){
        if ([[arrYachtDetail valueForKey:@"bookings"] count]>0){
            return 44;
        }
        else{
            return 0;
        }
    }
    else{
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"YachtBookingListTableViewCell";
    YachtBookingListTableViewCell *cell = [__tableViewYachtBookingList dequeueReusableCellWithIdentifier:@"YachtBookingListTableViewCell"];
    if (cell == nil) {
        NSArray *nib =[[NSBundle mainBundle]loadNibNamed:simpleTableIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[[[arrYachtDetail valueForKey:@"bookings"]objectAtIndex:indexPath.row]valueForKey:@"user"] valueForKey:@"image"]];
    NSURL *url = [NSURL URLWithString:strUrl];
    [cell.imageViewUser sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"PlaceHolder_yachtOwner@2x.png"]];
    cell.labelYachtName.text = [[[[arrYachtDetail valueForKey:@"bookings"]objectAtIndex:indexPath.row] valueForKey:@"user"] valueForKey:@"name"];
    NSString *strStartDate = [[[arrYachtDetail valueForKey:@"bookings"] objectAtIndex:indexPath.row] valueForKey:@"start_date"];
    NSString *strStartTime = [[[arrYachtDetail valueForKey:@"bookings"] objectAtIndex:indexPath.row] valueForKey:@"start_time"];

    
    NSString *strTimeDuration = [[[arrYachtDetail valueForKey:@"bookings"]objectAtIndex:indexPath.row]valueForKey:@"term"];
    int hours = [[[[arrYachtDetail valueForKey:@"bookings"]objectAtIndex:indexPath.row]valueForKey:@"hours"] intValue];
    strStartTime = [self FormattedTime:strStartTime];
    strStartDate = [self FormattedDate:strStartDate];
    cell.labelBookingDate.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Booking Date: %@",strStartDate] andRangeString:@"Booking Date:"];
    cell.labelDuration.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Booking Time: %@",strStartTime] andRangeString:@"Booking Time:"];
    if ([strTimeDuration isEqualToString:@"per_day"] || [strTimeDuration isEqualToString:@"half_day"]){
        cell.labelTimeDuration.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Duration: %@ (%d hours)",[self timeDurationforBookings:strTimeDuration],hours] andRangeString:@"Duration:"];
    }
    else{
        cell.labelTimeDuration.attributedText = [self AttributedTextForOwner:[NSString stringWithFormat:@"Duration: %@",[self timeDurationforBookings:strTimeDuration]] andRangeString:@"Duration:"];
    }
    NSString *strPrice = [NSString stringWithFormat:@"%@",[[[arrYachtDetail valueForKey:@"bookings"]objectAtIndex:indexPath.row]valueForKey:@"booking_price"]];
    NSString *strCurrency = [[[arrYachtDetail valueForKey:@"bookings"]objectAtIndex:indexPath.row]valueForKey:@"booking_currency_symbol"];
//    NSString *strUrl = [[[self.arrNewBookings objectAtIndex:indexPath.section]valueForKey:@"current_user"] valueForKey:@"image"];
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseUrlImage,strUrl]];
   // [cell.imageViewUser sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    cell.labelAmount.attributedText = [self PlainStringToAttributedStringForPrice:strPrice andCurrency:strCurrency];
    cell.buttonCall.tag = indexPath.row;
    cell.buttonMessage.tag = indexPath.row;
    [cell.buttonCall addTarget:self action:@selector(ButtonCallPressed:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 130;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }
    else{
        return 68;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 1) {
        [[CommonMethods sharedInstance]setButtonShadow:buttonEdit andShadowcolor:[UIColor colorWithRed:57.0/255.0 green:130.0/255.0 blue:240.0/255.0 alpha:1.0]];
        [[CommonMethods sharedInstance]setButtonShadow:buttonDelete andShadowcolor:[UIColor colorWithRed:220.0/255.0 green:67.0/255.0 blue:72.0/255.0 alpha:1.0]];
        // [[CommonMethods sharedInstance]DropShadow:buttonEdit UIColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Delete_Button@2x.png"]] andShadowRadius:4.0];
        return _viewForFooter;
    }
    else
        return nil;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UserProfileTVC *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileTVC"];
    controller.strId = [[[[arrYachtDetail valueForKey:@"bookings"]objectAtIndex:indexPath.row]valueForKey:@"user"] valueForKey:@"id"];
    [self.navigationController pushViewController:controller animated:true];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (self.collectionViewYachtImage) {
        CGFloat pageWidth = self.collectionViewYachtImage.frame.size.width;
        float currentPage = self.collectionViewYachtImage.contentOffset.x / pageWidth;
        
        if (0.0f != fmodf(currentPage, 1.0f))
        {
            pcImageYacht.currentPage = currentPage + 1;
        }
        else
        {
            pcImageYacht.currentPage = currentPage;
        }
        NSLog(@"finishPage: %ld", (long)pcImageYacht.currentPage);
    }
}


-(NSString *)FormattedDate:(NSString *)startDate{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *strDate = [formatter dateFromString:startDate];
    [formatter setDateFormat:@"MMM dd yyyy"];
    startDate = [formatter stringFromDate:strDate];
    return startDate;
}
-(NSString *)FormattedTime:(NSString *)startTime{
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setTimeZone:sourceTimeZone];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *strTime = [formatter dateFromString:startTime];
    [formatter setDateFormat:@"hh:mm a"];
    startTime = [formatter stringFromDate:strTime];
    
    return startTime;
}
-(NSAttributedString *)AttributedTextForOwner:(NSString *)stringToShow andRangeString:(NSString *)boldString{
    
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor lightGrayColor],
                              NSFontAttributeName: FontOpenSans(12)
                              };
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:stringToShow attributes:attribs];
    
    
    UIFont *boldFont = FontOpenSansBold(12);
    
    NSRange range = [stringToShow rangeOfString:boldString];
    [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                    NSFontAttributeName:boldFont} range:range];
    return  attributedText;
}
-(NSString *)timeDurationforBookings:(NSString *)strDuration{
    NSString *str;
    
    if ([strDuration isEqualToString:@"per_day"]){
        str = @"Full Day";
    }
    else if ([strDuration isEqualToString:@"half_day"]){
        str = @"Half Day";
    }
    else if ([strDuration isEqualToString:@"per_hour"]){
        str = @"Hour";
    }
    else if([strDuration isEqualToString:@"week"]) {
        str = @"Week";
    }
    else{
        str = @"";
    }
    return str;
    
}
-(NSMutableAttributedString *)PlainStringToAttributedStringForPrice:(NSString *)strPrice andCurrency:(NSString *)strCurrency{
    
    // NSString *strPrice = [[[[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"yacht"]valueForKey:@"price"] stringValue];
    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@%@",strCurrency,strPrice]];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:attrText];
    strPrice  = [strPrice stringByAppendingString:strCurrency];
    NSRange range = [strPrice rangeOfString:strPrice];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:FontOpenSansSemiBold(12)} range:range];
    return attributedText;
}
/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
