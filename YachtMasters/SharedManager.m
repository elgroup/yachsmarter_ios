//
//  SharedManager.m
//  GonzApp
//
//  Created by Chandresh on 8/2/16.
//  Copyright © 2016 Chandresh. All rights reserved.
//

#import "SharedManager.h"

static SharedManager *sharedManager;

@implementation SharedManager

@synthesize isNetAvailable = _isNetAvailable;

@synthesize userDefaults =_userDefaults;
@synthesize deviceToken;
@synthesize deviceType=_deviceType;
@synthesize latitude=_latitude;
@synthesize longitude=_longitude;

+(SharedManager *)sharedInstance
{
    if(sharedManager == nil)
    {
        sharedManager = [[SharedManager alloc] init];
        
        sharedManager.userDefaults = [NSUserDefaults standardUserDefaults];
        Reachability *reach = [Reachability reachabilityForInternetConnection];
        NetworkStatus internetStatus = [reach currentReachabilityStatus];
      
        
        if(internetStatus == NotReachable)
        {
            NSLog(@"Internet Disconnected");
            sharedManager.isNetAvailable = NO;  // Internet not Connected
        }
        else if (internetStatus == ReachableViaWiFi)
        {
            NSLog(@"Connected via WIFI");
            sharedManager.isNetAvailable = YES; // Connected via WIFI
        }
        else if (internetStatus == ReachableViaWWAN)
        {
            NSLog(@"Connected via WWAN");
            sharedManager.isNetAvailable = YES; // Connected via WWAN
        }
        if (sharedManager.deviceToken.length <= 0) {
            sharedManager.deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"];
        }
    }
    return sharedManager;
}


@end
