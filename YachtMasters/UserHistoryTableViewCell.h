//
//  UserHistoryTableViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 28/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserHistoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewBackGround;
@property (weak, nonatomic) IBOutlet UIView *viewROund;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewRound;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelMonth;
@property (weak, nonatomic) IBOutlet UILabel *labelYachtName;
@property (weak, nonatomic) IBOutlet UILabel *labelYachAddress;
@property (weak, nonatomic) IBOutlet UILabel *labelYachtPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelCommentForYacht;
@property (weak, nonatomic) IBOutlet UIButton *buttonBookNow;
@property (weak, nonatomic) IBOutlet UIButton *buttonBookAgain;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;

@end
