//
//  OwnerDetailTVC.m
//  YachtMasters
//
//  Created by Anvesh on 27/07/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "OwnerDetailTVC.h"
#import "CommonMethods.h"
#import "Constants.h"
#import "WebServiceManager.h"
#import "MBProgressHUD.h"
#import "OwnerYachtCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "ChatViewController.h"
#import "AppDelegate.h"
#import "YachtMasters-Swift.h"


@interface OwnerDetailTVC ()<UICollectionViewDelegate,UICollectionViewDataSource>{
    NSString *strText;
    // CGSize Height;
    BOOL isReadMore;
    NSMutableArray *arrYacht;
    NSMutableArray *arrResponse;
    CGSize heightText;
    MBProgressHUD *hud;
}

@end

@implementation OwnerDetailTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    strText = [[NSString alloc]init];
    arrYacht = [[NSMutableArray alloc]init];
    arrResponse = [[NSMutableArray alloc]init];
    [[CommonMethods sharedInstance]CornerRadius:buttonFollow Radius:buttonFollow.frame.size.height/2 BorderWidth:1.0 andColorForBorder:[UIColor colorWithRed:41.0/255.0 green:141.0/255.0 blue:241.0/255.0 alpha:1.0]];
    
    UINib *cellNib1 = [UINib nibWithNibName:@"OwnerYachtCollectionViewCell" bundle:nil];
    [collectionViewYachts registerNib:cellNib1 forCellWithReuseIdentifier:@"OwnerYachtCollectionViewCell"];
    [[CommonMethods sharedInstance]DropShadow:buttonBack UIColor:[UIColor blackColor] andShadowRadius:5.0];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.hidesBottomBarWhenPushed = true;
    self.tabBarController.tabBar.hidden = true;
    self.navigationItem.leftBarButtonItem=nil;
    self.navigationItem.hidesBackButton=YES;
    self.navigationController.navigationBar.hidden = true;
    [self ApiForOwnerProfile];
}

#pragma mark - Button Clicks
// Button Back Action Method
- (IBAction)ButtonBackPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

// Button Read More Method
- (IBAction)ButtonReadMorePressed:(UIButton *)sender {
    
    //sender.selected = !sender.selected;
    if (isReadMore) {
        isReadMore = false;
    }
    else{
    isReadMore = true;
    }
    [self.tableView reloadData];
    
}

// Button Call Method
- (IBAction)ButtonCallPressed:(id)sender {
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",[arrResponse valueForKey:@"phone_number"]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl options:@{} completionHandler:nil];
//        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        [[CommonMethods sharedInstance]AlertMessage:@"Call facility is not available!!!" andViewController:self];
        //calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        // [calert show];
    }
    
}

//Button Message method
- (IBAction)ButtonMessagePressed:(id)sender {
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
    NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
    NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
    NSString *strOwnerId = [arrResponse  valueForKey:@"id"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:strOwnerId forKey:@"recipient_id"];
    NSMutableDictionary *dictResponse = [[NSMutableDictionary alloc]init];
    [dictResponse setObject:dict forKey:@"conversations"];
    [WebServiceManager postRequestWithUrlString:[NSString stringWithFormat:@"%@conversations",kBaseUrl] withPostString:dictResponse emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
        NSLog(@"%@",result);
        AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.socket joinChannel:@"ConversationChannel"];
        ChatViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
        controller.arrCurrentUser = [arrResponse valueForKey:@"current_user"];
        controller.arrConversations = [[result valueForKey:@"conversation"] mutableCopy];
        [self.navigationController pushViewController:controller animated:true];
        
    } failure:^(NSString *msg, BOOL success) {
        
    }];
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
    
}

//Button Follow Action
- (IBAction)ButtonFollowPressed:(id)sender {
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        buttonFollow.userInteractionEnabled = false;
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        if (buttonFollow.selected) {
            NSString *strOwnerId = [arrResponse  valueForKey:@"id"];
            [WebServiceManager deleteRequestWithUrlString:[NSString stringWithFormat:@"%@follows/%@",kBaseUrl,strOwnerId] emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    buttonFollow.selected = false;
                    buttonFollow.userInteractionEnabled = true;
                });
            } failure:^(NSString *msg, BOOL success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    buttonFollow.userInteractionEnabled = true;
                });
            }];
        }
        else{
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:[arrResponse valueForKey:@"id"] forKey:@"owner_id"];
            [WebServiceManager postRequestWithUrlString:[NSString stringWithFormat:@"%@follows",kBaseUrl] withPostString:dict emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    buttonFollow.userInteractionEnabled = true;
                    buttonFollow.selected = true;
                });
            } failure:^(NSString *msg, BOOL success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    buttonFollow.userInteractionEnabled = true;
                });
            }];
        }
    }
    else{
        [[CommonMethods sharedInstance]AlertMessage:NoInternet andViewController:self];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Collection view data source && Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (arrYacht.count >0) {
        return arrYacht.count;
    }else{
        return 0;
    }
    
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    OwnerYachtCollectionViewCell *cell = [collectionViewYachts dequeueReusableCellWithReuseIdentifier:@"OwnerYachtCollectionViewCell" forIndexPath:indexPath];
    if (cell == nil)    {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"OwnerYachtCollectionViewCell" owner:self options:nil];
        cell = nibArray[0];
    }
    NSString *strPrice = [[arrYacht valueForKey:@"price"] objectAtIndex:indexPath.row];
    NSString *strCurrency = [[arrYacht valueForKey:@"currency_symbol"]objectAtIndex:indexPath.row];
    cell.labelPrice.attributedText = [self PlainStringToAttributedStringForPrice:strPrice andCurrency:strCurrency];
    cell.buttonFavourite.tag = indexPath.row;
   /// [cell.buttonFavourite addTarget:self action:@selector(favoriteTheYacht:) forControlEvents:UIControlEventTouchUpInside];
    cell.labelYachtName.text = [[arrYacht objectAtIndex:indexPath.row]valueForKey:@"name"];
    cell.labelNumberOfReviews.text =[NSString stringWithFormat:@"(%@)",[[[arrYacht objectAtIndex:indexPath.row]valueForKey:@"rating"] stringValue]] ;
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[[arrYacht objectAtIndex:indexPath.row] valueForKey:@"image"]];
    NSURL *url = [NSURL URLWithString:strUrl];
    [cell.imageViewYacht sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"PlaceHolder_yachtOwner@2x"]];
    NSInteger intRating = [[[arrYacht objectAtIndex:indexPath.row] valueForKey:@"rating"] integerValue];
    switch (intRating) {
        case 0:
            cell.imageViewStar1.image = [UIImage imageNamed:@"star outline.png"];
            cell.imageViewStar2.image = [UIImage imageNamed:@"star outline.png"];
            cell.imageViewStar3.image = [UIImage imageNamed:@"star outline.png"];
            cell.imageViewStar4.image = [UIImage imageNamed:@"star outline.png"];
            cell.imageViewStar5.image = [UIImage imageNamed:@"star outline.png"];
            break;
        case 1:
            cell.imageViewStar1.image = [UIImage imageNamed:@"star@2x.png"];
            cell.imageViewStar2.image = [UIImage imageNamed:@"star outline.png"];
            cell.imageViewStar3.image = [UIImage imageNamed:@"star outline.png"];
            cell.imageViewStar4.image = [UIImage imageNamed:@"star outline.png"];
            cell.imageViewStar5.image = [UIImage imageNamed:@"star outline.png"];
            break;
        case 2:
            cell.imageViewStar1.image = [UIImage imageNamed:@"star@2x.png"];
            cell.imageViewStar2.image = [UIImage imageNamed:@"star@2x.png"];
            cell.imageViewStar3.image = [UIImage imageNamed:@"star outline.png"];
            cell.imageViewStar4.image = [UIImage imageNamed:@"star outline.png"];
            cell.imageViewStar5.image = [UIImage imageNamed:@"star outline.png"];
            break;
        case 3:
            cell.imageViewStar1.image = [UIImage imageNamed:@"star@2x.png"];
            cell.imageViewStar2.image = [UIImage imageNamed:@"star@2x.png"];
            cell.imageViewStar3.image = [UIImage imageNamed:@"star@2x.png"];
            cell.imageViewStar4.image = [UIImage imageNamed:@"star outline.png"];
            cell.imageViewStar5.image = [UIImage imageNamed:@"star outline.png"];
            break;
            
        case 4:
            cell.imageViewStar1.image = [UIImage imageNamed:@"star@2x.png"];
            cell.imageViewStar2.image = [UIImage imageNamed:@"star@2x.png"];
            cell.imageViewStar3.image = [UIImage imageNamed:@"star@2x.png"];
            cell.imageViewStar4.image = [UIImage imageNamed:@"star@2x.png"];
            cell.imageViewStar5.image = [UIImage imageNamed:@"star outline.png"];
            break;
        case 5:
            cell.imageViewStar1.image = [UIImage imageNamed:@"star@2x.png"];
            cell.imageViewStar2.image = [UIImage imageNamed:@"star@2x.png"];
            cell.imageViewStar3.image = [UIImage imageNamed:@"star@2x.png"];
            cell.imageViewStar4.image = [UIImage imageNamed:@"star@2x.png"];
            cell.imageViewStar5.image = [UIImage imageNamed:@"star@2x.png"];
            break;
            
        default:
            break;
    }
    
    
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(302.0,250.0);
    // return CGSizeMake(100, 60);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    ///#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //#warning Incomplete implementation, return the number of rows
    if (arrResponse.count > 0) {
        return 4;
    }
    else{
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 1) {
        
        if (isReadMore){
            //(buttonReadMore.selected) {
            buttonReadMore.selected = true;
            NSLog(@"bigger");
            labelDescription.numberOfLines = 0;
            CGSize Height = [[CommonMethods sharedInstance]findHeightForText:strText havingWidth:SCREEN_WIDTH - 75 andFont:FontOpenSans(13)];
            labelDescription.text = @"";
            labelDescription.text = [arrResponse valueForKey:@"about"];
            buttonReadMore.hidden = false;
            return 90 + Height.height -30;
        }
        else {
            labelDescription.numberOfLines = 0;
            if (heightText.height > 80.0) {
                buttonReadMore.selected = false;
                buttonReadMore.hidden = true;
                return 90;
            }
            else{
                return heightText.height+10;
            }
            
        }
    }
    else{
        if (!isReadMore) {
             [self addReadMoreStringToUILabel:labelDescription];
        }
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - Favorite the Yacht

-(void)favoriteTheYacht:(id)sender{
    
}


#pragma mark - attributed string for price
-(NSMutableAttributedString *)PlainStringToAttributedStringForPrice:(NSString *)strPrice andCurrency:(NSString *)strCurrency{
    
    // NSString *strPrice = [[[[[dictBookings valueForKey:@"active"]objectAtIndex:indexPath.row]valueForKey:@"yacht"]valueForKey:@"price"] stringValue];
    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@%@/n",strCurrency,strPrice]];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithAttributedString:attrText];
    strPrice  = [strPrice stringByAppendingString:strCurrency];
    NSRange range = [strPrice rangeOfString:strPrice];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:FontOpenSansSemiBold(12)} range:range];
    return attributedText;
}
#pragma mark - Api For Owner profile
-(void)ApiForOwnerProfile{
    if ([[CommonMethods sharedInstance]checkInternetConnection]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
        
        NSString *strUrl =[NSString stringWithFormat:@"%@users/get_profile/%@?role=%@",kBaseUrl,_strId,Owner];
        if (self.lat && self.lang) {
            strUrl = [NSString stringWithFormat:@"%@&latitude=%f&longitude=%f",strUrl,self.lat,self.lang];
        }
        
        [WebServiceManager getRequestUrlString:strUrl emailString:str accessToken:strAuthToken completionSuccess:^(NSDictionary *result, BOOL success) {
            arrResponse = [result valueForKey:@"user"];
            strText = [arrResponse valueForKey:@"about"];
//            NSMutableAttributedString* attrString = [[NSMutableAttributedString  alloc] initWithString:strText];
//            NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
//            [style setLineSpacing:4];
//            [attrString addAttribute:NSParagraphStyleAttributeName
//                               value:style
//                               range:NSMakeRange(0, strText.length)];
            labelDescription.text = strText;
            [self addReadMoreStringToUILabel:labelDescription];

            
            NSString *strUrl = [NSString stringWithFormat:@"%@%@",kBaseUrlImage,[arrResponse valueForKey:@"image"]];
            NSURL *url = [NSURL URLWithString:strUrl];
            [imageViewProfilePicOwner sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            if ([[arrResponse valueForKey:@"is_following"] boolValue]) {
                [buttonFollow setTitle:@"Following" forState:UIControlStateNormal];
                buttonFollow.selected = true;
            }
            else{
                [buttonFollow setTitle:@"Follow" forState:UIControlStateNormal];
                buttonFollow.selected = false;
            }
            NSInteger intReviewCount = [[arrResponse valueForKey:@"reviews_count"] integerValue];
            //labelReview.text = [NSString stringWithFormat:@"Ratings",intReviewCount];
            NSInteger intRating = [[arrResponse valueForKey:@"avg_rating"] integerValue];
            [[CommonMethods sharedInstance]RatingAndStar1:imageViewStar1 Star2:imageViewStar2 Star3:imageViewStar3 Star4:imageViewStar4 Star5:imageViewStar5 andRating:intRating];
            
            labelOwnerName.text = [arrResponse valueForKey:@"name"];
            labelAddress.text =  [NSString stringWithFormat:@"%@",[arrResponse valueForKey:@"address"] ];
            arrYacht = [arrResponse valueForKey:@"yachts"];
            heightText = [[CommonMethods sharedInstance]findHeightForText:strText havingWidth:SCREEN_WIDTH - 75 andFont:FontOpenSans(13)];
            if (heightText.height > 80.0) {
              //  buttonReadMore.hidden = false;
            }
            else{
                buttonReadMore.hidden = true;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [collectionViewYachts reloadData];
            });
            
            
            [self.tableView reloadData];
        } failure:^(NSString *msg, BOOL success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[MBProgressHUD hideHUDForView:self.view.window animated:YES];
                [[LoaderNew sharedLoader]hideLoader];
                [[CommonMethods sharedInstance]AlertMessage:msg andViewController:self];
            });
            
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
//            hud = [MBProgressHUD showHUDAddedTo:self.view.window animated:YES];
//            hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
            [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
            
        });
    }
}

- (void)addReadMoreStringToUILabel:(UILabel*)label
{
    NSString *readMoreText = @" ...Read More";
    NSInteger lengthForString = label.text.length;
    if (lengthForString >= 50)
    {
        NSInteger lengthForVisibleString = [self fitString:label.text intoLabel:label];
        NSMutableString *mutableString = [[NSMutableString alloc] initWithString:label.text];
        NSString *trimmedString = [mutableString stringByReplacingCharactersInRange:NSMakeRange(lengthForVisibleString, (label.text.length - lengthForVisibleString)) withString:@""];
        NSInteger readMoreLength = readMoreText.length;
        NSString *trimmedForReadMore = [trimmedString stringByReplacingCharactersInRange:NSMakeRange((trimmedString.length - readMoreLength), readMoreLength) withString:@""];
        NSMutableAttributedString *answerAttributed = [[NSMutableAttributedString alloc] initWithString:trimmedForReadMore attributes:@{
                                                                                                                                        NSFontAttributeName : label.font
                                                                                                                                        }];
        
        NSMutableAttributedString *readMoreAttributed = [[NSMutableAttributedString alloc] initWithString:readMoreText attributes:@{NSFontAttributeName : FontOpenSans(13),NSForegroundColorAttributeName : [UIColor colorWithRed:93.0/255.0 green:193.0/255.0 blue:241.0/255.0 alpha:1.0] }];
        
        [answerAttributed appendAttributedString:readMoreAttributed];
        label.attributedText = answerAttributed;
        
        UITapGestureRecognizer *readMoreGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ButtonReadMorePressed:)];
        //        readMoreGesture.tag = 1;
        readMoreGesture.numberOfTapsRequired = 1;
        [label addGestureRecognizer:readMoreGesture];
        
        label.userInteractionEnabled = YES;
    }
    else {
        
        NSLog(@"No need for 'Read More'...");
        
    }
}

- (NSUInteger)fitString:(NSString *)string intoLabel:(UILabel *)label
{
    UIFont *font           = label.font;
    NSLineBreakMode mode   = label.lineBreakMode;
    
    CGFloat labelWidth     = label.frame.size.width;
    CGFloat labelHeight    = label.frame.size.height;
    CGSize  sizeConstraint = CGSizeMake(labelWidth, CGFLOAT_MAX);
    
    //    if (SYSTEM_VERSION_GREATER_THAN(iOS_7))
    //    {
    NSDictionary *attributes = @{ NSFontAttributeName : font };
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:string attributes:attributes];
    CGRect boundingRect = [attributedText boundingRectWithSize:sizeConstraint options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    {
        if (boundingRect.size.height > labelHeight)
        {
            NSUInteger index = 0;
            NSUInteger prev;
            NSCharacterSet *characterSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
            
            do
            {
                prev = index;
                if (mode == NSLineBreakByCharWrapping)
                    index++;
                else
                    index = [string rangeOfCharacterFromSet:characterSet options:0 range:NSMakeRange(index + 1, [string length] - index - 1)].location;
            }
            
            while (index != NSNotFound && index < [string length] && [[string substringToIndex:index] boundingRectWithSize:sizeConstraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.height <= labelHeight);
            
            return prev;
        }
    }
    //    }
    //    else
    //    {
    //        if ([string sizeWithFont:font constrainedToSize:sizeConstraint lineBreakMode:mode].height > labelHeight)
    //        {
    //            NSUInteger index = 0;
    //            NSUInteger prev;
    //            NSCharacterSet *characterSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    //
    //            do
    //            {
    //                prev = index;
    //                if (mode == NSLineBreakByCharWrapping)
    //                    index++;
    //                else
    //                    index = [string rangeOfCharacterFromSet:characterSet options:0 range:NSMakeRange(index + 1, [string length] - index - 1)].location;
    //            }
    //
    //            while (index != NSNotFound && index < [string length] && [[string substringToIndex:index] sizeWithFont:font constrainedToSize:sizeConstraint lineBreakMode:mode].height <= labelHeight);
    //
    //            return prev;
    //        }
    //    }
    
    return [string length];
}


@end
