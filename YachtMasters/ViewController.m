//
//  ViewController.m
//  YachtMasters
//
//  Created by Anvesh on 06/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "ViewController.h"
#import "FRHyperLabel.h"
#import "OTPViewController.h"
#import "Constants.h"
#import "WebServiceManager.h"
#import "CommonMethods.h"
#import "RPFloatingPlaceholderTextField.h"
#import "MBProgressHUD.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "Terms&ConditionTVC.h"
#import "YachtMasters-Swift.h"
#import "PrivacyPolicyViewController.h"

@interface ViewController ()<UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,MBProgressHUDDelegate>{
    BOOL isPickerDisplay;
    UIToolbar *toolBar;
    BOOL isTermsAndCondition;
    __weak IBOutlet RPFloatingPlaceholderTextField *textFieldPhoneNumber;
    MBProgressHUD *hud;
    AppDelegate *appDelegate;
    NSArray *arrayCountryCode;
    IBOutlet NSLayoutConstraint *lcHeightForButtonverify;
    IBOutlet UIButton *buttonTerms;
}
@property (weak, nonatomic) IBOutlet UIView *view3;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;
@property (weak, nonatomic) IBOutlet FRHyperLabel *labeltermsAndConditions;
@property (weak, nonatomic) IBOutlet UIButton *buttonVerify;
@property (weak, nonatomic) IBOutlet UIButton *buttonCountryCode;
@property (strong, nonatomic) UIPickerView *pickerCountryCode;
@property (weak, nonatomic) IBOutlet UIButton *buttonTermsAndCondition;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = true;
    self.navigationItem.leftBarButtonItem=nil;
    self.navigationItem.hidesBackButton=YES;
    //self.navigationController.navigationItem.hidesBackButton = true;
    self.view1.layer.cornerRadius = 18;
    self.view1.clipsToBounds = true;
    self.label1.layer.cornerRadius =13;
    self.label1.clipsToBounds = true;
    
    self.view2.layer.cornerRadius = 14;
    self.view2.clipsToBounds = true;
    self.label2.layer.cornerRadius = 13;
    self.label2.clipsToBounds = true;
    
    self.view3.layer.cornerRadius = 14;
    self.view3.clipsToBounds = true;
    self.label3.layer.cornerRadius = 13;
    self.label3.clipsToBounds = true;
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"gbpic"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    UIColor *color = [UIColor whiteColor];
    textFieldPhoneNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Phone Number" attributes:@{NSForegroundColorAttributeName: color}];
    
    
    NSDictionary * linkAttributes = @{NSForegroundColorAttributeName:buttonTerms.titleLabel.textColor, NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)};
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:buttonTerms.titleLabel.text attributes:linkAttributes];
    [buttonTerms.titleLabel setAttributedText:attributedString];
    
    self.buttonVerify.layer.cornerRadius = 22.5;
    self.buttonVerify.clipsToBounds = true;
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"generated" ofType:@"json"];
    NSData *content = [[NSData alloc] initWithContentsOfFile:filePath];
    NSDictionary *dictCountryCode = [NSJSONSerialization JSONObjectWithData:content options:kNilOptions error:nil];
    arrayCountryCode = [dictCountryCode valueForKey:@"countries"];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonClicked:)];
    UIToolbar *toolBarPhoneNumber = [[UIToolbar alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-200, self.view.frame.size.width, 44)];
    [toolBarPhoneNumber setBarStyle:UIBarStyleBlackOpaque];
    NSArray *toolbarItems = [NSArray arrayWithObjects:doneButton, nil];
    [toolBarPhoneNumber setItems:toolbarItems];
    doneButton.tintColor = [UIColor lightGrayColor];
    textFieldPhoneNumber.inputAccessoryView = toolBarPhoneNumber;
    
    
    toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0.0, self.view.frame.size.height, self.view.frame.size.width, 50)];
    [toolBar setBarStyle:UIBarStyleBlackOpaque];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(DoneBtnClicked:)];
    toolBar.items = @[barButtonDone];
    barButtonDone.tintColor=[UIColor lightGrayColor];
    [self.view addSubview:toolBar];
    _pickerCountryCode = [[UIPickerView alloc]initWithFrame:CGRectMake(0.0, [[UIScreen mainScreen] bounds].size.height+50, [[UIScreen mainScreen]bounds].size.width,190.0)];

    if(IS_IPHONE_X || IS_IPHONE_6P){
        _pickerCountryCode = [[UIPickerView alloc]initWithFrame:CGRectMake(0.0, [[UIScreen mainScreen] bounds].size.height+50, [[UIScreen mainScreen]bounds].size.width,220.0)];
    }
    [self.view addSubview:_pickerCountryCode];
    _pickerCountryCode.backgroundColor = [UIColor lightGrayColor];
    _pickerCountryCode.delegate = self;
    _pickerCountryCode.dataSource = self;
}

#pragma mark - Verify Button Action

- (IBAction)ButtonVerifyClicked:(id)sender {
    if(textFieldPhoneNumber.text.length == 0){
        [[CommonMethods sharedInstance]AlertMessage:@"Please Enter the Phone Number" andViewController:self];
    }
    else if (textFieldPhoneNumber.text.length < 8 || textFieldPhoneNumber.text.length > 15 ){
        [[CommonMethods sharedInstance]AlertMessage:@"Please Enter the valid Phone Number" andViewController:self];
    }
    else if(!isTermsAndCondition)
    {
        [[CommonMethods sharedInstance]AlertMessage:@"Accept the Terms & Conditions to continue" andViewController:self];
    }
    else{
        
        BOOL isInternet = [[CommonMethods sharedInstance]checkInternetConnection];
        if (isInternet) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //                hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                //                hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
                [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
                
            });
            NSString *phoneNumber = [[textFieldPhoneNumber.text componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet]] componentsJoinedByString:@""];
            NSString *strCode = self.buttonCountryCode.titleLabel.text;
            strCode = [strCode stringByReplacingOccurrencesOfString:@" " withString:@""];
            phoneNumber = [strCode stringByAppendingString:phoneNumber];
            NSDictionary *dict =[NSDictionary  dictionaryWithObject:phoneNumber forKey:@"number"];
            // [dict setValue:_textFieldPhoneNumber.text forKey:@"number"];
            NSMutableDictionary *dictRequest = [[NSMutableDictionary alloc]init];
            [dictRequest setObject:dict forKey:@"phones"];
            NSLog(@"%@",dictRequest);
            [WebServiceManager PostRequest:[NSString stringWithFormat:@"%@users/phones",kBaseUrl] andParam:dictRequest andcompletionhandler:^(NSArray *returArray, NSError *error) {
                if (!error) {
                    if ([returArray count] >0) {
                        if([[returArray valueForKey:@"status"] boolValue]){
                            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                            [defaults setObject:[returArray valueForKey:@"phone_id"] forKey:@"phone_id"];
                            [defaults setObject:phoneNumber forKey:@"phoneNumber"];
                            // [defaults setInteger:[[returArray valueForKey:@"phone_id"] intValue] forKey:@"phone_id"];
                            //  [defaults setInteger:[phoneNumber intValue] forKey:@"phoneNumber"];
                            [defaults synchronize];
                            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                            OTPViewController *controller = [story instantiateViewControllerWithIdentifier:@"OTPViewController"];
                            controller.strNumber = phoneNumber;
                            [self.navigationController pushViewController:controller animated:true];
                        }
                        else if(![[returArray valueForKey:@"status"] boolValue]){
                            dispatch_async(dispatch_get_main_queue(), ^{
                                //                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                                [[LoaderNew sharedLoader]hideLoader];
                                [[CommonMethods sharedInstance]AlertMessage:[returArray valueForKey:@"message"] andViewController:self];
                            });
                        }
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                            [[LoaderNew sharedLoader]hideLoader];
                            [[CommonMethods sharedInstance]AlertMessage:@"Enter valid phone number" andViewController:self];
                        });
                    }
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        [[LoaderNew sharedLoader]hideLoader];
                        [[CommonMethods sharedInstance]AlertMessage:[returArray valueForKey:@"message"] andViewController:self];
                        
                    });
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [[LoaderNew sharedLoader]hideLoader];
                });
            }];
        }
        else{
            [[CommonMethods sharedInstance]AlertMessage:@"No internet connection" andViewController:self];
        }
    }
    //        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //       OTPViewController *controller = [story instantiateViewControllerWithIdentifier:@"OTPViewController"];
    //     //   controller.strNumber = phoneNumber;
    //        [self.navigationController pushViewController:controller animated:true];
    
}


-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (IS_IPHONE_6P) {
        lcHeightForButtonverify.constant = 50;
        self.buttonVerify.titleLabel.font = FontOpenSansBold(16);
    }
}
-(BOOL)hidesBottomBarWhenPushed
{
    return YES;
}
#pragma mark - Country Code Button Action

- (IBAction)buttonCountryCodeClicked:(id)sender {
    [self PickerHideShow];
}

-(void)PickerHideShow{
    if (!isPickerDisplay) {
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
            [textFieldPhoneNumber resignFirstResponder];
            
        }];
        [UIView animateWithDuration:0.25 animations:^{
            
            
            CGRect temp1 = toolBar.frame;

            if(IS_IPHONE_6P || IS_IPHONE_X){
                temp1.origin.y = self.view.frame.size.height - toolBar.frame.size.height-220;
            }
            else{
                temp1.origin.y = self.view.frame.size.height - toolBar.frame.size.height-190;

            }
            toolBar.frame = temp1;
            
            CGRect temp = self.pickerCountryCode.frame;
            temp.origin.y = self.view.frame.size.height - self.pickerCountryCode.frame.size.height;
            self.pickerCountryCode.frame = temp;
            
            
            
        } completion:^(BOOL finished) {
            NSLog(@"picker displayed");
            isPickerDisplay = YES;
        }];
    }else {
        [UIView animateWithDuration:0.25 animations:^{
            
            CGRect temp1 = toolBar.frame;
            temp1.origin.y = self.view.frame.size.height;
            toolBar.frame = temp1;
            
            CGRect temp = self.pickerCountryCode.frame;
            temp.origin.y = self.view.frame.size.height;
            self.pickerCountryCode.frame = temp;
        } completion:^(BOOL finished) {
            NSLog(@"picker hide");
            isPickerDisplay = NO;
        }];
    }
    
    
}


#pragma mark - done button action
-(void)DoneBtnClicked:(id)sender{
    
    [self PickerHideShow];
}

#pragma mark - ToolBar For TextField Phone Number
-(void)doneButtonClicked:(id)sender{
    
    //[_textFieldPhoneNumber resignFirstResponder];
    [self.view endEditing:true];
    //    [UIView animateWithDuration:0.3 animations:^{
    //        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
    //    }];
    if (IS_IPHONE_5) {
        [UIView animateWithDuration:0.30 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
        }];
    }
    
    
}

-(IBAction)ButtonTermsCLicked:(id)sender{
    PrivacyPolicyViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicyViewController"];
    controller.isTerms = true;
    [self.navigationController pushViewController:controller animated:true];
}

#pragma mark - Button Action For terms And Condition

- (IBAction)ButtonTermsAndConditionsClicked:(id)sender {
    if (!isTermsAndCondition) {
        _buttonTermsAndCondition.selected = true;
        //_buttonTermsAndCondition.backgroundColor = [UIColor clearColor];
        isTermsAndCondition = true;
        
    }
    else{
        _buttonTermsAndCondition.selected = false;
        // _buttonTermsAndCondition.backgroundColor = [UIColor greenColor];
        isTermsAndCondition = false;
    }
}


#pragma mark-TextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField==textFieldPhoneNumber ) {
        [UIView animateWithDuration:0.0 animations:^{
            CGRect temp1 = toolBar.frame;
            temp1.origin.y = self.view.frame.size.height;
            toolBar.frame = temp1;
            
            CGRect temp = self.pickerCountryCode.frame;
            temp.origin.y = self.view.frame.size.height;
            self.pickerCountryCode.frame = temp;
        } completion:^(BOOL finished) {
            NSLog(@"picker hide");
            isPickerDisplay = NO;
        }];
        
        if (IS_IPHONE_5) {
            [UIView animateWithDuration:0.30 animations:^{
                self.view.frame = CGRectMake(self.view.frame.origin.x, -50, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height-50);
            }];
            
        }
        
    }
    
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField==textFieldPhoneNumber) {
        int length = [self getLength:textField.text];
        
        if(length > 10 && length <=15)
        {
            if(range.length == 0)
                return NO;
        }
        
        if(length == 3)
        {
            NSString *num = [self formatNumber:textField.text];
            textField.text = [NSString stringWithFormat:@"%@-",num];
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@",[num substringToIndex:3]];
        }
        
        else if(length == 7)
        {
            NSString *num = [self formatNumber:textField.text];
            
            textField.text = [NSString stringWithFormat:@"%@-%@",[num  substringToIndex:3],[num substringFromIndex:3]];
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"%@-%@",[num substringToIndex:3],[num substringFromIndex:3]];
        }
        
        else if(length == 8)
        {
            
            NSString *num = [self formatNumber:textField.text];
            
            
            NSString *subString = [num substringWithRange: NSMakeRange(3,3)];
            
            
            
            textField.text = [NSString stringWithFormat:@"(%@) %@-%@",[num  substringToIndex:3],subString,[num substringFromIndex:6]];
            
            if(range.length > 0)
                textField.text = [NSString stringWithFormat:@"(%@) %@-%@",[num substringToIndex:3],subString,[num substringFromIndex:6]];
        }
    }
    
    return 15;
}

#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return arrayCountryCode.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    
    
    return [[arrayCountryCode valueForKey:@"name"] objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [_buttonCountryCode setTitle:[[arrayCountryCode valueForKey:@"code"] objectAtIndex:row] forState:UIControlStateNormal];
}

#pragma mark- PhoneMasking
-(NSString*)formatNumber:(NSString*)mobileNumber{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    int length = (int)[mobileNumber length];
    if(length > 10){
        mobileNumber = [mobileNumber substringFromIndex: length-10];
    }
    return mobileNumber;
}


-(int)getLength:(NSString*)mobileNumber{
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
    int length = (int)[mobileNumber length];
    return length;
    
}
- (IBAction)ButtonLoginPressed:(id)sender {
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    LoginViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"]; //or the homeController
    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
    appDelegate.window.rootViewController = navController;
    //  [self.navigationController pushViewController:loginController animated:true];
    // [self.navigationController popToRootViewControllerAnimated:true];
}
- (IBAction)ScreenTapped:(id)sender {
    [self.view endEditing:true];
    if (IS_IPHONE_5) {
        [UIView animateWithDuration:0.30 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, 0, [[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height);
        }];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
