//
//  WebServiceManager.m
//  WebServiceManager
//
//  Created by Shiv Kumar on 10/11/16.
//  Copyright © 2016 Shiv Kumar. All rights reserved.
//

#import "WebServiceManager.h"

//#import "Reachability.h"

NSString *const kSERVER_ISSUE       = @"Opps! \nSomething went wrong. Please try later.";
NSString *kSuccess                  = @"status";
NSString *kMsg                      = @"message";
#define PUT                     @"PUT"
#define DELETE                  @"DELETE"
#define POST                    @"POST"
NSString *kStatus                   = @"status";
@implementation WebServiceManager
@synthesize accessTokens;
@synthesize headerEmail;

#pragma mark ❉====❉====❉ POST Request ❉====❉====❉

+(void)postRequestWithUrlString:(NSString *)urlStr withPostString:(NSMutableDictionary *)postDict emailString:(NSString *)email accessToken:(NSString *)aToken completionSuccess:(Success)completionBlock failure:(Failure)failure;{
  //  NSLog(@"URL String 👉🏾: %@ \n Post String 👉🏾: \n%@",urlStr,postStr);
    NSData *__jsonData;
    NSString *__jsonString;
    //  __jsonData = [parameter dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    if([NSJSONSerialization isValidJSONObject:postDict])
    {
        __jsonData = [NSJSONSerialization dataWithJSONObject:postDict options:0 error:nil];
        __jsonString = [[NSString alloc]initWithData:__jsonData encoding:NSUTF8StringEncoding];
    }

    //NSData *postData = [postStr dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:true];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[__jsonData length]];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:POST];
    [request setValue:aToken     forHTTPHeaderField:@"auth-token"];
    [request setValue:email      forHTTPHeaderField:@"role"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:__jsonData];
    [request setTimeoutInterval:40.0f];
    
    NSURLSessionDataTask *task ;
    NSURLSession *session = [NSURLSession sharedSession];
    task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                                                
                                                if([data length] > 0 && error == nil)
                                                {
                                                    NSDictionary *result = [NSDictionary dictionaryWithDictionary:[NSJSONSerialization JSONObjectWithData:data
                                                                                                                                                  options:NSJSONReadingAllowFragments
                                                                                                                                                    error:&error]];
                                                    NSLog(@"Response : %@",result );
                                                    
                                                    if ([result isKindOfClass:[NSNull class]] || result.count == 0 || result==nil)
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            failure(kSERVER_ISSUE, false);
                                                        });
                                                    }
                                                    else
                                                    {
                                                        BOOL success = [[result valueForKey:kSuccess] boolValue];
                                                        if (success)
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                completionBlock(result,success);
                                                            });
                                                        }
                                                        else
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                failure([result valueForKey:kMsg], success);
                                                            });
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    failure(error.localizedDescription, false);
                                                }
                                                [task suspend];
                                            }];
    [task resume];
    
}

#pragma mark ❉====❉====❉ GET Request ❉====❉====❉

+(void)getRequestUrlString:(NSString *)urlString emailString:(NSString *)email accessToken:(NSString *)aToken completionSuccess:(Success)completionBlock failure:(Failure)failure{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",urlString]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setValue:aToken forHTTPHeaderField:@"auth-token"];
    [request setValue:email forHTTPHeaderField:@"role"];
    [request setTimeoutInterval:100.0f];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error){
                                      
                                      if ([data length] > 0 && error == nil)
                                      {
                                          NSDictionary * result =[NSJSONSerialization JSONObjectWithData:data
                                                                                                 options:NSJSONReadingMutableContainers
                                                                                                   error:nil];
                                          NSLog(@"Response 👉🏾: %@",result );
                                          
                                          if ([result isKindOfClass:[NSNull class]] || result == nil)
                                          {
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  failure(kSERVER_ISSUE, false);
                                              });
                                          }
                                          else
                                          {
                                              BOOL success = [[result valueForKey:kSuccess] boolValue];
                                              if (success)
                                              {
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      completionBlock(result,success);
                                                  });
                                              }
                                              else
                                              {
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      failure([result valueForKey:kMsg], success);
                                                  });
                                              }
                                          }
                                      }
                                      else if ([data length] == 0 && error == nil)
                                      {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              failure(kSERVER_ISSUE, false);
                                          });
                                      }
                                      else if (error != nil)
                                      {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              failure(error.localizedDescription, false);
                                              NSLog(@"%@",error.localizedDescription);
                                          });
                                      }
                                  }];
    [task resume];
}

+(void)putRequestWithUrlString:(NSString *)urlStr withPostString:(NSMutableDictionary *)postDict emailString:(NSString *)email accessToken:(NSString *)aToken completionSuccess:(Success)completionBlock failure:(Failure)failure
{
   // NSLog(@"URL String 👉🏾: %@ \n Post String : \n%@",urlStr,postStr);
    
    NSData *__jsonData;
    NSString *__jsonString;
    //  __jsonData = [parameter dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    if([NSJSONSerialization isValidJSONObject:postDict])
    {
        __jsonData = [NSJSONSerialization dataWithJSONObject:postDict options:0 error:nil];
        __jsonString = [[NSString alloc]initWithData:__jsonData encoding:NSUTF8StringEncoding];
    }
    
    //NSData *postData = [postStr dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:true];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[__jsonData length]];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:PUT];
    [request setValue:aToken     forHTTPHeaderField:@"auth-token"];
    [request setValue:email      forHTTPHeaderField:@"role"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:__jsonData];
    [request setTimeoutInterval:1000.0f];
    
    NSURLSessionDataTask *task ;
    NSURLSession *session = [NSURLSession sharedSession];
    task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        if([data length] > 0 && error == nil)
        {
            NSDictionary *result = [NSDictionary dictionaryWithDictionary:[NSJSONSerialization JSONObjectWithData:data
                                                                                                          options:NSJSONReadingAllowFragments
                                                                                                            error:&error]];
            NSLog(@"Response : %@",result );
            
            if ([result isKindOfClass:[NSNull class]] || result.count == 0 || result==nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    failure(kSERVER_ISSUE, false);
                });
            }
            else
            {
                BOOL success = [[result valueForKey:kSuccess] boolValue];
                if (success)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completionBlock(result,success);
                    });
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        failure([result valueForKey:kMsg], success);
                    });
                }
            }
        }
        else
        {
            failure(error.localizedDescription, false);
        }
        [task suspend];
    }];
    [task resume];
}

#pragma mark ❉====❉====❉ DELETE Request ❉====❉====❉
+(void)deleteRequestWithUrlString:(NSString *)urlStr emailString:(NSString *)email accessToken:(NSString *)aToken completionSuccess:(Success)completionBlock failure:(Failure)failure
{
   
    NSLog(@"URL String 👉🏾: %@ \n ",urlStr);
    
    NSURL *url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:DELETE];
    [request setValue:aToken forHTTPHeaderField:@"auth-token"];
    [request setValue:email forHTTPHeaderField:@"role"];
    [request setTimeoutInterval:40.0f];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task;
    task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                                                
                                                if([data length] > 0 && error == nil)
                                                {
                                                    NSDictionary *result = [NSDictionary dictionaryWithDictionary:[NSJSONSerialization JSONObjectWithData:data
                                                                                                                                                  options:NSJSONReadingAllowFragments
                                                                                                                                                    error:&error]];
                                                    NSLog(@"Response 👉🏾: %@",result );
                                                    
                                                    if ([result isKindOfClass:[NSNull class]] || result.count == 0 || result==nil)
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            failure(kSERVER_ISSUE, false);
                                                        });
                                                    }
                                                    else
                                                    {
                                                        BOOL success = [[result valueForKey:kSuccess] boolValue];
                                                        if (success)
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                completionBlock(result,success);
                                                            });
                                                        }
                                                        else
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                failure([result valueForKey:kMsg], success);
                                                            });
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    failure(error.localizedDescription, false);
                                                }
                                                [task suspend];
                                            }];
    [task resume];
    
}

///:: Recieved Message Request Method
+(void)recievedMSGRequestWithUrlString:(NSString *)urlStr
                        withPostString:(NSString *)postStr
                     completionSuccess:(Success)completionBlock
                               failure:(Failure)failure
{
    NSLog(@"url String : %@ \n Post String : \n%@",urlStr,postStr);
    
    NSData *postData = [postStr dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:true];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSURL *url = [NSURL URLWithString:urlStr];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:POST];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    [request setTimeoutInterval:40.0f];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                                                
                                                if([data length] > 0 && error == nil)
                                                {
                                                    NSDictionary *result = [NSDictionary dictionaryWithDictionary:[NSJSONSerialization JSONObjectWithData:data
                                                                                                                                                  options:NSJSONReadingAllowFragments
                                                                                                                                                    error:&error]];
                                                    printf("Responce:🇮🇳 %s",[NSString stringWithFormat:@"%@",result].UTF8String);
                                                    
                                                    if ([result isKindOfClass:[NSNull class]] || result.count == 0 || result==nil)
                                                    {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            failure(kSERVER_ISSUE, false);
                                                        });
                                                    }
                                                    else
                                                    {
                                                        BOOL status = [[result valueForKey:kStatus] boolValue];
                                                        if (status)
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                completionBlock(result,status);
                                                            });
                                                        }
                                                        else
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                failure([result valueForKey:kMsg], status);
                                                            });
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        failure(error.localizedDescription, false);
                                                    });
                                                }
                                                [task suspend];
                                            }];
    [task resume];
    
}


+(void)PostRequest:(NSString *)urlString andParam:(NSMutableDictionary *)dict andcompletionhandler:(void(^)(NSArray *returnArray, NSError *error)) completionBlock{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    NSData *__jsonData;
    NSString *__jsonString;
    //  __jsonData = [parameter dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    if([NSJSONSerialization isValidJSONObject:dict])
    {
        __jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
        __jsonString = [[NSString alloc]initWithData:__jsonData encoding:NSUTF8StringEncoding];
    }

    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    defaultConfigObject.timeoutIntervalForRequest = 300.0;
    defaultConfigObject.timeoutIntervalForResource = 600.0;
    
    NSString *cachePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"/nsurlsessiondemo.cache"];
    
    NSURLCache *myCache = [[NSURLCache alloc] initWithMemoryCapacity: 16384
                                                        diskCapacity: 268435456
                                                            diskPath: cachePath];
    defaultConfigObject.URLCache = myCache;
    
    defaultConfigObject.requestCachePolicy = NSURLRequestUseProtocolCachePolicy;
    
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject
                                                                      delegate: nil
                                                                 delegateQueue: [NSOperationQueue mainQueue]];
    
    
    request.HTTPShouldHandleCookies = YES;
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSHTTPCookieAcceptPolicy currentPolicy = [cookieJar cookieAcceptPolicy];
    [cookieJar setCookieAcceptPolicy: NSHTTPCookieAcceptPolicyAlways];
    
    [cookieJar setCookieAcceptPolicy: currentPolicy];
    
    [request setHTTPBody:__jsonData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[__jsonData length]] forHTTPHeaderField:@"Content-Length"];
    [[delegateFreeSession dataTaskWithRequest:request  completionHandler:^(NSData *data, NSURLResponse *response,
                                                                           NSError *error){
          NSError *jsonParsingError = nil;
          NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers|NSJSONReadingAllowFragments error:&jsonParsingError];

          
          completionBlock (jsonArray, error);
          
          
      }
      
      ]resume];
    
    
}


+(void)PutRequest:(NSString *)urlString andParam:(NSMutableDictionary *)dict andcompletionhandler:(void(^)(NSArray *returnArray, NSError *error)) completionBlock{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"PUT"];
    NSData *__jsonData;
    NSString *__jsonString;
    //  __jsonData = [parameter dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    if([NSJSONSerialization isValidJSONObject:dict])
    {
        __jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
        __jsonString = [[NSString alloc]initWithData:__jsonData encoding:NSUTF8StringEncoding];
    }
    
    
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    defaultConfigObject.timeoutIntervalForRequest = 300.0;
    defaultConfigObject.timeoutIntervalForResource = 600.0;
    
    NSString *cachePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"/nsurlsessiondemo.cache"];
    
    NSURLCache *myCache = [[NSURLCache alloc] initWithMemoryCapacity: 16384
                                                        diskCapacity: 268435456
                                                            diskPath: cachePath];
    defaultConfigObject.URLCache = myCache;
    
    defaultConfigObject.requestCachePolicy = NSURLRequestUseProtocolCachePolicy;
    
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject
                                                                      delegate: nil
                                                                 delegateQueue: [NSOperationQueue mainQueue]];
    
    
    request.HTTPShouldHandleCookies = YES;
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSHTTPCookieAcceptPolicy currentPolicy = [cookieJar cookieAcceptPolicy];
    [cookieJar setCookieAcceptPolicy: NSHTTPCookieAcceptPolicyAlways];
    
    [cookieJar setCookieAcceptPolicy: currentPolicy];
    
    [request setHTTPBody:__jsonData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[__jsonData length]] forHTTPHeaderField:@"Content-Length"];
    [[delegateFreeSession dataTaskWithRequest:request  completionHandler:^(NSData *data, NSURLResponse *response,
                                                                           NSError *error){
        NSError *jsonParsingError = nil;
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers|NSJSONReadingAllowFragments error:&jsonParsingError];
        
        
        completionBlock (jsonArray, error);
        
        
    }
      
      ]resume];
    
    
}





//#pragma mark - ❉===❉=== NETWORK STATUS ===❉===❉
//+(BOOL)networkStatus
//{
//    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
//    return networkStatus;
//}

@end
