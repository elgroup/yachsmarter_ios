//
//  AppDelegate.m
//  YachtMasters
//
//  Created by Anvesh on 06/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "AddYachtViewController.h"
#import "SWRevealViewController.h"
#import "MenuViewController.h"
#import "HomeTabBarViewController.h"
#import "ViewController.h"
#import "SearchYachtViewController.h"
#import "Constants.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "FirstViewController.h"
#import "ReviewViewController.h"
#import "Reachability.h"
#import "BookingConfirmationViewController.h"
#import "YachtMasters-Swift.h"
#import "NewBookingVC.h"
#import "NewBookingNotificationViewController.h"
#import "CustomNavigationViewController.h"

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@implementation UINavigationBar (CustomHeight)
//- (CGSize)sizeThatFits:(CGSize)size {
//    
//    return CGSizeMake(SCREEN_WIDTH, 200);
//}
@end
@interface AppDelegate (){
    Reachability *reachability;
    BOOL isNotofication;
}
//@property (nonatomic, strong) PSWebSocketServer *server;
//@property (nonatomic,strong) PSWebSocket *socket;
@property (nonatomic, assign) BOOL hasInet;



@end

@implementation AppDelegate
@synthesize dictFilterItems,isNotification;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [NSThread sleepForTimeInterval:3.0];
    UIApplication.sharedApplication.statusBarStyle = UIStatusBarStyleDefault;
    [self registerForRemoteNotification];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    //pk_live_oiTyXNgdwn43q967sLSVUSnw
    // sk_test_d9DWvxlmtoL3rYr0X1yyNuz700rODFLIBO"
    //pk_test_haUjSh3XuspwVwr2OJazrDub00eHVKvXAp
    [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:@"pk_test_lEytqW6gufmddzbnlemcFRfC00me4UZ99x"];
    [GMSPlacesClient provideAPIKey:kAPIKey];
    [GMSServices provideAPIKey:kAPIKey];
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    dictFilterItems = [[NSMutableDictionary alloc]init];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [defaults setObject:dict forKey:@"JourneyDate"];
    [self setUpRechability];
    BOOL islogin  = [defaults boolForKey:@"isLogIn"];
    self.socket = [AppSocket sharedSocket];
    if (!islogin) {
        LoginViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"]; //or the homeController
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
        self.window.rootViewController = navController;
        [self.window makeKeyAndVisible];
        
    }
    
    else if (islogin){
        NSString *strRole = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
        
        if (self.hasInet) {
        [self.socket startSerevr];
        [self.socket initializeSocket];
        }
        
        if ([strRole isEqualToString:@"customer"]) {
            SearchYachtViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SearchYacht"]; //or the homeController
            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
            self.window.rootViewController = navController;
        }
        else{
            UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SWRevealViewController *controller = [mainStoryBoard instantiateViewControllerWithIdentifier:@"SwRootController"]; //or the homeController
            self.window.rootViewController = controller;
        }
    }
    else{
        ViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ViewController"]; //or the homeController
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
        self.window.rootViewController = navController;
    }
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    // Add any custom logic here.
    return handled;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary *)options {
    return [[FBSDKApplicationDelegate sharedInstance]application:app openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    [self.socket unSubscribeChannel:@"AppearanceChannel"];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    //  [self initSocket];
    [self.socket startSerevr];
    [self.socket initializeSocket];
    [self applicationDidBecomeActive:application];
    // [self.window.rootViewController beginAppearanceTransition:true animated:true];
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    if (isNotification) {
    CustomNavigationViewController *controller =(CustomNavigationViewController *)self.window.rootViewController;
    if (controller.viewControllers.count > 0){
        SWRevealViewController *obj = (SWRevealViewController *)controller.viewControllers.firstObject;
        HomeTabBarViewController *tabBar = (HomeTabBarViewController *)obj.frontViewController;
        tabBar.selectedIndex = 1;
        
    }
        isNotification = false;
    }
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
#pragma mark - Remote Notification Delegate // <= iOS 9.x

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
   // NSString *strDevicetoken = [[NSString alloc]initWithFormat:@"%@",[[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    NSString *strDevicetoken = [NSString stringWithFormat:@"%@",[self stringFromDeviceToken:deviceToken]];
    NSLog(@"Device Token = %@",strDevicetoken);
    self.strDeviceToken = strDevicetoken;
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    NSLog(@"Push Notification Information : %@",userInfo);
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    NSLog(@"%@ = %@", NSStringFromSelector(_cmd), error);
    NSLog(@"Error = %@",error);
}

- (NSString *)stringFromDeviceToken:(NSData *)deviceToken {
    NSUInteger length = deviceToken.length;
    if (length == 0) {
        return nil;
    }
    const unsigned char *buffer = deviceToken.bytes;
    NSMutableString *hexString  = [NSMutableString stringWithCapacity:(length * 2)];
    for (int i = 0; i < length; ++i) {
        [hexString appendFormat:@"%02x", buffer[i]];
    }
    return [hexString copy];
}

#pragma mark - UNUserNotificationCenter Delegate // >= iOS 10

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    NSLog(@"User Info = %@",notification.request.content.userInfo);
    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    NSLog(@"User Info = %@",response.notification.request.content.userInfo);
    isNotification = true;
    if ([response.notification.request.content.userInfo[@"aps"][@"status"] isEqualToString:@"declined"] || [response.notification.request.content.userInfo[@"aps"][@"status"] isEqualToString:@"cancelled"] || [response.notification.request.content.userInfo[@"aps"][@"status"] isEqualToString:@"approved"]){
    BookingConfirmationViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"BookingConfirmationViewController"]; //or the homeControlle
    loginController.dictPushNotificationResponse = response.notification.request.content.userInfo;
    loginController.strStatus = response.notification.request.content.userInfo[@"aps"][@"status"];
        [self.window.rootViewController presentViewController:loginController animated:true completion:nil];
    }
    else if ([response.notification.request.content.userInfo[@"aps"][@"status"] isEqualToString:@"new_booking"]){
        NewBookingNotificationViewController *newBookingController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NewBookingNotificationViewController"]; //or the homeControlle
         newBookingController.dictYacht = response.notification.request.content.userInfo[@"aps"][@"booking"];
        [self.window.rootViewController presentViewController:newBookingController animated:true completion:nil];
    }
    else if ([response.notification.request.content.userInfo[@"aps"][@"status"] isEqualToString:@"new_message"]){
            UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            CustomNavigationViewController *controller = [mainStoryBoard instantiateViewControllerWithIdentifier:@"SwRootController"]; //or the homeController
            self.window.rootViewController = controller;
        self.isNotification = true;
    }
    //  completionHandler();
}

#pragma mark - Class Methods

/**
 Notification Registration
 */
- (void)registerForRemoteNotification {
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( granted ){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                    
                });
                
            }else {
                self.strDeviceToken = @"";
                NSLog(@"User denied access ");
            }
        }];
    }
    else {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}



//#pragma mark - PSWebSocketDelegate
//- (void)webSocketDidOpen:(PSWebSocket *)webSocket {
//    NSLog(@"The websocket handshake completed and is now open!");
//    [webSocket send:@"Hello world!"];
//}
//- (void)webSocket:(PSWebSocket *)webSocket didReceiveMessage:(id)message {
//    NSLog(@"The websocket received a message: %@", message);
//}
//- (void)webSocket:(PSWebSocket *)webSocket didFailWithError:(NSError *)error {
//    NSLog(@"The websocket handshake/connection failed with an error: %@", error);
//}
//- (void)webSocket:(PSWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
//    NSLog(@"The websocket closed with code: %@, reason: %@, wasClean: %@", @(code), reason, (wasClean) ? @"YES" : @"NO");
//}
//
//#pragma mark - PSWebSocketServerDelegate
//
//- (void)serverDidStart:(PSWebSocketServer *)server {
//    NSLog(@"Server did start…");
//}
//- (void)serverDidStop:(PSWebSocketServer *)server {
//    NSLog(@"Server did stop…");
//}
//- (BOOL)server:(PSWebSocketServer *)server acceptWebSocketWithRequest:(NSURLRequest *)request {
//    NSLog(@"Server should accept request: %@", request);
//    return YES;
//}
//- (void)server:(PSWebSocketServer *)server webSocket:(PSWebSocket *)webSocket didReceiveMessage:(id)message {
//    NSLog(@"Server websocket did receive message: %@", message);
//}
//- (void)server:(PSWebSocketServer *)server webSocketDidOpen:(PSWebSocket *)webSocket {
//    NSLog(@"Server websocket did open");
//}
//- (void)server:(PSWebSocketServer *)server webSocket:(PSWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
//    NSLog(@"Server websocket did close with code: %@, reason: %@, wasClean: %@", @(code), reason, @(wasClean));
//}
//- (void)server:(PSWebSocketServer *)server webSocket:(PSWebSocket *)webSocket didFailWithError:(NSError *)error {
//    NSLog(@"Server websocket did fail with error: %@", error);
//}
//
//
//
//-(void)initSocket{
//    // ws://34.212.103.14/cable?token=SMHJq5zs4QrzoFdB32ic
//    NSString *strAuthToken = [[NSUserDefaults standardUserDefaults]valueForKey:Auth_token];
//   // NSString *strUrl = [NSString stringWithFormat:@"ws://gharwale.com/cable?token=%@",strAuthToken];
//     NSString *strUrl = [NSString stringWithFormat:@"ws://192.168.1.24:3003/cable?token=%@",strAuthToken];
//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
//    self.socket = [PSWebSocket clientSocketWithRequest:request];
//    self.socket.delegate = self;
//    [self.socket open];
//}
//
//-(void)joinChannel:(NSString *)channelName{
//    NSString *strChannel = [NSString stringWithFormat:@"{ \"channel\": \"%@\" }",channelName];
//    id data = @{@"command": @"subscribe",@"identifier": strChannel};
//    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:data options:0 error:nil];
//    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    [self.socket send:myString];
//}
//
//-(void)UnsubscribeChannel:(NSString *)channelName{
//    NSString *strChannel = [NSString stringWithFormat:@"{ \"channel\": \"%@\" }",channelName];
//    id data = @{@"command": @"unsubscribe",@"identifier": strChannel};
//
//    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:data options:0 error:nil];
//
//    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//
//    NSLog(@"myString= %@", myString);
//
//    [self.socket send:myString];
//}
//
//
//
//#pragma mark - PSWebSocketDelegate Methods -
//
//-(void)webSocketDidOpen:(PSWebSocket *)webSocket
//{
//    NSLog(@"The websocket handshake completed and is now open!");
//    [self joinChannel:@"AppearanceChannel"];
//    [self joinChannel:@"RoomChannel"];
//    [self joinChannel:@"ConversationChannel"];
//
//}
//
//-(void)webSocket:(PSWebSocket *)webSocket didReceiveMessage:(id)message{
//    NSData *data = [message dataUsingEncoding:NSUTF8StringEncoding];
//    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//
//    NSString *messageType = json[@"type"];
//
//    if(![messageType isEqualToString:@"ping"] && ![messageType isEqualToString:@"welcome"] && ![messageType isEqualToString:@"confirm_subscription"]){
//        if ([[json valueForKey:@"identifier"] isEqualToString:@"{ \"channel\": \"AppearanceChannel\" }"]) {
//
//        }
//        else{
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"MessageRecieved" object:json];
//        }
//        NSLog(@"The websocket received a message: %@", json[@"message"]);
//    }
//}
//
//-(void)webSocket:(PSWebSocket *)webSocket didFailWithError:(NSError *)error{
//    NSLog(@"The websocket handshake/connection failed with an error: %@", error);
//    //[self initSocket];
//
//}
//
//-(void)webSocket:(PSWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean{
//    //[self initSocket];
//    NSLog(@"The websocket closed with code: %@, reason: %@, wasClean: %@", @(code), reason, (wasClean) ? @"YES": @"NO");
//}
//
//
//


- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    return UIInterfaceOrientationMaskPortrait;
}


#pragma mark - Check For internet connection
-(void)setUpRechability{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNetworkChange:) name:kReachabilityChangedNotification object:nil];
    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable){
        NSLog(@"no");
        self.hasInet-=NO;
    }
    else if(remoteHostStatus == ReachableViaWiFi)  {
        NSLog(@"wifi");
        self.hasInet-=YES;
    }
    else if(remoteHostStatus == ReachableViaWWAN){
        NSLog(@"cell");
        self.hasInet-=YES;
    }
}

- (void) handleNetworkChange:(NSNotification *)notice{
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
     BOOL islogin  = [[NSUserDefaults standardUserDefaults] boolForKey:@"isLogIn"];
    if (remoteHostStatus == NotReachable){
        NSLog(@"no");
        self.hasInet-=NO;
    }
    else if (remoteHostStatus == ReachableViaWiFi) {
        NSLog(@"wifi");
        self.hasInet-=YES;
    }
    else if(remoteHostStatus == ReachableViaWWAN){
        NSLog(@"cell");
        self.hasInet-=YES;
    }
    if (self.hasInet && islogin) {
        [self.socket startSerevr];
        [self.socket initializeSocket];
    }
}




@end
