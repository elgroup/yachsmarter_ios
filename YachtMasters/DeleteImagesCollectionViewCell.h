//
//  DeleteImagesCollectionViewCell.h
//  YachtMasters
//
//  Created by Anvesh on 03/08/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeleteImagesCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *buttonDelete;
@property (weak, nonatomic) IBOutlet UIImageView *imageYacht;

@end
