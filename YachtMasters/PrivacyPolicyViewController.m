//
//  PrivacyPolicyViewController.m
//  YachtMasters
//
//  Created by Anvesh on 17/05/18.
//  Copyright © 2018 EL. All rights reserved.
//

#import "PrivacyPolicyViewController.h"
#import "Constants.h"
#import "YachtMasters-Swift.h"
#import "CommonMethods.h"
#import <WebKit/WebKit.h>

@interface PrivacyPolicyViewController ()<WKNavigationDelegate,WKUIDelegate>{
IBOutlet WKWebView *wkWebView;
}
@end

@implementation PrivacyPolicyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self CustomNavigationBar];
    self.tabBarController.hidesBottomBarWhenPushed = true;
    self.tabBarController.tabBar.hidden = true;
    NSString *str;
    if (self.isTerms) {
        str =  [NSString stringWithFormat:@"https://yachtsmarter.co/terms-condition"];
    }
    else{
        str =  [NSString stringWithFormat:@"https://yachtsmarter.co/privacy-policy"];
    }
    
    NSURL *url = [NSURL URLWithString:str];
    wkWebView.navigationDelegate = self;
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [wkWebView loadRequest:urlRequest];
    [[LoaderNew sharedLoader]showLoaderOnScreenWithVc:self.view];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

# pragma mark - WKWEbview Delegate
-(void) webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    [[LoaderNew sharedLoader]hideLoader];
}
-(void) webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    [[LoaderNew sharedLoader]hideLoader];
}
-(void)CustomNavigationBar{
    
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = false;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"topstrip3.png"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor colorWithRed:40.0/255.0 green:107.0/255.0 blue:240.0/255.0 alpha:1.0].CGColor;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    self.navigationController.navigationBar.layer.shadowRadius = 7.0f;
    self.navigationController.navigationBar.layer.shadowOpacity = 1.0f;
    self.navigationController.navigationBar.layer.masksToBounds=NO;
    
    UIImage* image3 = [UIImage imageNamed:@"back arrow.png"];
    CGRect frameimg = CGRectMake(15,10, 30,30);
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg];
    //[backButton setBackgroundColor:[UIColor redColor]];
    [backButton addTarget:self action:@selector(ButtonBackPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarButton =[[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0.0, 10.0, 15.0, 15.0)];
    imageView.image = image3;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backButton addSubview:imageView];
    if (self.isTerms) {
        self.title = @"Term & Conditions";
    }
    else{
        self.title = @"Privacy Policy";
        
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:FontOpenSansSemiBold(13)}];
}

-(void)ButtonBackPressed{
    self.navigationItem.hidesBackButton = true;
    self.navigationController.navigationBar.hidden = true;
    
    [self.navigationController popViewControllerAnimated:true];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
