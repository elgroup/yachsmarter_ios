//
//  MessageTableViewCell.m
//  YachtMasters
//
//  Created by Anvesh on 23/06/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "MessageTableViewCell.h"

@implementation MessageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _imageViewProfilePicture.layer.cornerRadius = 35.0;
    _imageViewProfilePicture.clipsToBounds = true;
    _labelMessageNotification.layer.cornerRadius = 10.0;
    _labelMessageNotification.clipsToBounds = true;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
