//
//  BookingConfirmationViewController.m
//  YachtMasters
//
//  Created by Anvesh on 05/09/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import "BookingConfirmationViewController.h"
#import "AppDelegate.h"
#import "Constants.h"

@interface BookingConfirmationViewController (){
    AppDelegate *delegate;
}
@property (weak, nonatomic) IBOutlet UIImageView *imageViewStatus;
@property (weak, nonatomic) IBOutlet UIButton *buttonHome;
@property (weak, nonatomic) IBOutlet UILabel *labelStatus;
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;

@end

@implementation BookingConfirmationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = true;
    self.navigationController.navigationItem.hidesBackButton = true;
    self.navigationController.navigationItem.backBarButtonItem = nil;
    delegate =  (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *strRole = [[NSUserDefaults standardUserDefaults]valueForKey:Role];
    if ([strRole isEqualToString: Customer]) {
    if ([_strStatus isEqualToString:@"declined"] || [_strStatus isEqualToString:@"cancelled"]) {
        _labelStatus.text = @"Your request has been declined!";
        _labelMessage.hidden = true;
        _imageViewStatus.image = [UIImage imageNamed:@"Declined@2x.png"];
    }
    else if ([_strStatus isEqualToString:@"approved"]){
        _labelStatus.text = @"Your request has been approved!";
        _labelMessage.hidden = false;
        _labelMessage.text = @"We will contact with you soon :)";
        _imageViewStatus.image = [UIImage imageNamed:@"Approved@2x.png"];
    }
    else{
        
    }
    }
    else{
        if ([_strStatus isEqualToString:@"cancelled"]) {
            _labelStatus.text = @"Your booking has been cancelled!";
            _labelMessage.hidden = true;
            _imageViewStatus.image = [UIImage imageNamed:@"Declined@2x.png"];
        }
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)ButtonHomePressed:(id)sender {
    if (delegate.isNotification){
        [self dismissViewControllerAnimated:true completion:^{
            delegate.isNotification = false;
        }];
    }
    else{
    [self.navigationController popToRootViewControllerAnimated:true];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
