//
//  BookingConfirmationViewController.h
//  YachtMasters
//
//  Created by Anvesh on 05/09/17.
//  Copyright © 2017 EL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingConfirmationViewController : UIViewController
@property (nonatomic,strong) NSDictionary *dictPushNotificationResponse;
@property (nonatomic,strong) NSString *strStatus;


@end
